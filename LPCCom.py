

from TPM_Communication import *

import time
from TS_Globals import TS_Globals

MODE_RX = 0x00
MODE_TX = 0x01

LPC_START = 0x05
LPC_CYCTYPE_WRITE = 0x02
LPC_CYCTYPE_READ = 0x00
LPC_TAR = 0x0F
LPC_SYNC = 0x00

LPC_CLOCK_DELAY = 0.0000001

SERIRQ_SLOT=0x01;

class LPCCom(TPM_Communication):
    
    lad_mode = MODE_TX
    interface_type = TS_Globals.TPM_INTERFACE_LPC
    

    def __init__(self):


        #LPC
        GPIO.setup(LFRAME, GPIO.OUT)
        GPIO.setup(LCLK, GPIO.OUT)
        GPIO.setup(LAD0, GPIO.OUT)
        GPIO.setup(LAD1, GPIO.OUT)
        GPIO.setup(LAD2, GPIO.OUT)
        GPIO.setup(LAD3, GPIO.OUT)
        GPIO.setup(LPCPD, GPIO.OUT)
        GPIO.output(LPCPD, GPIO.HIGH)
        GPIO.setup(SERIRQ, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(CLKRUN, GPIO.OUT)
        GPIO.output(CLKRUN, GPIO.HIGH)

        
        self.sendTimeout = 1 #1s
        self.receiveTimeout = 120 #10s
        self.portsTx()
        TPM_Communication.sInterface = self
        
    def setVDD(self, state):
        if(state==0):
            GPIO.output(POWER_3V3, GPIO.HIGH)
            GPIO.output(RST, GPIO.LOW)
            GPIO.output(PP, GPIO.LOW)
            GPIO.output(LFRAME, GPIO.LOW)
            GPIO.output(LCLK, GPIO.LOW)
            GPIO.output(LAD0, GPIO.LOW)
            GPIO.output(LAD1, GPIO.LOW)
            GPIO.output(LAD2, GPIO.LOW)
            GPIO.output(LAD3, GPIO.LOW)
            GPIO.output(LPCPD, GPIO.LOW)
            GPIO.output(CLKRUN, GPIO.LOW)
            GPIO.output(LCLK, GPIO.LOW)
            GPIO.setup(SERIRQ, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
            
        elif(state==1):
            GPIO.output(POWER_3V3, GPIO.LOW)
            GPIO.output(LPCPD, GPIO.HIGH)
            GPIO.setup(SERIRQ, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.output(CLKRUN, GPIO.HIGH)
            self.reset()

    def reset(self):
        
        TPM_Communication.reset(self)
        self.portsTx()
        #self.writeRegister(TPM_INT_ENABLE, bytearray([0x09, 0x00, 0x00, 0x80]))
        #self.writeRegister(TPM_INT_VECTOR, bytearray([SERIRQ_SLOT]))        
        
    def portsRx(self):
        GPIO.setup(LAD0, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(LAD1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(LAD2, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(LAD3, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        self.lad_mode=MODE_RX

    def portsTx(self):
        GPIO.setup(LAD0, GPIO.OUT)
        GPIO.setup(LAD1, GPIO.OUT)
        GPIO.setup(LAD2, GPIO.OUT)
        GPIO.setup(LAD3, GPIO.OUT)
        self.lad_mode=MODE_TX

    def setLAD(self, nibble):
        if (self.lad_mode!=MODE_TX):
            self.portsTx()
        
        GPIO.output(LAD0, GPIO.LOW) if ((nibble&0x01)==0) else GPIO.output(LAD0, GPIO.HIGH)
        GPIO.output(LAD1, GPIO.LOW) if ((nibble&0x02)==0) else GPIO.output(LAD1, GPIO.HIGH)
        GPIO.output(LAD2, GPIO.LOW) if ((nibble&0x04)==0) else GPIO.output(LAD2, GPIO.HIGH)
        GPIO.output(LAD3, GPIO.LOW) if ((nibble&0x08)==0) else GPIO.output(LAD3, GPIO.HIGH)

    def getLAD(self):
        ret = 0x00
        if (self.lad_mode==MODE_RX):
            if GPIO.input(LAD0):ret |=0x01
            if GPIO.input(LAD1):ret |=0x02
            if GPIO.input(LAD2):ret |=0x04
            if GPIO.input(LAD3):ret |=0x08
        return ret
        
    def lpcClock(self, tear=False):
        ret = 0x00
        if(tear==False):
            GPIO.output(LCLK, GPIO.LOW)
        time.sleep(LPC_CLOCK_DELAY)
        if (self.lad_mode==MODE_RX):
            ret=self.getLAD()
        GPIO.output(LCLK, GPIO.HIGH)
        time.sleep(LPC_CLOCK_DELAY)
        return ret
    
    def sirqClock(self):
        ret = 0x00
        GPIO.output(LCLK, GPIO.LOW)
        time.sleep(LPC_CLOCK_DELAY)
        ret=GPIO.input(SERIRQ)
        GPIO.output(LCLK, GPIO.HIGH)
        time.sleep(LPC_CLOCK_DELAY)
        return ret

    def txSTART(self):
        
        self.lpcClock()
        self.lpcClock()
        self.setLAD(LPC_START)
        GPIO.output(LFRAME, GPIO.LOW)
        self.lpcClock()
        GPIO.output(LFRAME, GPIO.HIGH)
        
    def txTAR(self):
        self.setLAD(LPC_TAR)
        self.lpcClock()
        self.portsRx()
        self.lpcClock()
        
    def rxTAR(self):
        self.lpcClock()
        self.portsTx()
        self.setLAD(LPC_TAR)
        self.lpcClock()
        
    def readRegister(self, lpc_reg, size=-1):
        
        if(size==-1):
            size=lpc_reg.size
            
        rx_buffer=bytearray()    
        for i in range (0,size):
            addr = (self.locality | (lpc_reg.addr+i))
            self.txSTART()
            self.setLAD(LPC_CYCTYPE_READ)
            self.lpcClock()
            self.setLAD(addr>>12)
            self.lpcClock()
            self.setLAD(addr>>8)
            self.lpcClock()
            self.setLAD(addr>>4)
            self.lpcClock()
            self.setLAD(addr)
            self.lpcClock()
            self.txTAR()
            sync = self.lpcClock()      #SYNC
            #if(sync!=0x00):
            #print "rxsync " + str(sync)
            rx_byte = self.lpcClock()
            rx_byte |= (self.lpcClock()<<4)
            self.rxTAR()
            rx_buffer.append(rx_byte)
            
        lpc_reg.Fill(rx_buffer)
        
    def writeRegister(self, lpc_reg, data):
        fifoReg = False
        tear = False
        if(lpc_reg==TPM_DATA_FIFO): fifoReg=True
        size = len(data)
        for i in range (0,size):
            addr = (self.locality | (lpc_reg.addr+i))
            txData = bytearray([LPC_CYCTYPE_WRITE,(addr>>12)&0x0F,(addr>>8)&0x0F,(addr>>4)&0x0F,(addr)&0x0F, (data[i])&0x0F, (data[i]>>4)&0x0F])
            self.txSTART()
            for j in range(0,7):
                self.setLAD(txData[j])
                if(fifoReg==False):
                    self.lpcClock()
                else:
                    tear = False    
                    if self.clockTearStart==0 and self.clockTearCount>0:
                        tear = True 
                        self.clockTearCount -=1
                    elif(self.clockTearStart>0):
                        self.clockTearStart -=1 
                    self.lpcClock(tear)  
                    self.txClocks +=1   
                    
            """
            self.setLAD(LPC_CYCTYPE_WRITE)
            self.lpcClock()
            self.setLAD(addr>>12)
            self.lpcClock()
            self.setLAD(addr>>8)
            self.lpcClock()
            self.setLAD(addr>>4)
            self.lpcClock()
            self.setLAD(addr)
            self.lpcClock()
            self.setLAD(data[i])
            self.lpcClock()
            self.setLAD(data[i]>>4)
            self.lpcClock()
            """
            self.txTAR()
            sync = self.lpcClock(tear)      #SYNC
            #if(sync!=0x00):
            #print "txsync " + str(sync) + "\n"
            self.rxTAR()
                
    def dataAvailable(self):            # Serial interrupt handling (experimental) 
        ret = False
        self.lpcClock()
        GPIO.setup(SERIRQ, GPIO.OUT)
        GPIO.output(SERIRQ, GPIO.LOW)
        for i in range(4):
            self.lpcClock()
        GPIO.setup(SERIRQ, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        self.lpcClock()
        self.lpcClock()
        for i in range(0,(SERIRQ_SLOT+1)):
            x= self.sirqClock()
            x|= self.sirqClock()<<1
            x|= self.sirqClock()<<2
            
            if (i == SERIRQ_SLOT) and  (x==6): ret = True
        GPIO.setup(SERIRQ, GPIO.OUT)
        GPIO.output(SERIRQ, GPIO.LOW)
        for i in range (3):
            self.lpcClock()
        GPIO.setup(SERIRQ, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        self.lpcClock()
        self.lpcClock()
                
        return ret
