import time
from datetime import datetime
from TS_Globals import HEX, TEXT, ASCII, BINARY

from TestInterpreter import TestManagerGateway
from Utils import bytearray2hex, hex2bytearray

def log(value):

    if(isinstance(value, bytearray)):
        TestManagerGateway.testManager.writeLog("\nHex: " + bytearray2hex(value),True) # @UndefinedVariable
    elif(isinstance(value, (int, long))):
        TestManagerGateway.testManager.writeLog("\n" + str(value),True) # @UndefinedVariable
    else:
        TestManagerGateway.testManager.writeLog("\n" + value,True) # @UndefinedVariable

def treeLog(value):
    if(isinstance(value, bytearray)):
        value = "Hex: " + bytearray2hex(value)
    elif(isinstance(value, (int, long))):
        value = str(value)
    log("\n" + value)
    TestManagerGateway.testManager.insertTreeOutput("", 'end', value, True) # @UndefinedVariable
        
def step(params):
    TestManagerGateway.testManager.testStep(params) # @UndefinedVariable

def treeOutput(value):
    TestManagerGateway.testManager.treeOutputState(value) # @UndefinedVariable
    
def header(name,  description):
    
    TestManagerGateway.testManager.writeLog("===============================================================",True) # @UndefinedVariable
    TestManagerGateway.testManager.writeLog("Test: " + name,True) # @UndefinedVariable
    TestManagerGateway.testManager.writeLog("Description: " + description,True)         # @UndefinedVariable
    TestManagerGateway.testManager.writeLog("",True) # @UndefinedVariable
    TestManagerGateway.testManager.writeLog("Execution Date: " + time.strftime("%Y-%m-%d %H:%M:%S"),True) # @UndefinedVariable
    TestManagerGateway.testManager.writeLog("",True) # @UndefinedVariable
    TestManagerGateway.testManager.writeChipInfo() # @UndefinedVariable
    TestManagerGateway.testManager.writeLog("===============================================================",True) # @UndefinedVariable
    TestManagerGateway.testManager.writeLog("",True) # @UndefinedVariable

def openFile(fileName):
        hdl=None
        try:
            hdl = open(fileName, 'w')
            TestManagerGateway.testManager.appendFileHandle(hdl)  # @UndefinedVariable
        except Exception, e:
            TestManagerGateway.testManager.increaseException(str(e))  # @UndefinedVariable
        return hdl

def appendFile(handle,  separator,  value,  output):
    if isinstance(value, basestring):
        if(output!=TEXT):
            value = hex2bytearray(value.replace(" ", ""))
    elif isinstance(value, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of data")  # @UndefinedVariable
    
    if isinstance(separator, basestring):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Separator must be a string")  # @UndefinedVariable
        return
    if (output==HEX):
        text = separator + str(''.join(format(x, '02x') for x in value)).upper()
    elif(output==BINARY):
        text = separator + str(''.join(format(x, '08b') for x in value)).upper()
    elif(output==ASCII) or (output==TEXT):
        text = separator + str(value)
    
    try:
        handle.write(text)
    except Exception, e:
        TestManagerGateway.testManager.increaseException(str(e))  # @UndefinedVariable

def createLogFile():
    TestManagerGateway.testManager.createLogFile()  # @UndefinedVariable

def disableLogOutput():
    TestManagerGateway.testManager.logOutputState(False)  # @UndefinedVariable

def timestamp():
    return datetime.now().strftime("_%y%m%d_%H%M%S")
 
