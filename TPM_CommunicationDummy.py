
from TS_Globals import LOCALITY_NONE


COMMUNICATION_OK = 0
COMMUNICATION_TIMEOUT = 1
COMMUNICATION_BRUST_TIMEOUT = 2

EXPECT_MORE_DATA = 0x01

RST=11              #
PP=13
LAD0=MISO=LAD0_MISO=21
LAD1=MOSI=LAD1_MOSI=19
LAD2=PIRQ=LAD2_PIRQ=12
LAD3=22
LCLK=SPI_CLK=LCLK_SPICLK=23
LFRAME=SPI_CS=LFRAME_SPICS=24
SERIRQ=26
LPCPD=29
CLKRUN=31
TXRX_TRIGGER = 37
POWER_3V3 = 7

class TPM_CommunicationDummy:
    
    locality = LOCALITY_NONE
    sendTimeout = 1
    receiveTimeout = 10
    clockTearStart = 0
    clockTearCount = 0
    txClocks = 0
    isDummy = True
    sInterface = None
    pollingInterval = 0.01

    @staticmethod
    def getInterface():
        return TPM_CommunicationDummy.sInterface

    def __init__(self):
        pass
    
    def reset(self):
        pass        
        
    def setClockTearing(self, start, length=1):
        self.clockTearStart = start
        self.clockTearCount = length
        
    def setVDD(self, state):
        pass        

    def setPP(self, pp):
        pass
    def setLocality(self, locality):
        pass
    def getTxClocks(self):
        return self.txClocks
    def setRxTimeout(self, timeout):
        self.receiveTimeout = timeout
    def setPollingInterval(self, interval):
        self.pollingInterval = interval
        
    def setDefaultValues():
        self.pollingInterval = 0.01
        self.receiveTimeout = 10
        self.sendTimeout = 1
        
    def receiveData(self, data):
        pass
        
    def sendData(self, data):
        pass
            
    def receive(self, data, gui=None):
        pass            
    def send(self, data, gui=None, command=""):
        pass
    
    @staticmethod
    def cleanUp():
        pass
        
