
from TS_Globals import ON, OFF
from TS_Globals import LOCALITY_0, LOCALITY_1, LOCALITY_2, LOCALITY_3, LOCALITY_4

from TestInterpreter import TestManagerGateway

def setClockTearing(start, length=1):
    if isinstance(start,( int, long )):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of start") # @UndefinedVariable
    
    if isinstance(length,( int, long )):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of start") # @UndefinedVariable
    TestManagerGateway.testManager.communicationInterface().setClockTearing(start, length) # @UndefinedVariable

def getTxClocks():
    txClocks = TestManagerGateway.testManager.communicationInterface().getTxClocks() # @UndefinedVariable
    TestManagerGateway.testManager.writeLog("Tx clocks:" + str(txClocks)) # @UndefinedVariable
    return txClocks

def readTISRegister(register):    
    TestManagerGateway.testManager.writeLog(TestManagerGateway.testManager.readTISRegister(register)) # @UndefinedVariable

def setPP(pp):
    if(pp==OFF):
        TestManagerGateway.testManager.communicationInterface().setPP(OFF) # @UndefinedVariable
        TestManagerGateway.testManager.writeLog("PP OFF") # @UndefinedVariable
    elif(pp==ON):
        TestManagerGateway.testManager.writeLog("PP ON") # @UndefinedVariable
        TestManagerGateway.testManager.communicationInterface().setPP(ON) # @UndefinedVariable
    else:
        TestManagerGateway.testManager.increaseException("PP must be ON or OFF") # @UndefinedVariable

def setVDD(vdd):
    if(vdd==OFF):
        TestManagerGateway.testManager.communicationInterface().setVDD(OFF) # @UndefinedVariable
        TestManagerGateway.testManager.writeLog("Power OFF") # @UndefinedVariable
    elif(vdd==ON):
        TestManagerGateway.testManager.communicationInterface().setVDD(ON) # @UndefinedVariable
        TestManagerGateway.testManager.writeLog("Power ON") # @UndefinedVariable
    else:
        TestManagerGateway.testManager.increaseException("VDD must be ON or OFF") # @UndefinedVariable

def TPMreset():
    TestManagerGateway.testManager.writeLog("TPM reset") # @UndefinedVariable
    TestManagerGateway.testManager.communicationInterface().reset() # @UndefinedVariable

def setLocality(locality):
    if(locality==0):
        newLocality = LOCALITY_0        
    elif(locality==1):
        newLocality = LOCALITY_1
    elif(locality==2):
        newLocality = LOCALITY_2
    elif(locality==3):
        newLocality = LOCALITY_3
    elif(locality==4):
        newLocality = LOCALITY_4
    else:
        TestManagerGateway.testManager.increaseException("Locality must be 0 to 4") # @UndefinedVariable
        return
    
    if(TestManagerGateway.testManager.communicationInterface().setLocality(newLocality)==True):  # @UndefinedVariable
        TestManagerGateway.testManager.writeLog("Locality set to " + str(locality))  # @UndefinedVariable
    else:
        TestManagerGateway.testManager.increaseException("Locality not set") # @UndefinedVariable

def setPollingInterval(interval):
    TestManagerGateway.testManager.communicationInterface().setPollingInterval(interval)

    