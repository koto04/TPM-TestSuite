from Tkinter import *
import Tkinter
import StringIO
import base64
import tkFont
from TS_Globals import TS_Globals
from TS_Globals import STRUCT_NONE, STRUCT_TPM_RC, STRUCT_TPM_ALG_ID, STRUCT_TPMA_OBJECT, STRUCT_TPMA_NV, STRUCT_TPMA_CC

if(TS_Globals.TS_TPM_VERSION==TS_Globals.TPM_VERSION_1_2):
    import StructInterpreter_V1 as StructInterpreter
else:
    import StructInterpreter_V2 as StructInterpreter

class MessageBox(object):

    root = None
    text = None
    msg = ""

    def __init__(self, msg, dict_key=None, boxwidth=48, translate=False, dataStruct=STRUCT_NONE):
        tki = Tkinter
        self.msg=msg
        self.dataStruct = dataStruct
        self.top = tki.Toplevel(MessageBox.root)

        frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        frm.pack(fill='both', expand=True)

        textFont = tkFont.Font(family="Courier",size=12)
        self.text = tki.Text(frm, font=textFont, relief=FLAT,  width=boxwidth )
        self.text.insert(END, msg)
        self.text.pack(padx=4, pady=4)

        caller_wants_an_entry = dict_key is not None

        if caller_wants_an_entry:
            self.entry = tki.Entry(frm)
            self.entry.pack(pady=4)

            b_submit = tki.Button(frm, text='Submit')
            b_submit['command'] = lambda: self.entry_to_dict(dict_key)
            b_submit.pack()
        
        if (translate==True):
            b_translateHex = tki.Button(frm, text='HEX')
            b_translateHex['command'] = self.translate_hex
            b_translateHex.pack(padx=4, pady=4, side=LEFT)
            
            b_translateASCII = tki.Button(frm, text='ASCII')
            b_translateASCII['command'] = self.translate_ascii
            b_translateASCII.pack(padx=4, pady=4, side=LEFT)
            
            if(dataStruct!=STRUCT_NONE):
                b_translateDatatype = tki.Button(frm, text='DATA')
                b_translateDatatype['command'] = self.translate_data
                b_translateDatatype.pack(padx=4, pady=4, side=LEFT)
                

        b_cancel = tki.Button(frm, text='close')
        b_cancel['command'] = self.top.destroy
        b_cancel.pack(padx=4, pady=4)

    def translate_data(self):
        self.text.delete(1.0, END)
        s = self.msg.replace(" ", "")
        s = s.replace("\n", "")
        if(len(s)==0):
            return
        else:
            b=bytearray(base64.b16decode(s, casefold=True))
        
        if (STRUCT_TPMA_OBJECT == self.dataStruct):    
            text ="Interpretation of TPMA_OBJECT Bits (Part 2, p:77)\n\n"
        elif (STRUCT_TPM_ALG_ID == self.dataStruct):  
            text ="Interpretation of TPM_ALG_ID (Part 2, p:41)\n\n"
        elif (STRUCT_TPM_RC == self.dataStruct):  
            text ="Interpretation of TPM_RC (Part 2, p:49)\n\n"
        elif (STRUCT_TPMA_NV == self.dataStruct):  
            text ="Interpretation of TPMA_NV (Part 2, p:155)\n\n"
        elif (STRUCT_TPMA_CC == self.dataStruct):  
            text ="Interpretation of TPMA_CC (Part 2, p:89)\n\n"
            
        self.text.insert(END,text)
        self.text.insert(END,StructInterpreter.interpretStructure(self.dataStruct,b))
        
        
    def translate_ascii(self):
        s = self.msg.replace(" ", "")
        buf=s.split("\n")
        self.text.delete(1.0, END)
        for i in range(len(buf)):
            c = bytearray.fromhex(buf[i])
            for j in range(len(c)):
                if(c[j]<33) or (c[j]>126):
                    self.text.insert(END,chr(3))
                else:
                    self.text.insert(END,chr(c[j]))
                                  
            self.text.insert(END,"\n")
        
        
    def translate_hex(self):
        self.text.delete(1.0, END)
        self.text.insert(END, self.msg)
        
    def entry_to_dict(self, dict_key):
        data = self.entry.get()
        if data:
            d, key = dict_key
            d[key] = data
            self.top.destroy()
