from TPM_Constants_V2 import *
from TS_Globals import STRUCT_TPM_RC, STRUCT_TPM_ALG_ID, STRUCT_TPMA_OBJECT, STRUCT_TPMA_NV, STRUCT_TPMA_CC

from Utils import hex2bytearray, bytearray2hex

def interpretStructure(structureType, data):
    if isinstance(data, basestring):
        data = hex2bytearray(data)
    elif isinstance(data, bytearray):
        pass
    if(structureType==STRUCT_TPM_ALG_ID): return interpret_TPM_ALG_ID(data)
    if(structureType==STRUCT_TPMA_OBJECT): return interpret_TPMA_OBJECT(data)
    if(structureType==STRUCT_TPMA_NV): return interpret_TPMA_NV(data)
    if(structureType==STRUCT_TPM_RC): return interpret_TPM_RC(data)
    if(structureType==STRUCT_TPMA_CC): return interpret_TPMA_CC(data)
    return ""
    
def interpret_TPMA_CC(data):
    if(len(data)!=4): return "Insufficient data bytes for TPMA_CC"
    attributes = []
    if (data[1]&0x40): attributes.append("nv")
    if (data[1]&0x80): attributes.append("extensive")
    if (data[0]&0x01): attributes.append("flushed" )
    attributes.append("cHandles=" + str((data[0]&0x0E)>>1))
    if (data[0]&0x10): attributes.append("rHandle" )
    if (data[0]&0x20): attributes.append("V" )
    attribute = ', '.join(attributes)
    if(data[2:]==bytearray([0x01, 0x1F])): command= "TPM_CC_NV_UndefineSpaceSpecial"
    elif(data[2:]==bytearray([0x01, 0x20])): command= "TPM_CC_EvictControl"
    elif(data[2:]==bytearray([0x01, 0x21])): command= "TPM_CC_HierarchyControl"
    elif(data[2:]==bytearray([0x01, 0x22])): command= "TPM_CC_NV_UndefineSpace"
    elif(data[2:]==bytearray([0x01, 0x24])): command= "TPM_CC_ChangeEPS"
    elif(data[2:]==bytearray([0x01, 0x25])): command= "TPM_CC_ChangePPS"
    elif(data[2:]==bytearray([0x01, 0x26])): command= "TPM_CC_Clear"
    elif(data[2:]==bytearray([0x01, 0x27])): command= "TPM_CC_ClearControl"
    elif(data[2:]==bytearray([0x01, 0x28])): command= "TPM_CC_ClockSet"
    elif(data[2:]==bytearray([0x01, 0x29])): command= "TPM_CC_HierarchyChangeAuth"
    elif(data[2:]==bytearray([0x01, 0x2A])): command= "TPM_CC_NV_DefineSpace"
    elif(data[2:]==bytearray([0x01, 0x2B])): command= "TPM_CC_PCR_Allocate"
    elif(data[2:]==bytearray([0x01, 0x2C])): command= "TPM_CC_PCR_SetAuthPolicy"
    elif(data[2:]==bytearray([0x01, 0x2D])): command= "TPM_CC_PP_Commands"
    elif(data[2:]==bytearray([0x01, 0x2E])): command= "TPM_CC_SetPrimaryPolicy"
    elif(data[2:]==bytearray([0x01, 0x2F])): command= "TPM_CC_FieldUpgradeStart"
    elif(data[2:]==bytearray([0x01, 0x30])): command= "TPM_CC_ClockRateAdjust"
    elif(data[2:]==bytearray([0x01, 0x31])): command= "TPM_CC_CreatePrimary"
    elif(data[2:]==bytearray([0x01, 0x32])): command= "TPM_CC_NV_GlobalWriteLock"
    elif(data[2:]==bytearray([0x01, 0x33])): command= "TPM_CC_GetCommandAuditDigest"
    elif(data[2:]==bytearray([0x01, 0x34])): command= "TPM_CC_NV_Increment"
    elif(data[2:]==bytearray([0x01, 0x35])): command= "TPM_CC_NV_SetBits"
    elif(data[2:]==bytearray([0x01, 0x36])): command= "TPM_CC_NV_Extend"
    elif(data[2:]==bytearray([0x01, 0x37])): command= "TPM_CC_NV_Write"
    elif(data[2:]==bytearray([0x01, 0x38])): command= "TPM_CC_NV_WriteLock"
    elif(data[2:]==bytearray([0x01, 0x39])): command= "TPM_CC_DictionaryAttackLockReset"
    elif(data[2:]==bytearray([0x01, 0x3A])): command= "TPM_CC_DictionaryAttackParameters"
    elif(data[2:]==bytearray([0x01, 0x3B])): command= "TPM_CC_NV_ChangeAuth"
    elif(data[2:]==bytearray([0x01, 0x3C])): command= "TPM_CC_PCR_Event"
    elif(data[2:]==bytearray([0x01, 0x3D])): command= "TPM_CC_PCR_Reset"
    elif(data[2:]==bytearray([0x01, 0x3E])): command= "TPM_CC_SequenceComplete"
    elif(data[2:]==bytearray([0x01, 0x3F])): command= "TPM_CC_SetAlgorithmSet"
    elif(data[2:]==bytearray([0x01, 0x40])): command= "TPM_CC_SetCommandCodeAuditStatus"
    elif(data[2:]==bytearray([0x01, 0x41])): command= "TPM_CC_FieldUpgradeData"
    elif(data[2:]==bytearray([0x01, 0x42])): command= "TPM_CC_IncrementalSelfTest"
    elif(data[2:]==bytearray([0x01, 0x43])): command= "TPM_CC_SelfTest"
    elif(data[2:]==bytearray([0x01, 0x44])): command= "TPM_CC_Startup"
    elif(data[2:]==bytearray([0x01, 0x45])): command= "TPM_CC_Shutdown"
    elif(data[2:]==bytearray([0x01, 0x46])): command= "TPM_CC_StirRandom"
    elif(data[2:]==bytearray([0x01, 0x47])): command= "TPM_CC_ActivateCredential"
    elif(data[2:]==bytearray([0x01, 0x48])): command= "TPM_CC_Certify"
    elif(data[2:]==bytearray([0x01, 0x49])): command= "TPM_CC_PolicyNV"
    elif(data[2:]==bytearray([0x01, 0x4A])): command= "TPM_CC_CertifyCreation"
    elif(data[2:]==bytearray([0x01, 0x4B])): command= "TPM_CC_Duplicate"
    elif(data[2:]==bytearray([0x01, 0x4C])): command= "TPM_CC_GetTime"
    elif(data[2:]==bytearray([0x01, 0x4D])): command= "TPM_CC_GetSessionAuditDigest"
    elif(data[2:]==bytearray([0x01, 0x4E])): command= "TPM_CC_NV_Read"
    elif(data[2:]==bytearray([0x01, 0x4F])): command= "TPM_CC_NV_ReadLock"
    elif(data[2:]==bytearray([0x01, 0x50])): command= "TPM_CC_ObjectChangeAuth"
    elif(data[2:]==bytearray([0x01, 0x51])): command= "TPM_CC_PolicySecret"
    elif(data[2:]==bytearray([0x01, 0x52])): command= "TPM_CC_Rewrap"
    elif(data[2:]==bytearray([0x01, 0x53])): command= "TPM_CC_Create"
    elif(data[2:]==bytearray([0x01, 0x54])): command= "TPM_CC_ECDH_ZGen"
    elif(data[2:]==bytearray([0x01, 0x55])): command= "TPM_CC_HMAC"
    elif(data[2:]==bytearray([0x01, 0x56])): command= "TPM_CC_Import"
    elif(data[2:]==bytearray([0x01, 0x57])): command= "TPM_CC_Load"
    elif(data[2:]==bytearray([0x01, 0x58])): command= "TPM_CC_Quote"
    elif(data[2:]==bytearray([0x01, 0x59])): command= "TPM_CC_RSA_Decrypt"
    elif(data[2:]==bytearray([0x01, 0x5B])): command= "TPM_CC_HMAC_Start"
    elif(data[2:]==bytearray([0x01, 0x5C])): command= "TPM_CC_SequenceUpdate"
    elif(data[2:]==bytearray([0x01, 0x5D])): command= "TPM_CC_Sign"
    elif(data[2:]==bytearray([0x01, 0x5E])): command= "TPM_CC_Unseal"
    elif(data[2:]==bytearray([0x01, 0x60])): command= "TPM_CC_PolicySigned"
    elif(data[2:]==bytearray([0x01, 0x61])): command= "TPM_CC_ContextLoad"
    elif(data[2:]==bytearray([0x01, 0x62])): command= "TPM_CC_ContextSave"
    elif(data[2:]==bytearray([0x01, 0x63])): command= "TPM_CC_ECDH_KeyGen"
    elif(data[2:]==bytearray([0x01, 0x64])): command= "TPM_CC_EncryptDecrypt"
    elif(data[2:]==bytearray([0x01, 0x65])): command= "TPM_CC_FlushContext"
    elif(data[2:]==bytearray([0x01, 0x67])): command= "TPM_CC_LoadExternal"
    elif(data[2:]==bytearray([0x01, 0x68])): command= "TPM_CC_MakeCredential"
    elif(data[2:]==bytearray([0x01, 0x69])): command= "TPM_CC_NV_ReadPublic"
    elif(data[2:]==bytearray([0x01, 0x6A])): command= "TPM_CC_PolicyAuthorize"
    elif(data[2:]==bytearray([0x01, 0x6B])): command= "TPM_CC_PolicyAuthValue"
    elif(data[2:]==bytearray([0x01, 0x6C])): command= "TPM_CC_PolicyCommandCode"
    elif(data[2:]==bytearray([0x01, 0x6D])): command= "TPM_CC_PolicyCounterTimer"
    elif(data[2:]==bytearray([0x01, 0x6E])): command= "TPM_CC_PolicyCpHash"
    elif(data[2:]==bytearray([0x01, 0x6F])): command= "TPM_CC_PolicyLocality"
    elif(data[2:]==bytearray([0x01, 0x70])): command= "TPM_CC_PolicyNameHash"
    elif(data[2:]==bytearray([0x01, 0x71])): command= "TPM_CC_PolicyOR"
    elif(data[2:]==bytearray([0x01, 0x72])): command= "TPM_CC_PolicyTicket"
    elif(data[2:]==bytearray([0x01, 0x73])): command= "TPM_CC_ReadPublic"
    elif(data[2:]==bytearray([0x01, 0x74])): command= "TPM_CC_RSA_Encrypt"
    elif(data[2:]==bytearray([0x01, 0x76])): command= "TPM_CC_StartAuthSession"
    elif(data[2:]==bytearray([0x01, 0x77])): command= "TPM_CC_VerifySignature"
    elif(data[2:]==bytearray([0x01, 0x78])): command= "TPM_CC_ECC_Parameters"
    elif(data[2:]==bytearray([0x01, 0x79])): command= "TPM_CC_FirmwareRead"
    elif(data[2:]==bytearray([0x01, 0x7A])): command= "TPM_CC_GetCapability"
    elif(data[2:]==bytearray([0x01, 0x7B])): command= "TPM_CC_GetRandom"
    elif(data[2:]==bytearray([0x01, 0x7C])): command= "TPM_CC_GetTestResult"
    elif(data[2:]==bytearray([0x01, 0x7D])): command= "TPM_CC_Hash"
    elif(data[2:]==bytearray([0x01, 0x7E])): command= "TPM_CC_PCR_Read"
    elif(data[2:]==bytearray([0x01, 0x7F])): command= "TPM_CC_PolicyPCR"
    elif(data[2:]==bytearray([0x01, 0x80])): command= "TPM_CC_PolicyRestart"
    elif(data[2:]==bytearray([0x01, 0x81])): command= "TPM_CC_ReadClock"
    elif(data[2:]==bytearray([0x01, 0x82])): command= "TPM_CC_PCR_Extend"
    elif(data[2:]==bytearray([0x01, 0x83])): command= "TPM_CC_PCR_SetAuthValue"
    elif(data[2:]==bytearray([0x01, 0x84])): command= "TPM_CC_NV_Certify"
    elif(data[2:]==bytearray([0x01, 0x85])): command= "TPM_CC_EventSequenceComplete"
    elif(data[2:]==bytearray([0x01, 0x86])): command= "TPM_CC_HashSequenceStart"
    elif(data[2:]==bytearray([0x01, 0x87])): command= "TPM_CC_PolicyPhysicalPresence"
    elif(data[2:]==bytearray([0x01, 0x88])): command= "TPM_CC_PolicyDuplicationSelect"
    elif(data[2:]==bytearray([0x01, 0x89])): command= "TPM_CC_PolicyGetDigest"
    elif(data[2:]==bytearray([0x01, 0x8A])): command= "TPM_CC_TestParms"
    elif(data[2:]==bytearray([0x01, 0x8B])): command= "TPM_CC_Commit"
    elif(data[2:]==bytearray([0x01, 0x8C])): command= "TPM_CC_PolicyPassword"
    elif(data[2:]==bytearray([0x01, 0x8D])): command= "TPM_CC_ZGen_2Phase"
    elif(data[2:]==bytearray([0x01, 0x8E])): command= "TPM_CC_EC_Ephemeral"
    elif(data[2:]==bytearray([0x01, 0x8F])): command= "TPM_CC_PolicyNvWritten"
    else: command=bytearray2hex(data[2:])
    return command + " " + attribute + "\n"
     
    
def interpret_TPMA_OBJECT(data):
    if(len(data)!=4): return "Insufficient data bytes for TPMA_OBJECT"
    text = "fixedTPM: " + ("SET" if (data[3]&0x02) else "CLEAR") + "\n"
    text += "stClear: " + ("SET" if (data[3]&0x04) else "CLEAR") + "\n"
    text += "fixedParent: " + ("SET" if (data[3]&0x10) else "CLEAR") + "\n"
    text += "sensitiveDataOrigin: " + ("SET" if (data[3]&0x20) else "CLEAR") + "\n"
    text += "userWithAuth: " + ("SET" if (data[3]&0x40) else "CLEAR") + "\n"
    text += "adminWithPolicy: " + ("SET" if (data[3]&0x80) else "CLEAR") + "\n"
    text += "noDA: " + ("SET" if (data[2]&0x04) else "CLEAR") + "\n"
    text += "encryptedDuplication: " + ("SET" if (data[2]&0x08) else "CLEAR") + "\n"
    text += "restricted: " + ("SET" if (data[1]&0x01) else "CLEAR") + "\n"
    text += "decrypt: " + ("SET" if (data[1]&0x02) else "CLEAR") + "\n"
    text += "sign: " + ("SET" if (data[1]&0x04) else "CLEAR") + "\n"
    return text

def interpret_TPMA_NV(data):
    if(len(data)!=4): return "Insufficient data bytes for TPMA_NV"
    text  = "TPMA_NV_PPWRITE: " + ("SET" if (data[3]&0x01) else "CLEAR") + "\n"
    text += "TPMA_NV_OWNERWRITE: " + ("SET" if (data[3]&0x02) else "CLEAR") + "\n"
    text += "TPMA_NV_AUTHWRITE: " + ("SET" if (data[3]&0x04) else "CLEAR") + "\n"
    text += "TPMA_NV_POLICYWRITE: " + ("SET" if (data[3]&0x08) else "CLEAR") + "\n"
    text += "TPMA_NV_COUNTER: " + ("SET" if (data[3]&0x10) else "CLEAR") + "\n"
    text += "TPMA_NV_BITS: " + ("SET" if (data[3]&0x20) else "CLEAR") + "\n"
    text += "TPMA_NV_EXTENDED: " + ("SET" if (data[3]&0x40) else "CLEAR") + "\n"
    text += "TPMA_NV_POLICY_DELETE: " + ("SET" if (data[2]&0x04) else "CLEAR") + "\n"
    text += "TPMA_NV_WRITELOCKED: " + ("SET" if (data[2]&0x08) else "CLEAR") + "\n"
    text += "TPMA_NV_WRITEALL: " + ("SET" if (data[2]&0x10) else "CLEAR") + "\n"
    text += "TPMA_NV_WRITEDEFINE: " + ("SET" if (data[2]&0x20) else "CLEAR") + "\n"
    text += "TPMA_NV_WRITE_STCLEAR: " + ("SET" if (data[2]&0x40) else "CLEAR") + "\n"
    text += "TPMA_NV_GLOBALLOCK: " + ("SET" if (data[2]&0x80) else "CLEAR") + "\n"
    text += "TPMA_NV_PPREAD: " + ("SET" if (data[1]&0x01) else "CLEAR") + "\n"
    text += "TPMA_NV_OWNERREAD: " + ("SET" if (data[1]&0x02) else "CLEAR") + "\n"
    text += "TPMA_NV_AUTHREAD: " + ("SET" if (data[1]&0x04) else "CLEAR") + "\n"
    text += "TPMA_NV_POLICYREAD: " + ("SET" if (data[1]&0x08) else "CLEAR") + "\n"
    text += "TPMA_NV_NO_DA: " + ("SET" if (data[0]&0x02) else "CLEAR") + "\n"
    text += "TPMA_NV_ORDERLY: " + ("SET" if (data[0]&0x04) else "CLEAR") + "\n"
    text += "TPMA_NV_CLEAR_STCLEAR: " + ("SET" if (data[0]&0x08) else "CLEAR") + "\n"
    text += "TPMA_NV_READLOCKED: " + ("SET" if (data[0]&0x10) else "CLEAR") + "\n"
    text += "TPMA_NV_WRITTEN: " + ("SET" if (data[0]&0x20) else "CLEAR") + "\n"
    text += "TPMA_NV_PLATFORMCREATE: " + ("SET" if (data[0]&0x40) else "CLEAR") + "\n"
    text += "TPMA_NV_READ_STCLEAR: " + ("SET" if (data[0]&0x80) else "CLEAR") + "\n"
    return text

def interpret_TPM_ALG_ID(alg):

    if(len(alg)!=2): return "Insufficient data bytes for TPM_ALG_ID"
    if(alg==bytearray([0x00, 0x01])): return "TPM_ALG_RSA"
    if(alg==bytearray([0x00, 0x04])): return "TPM_ALG_SHA1"
    if(alg==bytearray([0x00, 0x05])): return "TPM_ALG_HMAC"
    if(alg==bytearray([0x00, 0x06])): return "TPM_ALG_AES"
    if(alg==bytearray([0x00, 0x07])): return "TPM_ALG_MGF1"
    if(alg==bytearray([0x00, 0x08])): return "TPM_ALG_KEYEDHASH"
    if(alg==bytearray([0x00, 0x0A])): return "TPM_ALG_XOR"
    if(alg==bytearray([0x00, 0x0B])): return "TPM_ALG_SHA256"
    if(alg==bytearray([0x00, 0x0C])): return "TPM_ALG_SHA384"
    if(alg==bytearray([0x00, 0x0D])): return "TPM_ALG_SHA512"
    if(alg==bytearray([0x00, 0x10])): return "TPM_ALG_NULL"
    if(alg==bytearray([0x00, 0x12])): return "TPM_ALG_SM3_256"
    if(alg==bytearray([0x00, 0x13])): return "TPM_ALG_SM4"
    if(alg==bytearray([0x00, 0x14])): return "TPM_ALG_RSASSA"
    if(alg==bytearray([0x00, 0x15])): return "TPM_ALG_RSAES"
    if(alg==bytearray([0x00, 0x16])): return "TPM_ALG_RSAPSS"
    if(alg==bytearray([0x00, 0x17])): return "TPM_ALG_OAEP"
    if(alg==bytearray([0x00, 0x18])): return "TPM_ALG_ECDSA"
    if(alg==bytearray([0x00, 0x19])): return "TPM_ALG_ECDH"
    if(alg==bytearray([0x00, 0x1A])): return "TPM_ALG_ECDAA"
    if(alg==bytearray([0x00, 0x1B])): return "TPM_ALG_SM2"
    if(alg==bytearray([0x00, 0x1C])): return "TPM_ALG_ECSCHNORR"
    if(alg==bytearray([0x00, 0x1D])): return "TPM_ALG_ECMQV"
    if(alg==bytearray([0x00, 0x20])): return "TPM_ALG_KDF1_SP800_56A"
    if(alg==bytearray([0x00, 0x21])): return "TPM_ALG_KDF2"
    if(alg==bytearray([0x00, 0x22])): return "TPM_ALG_KDF1_SP800_108"
    if(alg==bytearray([0x00, 0x23])): return "TPM_ALG_ECC"
    if(alg==bytearray([0x00, 0x25])): return "TPM_ALG_SYMCIPHER"
    if(alg==bytearray([0x00, 0x26])): return "TPM_ALG_CAMELLIA"
    if(alg==bytearray([0x00, 0x40])): return "TPM_ALG_CTR"
    if(alg==bytearray([0x00, 0x41])): return "TPM_ALG_OFB"
    if(alg==bytearray([0x00, 0x42])): return "TPM_ALG_CBC"
    if(alg==bytearray([0x00, 0x43])): return "TPM_ALG_CFB"
    if(alg==bytearray([0x00, 0x44])): return "TPM_ALG_ECB"
    return "Unknown TPM_ALG_ID"
    

        
def interpret_TPM_RC(rc):
    message = "RC: "
    if(int(rc[2])==0) and (int(rc[3])==0):
        message +="success (TPM_RC_SUCCESS)"  
    elif(rc[2]==0x00) and (rc[3]==0x1E):
        message +="(TPM_RC_BAD_TAG)"
    elif(rc[3]&0x80):
        if(rc[3]&0x40):
            message += "Parameter " + str(int(rc[2]&0x0F)) + ": "
        else:
            if(rc[2]&0x80):
                message +="Session " + str(int(rc[2]&0x07)) + ": "
            else:
                message +="Handle " + str(int(rc[2]&0x07)) + ": "
        if(rc[3]&0x3F==0x01):
            message += "symmetric algorithm not supported or not correct (TPM_RC_ASYMMETRIC)"
        elif(rc[3]&0x3F==0x02):
            message += "inconsistent attributes (TPM_RC_ATTRIBUTES)"
        elif(rc[3]&0x3F==0x03):
            message += "hash algorithm not supported or not appropriate (TPM_RC_HASH)"
        elif(rc[3]&0x3F==0x04):
            message += "value is out of range or is not correct for the context (TPM_RC_VALUE)"
        elif(rc[3]&0x3F==0x05):
            message += "hierarchy is not enabled or is not correct for the use (TPM_RC_HIERARCHY)"
        elif(rc[3]&0x3F==0x06):
            message += ""
        elif(rc[3]&0x3F==0x07):
            message += "key size is not supported (TPM_RC_KEY_SIZE)"
        elif(rc[3]&0x3F==0x08):
            message += "mask generation function not supported (TPM_RC_MGF)"
        elif(rc[3]&0x3F==0x09):
            message += "mode of operation not supported (TPM_RC_MODE)"
        elif(rc[3]&0x3F==0x0A):
            message += "the type of the value is not appropriate for the use (TPM_RC_TYPE)"
        elif(rc[3]&0x3F==0x0B):
            message += "the handle is not correct for the use (TPM_RC_HANDLE)"
        elif(rc[3]&0x3F==0x0C):
            message += "unsupported key derivation function or function not appropriate for use (TPM_RC_KDF)"
        elif(rc[3]&0x3F==0x0D):
            message += "value was out of allowed range (TPM_RC_RANGE)"
        elif(rc[3]&0x3F==0x0E):
            message += "the  authorization  HMAC  check  failed  and  DA counter incremented (TPM_RC_AUTH_FAIL)"
        elif(rc[3]&0x3F==0x0F):
            message += " invalid nonce size (TPM_RC_NONCE)"
        elif(rc[3]&0x3F==0x10):
            message += "authorization requires assertion of PP (TPM_RC_PP)"
        elif(rc[3]&0x3F==0x12):
            message += "unsupported or incompatible scheme (TPM_RC_SCHEME)"
        elif(rc[3]&0x3F==0x15):
            message += "structure is the wrong size (TPM_RC_SIZE)"  
        elif(rc[3]&0x3F==0x16):
            message += " unsupported symmetric algorithm or key size, or not appropriate for instance (TPM_RC_SYMMETRIC)"
        elif(rc[3]&0x3F==0x17):
            message += "incorrect structure tag (TPM_RC_TAG)"
        elif(rc[3]&0x3F==0x18):
            message += "union selector is incorrect (TPM_RC_SELECTOR)"
        elif(rc[3]&0x3F==0x1A):
            message += "the TPM was unable to unmarshal a value because there were not enough octets in the input buffer (TPM_RC_INSUFFICIENT)"
        elif(rc[3]&0x3F==0x1B):
            message += "the signature is not valid (TPM_RC_SIGNATURE)"
        elif(rc[3]&0x3F==0x1C):
            message += "key fields are not compatible with the selected use (TPM_RC_KEY)"
        elif(rc[3]&0x3F==0x1D):
            message += "a policy check failed (TPM_RC_POLICY_FAIL)"
        elif(rc[3]&0x3F==0x1F):
            message += "integrity check failed (TPM_RC_INTEGRITY)"
        elif(rc[3]&0x3F==0x20):
            message += "invalid ticket (TPM_RC_TICKET)"
        elif(rc[3]&0x3F==0x21):
            message += "reserved bits not set to zero as required (TPM_RC_RESERVED_BITS)"
        elif(rc[3]&0x3F==0x22):
            message += "authorization failure without DA implications (TPM_RC_BAD_AUTH)"
        elif(rc[3]&0x3F==0x23):
            message += "the policy has expired (TPM_RC_EXPIRED)"
        elif(rc[3]&0x3F==0x24):
            message += "the commandCode in the policy is not the commandCode of the command or the command code in a policy command references a command that is not implemented (TPM_RC_POLICY_CC)"
        elif(rc[3]&0x3F==0x25):
            message += "public and sensitive portions of an object are not cryptographically bound (TPM_RC_BINDING)"
        elif(rc[3]&0x3F==0x26):
            message += "curve not supported (TPM_RC_CURVE)"
        elif(rc[3]&0x3F==0x27):
            message += "point is not on the required curve (TPM_RC_ECC_POINT)"
    else:
        if(rc[2]&0x09==0x01):
            if(rc[3]==0x00):
                message += "TPM not initialized (TPM_RC_INITIALIZE)"
            elif(rc[3]==0x01):
                message += "commands not being accepted because of a TPM failure (TPM_RC_FAILURE)"
            elif (rc[3]==0x03):
                message += "improper use of a sequence handle (TPM_RC_SEQUENCE)"
            elif (rc[3]==0x0B):
                message += " (TPM_RC_PRIVATE)"
            elif (rc[3]==0x19):
                message += " (TPM_RC_HMAC )"
            elif (rc[3]==0x20):
                message += " (TPM_RC_DISABLED)"
            elif (rc[3]==0x21):
                message += "command failed because audit sequence required exclusivity (TPM_RC_EXCLUSIVE)"
            elif (rc[3]==0x24):
                message += "authorization handle is not correct for command (TPM_RC_AUTH_TYPE)"
            elif (rc[3]==0x25):
                message += "command  requires  an  authorization  session  for handle and it is not present. (TPM_RC_AUTH_MISSING)"
            elif (rc[3]==0x26):
                message += "policy  Failure  In  Math  Operation  or  an  invalid authPolicy value (TPM_RC_POLICY)"
            elif (rc[3]==0x27):
                message += "PCR check fail (TPM_RC_PCR)"
            elif (rc[3]==0x28):
                message += "PCR have changed since checked. (TPM_RC_PCR_CHANGED)"
            elif (rc[3]==0x2D):
                message += "for all commands other than TPM2_FieldUpgradeData(), this code indicates that the TPM is in field upgrade mode; for TPM2_FieldUpgradeData(), this code indicates that the TPM is not in field upgrade mode (TPM_RC_UPGRADE)"
            elif (rc[3]==0x2E):
                message += "context ID counter is at maximum. (TPM_RC_TOO_MANY_CONTEXTS)"
            elif (rc[3]==0x2F):
                message += "authValue or authPolicy is not available for selected entity.(TPM_RC_AUTH_UNAVAILABLE)"
            elif (rc[3]==0x30):
                message += "a _TPM_Init and Startup(CLEAR) is required before the TPM can resume operation.(TPM_RC_REBOOT)"
            elif (rc[3]==0x31):
                message += "the protection algorithms (hash and symmetric) are not reasonably  balanced. The digest size of the hash  must be larger than the key size of the symmetric algorithm. (TPM_RC_UNBALANCED)"
            elif (rc[3]==0x42):
                message += "command commandSize value is inconsistent with contents of the command buffer; either the size is not the same as the octets loaded by the hardware interface layer or the value is not large enough to hold a command header (TPM_RC_COMMAND_SIZE)"
            elif (rc[3]==0x43):
                message += "command code not supported (TPM_RC_COMMAND_CODE)"
            elif (rc[3]==0x44):
                message += "the value of authorizationSize is out of range or the number  of octets in the Authorization Area is greater than required (TPM_RC_AUTHSIZE)"
            elif (rc[3]==0x45):
                message += "use  of  an  authorization session with a context command or another command  that cannot have an authorization session.(TPM_RC_AUTH_CONTEXT)"
            elif (rc[3]==0x46):
                message += "NV offset+size is out of range. (TPM_RC_NV_RANGE)"
            elif (rc[3]==0x47):
                message += "Requested allocation size is larger than allowed.(TPM_RC_NV_SIZE)"
            elif (rc[3]==0x48):
                message += "NV access locked. (TPM_RC_NV_LOCKED)"
            elif (rc[3]==0x49):
                message += "NV access authorization fails in command actions (this failure does not affect lockout.action) (TPM_RC_NV_AUTHORIZATION)"
            elif (rc[3]==0x4A):
                message += "an NV Index is used before being initialized or the state saved by TPM2_Shutdown(STATE) could not be restored(TPM_RC_NV_UNINITIALIZED)"
            elif (rc[3]==0x4B):
                message += "insufficient space for NV allocation (TPM_RC_NV_SPACE)"
            elif (rc[3]==0x4C):
                message += "NV Index or persistend object already defined (TPM_RC_NV_DEFINED)"
            elif (rc[3]==0x50):
                message += "context in TPM2_ContextLoad() is not valid (TPM_RC_BAD_CONTEXT)"
            elif (rc[3]==0x51):
                message += "cpHash value already set or not correct for use (TPM_RC_CPHASH)"
            elif (rc[3]==0x52):
                message += "handle for parent is not a valid parent (TPM_RC_PARENT)"
            elif (rc[3]==0x53):
                message += "some function needs testing (TPM_RC_NEEDS_TEST)"
            elif (rc[3]==0x54):
                message += "returned when an internal function cannot process a request due to an unspecified problem. This code is usually related to invalid parameters that are not properly filtered by the input unmarshaling code. (TPM_RC_NO_RESULT)"
            elif (rc[3]==0x55):
                message += "the sensitive area did not unmarshal correctly after decryption - this code is used in lieu of the other unmarshaling  errors  so  that  an  attacker  cannot determine where the unmarshaling error occurred (TPM_RC_SENSITIVE)"
        elif(rc[2]&0x09==0x09):
            if(rc[3]==0x01):
                message += "gap for context ID is too large (TPM_RC_CONTEXT_GAP)"
            elif(rc[3]==0x02):
                message += "out of memory for object contexts (TPM_RC_OBJECT_MEMORY)"
            elif(rc[3]==0x03):
                message += "out of memory for session contexts (TPM_RC_SESSION_MEMORY)"
            elif(rc[3]==0x04):
                message += "out of shared object/session memory  or need space for internal operations (TPM_RC_MEMORY)"
            elif(rc[3]==0x05):
                message += "out of session handles - a session must be flushed before a new session may be created (TPM_RC_SESSION_HANDLES)"
            elif(rc[3]==0x06):
                message += "out of object handles - the handle space for objects is depleted and a reboot is required (TPM_RC_OBJECT_HANDLES)"
            elif(rc[3]==0x07):
                message += "bad locality (TPM_RC_LOCALITY)"
            elif(rc[3]==0x08):
                message += "the TPM has suspended operation on  the command; forward progress was made and the command may be retried. (TPM_RC_YIELDED)"
            elif(rc[3]==0x09):
                message += "the command was canceled (TPM_RC_CANCELED)"
            elif(rc[3]==0x0A):
                message += "TPM is performing self-tests (TPM_RC_TESTING)"
            elif(rc[3]==0x10):
                message += "the 1st handle in the handle area  references a transient object or session that is not loaded (TPM_RC_REFERENCE_H0)"
            elif(rc[3]==0x11):
                message += "the 2nd handle in the handle area  references a transient object or session that is not loaded (TPM_RC_REFERENCE_H1)"
            elif(rc[3]==0x12):
                message += "the 3rd handle in the handle area  references a transient object or session that is not loaded (TPM_RC_REFERENCE_H2)"
            elif(rc[3]==0x13):
                message += "the 4th handle in the handle area  references a transient object or session that is not loaded (TPM_RC_REFERENCE_H3)"
            elif(rc[3]==0x14):
                message += "the 5th handle in the handle area  references a transient object or session that is not loaded (TPM_RC_REFERENCE_H4)"
            elif(rc[3]==0x15):
                message += "the 6th handle in the handle area  references a transient object or session that is not loaded (TPM_RC_REFERENCE_H5)"
            elif(rc[3]==0x16):
                message += "the 7th handle in the handle area  references a transient object or session that is not loaded (TPM_RC_REFERENCE_H6)"
            elif(rc[3]==0x18):
                message += "the 1st authorization session handle  references a session that is not loaded (TPM_RC_REFERENCE_S0)"
            elif(rc[3]==0x19):
                message += "the 2nd authorization session handle  references a session that is not loaded (TPM_RC_REFERENCE_S1)"
            elif(rc[3]==0x1A):
                message += "the 3rd authorization session handle  references a session that is not loaded (TPM_RC_REFERENCE_S2)"
            elif(rc[3]==0x1B):
                message += "the 4th authorization session handle  references a session that is not loaded (TPM_RC_REFERENCE_S3)"
            elif(rc[3]==0x1C):
                message += "the 5th authorization session handle  references a session that is not loaded (TPM_RC_REFERENCE_S4)"
            elif(rc[3]==0x1D):
                message += "the 6th authorization session handle  references a session that is not loaded (TPM_RC_REFERENCE_S5)"
            elif(rc[3]==0x1E):
                message += "the 7th authorization session handle  references a session that is not loaded (TPM_RC_REFERENCE_S6)"
            elif(rc[3]==0x20):
                message += "the TPM is rate-limiting accesses to prevent wearout of NV (TPM_RC_NV_RATE)"
            elif(rc[3]==0x21):
                message += "authorizations for objects subject to DA protection are not allowed at this time because the TPM is in DA lockout mode (TPM_RC_LOCKOUT)"
            elif(rc[3]==0x22):
                message += "the TPM was not able to start the command (TPM_RC_RETRY)"
            elif(rc[3]==0x23):
                message += "the command may require writing of NV and NV is not current accessible (TPM_RC_NV_UNAVAILABLE)"
            elif(rc[3]==0x7F):
                message += "this value is reserved and shall not be returned by the TPM (TPM_RC_NOT_USED)"
                
    return message
