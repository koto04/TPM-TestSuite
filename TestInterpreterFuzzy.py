from TestInterpreter import TestManagerGateway

def getFuzzyResponse():
    return TestManagerGateway.testManager.fuzzyResponse() # @UndefinedVariable

def setFuzzyLevel(level):
    
    if level==1:
        TestManagerGateway.testManager.sFuzzyLevel = 1
    elif level==2:
        TestManagerGateway.testManager.sFuzzyLevel = 2
    else:
        TestManagerGateway.testManager.sFuzzyLevel = 0
        
