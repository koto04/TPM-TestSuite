
class TPM_ACCESS: 
    addr = 0x0000
    size = 1
    tpmRegValidStsFlag = 0x80
    activeLocalityFlag = 0x20
    beenSeizedFlag = 0x10
    SeizeFlag = 0x08
    pendingRequestFlag = 0x04
    requestUseFlag  = 0x02
    tpmEstablishmentFlag = 0x01

    tpmRegValidSts = 0
    activeLocality = 0
    beenSeized = 0
    Seize = 0
    pendingRequest = 0
    requestUse  = 0
    tpmEstablishment = 0
    
    @staticmethod
    def Fill(data):
        TPM_ACCESS.tpmRegValidSts           = (data[0]>>7) & 0x01
        TPM_ACCESS.activeLocality           = (data[0]>>5) & 0x01
        TPM_ACCESS.beenSeized               = (data[0]>>4) & 0x01
        TPM_ACCESS.Seize                    = (data[0]>>3) & 0x01
        TPM_ACCESS.pendingRequest           = (data[0]>>2) & 0x01
        TPM_ACCESS.requestUse               = (data[0]>>1) & 0x01
        TPM_ACCESS.tpmEstablishment         = (data[0]>>7) & 0x01

    @staticmethod
    def Print():
        ret = ""
        ret += "TPM_ACCESS" + "\n"
        ret +=  "tpmRegValidSts: " + str(TPM_ACCESS.tpmRegValidSts) + "\n"
        ret +=  "activeLocality: " + str(TPM_ACCESS.activeLocality) + "\n"
        ret +=  "beenSeized: " + str(TPM_ACCESS.beenSeized) + "\n"
        ret +=  "Seize: " + str(TPM_ACCESS.Seize) + "\n"
        ret +=  "pendingRequest: " + str(TPM_ACCESS.pendingRequest) + "\n"
        ret +=  "requestUse: " + str(TPM_ACCESS.requestUse) + "\n"
        ret +=  "tpmEstablishment: " + str(TPM_ACCESS.tpmEstablishment) + "\n"
        ret +=  ""  + "\n"
        print ret
        return ret
        
class TPM_INT_ENABLE:              
    addr = 0x0008
    size = 4
    globalIntEnable = 0
    commandReadyEnable = 0
    typePolarity = 0
    localityChangeIntEnable = 0
    stsValidIntEnable = 0
    dataAvailIntEnable  = 0
    
    @staticmethod
    def Fill(data):
        TPM_INT_ENABLE.globalIntEnable           = (data[3]>>7) & 0x01
        TPM_INT_ENABLE.commandReadyEnable        = (data[0]>>7) & 0x01
        TPM_INT_ENABLE.typePolarity              = (data[0]>>3) & 0x03
        TPM_INT_ENABLE.localityChangeIntEnable   = (data[0]>>2) & 0x01
        TPM_INT_ENABLE.stsValidIntEnable         = (data[0]>>1) & 0x01
        TPM_INT_ENABLE.dataAvailIntEnable        = (data[0]) & 0x01
        pass
    
    @staticmethod
    def Print():
        ret = ""  + "\n"
        ret +=  "TPM_INT_ENABLE"  + "\n"
        ret +=  "globalIntEnable: " + str(TPM_INT_ENABLE.globalIntEnable) + "\n"
        ret +=  "commandReadyEnable: " + str(TPM_INT_ENABLE.commandReadyEnable) + "\n"
        ret +=  "typePolarity: " + str(TPM_INT_ENABLE.typePolarity) + "\n"
        ret +=  "localityChangeIntEnable: " + str(TPM_INT_ENABLE.localityChangeIntEnable) + "\n"
        ret +=  "stsValidIntEnable: " + str(TPM_INT_ENABLE.stsValidIntEnable) + "\n"
        ret +=  "dataAvailIntEnable: " + str(TPM_INT_ENABLE.dataAvailIntEnable) + "\n"
        ret +=  "" + "\n"
        print ret
        return ret
           
class TPM_INT_VECTOR:              
    addr = 0x000C
    size = 1
    sirqVec = 0

    @staticmethod
    def Fill(data):
        TPM_INT_VECTOR.sirqVec           = (data[0]) & 0x0f
    
    @staticmethod
    def Print():
        ret=""
        ret += "TPM_INT_VECTOR" + "\n"
        ret += "sirqVec: " + str(TPM_INT_VECTOR.sirqVec) + "\n"
        ret += "" + "\n"
        print ret
        return ret

class TPM_INT_STATUS:              
    addr = 0x0010
    size = 4
    dataAvilIntOccured = 0
    stsValidIntOccured = 0
    localityChangeIntOccured = 0
    commonReadyIntOccured = 0

    @staticmethod
    def Fill(data):
        TPM_INT_STATUS.dataAvilIntOccured           = (data[0]) & 0x01
        TPM_INT_STATUS.stsValidIntOccured           = (data[0]) & 0x02
        TPM_INT_STATUS.localityChangeIntOccured     = (data[0]) & 0x04
        TPM_INT_STATUS.commonReadyIntOccured        = (data[0]) & 0x80
    
    @staticmethod
    def Print():
        ret=""
        ret += "TPM_INT_STATUS" + "\n"
        ret += "dataAvilIntOccured: " + str(TPM_INT_STATUS.dataAvilIntOccured) + "\n"
        ret += "stsValidIntOccured: " + str(TPM_INT_STATUS.stsValidIntOccured) + "\n"
        ret += "localityChangeIntOccured: " + str(TPM_INT_STATUS.localityChangeIntOccured) + "\n"
        ret += "commonReadyIntOccured: " + str(TPM_INT_STATUS.commonReadyIntOccured) + "\n"
        ret += "" + "\n"
        print ret
        return ret

class TPM_INTF_CAPABILITY:              
    addr = 0x0014
    size = 4
    InterfaceVersion = 0
    DataTransferSizeSupport = 0
    BurstCountStatic = 0
    CommandReadyIntSupport = 0
    InterruptEdgeFalling = 0
    InterruptEdgeRising = 0
    InterruptLevelLow = 0
    InterruptLevelHigh = 0
    LocalityChangeIntSupport = 0
    stsValidIntSupport = 0
    dataAvailIntSupport = 0
    

    @staticmethod
    def Fill(data):
        val = data[0] + (data[1]<<8) + (data[2]<<16) + (data[3]<<24)
        TPM_INTF_CAPABILITY.InterfaceVersion = ((val>>28)&0x07)
        TPM_INTF_CAPABILITY.DataTransferSizeSupport = ((val>>9)&0x03)
        TPM_INTF_CAPABILITY.BurstCountStatic = ((val>>8)&0x01)
        TPM_INTF_CAPABILITY.CommandReadyIntSupport = ((val>>7)&0x01)
        TPM_INTF_CAPABILITY.InterruptEdgeFalling = ((val>>6)&0x01)
        TPM_INTF_CAPABILITY.InterruptEdgeRising = ((val>>5)&0x01)
        TPM_INTF_CAPABILITY.InterruptLevelLow = ((val>>4)&0x01)
        TPM_INTF_CAPABILITY.InterruptLevelHigh = ((val>>3)&0x01)
        TPM_INTF_CAPABILITY.LocalityChangeIntSupport = ((val>>2)&0x01)
        TPM_INTF_CAPABILITY.stsValidIntSupport = ((val>>1)&0x01)
        TPM_INTF_CAPABILITY.dataAvailIntSupport = (val&0x01)
        
    
    @staticmethod
    def Print():
        ret = ""
        ret += "TPM_INTF_CAPABILITY" + "\n"
        ret += "InterfaceVersion: " + str(TPM_INTF_CAPABILITY.InterfaceVersion) + "\n"
        ret += "DataTransferSizeSupport: " + str(TPM_INTF_CAPABILITY.DataTransferSizeSupport) + "\n"
        ret += "BurstCountStatic: " + str(TPM_INTF_CAPABILITY.BurstCountStatic) + "\n"
        ret += "CommandReadyIntSupport: " + str(TPM_INTF_CAPABILITY.CommandReadyIntSupport) + "\n"
        ret += "InterruptEdgeFalling: " + str(TPM_INTF_CAPABILITY.InterruptEdgeFalling) + "\n"
        ret += "InterruptEdgeRising: " + str(TPM_INTF_CAPABILITY.InterruptEdgeRising) + "\n"
        ret += "InterruptLevelLow: " + str(TPM_INTF_CAPABILITY.InterruptLevelLow) + "\n"
        ret += "InterruptLevelHigh: " + str(TPM_INTF_CAPABILITY.InterruptLevelHigh) + "\n"
        ret += "LocalityChangeIntSupport: " + str(TPM_INTF_CAPABILITY.LocalityChangeIntSupport) + "\n"
        ret += "stsValidIntSupport: " + str(TPM_INTF_CAPABILITY.stsValidIntSupport) + "\n"
        ret += "dataAvailIntSupport: " + str(TPM_INTF_CAPABILITY.dataAvailIntSupport) + "\n"
        ret += "" + "\n"
        print ret
        return ret

class TPM_STS:              
    addr = 0x0018
    size = 4
    stsValidFlag = 0x80
    commandReadyFlag= 0x40
    tpmGoFlag = 0x20
    dataAvailFlag = 0x10
    ExpectFlag = 0x08
    selfTestDoneFlag = 0x04
    responseRetryFlag = 0x02
    resetEstablishmentBitFlag = 0x02000000
    commandCancelFlag = 0x01000000

    tpmFamily = 0
    burstCount = 0
    stsValid = 0
    commandReady= 0
    tpmGo = 0
    dataAvail = 0
    Expect = 0
    selfTestDone = 0
    responseRetry = 0
    
    @staticmethod
    def Fill(data):
        val = data[0] + (data[1]<<8) + (data[2]<<16) + (data[3]<<24)
        TPM_STS.tpmFamily       = (val>>26)&0x03
        TPM_STS.burstCount      = (val>>8)&0xffff
        TPM_STS.stsValid        = (val>>7)&0x01
        TPM_STS.commandReady    = (val>>6)&0x01
        TPM_STS.tpmGo           = (val>>5)&0x01
        TPM_STS.dataAvail       = (val>>4)&0x01
        TPM_STS.Expect          = (val>>3)&0x01
        TPM_STS.selfTestDone    = (val>>2)&0x01
        TPM_STS.responseRetry   = (val>>1)&0x01
       
    @staticmethod
    def Print():
        family = "Unknown"
        if (TPM_STS.tpmFamily==0): family = "TPM 1.2 Family"
        if (TPM_STS.tpmFamily==1): family = "TPM 2.0 Family"
        ret = ""
        ret += "TPM_STS" + "\n"
        ret += "tpmFamily: " + family + "\n"
        ret += "burstCount: " + str(TPM_STS.burstCount) + "\n"
        ret += "stsValid: " + str(TPM_STS.stsValid) + "\n"
        ret += "commandReady: " + str(TPM_STS.commandReady) + "\n"
        ret += "tpmGo: " + str(TPM_STS.tpmGo) + "\n"
        ret += "dataAvail: " + str(TPM_STS.dataAvail) + "\n"
        ret += "Expect: " + str(TPM_STS.Expect) + "\n"
        ret += "selfTestDone: " + str(TPM_STS.selfTestDone) + "\n"
        ret += "responseRetry: " + str(TPM_STS.responseRetry) + "\n"
        ret += ""
        print ret
        return ret
        
    
class TPM_DATA_FIFO:              
    addr = 0x0024
    size = 4
    
    data = []
    
    @staticmethod
    def Fill(data):
        TPM_DATA_FIFO.data = bytearray(data)
           
    @staticmethod
    def Print():
        ret = ""
        ret += "TPM_DATA_FIFO" + "\n"
        ret +=  "data: " + list(TPM_DATA_FIFO.data) + "\n"
        ret +=  "" + "\n"
        print ret
        return ret

class TPM_XDATA_FIFO:              
    addr = 0x0080
    size = 4

    @staticmethod
    def Fill(data):
        pass
    
    @staticmethod
    def Print():
        print ""

class TPM_DID_VID:              
    addr = 0x0F00
    size = 4
    did = 0
    vid = 0
    
    @staticmethod
    def Fill(data):
        val = data[0] + (data[1]<<8) + (data[2]<<16) + (data[3]<<24)
        TPM_DID_VID.did     = (val>>16)
        TPM_DID_VID.vid     = (val & 0xffff)
    
    @staticmethod
    def Print():
        ret = ""
        ret += "TPM_DID_VID" + "\n"
        ret += "did: " + str(TPM_DID_VID.did) + " (" + format(TPM_DID_VID.did, '04x') + ")" + "\n"
        ret += "vid: " + str(TPM_DID_VID.vid) + " (" + format(TPM_DID_VID.vid, '04x') + ")" + "\n"
        ret += "" + "\n"
        print ret
        return ret

class TPM_RID:              
    addr = 0x0F04
    size = 1
    rid = 0
    
    @staticmethod
    def Fill(data):
        TPM_RID.rid = data[0]
        pass
    
    @staticmethod
    def Print():
        ret = ""
        ret += "TPM_DID_VID" + "\n"
        ret += "rid: " + str(TPM_RID.rid) + " (" + format(TPM_RID.rid, '04x') + ")" + "\n"
        ret += "" + "\n"
        print ret
        return ret
