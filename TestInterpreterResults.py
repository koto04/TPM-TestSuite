from TestInterpreter import TestManagerGateway

def setPass(text):
        TestManagerGateway.testManager.increasePass(text) # @UndefinedVariable
def setFail(text):
        TestManagerGateway.testManager.increaseFailure(text) # @UndefinedVariable
def lastResult():
    return TestManagerGateway.testManager.lastResult() # @UndefinedVariable
