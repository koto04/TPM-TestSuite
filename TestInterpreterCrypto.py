from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from Crypto import Random 
from Crypto.Util import asn1
from RSAKey import RSAKey
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import hashlib, hmac, base64

from TPM_Object import Object

from TS_Globals import TS_Globals

if(TS_Globals.TS_TPM_VERSION==TS_Globals.TPM_VERSION_1_2):
    from TPM_Session_V1 import Session
else:
    from TPM_Constants_V2 import TPM_ALG_SHA1, TPM_ALG_SHA256, TPM_ALG_CBC, TPM_ALG_ECB, TPM_ALG_CTR, TPM_ALG_OFB
    from TPM_Session_V2 import Session


from TestInterpreter import TestManagerGateway
from TestInterpreterOperations import concat, i2osp, xor
from Utils import hex2bytearray, bytearray2int

def encryptParameter(sessionHandle, parameter, nonceCaller):
    if isinstance(sessionHandle, basestring):
        sessionHandle = hex2bytearray(sessionHandle.replace(" ", ""))
    if isinstance(parameter, basestring):
        parameter = hex2bytearray(parameter.replace(" ", ""))
    if isinstance(nonceCaller, basestring):
        nonceCaller = hex2bytearray(nonceCaller.replace(" ", ""))
    
    s = Session.getSession(sessionHandle)
    return s.calcEncryptedParameter(parameter, nonceCaller)

def mgf1(data, length, hashFunc=hashlib.sha1):
    counter = 0
    output = ''
    while (len(output)<length):
        C = i2osp(counter, 4)
        output +=hashFunc(data + C).digest()
        counter +=1
    return bytearray(output[:length])

def OAEPEncoding(m,pHash,seed):
    hLen = len(pHash)
    ba = bytearray(concat([seed, pHash, '01', m]))
    db = concat([pHash, '01', m])
    dbMask = mgf1(seed, len(m)+20+1)
    maskedDB = xor(db,dbMask)
    seedMask = mgf1(maskedDB,20)
    maskedSeed = xor(seed,seedMask)
    return concat([maskedSeed, maskedDB])

def KDFa(hashAlg, key, label, contextU, contextV, bits): 

    if isinstance(hashAlg, basestring):
        hashAlg = hex2bytearray(hashAlg.replace(" ", ""))
    elif isinstance(hashAlg, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of hashAlg")  # @UndefinedVariable
    
    if isinstance(key, basestring):
        key = hex2bytearray(key)
    elif isinstance(key, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of key")  # @UndefinedVariable

    if isinstance(label, basestring):
        label = hex2bytearray(label)
    elif isinstance(label, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of label")  # @UndefinedVariable

    if isinstance(contextU, basestring):
        contextU = hex2bytearray(contextU)
    elif isinstance(contextU, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of contextU")  # @UndefinedVariable
    
    if isinstance(contextV, basestring):
        contextV = hex2bytearray(contextV)
    elif isinstance(contextV, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of contextV")  # @UndefinedVariable

    if isinstance(bits, basestring):
        bits = hex2bytearray(bits)
    elif isinstance(bits, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of bits") # @UndefinedVariable
    
    i = bytearray([0x00, 0x00, 0x00, 0x01])
    if(hashAlg==TPM_ALG_SHA1):
        pHMac = hmac.new(key, msg=i+label+contextU+contextV+bits,  digestmod=hashlib.sha1).hexdigest()
        return bytearray(base64.b16decode(pHMac, casefold=True))
    elif(hashAlg==TPM_ALG_SHA256):
        pHMac = hmac.new(key, msg=i+label+contextU+contextV+bits,  digestmod=hashlib.sha256).hexdigest()
        return bytearray(base64.b16decode(pHMac, casefold=True))

def generateRSAkey(length):
    
    if isinstance(length,( int, long )):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of length")  # @UndefinedVariable
        return
    if(length!=512) and (length!=1024) and (length!=2048):
        TestManagerGateway.testManager.increaseException("Unknown key length")  # @UndefinedVariable
        return
    return RSAKey(length)

def RSAencrypt (key, message, e=65537):

    if isinstance(key, basestring):
        key = hex2bytearray(key)
    elif isinstance(key, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of key")  # @UndefinedVariable
        
    if isinstance(message, basestring):
        message = hex2bytearray(message)
    elif isinstance(message, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of message")  # @UndefinedVariable

    message = str(message)
    param = (bytearray2int(key), bytearray2int(e))
    pubkey = RSA.construct(param)
    enc = pubkey.encrypt(message,0)
    return bytearray(enc[0])

def AESdecrypt(mode, key, enc, iv=bytearray(16)):    
    if isinstance(mode, basestring):
        mode = hex2bytearray(mode)
    elif isinstance(mode, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of mode")  # @UndefinedVariable

    if isinstance(key, basestring):
        key = hex2bytearray(key)
    elif isinstance(key, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of key")  # @UndefinedVariable
        
    if isinstance(enc, basestring):
        enc = hex2bytearray(enc)
    elif isinstance(enc, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of enc")  # @UndefinedVariable

    if isinstance(iv, basestring):
        iv = hex2bytearray(iv)
    elif isinstance(iv, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of iv")  # @UndefinedVariable
    
    aesMode = AES.MODE_CFB
    if mode == TPM_ALG_CBC:
        aesMode = AES.MODE_CBC
    elif mode == TPM_ALG_ECB:
        aesMode = AES.MODE_ECB
    elif mode == TPM_ALG_CTR:
        aesMode = AES.MODE_CTR
    elif mode == TPM_ALG_OFB:
        aesMode = AES.MODE_OFB 
    #padding to the right size    
    enc = str(enc)
    ml = len(enc)
    padlen = 16-(ml%16)
    if padlen==16: l=0
    while l!=0:
        enc+='\000'
        l=-1

    cipher = AES.new(str(key), aesMode, str(iv), segment_size=128) 
    decrypted = cipher.decrypt(enc)
    decrypted = decrypted[:len(decrypted)-padlen]
    TestManagerGateway.testManager.writeLog(bytearray(decrypted))   # @UndefinedVariable
    return bytearray(decrypted)

def sha256(value):
    if isinstance(value, basestring):
        value = hex2bytearray(value)
    elif isinstance(value, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of data")  # @UndefinedVariable
        return bytearray()
    return bytearray(hashlib.sha256(value).digest())

def sha1(value):
    if isinstance(value, basestring):
        value = hex2bytearray(value)
    elif isinstance(value, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of data")  # @UndefinedVariable
        return bytearray()
    return bytearray(hashlib.sha1(value).digest())

def encryptRSAOAEP_SHA1_MGF1(key,  message):
    if isinstance(key, basestring):
        key = hex2bytearray(key)
    elif isinstance(key, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of key")  # @UndefinedVariable
        return bytearray()

    if isinstance(message, basestring):
        message = hex2bytearray(message)
    elif isinstance(message, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of message")  # @UndefinedVariable
        return bytearray()
    k_long=0
    for k in key:
        k_long = (k_long<<8) + k
        
    rsakey = RSA.construct((long(k_long), long(65537)))
    rsakey = PKCS1_OAEP.new(rsakey,  label='TCPA')
    encrypted = rsakey.encrypt(str(message))
    return bytearray(encrypted)

def ADIP(handle,  authValue):   
    session = Session.getSession(handle)
    if(session==None):
        TestManagerGateway.testManager.increaseException("Unknown Session for ADIP calculation") # @UndefinedVariable
        return bytearray()
    elif(len(session.sharedSecret)==0):
        TestManagerGateway.testManager.increaseException("Session for ADIP calculation has not a shared secret") # @UndefinedVariable
        return bytearray()
        
    if isinstance(authValue, basestring):
        authValue = hex2bytearray(authValue)
    elif isinstance(authValue, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of authValue in ADIP calculation") # @UndefinedVariable
        return bytearray()
     
    temp = bytearray();
    temp.extend(session.sharedSecret)
    temp.extend(session.authLastNonceEven)
    key = sha1(temp)
     
    result = bytearray()
    kIdx = 0
    for x in range(0, len(authValue)):
        p = authValue[x]
        q = key[kIdx]
        r = p^q
        s = [(r & (0xff << pos*8)) >> pos*8 for pos in range(0,-1,-1)]
        result.extend(s)
        kIdx = (kIdx + 1) % len(key)
    return result
         
def ADIP2(handle,  authValue,  nonceOdd):    

    session = Session.getSession(handle)
    if(session==None):
        TestManagerGateway.testManager.increaseException("Unknown Session for ADIP calculation") # @UndefinedVariable
        return bytearray()
    elif(len(session.sharedSecret)==0):
        TestManagerGateway.testManager.increaseException("Session for ADIP calculation has not a shared secret") # @UndefinedVariable
        return bytearray()
        
    if isinstance(authValue, basestring):
        authValue = hex2bytearray(authValue)
    elif isinstance(authValue, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of authValue in ADIP calculation") # @UndefinedVariable
        return bytearray()

    if isinstance(nonceOdd, basestring):
        nonceOdd = hex2bytearray(nonceOdd)
    elif isinstance(nonceOdd, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of nonceOdd for ADIP calculation") # @UndefinedVariable
        return bytearray()
     
    temp = bytearray();
    temp.extend(session.sharedSecret)
    temp.extend(nonceOdd)
    key = sha1(temp)
     
    result = bytearray()
    kIdx = 0
    for x in range(0, len(authValue)):
        p = authValue[x]
        q = key[kIdx]
        r = p^q
        s = [(r & (0xff << pos*8)) >> pos*8 for pos in range(0,-1,-1)]
        result.extend(s)
        kIdx = (kIdx + 1) % len(key)
    return result


