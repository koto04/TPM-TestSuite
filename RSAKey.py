from Crypto.PublicKey import RSA
from Crypto.Util import asn1

class RSAKey():
    
    def __init__(self,length):
        new_key = RSA.generate(length, e=65537)
        seq = asn1.DerSequence()
        seq.decode(new_key.exportKey("DER"))
        self.n = bytearray.fromhex('{:0192x}'.format(seq[1]))
        self.e = bytearray.fromhex('{:0192x}'.format(seq[2]))
        self.d = bytearray.fromhex('{:0192x}'.format(seq[3]))
        self.p = bytearray.fromhex('{:0192x}'.format(seq[4]))
        self.q = bytearray.fromhex('{:0192x}'.format(seq[5]))
       
