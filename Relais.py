import serial
import time
from __builtin__ import True

ON = 1
OFF = 0

class Relais():
    '''
    classdocs
    '''

    ComPort = None
    state = 0x0f
    
    @staticmethod
    def setComPort(comPort):
        if comPort.startswith("COM"):
            try:
                Relais.ComPort=serial.Serial(port=comPort, baudrate=9600, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=5)
                if(Relais.ComPort.isOpen()==False):
                    Relais.ComPort.open()
                Relais.ComPort.write(bytearray([0x50]))
                time.sleep(0.5)
                Relais.ComPort.write(bytearray([0x51]))
                time.sleep(1)
                Relais.ComPort.write(bytearray(Relais.state))
            except:
                Relais.ComPort = None
                return False
            
            return Relais.ComPort.isOpen()
            
        else:
            if(Relais.ComPort!=None):
                if(Relais.ComPort.isOpen()):
                    Relais.ComPort.close()
            Relais.ComPort = None
            return True
                        
    @staticmethod
    def setRelais(relais, status):
        if(Relais.ComPort==None):
            return False   
        if(relais>4) or (relais<0):
            return
        data = 0x01 << (relais-1)     
        if(status==OFF):
            Relais.state |= data
        elif(status==ON):
            Relais.state &= ~data
            
        Relais.ComPort.write(Relais.state)
        
    @staticmethod
    def sendResponse(data):
        if(Relais.ComPort==None):
            return False            
        Relais.ComPort.write(data)
        