from TPM_Communication import TPM_Communication
from TS_Globals import TS_Globals

class DummyCom(TPM_Communication):
    
    interface_type = TS_Globals.TPM_INTERFACE_DUMMY
    

    def __init__(self):
        
        self.sendTimeout = 1 #1s
        self.receiveTimeout = 120 #10s
        TPM_Communication.sInterface = self
        
    def setVDD(self, state):
        if(state==0):
            print "Power OFF"
        elif(state==1):
            print "Power ON"

    def reset(self):
        pass        

    def readRegister(self, lpc_reg, size=-1):
        pass
         
    def writeRegister(self, lpc_reg, data):
        pass
                                
    def setClockTearing(self, start, length=1):
        self.clockTearStart = start
        self.clockTearCount = length
        
    def setPP(self, pp):
        pass
    def setLocality(self, locality):
        pass
    def getTxClocks(self):
        return self.txClocks
    def setRxTimeout(self, timeout):
        self.receiveTimeout = timeout
        
    def receiveData(self, data):
        pass
        
    def sendData(self, data):
        pass
            
    def receive(self, data, gui=None):
        pass            
    def send(self, data, gui=None, command=""):
        pass
