from Tkinter import Label, Frame, Menu, PanedWindow, Scrollbar, Text, Toplevel
from Tkinter import BooleanVar, IntVar, StringVar
from Tkinter import END, TOP, BOTTOM, TRUE, BOTH, HORIZONTAL, VERTICAL, RIDGE, WORD, LEFT, RIGHT, X, Y, W, INSERT, OFF, ON


from ttk import Notebook
from ttk import Treeview
from threading import Thread
from datetime import datetime
from operator import itemgetter

import sys
import time
import platform

if platform.system() == "Windows":
    from TPM_CommunicationDummy import TPM_CommunicationDummy as TPM_Communication
else:
    from TPM_Communication import TPM_Communication

from TestSuite_MessageBox import MessageBox

import TPM_Commands_V1
import TPM_Commands_V2

from TestManager import TestManager
from TestDocumentor import TestDocumentor

from TS_Globals import TS_Globals

from ComPort import ComPort
from DPAFI import DPAFI

import StringIO
import tkFileDialog
import tkFont
import tkMessageBox

import os
import gc

from Utils import bytearray2hex
from GUI_TestSelection import GUI_TestSelection

__version__ = '1.6'
__version_date__ = ''

AUTOCREATE_LOG_FILE = 0x0001
DISABLE_LOG_OUTPUT = 0x0002
DPAFI_PORT = 0x0003
        
class TPM_TestSuite(Frame):
    

    def __init__(self, master=None):
        
        self.autoSaveLog = BooleanVar()
        self.autoComLog = BooleanVar()
        self.disableLogOutput = BooleanVar()
        self.forceDisableOutput = False #used in execute directory option
        
        self.DPAFIPort = StringVar()
        self.RelaisPort = StringVar()
        self.autoLogFile = ""
        self.lastResult = ""
        self.testStartTime = 0
        self.lastScriptPath = None
        self.master = master
        
        Frame.__init__(self, master)        
        self.mCodeOut = StringIO.StringIO()
        self.mCodeErr = StringIO.StringIO()
        self.mScriptPath = ''
        self.mPass = 0
        self.mFailure = 0
        self.mException = 0
        
        if(TS_Globals.TS_TPM_VERSION==TS_Globals.TPM_VERSION_1_2):
            self.mCommands = TPM_Commands_V1.TPM_command
        else:
            self.mCommands = TPM_Commands_V2.TPM_command
        
        self.createWidgets(master)
        
        self.checkKeys = ['checkResponseCode', 'checkData', 'checkDataLen', 'checkCommunication']
        self.stepKeys = ['step']
        self.logKeys = ['log', 'header', 'treeLog', 'treeOutput']
        self.commandKeys = ['disableLogOutput','createLogFile','setDpafiPort','getCommunicationStatus','int2bytes','intrandom','encryptParameter','intValue','ADIP','ADIP2','send', 'initialize',  'transmit', 'wait', 'concat', 'sha1', 'sha256', 'random', 'bytes', 'flags',  'object',  'addition', 'waitDPAFI', 'send2DPAFI', 'result2DPAFI','setRelais', 'TPMreset', 'setLocality', 'setPP','AESdecrypt','setRxTimeout','lastResult','setVDD','openFile','appendFile','setPollingInterval']
        self.depracted = ['TPMopen', 'TPMclose', 'TDDLopen', 'TDDLclose','TDDLreset','expectTpmDllResponse','TPMToolCommand']
        self.fuzzy = ['setFuzzyLevel','checkFuzzyResponse','getFuzzyResponse']
        self.flow = ['if', 'elif', 'else', 'break', 'continue', 'while']
        self.tryexc = ['try', 'except']
        MessageBox.root = master
        
        self.scriptRunning = False
        self.scriptAborted = False
            
    def increasePass(self, passText=None):
        self.mPass += 1

        pos = self.outOutput("Pass")
        if(self.disableLogOutput.get()==False):
            pos_end = str(pos) + " + 4c"
            self.formatOutput("pass", pos, pos_end)
            if(passText!=None):
                self.outOutput(passText)
            
        self.lastResult="pass"
        
    def increaseFailure(self, failureText=None):
        self.mFailure += 1
        text = "Failure " + str(self.mFailure)
        pos = self.outOutput(text)
        if(self.disableLogOutput.get()==False):
            pos_end = str(pos) + " + " + str(len(text)) + "c"
            self.formatOutput("failure", pos, pos_end)
            if(failureText!=None):
                self.outOutput(failureText)
        self.lastResult = "fail"
        
    def increaseException(self,  exceptionText=None):
        self.mException += 1
        if(exceptionText!=None):
            text = "Exception " + str(exceptionText)
        else:
            text = "Exception " + str(self.mException)        
        pos = self.outOutput(text)
        if(self.disableLogOutput.get()==False):
            pos_end = str(pos) + " + " + str(len(text)) + "c"
            self.formatOutput("exception", pos, pos_end)
        self.lastResult = "exce"
    
    def selectDPAFIPort(self):
        port = self.DPAFIPort.get()
        if port == "not connected": 
            DPAFI.resetComPort()
            return
        ret = DPAFI.setComPort(self.DPAFIPort.get())
        if ret==False:
            tkMessageBox.showerror(title="DPAFI", message="Port open failed")
            self.DPAFIPort.set("not connected")

    def textSelectAll(self,  event):
        event.widget.tag_add("sel","1.0","end")        
    def textCopy(self,  event):
        self.clipboard_clear()
        text = event.widget.get("sel.first", "sel.last")
        self.clipboard_append(text)
        
    def textCut(self,  event):
        self.textCopy(event)
        try:
            event.widget.delete("sel.first", "sel.last")
            self.refreshEditorPosition()
        except:
            pass
    def textPaste(self,  event):
        clipboard = self.clipboard_get()
        clipboard = clipboard.replace("\n", "\\n")
        clipboard = self.clipboard_get()
        try:
            start = event.widget.index("sel.first")
            end = event.widget.index("sel.last")
            event.widget.delete(start, end)
            self.refreshEditorPosition()
        except:
            # nothing was selected, so paste doesn't need
            # to delete anything
            pass

        # insert the modified clipboard contents
        event.widget.insert("insert", clipboard)

        
    def highlight(self):

        self.txtScript.tag_remove("cc_command", 1.0, END)
        self.txtScript.tag_remove("check", 1.0, END)
        self.txtScript.tag_remove("step", 1.0, END)
        self.txtScript.tag_remove("log", 1.0, END)
        self.txtScript.tag_remove("command", 1.0, END)
        self.txtScript.tag_remove("comment", 1.0, END)
        self.txtScript.tag_remove("depracted", 1.0, END)
        self.txtScript.tag_remove("fuzzy", 1.0, END)
        self.txtScript.tag_remove("flow", 1.0, END)
        self.txtScript.tag_remove("tryexc", 1.0, END)
        
        countVar = IntVar()
        for key in list(self.mCommands.keys()):
            end=1.0
            while True:
                start = self.txtScript.search("'" + key + "'", end, stopindex=END,  count=countVar)
                if(not start): break
                self.txtScript.tag_add("cc_command", start, "%s + %sc" % (start, countVar.get()))
                end = "%s + %sc" % (start, countVar.get())
        
        for key in self.checkKeys:
            end=1.0        
            while True:
                start = self.txtScript.search(key, end, stopindex=END,  count=countVar)
                if(not start): break
                self.txtScript.tag_add("check", start, "%s + %sc" % (start, countVar.get()))
                end = "%s + %sc" % (start, countVar.get())

        for key in self.stepKeys:
            end=1.0        
            while True:
                start = self.txtScript.search(key, end, stopindex=END,  count=countVar)
                if(not start): break
                end = self.txtScript.search("\n", start)
                self.txtScript.tag_add("step", start,end)

        for key in self.depracted:
            end=1.0        
            while True:
                start = self.txtScript.search(key, end, stopindex=END,  count=countVar)
                if(not start): break
                end = self.txtScript.search("\n", start)
                self.txtScript.tag_add("depracted", start,end)

        for key in self.logKeys:
            end=1.0        
            while True:
                start = self.txtScript.search(key, end, stopindex=END,  count=countVar)
                if(not start): break
                self.txtScript.tag_add("log", start, "%s + %sc" % (start, countVar.get()))
                end = "%s + %sc" % (start, countVar.get())

        for key in self.commandKeys:
            end=1.0        
            while True:
                start = self.txtScript.search(key, end, stopindex=END,  count=countVar)
                if(not start): break
                self.txtScript.tag_add("command", start, "%s + %sc" % (start, countVar.get()))
                end = "%s + %sc" % (start, countVar.get())
        
        for key in self.fuzzy:
            end=1.0        
            while True:
                start = self.txtScript.search(key, end, stopindex=END,  count=countVar)
                if(not start): break
                self.txtScript.tag_add("fuzzy", start, "%s + %sc" % (start, countVar.get()))
                end = "%s + %sc" % (start, countVar.get())
        for key in self.flow:
            end=1.0        
            while True:
                start = self.txtScript.search(key, end, stopindex=END,  count=countVar)
                if(not start): break
                self.txtScript.tag_add("flow", start, "%s + %sc" % (start, countVar.get()))
                end = "%s + %sc" % (start, countVar.get())
        for key in self.tryexc:
            end=1.0        
            while True:
                start = self.txtScript.search(key, end, stopindex=END,  count=countVar)
                if(not start): break
                self.txtScript.tag_add("tryexc", start, "%s + %sc" % (start, countVar.get()))
                end = "%s + %sc" % (start, countVar.get())

        #comments        
        end=1.0        
        while True:
            start = self.txtScript.search("#", end, stopindex=END,  count=countVar)
            if(not start): break
            end = self.txtScript.search("\n", start)
            self.txtScript.tag_add("comment", start,end)


    def createWidgets(self, master):
        top = self.winfo_toplevel()
        self.menuBar = Menu(top)
        top['menu'] = self.menuBar
        ports = ComPort.getAvailablePorts()
        self.mTPMVersion = IntVar()
        self.mSelectedPort = StringVar()
        master.bind_class("Text","<Control-a>", self.textSelectAll)
        master.bind_class("Text","<Control-x>", self.textCut)
        master.bind_class("Text","<Control-c>", self.textCopy)
        master.bind_class("Text","<Control-v>", self.textPaste)

        master.bind_class("Text", "<Control-Key-1>",  self.autoTextRC)
        master.bind_class("Text", "<Control-Key-2>",  self.autoTextSend)
        
        master.bind_class("Treeview","<Control-c>", self.treeCopy)
                
        #Menu Script
        self.subMenuScript = Menu(self.menuBar)
        self.menuBar.add_cascade(label='Script', menu=self.subMenuScript)
        self.subMenuScript.add_command(label='New', command = self.clearScript,  accelerator="Ctrl+N")
        self.subMenuScript.add_command(label='Load', command = self.loadScript,  accelerator="Ctrl+O")
        self.subMenuScript.add_command(label='Save', command = self.saveScript,  accelerator="Ctrl+S")
        self.subMenuScript.add_command(label='Save as', command = self.saveAsScript)
        self.subMenuScript.add_separator()
        self.subMenuScript.add_command(label='Generate test descriptions', command = self.documentTests)
        self.subMenuScript.add_command(label='Generate command matrix', command = self.commandMatrix)
        
        master.bind("<Control-n>", self.clearScript)
        master.bind("<Control-o>", self.loadScript)
        master.bind("<Control-s>", self.saveScript)
        
        self.mOpenScriptOpt = options = {}
        options['defaultextension'] = '.py'
        options['filetypes'] = [('all files', '.*'), ('python files', '.py')]
        options['parent'] = master
        options['initialdir'] = '.\\TestScript'
        
        
        #Menu Execute
        self.subMenuRun = Menu(self.menuBar)
        self.subMenuStop = Menu(self.menuBar)
        self.menuBar.add_cascade(label='Execute', menu=self.subMenuRun)
        self.subMenuRun.add_command(label='Execute Test', command = self.executeScript,  accelerator="F4")
        self.subMenuRun.add_command(label='Execute Directory', command = self.executeDirectory)
        self.menuBar.add_cascade(label='Stop', menu=self.subMenuStop, state="disabled")
        self.subMenuStop.add_command(label='Stop Execution', command = self.stopExecution,  accelerator="F5")

        master.bind("<F4>", self.executeScript)
        master.bind("<F5>", self.stopExecution)
  
        #Menu Log
        self.subMenuLog = Menu(self.menuBar)
        self.menuBar.add_cascade(label='Log', menu=self.subMenuLog)
        self.subMenuLog.add_command(label='Save as', command = self.saveAsLog)
        self.subMenuLog.add_checkbutton(label='Autocreate logfile', onvalue=True, offvalue=False, variable=self.autoSaveLog)
        #self.subMenuLog.add_checkbutton(label='Autocreate communication logfile', onvalue=True, offvalue=False, variable=self.autoComLog)
        self.subMenuLog.add_checkbutton(label='Disable log output', onvalue=True, offvalue=False, variable=self.disableLogOutput)

                
        #Menu DPAFI
        self.subMenuDPAFIPort = Menu(self.menuBar)
        self.menuBar.add_cascade(label='DPAFI-Port', menu=self.subMenuDPAFIPort)
        self.DPAFIPort.set("not connected")
        self.subMenuDPAFIPort.add_radiobutton(label="None", variable=self.DPAFIPort, value="not connected", command = lambda: self.selectDPAFIPort())
        for port in ports:
            if port=="local": port="not connected"
            self.subMenuDPAFIPort.add_radiobutton(label=port, variable=self.DPAFIPort, value=port, command = lambda: self.selectDPAFIPort())
        self.subMenuDPAFIPort.add_separator()
        self.subMenuDPAFIPort.add_command(label='Info', command = self.showDPAFIInfo)
        
        #Menu Power
        self.subMenuPower = Menu(self.menuBar)
        self.menuBar.add_cascade(label='Power', menu=self.subMenuPower)
        self.subMenuPower.add_command(label='Power on', command = self.powerOn)
        self.subMenuPower.add_command(label='Power off', command = self.powerOff)
        
        
        #Menu Info
        self.subMenuInfo = Menu(self.menuBar)
        self.menuBar.add_cascade(label='Info', menu=self.subMenuInfo)
        self.subMenuInfo.add_command(label='Board', command = self.showBoardInfo)
        self.subMenuInfo.add_command(label='OS', command = self.showOSInfo)
        self.subMenuInfo.add_command(label='Host', command = self.showHostInfo)
        
         
        '''        
        #Menu Relais
        self.subMenuRelaisPort = Menu(self.menuBar)
        self.menuBar.add_cascade(label='Relais-Port', menu=self.subMenuRelaisPort)
        self.RelaisPort.set("not connected")
        for port in ports:
            if port=="local": port="not connected"
            self.subMenuRelaisPort.add_radiobutton(label=port, variable=self.RelaisPort, value=port, command = lambda: self.selectRelaisPort())
        
        '''
            
        self.mVertical = PanedWindow(master,  orient=HORIZONTAL, sashwidth=8, sashrelief=RIDGE)
        self.mVertical.pack(fill=BOTH, expand=TRUE)
        self.mHorizontal =  PanedWindow(master,  orient=VERTICAL, sashwidth=8, sashrelief=RIDGE)
        self.mHorizontal.pack(fill=BOTH, expand=TRUE)
        
        self.frameEditor = Frame(self.mHorizontal)
        self.frameEditor.pack(expand=TRUE,  fill=BOTH)
        
        self.fileLabel = Label(self.frameEditor,  text="",  anchor=W)
        self.fileLabel.pack(side=TOP, fill=X)

        self.editorLabel = Label(self.frameEditor,  text="",  anchor=W)
        self.editorLabel.pack(side=BOTTOM, fill=X)
        
        self.scrollbarEditor = Scrollbar(self.frameEditor)
        self.scrollbarEditor.pack(side=RIGHT, fill=Y)

        #txtScript
        scriptFont = tkFont.Font(family="-*-lucidatypewriter-medium-r-*-*-*-140-*-*-*-*-*-*",size=10)

        self.txtScript =Text(self.frameEditor, spacing3=10, wrap=WORD, font=scriptFont)
        self.txtScript.config(tabs = ("5m","10m","15m","20m","25m","30m","35m","40m")) 
        self.txtScript.pack(side=LEFT,  fill=BOTH, expand=TRUE)
        self.txtScript.bind("<KeyRelease>", self.refreshEditorPosition)
        self.txtScript.bind("<ButtonRelease-1>", self.refreshEditorPosition)

        bold_font = tkFont.Font(self.txtScript, self.txtScript.cget("font"))
        bold_font.configure(weight="bold")
        
        self.txtScript.tag_configure("cc_command", foreground='darkblue',  font=bold_font)
        self.txtScript.tag_configure("check", foreground='darkgreen',  font=bold_font)
        self.txtScript.tag_configure("step", background='#FF99FF',  font=bold_font)
        self.txtScript.tag_configure("command", foreground='black',  font=bold_font)
        self.txtScript.tag_configure("log", foreground='orange',  font=bold_font)
        self.txtScript.tag_configure("comment", foreground='#33ff00')
        self.txtScript.tag_configure("depracted", background='red',  font=bold_font)
        self.txtScript.tag_configure("fuzzy", background='#99FFFF',  font=bold_font)
        self.txtScript.tag_configure("flow", foreground='#4040FF',  font=bold_font)
        self.txtScript.tag_configure("tryexc", foreground='#FF4040',  font=bold_font)

        self.txtScript.config(yscrollcommand=self.scrollbarEditor.set)
        self.scrollbarEditor.config(command=self.txtScript.yview)

        self.mNoteOutput = Notebook(master)
        self.mNoteOutput.pack(expand=TRUE,  fill=BOTH)
        
        self.mTabOutput = Frame(self.mNoteOutput)
        self.mTabOutput.pack(expand=TRUE,  fill=BOTH)
        self.mNoteOutput.add(self.mTabOutput, text = "Output", compound=TOP)
        self.scrollbarOutput = Scrollbar(self.mTabOutput)
        self.scrollbarOutput.pack(side=RIGHT, fill=Y)

        self.txtOutput = Text(self.mTabOutput)
        self.txtOutput.pack(fill=BOTH,  expand=TRUE)
        self.txtOutput.tag_configure("pass", background='green',  font=bold_font)
        self.txtOutput.tag_configure("failure", background='red',  font=bold_font)
        self.txtOutput.tag_configure("exception", foreground='#FFCC00',  font=bold_font)
        self.txtOutput.tag_configure("receivedata", foreground='blue',  font=bold_font)
        self.txtOutput.config(yscrollcommand=self.scrollbarOutput.set)
        self.scrollbarOutput.config(command=self.txtOutput.yview)

        
        '''
        self.mTabCommunication = Frame(self.mNoteOutput)
        self.mTabCommunication.pack(expand=TRUE,  fill=BOTH)
        self.mNoteOutput.add(self.mTabCommunication, text = "Communication")
        self.scrollbarCommunication = Scrollbar(self.mTabCommunication)
        self.scrollbarCommunication.pack(side=RIGHT, fill=Y)
        self.txtCommunication = Text(self.mTabCommunication)
        self.txtCommunication.pack(fill=BOTH,  expand=TRUE)
        self.txtCommunication.config(yscrollcommand=self.scrollbarCommunication.set)
        self.scrollbarCommunication.config(command=self.txtCommunication.yview)
        '''

        self.mFrameParameter = Frame(self.mVertical)
        self.mFrameParameter.pack(expand=TRUE,  fill=BOTH)

        self.scrollbarParameter = Scrollbar(self.mFrameParameter)
        self.scrollbarParameter.pack(side=RIGHT, fill=Y)

        self.treeParameter = Treeview(self.mFrameParameter)
        self.treeParameter.tag_configure('step', background='#FF99FF')
        self.treeParameter.pack(fill=BOTH,  expand=TRUE)
        
        self.treeParameter.bind("<Double-1>", self.treeParameterDoubleClick)


        
        self.treeParameter.config(yscrollcommand=self.scrollbarParameter.set)
        self.scrollbarParameter.config(command=self.treeParameter.yview)

        self.mHorizontal.add(self.frameEditor, height=500)
        self.mHorizontal.add(self.mNoteOutput)
        #self.mHorizontal.paneconfigure(self.frameEditor, height=10)
        
        self.mVertical.add(self.mHorizontal)
        self.mVertical.add(self.mFrameParameter)
    
        
    def autoTextRC(self,  event=None):
        try:
            event.widget.delete("sel.first", "sel.last")
            self.refreshEditorPosition()
        except:
            pass
        if(TS_Globals.TS_TPM_VERSION==TS_Globals.TPM_VERSION_1_2):
            self.txtScript.insert(INSERT,  "checkResponseCode(TPM_SUCCESS)\n")
        else:
            self.txtScript.insert(INSERT,  "checkResponseCode(TPM_RC_SUCCESS)\n")
        
        
    def autoTextSend(self,  event=None):
        try:
            event.widget.delete("sel.first", "sel.last")
            self.refreshEditorPosition()
        except:
            pass
        if(TS_Globals.TS_TPM_VERSION==TS_Globals.TPM_VERSION_1_2):
            self.txtScript.insert(INSERT,  "send(['TPM_ORD_', TPM_TAG_RQU_COMMAND, ])")
        else:    
            self.txtScript.insert(INSERT,  "send(['TPM_CC_', TPM_ST_NO_SESSIONS, ])")

    def treeParameterDoubleClick(self, event=None):
        if (len(self.treeParameter.selection())==0): return
        item = self.treeParameter.selection()[0]
        text = self.treeParameter.item(item,"text")
        tag = self.treeParameter.item(item,"tag")
        if not "=" in text: return
        parts = text.split("=")
        if(len(parts)==2):
            text = ''
            for i in range(0, len(parts[1]), 48):
                text += parts[1][i:(i+48)] + "\n"
            MessageBox(msg=text, translate=True, dataStruct=tag)
    
    def treeCopy(self, event=None):
        if (len(self.treeParameter.selection())==0): return
        item = self.treeParameter.selection()[0]
        text = self.treeParameter.item(item,"text")
        if not "=" in text: return
        parts = text.split("=")
        if(len(parts)!=2): return
        text = parts[0]
        self.clipboard_clear()
        self.clipboard_append(text)

    def refreshEditorPosition(self,  event=None):
        self.highlight()
        self.editorLabel.config(text = "Line " + str(self.txtScript.index(INSERT)))

    def scriptInitialDirectory(self):
        initialDir = os.path.expanduser('~') + "/TPM test cases"
        if (self.lastScriptPath==None):
            if(TS_Globals.TS_TPM_VERSION==TS_Globals.TPM_VERSION_1_2):
                initialDir += "/TestScript 1.2/"
            else:
                initialDir += "/TestScript 2.0/"
        else:
            initialDir = os.path.dirname(self.lastScriptPath)
        return initialDir
    
    def executeDirectory(self,  event=None):

        optInitialDir = self.scriptInitialDirectory()
        scriptFiles = []
        
        directoryName = tkFileDialog.askdirectory(initialdir=optInitialDir)
        
        if(directoryName): 
            for fileName in os.listdir(directoryName):
                if fileName.endswith(".py"):                    
                    scriptFiles.append(fileName)

            
            testSelector = GUI_TestSelection()
            testSelector.fileList(scriptFiles, directoryName)
            testSelector.focus_set()
            testSelector.grab_set()
            testSelector.transient(self.master)
            
            #testSelector.pack(side="top", fill="both", expand=True)
            self.master.wait_window(testSelector)
            #testSelector.destroy()
                      
            if len(scriptFiles)==0:
                return   
        
            thread = Thread(target = self.executeDirectoryThread, args = (event,directoryName,scriptFiles,))
            thread.start()
            return thread
        
    def executeDirectoryThread(self,  event=None, directoryName="", scriptFiles=[]):
        autoLog = self.autoSaveLog.get()
        #something to do
        testManager = TestManager(self)
        pathSubString = testManager.getPathSubString()
        autotestLogDirectory = "logs_TPM_" + pathSubString + '_' + datetime.now().strftime("_%y%m%d_%H%M%S") + "/"
        logpath = directoryName + "/logs/" + autotestLogDirectory
        if not os.path.exists(logpath):
            os.makedirs(logpath)
        testManager.treeOutputState(False)
        initFileName = directoryName + "/_initialize.py"
        if(os.path.exists(initFileName)):
            pass
        else:
            initFileName = ""
                   
        for fileName in scriptFiles:
            if(initFileName!=""):
                self.clearScript()
                initFileHandle = open(initFileName, 'r')
                self.txtScript.insert(END,  initFileHandle.read().replace(chr(0x0d),""))
                initFileHandle.close()
                self.update()
                self.refreshEditorPosition()
                self.fileLabel.config(text = self.mScriptPath)                    
                self.highlight()
                self.forceDisableOutput = True
                thread = self.executeScript(testManager=testManager)
                thread.join()    
                self.forceDisableOutput = False

                
            self.clearScript()
            self.mScriptPath = directoryName + "/" + fileName
            scriptFileHdl = open(self.mScriptPath, 'r')
            self.txtScript.insert(END,  scriptFileHdl.read().replace(chr(0x0d),""))
            scriptFileHdl.close()
            self.update()
            self.refreshEditorPosition()
            self.fileLabel.config(text = self.mScriptPath)                    
            self.highlight()
            logFile = logpath + "%result%_" + fileName + datetime.now().strftime("_%y%m%d_%H%M%S") + ".log"
            self.forceDisableOutput = True
            thread = self.executeScript(directoryLogfile=logFile,testManager=testManager)
            thread.join()  
            self.forceDisableOutput = False
            if(self.mException>0):
                logFileNew = logFile.replace("%result%", "EXCEPTION")
            elif(self.mFailure>0):
                logFileNew = logFile.replace("%result%", "FAIL")
            else:
                logFileNew = logFile.replace("%result%", "PASS")
            
            os.rename(logFile, logFileNew)
            gc.collect()
            if self.scriptAborted==True:
                break;
        self.autoSaveLog.set(autoLog)
        if self.scriptAborted==True:
            tkMessageBox.showinfo(title="Test execution", message="Directory execution aborted.")
        else:    
            tkMessageBox.showinfo(title="Test execution", message="Directory execution finished.")
        
    def loadScript(self, event=None):
        optInitialDir = self.scriptInitialDirectory()
        optInitialFile = None
        optFileTypes = [('scripts', '.py')]
        fileName = tkFileDialog.askopenfilename(initialdir=optInitialDir, filetypes=optFileTypes, initialfile=optInitialFile)
        if(fileName):
            self.lastScriptPath = fileName
            self.clearScript()
            self.mScriptPath = fileName
            scriptFileHdl = open(self.mScriptPath, 'r')
            self.txtScript.insert(END,  scriptFileHdl.read().replace(chr(0x0d),""))
            scriptFileHdl.close()
            self.update()
            self.refreshEditorPosition()
            self.fileLabel.config(text = self.mScriptPath)
            
            self.highlight()
    def clearScript(self, event=None):
        self.txtScript.delete(1.0, END)
        self.mScriptPath=""
        self.refreshEditorPosition()
        self.fileLabel.config(text = self.mScriptPath)

    def saveScript(self, event=None):
        if(self.mScriptPath==""):
            self.saveAsScript()
        else:
            scriptFileHdl = open(self.mScriptPath, 'w')
            scriptFileHdl.write(self.txtScript.get(1.0,END).strip("\n"))
            scriptFileHdl.close()
            self.fileLabel.config(text = self.mScriptPath)

    def saveAsScript(self, event=None):
        self.mSaveScriptOpt = options = {}
        options['defaultextension'] = '.py'
        options['filetypes'] = [('all files', '.*'), ('python files', '.py')]
        if(self.mScriptPath!=""):
            path, filename = os.path.split(self.mScriptPath)
            options['initialdir'] = path
            options['initialfile'] = filename
        fileName = tkFileDialog.asksaveasfilename(**self.mSaveScriptOpt)
        if(fileName):
            self.mScriptPath = fileName
            scriptFileHdl = open(self.mScriptPath, 'w')
            scriptFileHdl.write(self.txtScript.get(1.0,END).strip("\n"))
            scriptFileHdl.close()
            self.fileLabel.config(text = self.mScriptPath)

    def saveAsLog(self, event=None):
        self.mSaveLogOpt = options = {}
        options['defaultextension'] = '.log'
        options['filetypes'] = [('all files', '.*'), ('python files', '.log')]
        if(self.mScriptPath!=""):
            path, filename = os.path.split(self.mScriptPath)
            options['initialdir'] = path + "/logs/"
            options['initialfile'] = filename + datetime.now().strftime("_%y%m%d_%H%M%S") + ".log"
        fileName = tkFileDialog.asksaveasfilename(**self.mSaveLogOpt)
        if(fileName):
            scriptFileHdl = open(fileName, 'w')
            scriptFileHdl.write(self.txtOutput.get(1.0,END))
            scriptFileHdl.close()
            
    def showBoardInfo(self, event=None):
        boardInfo = os.popen("cat /proc/cpuinfo").read()
        MessageBox(boardInfo,None,60)
        
    def showHostInfo(self, event=None):
        boardInfo = os.popen("hostnamectl").read()
        MessageBox(boardInfo,None,60)

    def showOSInfo(self, event=None):
        boardInfo = os.popen("cat /etc/os-release").read()
        boardInfo += "\n" + os.popen("uname -r").read()
        MessageBox(boardInfo,None,60)
            
    def showDPAFIInfo(self, event=None):
        infoText = "Communication parameter: 19200,8,N,1\n\n"
        infoText += "waitDPAFI is waiting for the sequence: 0x07, 0x0A. Other bytes are ignored."        
        MessageBox(infoText,None,60)
        
    def documentTests(self,  event=None):
        options = {}
        directoryName = tkFileDialog.askdirectory(**options)
        
        if(directoryName): 
            dokuFileHdl = open(directoryName + "/documentation.txt", 'w')

            for i in os.listdir(directoryName):
                if i.endswith(".py"): 
                    dokuFileHdl.write("File: " + i + "\n")                  
                    scriptFileHdl = open(directoryName + "/" + i, 'r')
                    documentor = TestDocumentor(scriptFileHdl,  dokuFileHdl)
                    scriptFileHdl.close()
                    documentor.documentTest()
                    del documentor
            
            dokuFileHdl.close()
            tkMessageBox.showinfo(title="Documentation", message="Finished!")

    def commandMatrix(self,  event=None):
        options = {}
        directoryName = tkFileDialog.askdirectory(**options)
        
        commandsSorted = sorted(self.mCommands.items(), key=itemgetter(0))
        commands = [""]
        commandsMandatory = [""]
        for value in commandsSorted:
            commandName = value[1][0]
            if(commandName[0:3]=="TPM") or (commandName[0:3]=="TSC"):
                commands.append(commandName)
                if(value[1][3]==True):
                    commandsMandatory.append("M")
                else:
                    commandsMandatory.append("O")
        if(directoryName): 
            dokuFileHdl = open(directoryName + "/command_matrix.csv", 'w')
            dokuFileHdl.write(";".join(commands) + "\n")
            dokuFileHdl.write(";".join(commandsMandatory) + "\n")
            for i in os.listdir(directoryName):
                if i.endswith(".py"): 
                    scriptFileHdl = open(directoryName + "/" + i, 'r')
                    documentor = TestDocumentor(scriptFileHdl,  dokuFileHdl)
                    documentor.addCommands2Matrix(i,  commands)
                    scriptFileHdl.close()
                    del documentor
            
            dokuFileHdl.close()
            tkMessageBox.showinfo(title="Documentation", message="Finished!")
        
    def powerOff(self):
        TPM_Communication.getInterface().setVDD(OFF)
    def powerOn(self):
        TPM_Communication.getInterface().setVDD(ON)
    def createLogFile(self):
        if(self.mScriptPath!=""):

            path, filename = os.path.split(self.mScriptPath)
            path += "/logs/"
            if not os.path.exists(path):
                os.makedirs(path)
            
            filename += datetime.now().strftime("_%y%m%d_%H%M%S")
            filename += ".log"
            self.autoLogFile = path + filename
            logHdl = open(self.autoLogFile, 'w')
            logHdl.close()
    
    def setDisableLogOutput(self, value=True): 
        self.forceDisableOutput = value  
        
    def setDpafiPort(self, port):
        p = "/dev/ttyUSB" + str(port)    
        self.DPAFIPort.set(p)
        DPAFI.setComPort(self.DPAFIPort.get())
        
    
    def stopExecution(self):
        TS_Globals.gStopTestExecution = True
        self.scriptAborted = True
    
    def executeScript(self, event=None, directoryLogfile="", testManager=None):
        if(self.scriptRunning==True): return
        self.menuBar.entryconfig("Execute", state="disabled")
        self.menuBar.entryconfig("Stop", state="normal")
        TS_Globals.gStopTestExecution = False
        self.scriptRunning = True
        self.scriptAborted = False
        thread = Thread(target = self.executeScriptThread, args = (event, directoryLogfile, testManager))
        thread.start()
        return thread
        
    def executeFinished(self):
        self.menuBar.entryconfig("Execute", state="normal")
        self.menuBar.entryconfig("Stop", state="disabled")
        self.scriptRunning = False
        TPM_Communication.getInterface().setDefaultValues()
    
    def clearLogOutput(self):
        self.txtOutput.delete(1.0, END)
    
    def executeScriptThread(self, event=None, directoryLogfile="", testManager=None):
        
        #clear old Output
        self.autoLogFile = "";
        
        #if testManager==None:
        testManager = TestManager(self)
        
        self.txtOutput.delete(1.0, END)
        for i in self.treeParameter.get_children():
            self.treeParameter.delete(i)
        self.mPass = 0
        self.mFailure = 0
        self.mException = 0
        self.update()
        
        #autoLog on?
        self.autoLogFile = directoryLogfile
        if self.autoSaveLog.get() == True:
            self.createLogFile();

        #redirect stdout and stderr to own variables for gui output
        #sys.stdout = self.mCodeOut
        sys.stderr = self.mCodeErr

        if self.autoComLog.get() == True:
            testManager.sComPort.openComFile()

        commands = self.txtScript.get(1.0,END)
        
        if(self.disableLogOutput.get()==True):
            self.outOutput("Log output disabled!", True)
        
        self.testStartTime = time.time()
        
        while True:
            try:
                testManager.runTest(commands)
        
            except Exception, e:
                self.outOutput("Test execution exception",True)
                self.outOutput(str(e), True)
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                self.increaseException()
                if(self.mCodeErr.getvalue()!=""):
                    self.txtOutput.insert(END,  "Error: " + self.mCodeErr.getvalue())
                testManager.finishCleanUp()
                break
       
            testManager.finishCleanUp()
            del(testManager)
            stateDisableOutput = self.forceDisableOutput
            self.outOutput("",True)
            self.outOutput("",True)
            self.outOutput("===============================================================",True)
            self.outOutput("Total Passes: " + str(self.mPass),True)
            self.outOutput("Total Failures: " + str(self.mFailure),True)
            self.outOutput("Total Test Exceptions: " + str(self.mException),True)
            self.outOutput("Execution time: " + str(round((time.time()-self.testStartTime),3))+"s", True)
            self.outOutput("",True)
            if(self.mFailure!=0):
                text = "Test: Fail"
                pos = self.outOutput(text, True)
                pos_end = str(pos) + " + " + str(len(text)) + "c"
                self.formatOutput("failure", str(pos)+" + 6c", pos_end)
    
            elif(self.mException!=0):
                text = "Test: Inconclusive"
                pos = self.outOutput(text, True)
                pos_end = str(pos) + " + " + str(len(text)) + "c"
                self.formatOutput("exception", str(pos)+" + 6c", pos_end)
            else:
                text = "Test: Pass"
                pos = self.outOutput(text, True)
                pos_end = str(pos) + " + " + str(len(text)) + "c"
                self.formatOutput("pass", str(pos)+" + 6c", pos_end)
            self.outOutput("===============================================================")
            self.forceDisableOutput = stateDisableOutput
            break
        
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__
        gc.collect()
        if (directoryLogfile==""): self.forceDisableOutput = False
        self.executeFinished()

    def formatOutput(self, tag, pos, pos_end):
        if pos=="": return
        self.txtOutput.tag_add(tag, pos, pos_end)
        
    def outOutput(self,  text, force=False, vim=False):
        if isinstance(text, bytearray):
            text = bytearray2hex(text)
        if(self.autoLogFile!=""):
            logHdl = open(self.autoLogFile, 'a')
            logHdl.write(text + "\n")
            logHdl.close()
        if ((self.disableLogOutput.get()==True) and (force==False)): 
            return ""
        if (self.forceDisableOutput==True): 
            if (vim==True):
                stext=text
                self.txtOutput.delete(1.0, END)
                self.txtOutput.insert(END, stext)
            if force==False: return ""
        #insert timestamp
        ret = self.txtOutput.index(INSERT)
        self.txtOutput.insert(END,  text + "\n")
        self.txtOutput.see(END)
        self.update()
             
        return ret
        
        
