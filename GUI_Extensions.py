from Tkinter import *

            
class EditorClass(object):

    def __init__(self, master):
         
        self.lineNumbers = ''
        self.frame = Frame(master, bd=2, relief=SUNKEN)

        self.vScrollbar = Scrollbar(self.frame, orient=VERTICAL)
        self.vScrollbar.pack(fill='y', side=RIGHT)

        self.lnText = Text(self.frame,
                width = 4,
                padx = 4,
                highlightthickness = 0,
                takefocus = 0,
                bd = 0,
                background = 'lightgrey',
                foreground = 'darkblue', 
        )
        self.lnText.pack(side=LEFT, fill='y')

        self.text = Text(self.frame,
                width=16,
                bd=0,
                padx = 4,
                undo=True,
                background = 'white', 
                wrap=NONE
        )
        self._resetting_modified_flag = FALSE
        self.text.pack(side=LEFT, fill=BOTH, expand=1)
        self.text.bind('<<Modified>>', self._updateLineNumbers)

        self.vScrollbar.config(command=self.yview)
        self.text.config(yscrollcommand=self.yscrollcommand)
    
    def synchroScroll(self):
        self.updateLineNumbers()
        self.lnText.yview_moveto(self.text.yview()[0])
    def yscrollcommand(self,  *args):
        self.vScrollbar.set(*args)
        self.synchroScroll()
    def yview(self, *args):
        self.text.yview(*args)
        self.synchroScroll()

    def clearModifiedFlag(self):

        self._resetting_modified_flag = True

        try:

            self.text.tk.call(self.text._w, 'edit', 'modified', 0)

        finally:
            self._resetting_modified_flag = False
    def insert(self, pos, s):
        self.text.insert(pos, s)
    def getLineNumbers(self):
        
        line = '0'
        col= ''
        ln = ''
        
        # assume each line is at least 6 pixels high
        step = 6
        
        nl = '\n'
        lineMask = '    %s\n'
        indexMask = '@0,%d'
        
        for i in range(0, self.text.winfo_height(), step):
            
            ll, cc = self.text.index( indexMask % i).split('.')

            if line == ll:
                if col != cc:
                    col = cc
                    ln += nl
            else:
                line, col = ll, cc
                ln += (lineMask % line)[-5:]

        return ln
    def _updateLineNumbers(self,  event=None):
        self.updateLineNumbers()
        self.synchroScroll()
    def updateLineNumbers(self, event=None):

        if self._resetting_modified_flag: return
        self.clearModifiedFlag()
        ln=1
 
        lines = int(self.text.index('end').split('.')[0])
        self.lnText.config(state='normal')
        self.lnText.delete('1.0', END)
        for x in range(1,  lines):
            self.lnText.insert(str(x)+".0", str(ln) + "\n")
            ln+=1
        self.lnText.config(state='disabled')
        return
