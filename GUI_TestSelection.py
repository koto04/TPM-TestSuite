import Tkinter as tk
import tkFileDialog
import tkMessageBox

class GUI_TestSelection(tk.Toplevel):
    
    fileNames = []
    chkFiles = []
    directoryPath = ""
    
    def __init__(self):
        #tk.Frame.__init__(self, root, *args, **kwargs)
        #self.root = root
        self.chkFiles = []
        self.fileNames = []
        
        tk.Toplevel.__init__(self)   
        
        self.masterFrame = tk.Frame(self)
        self.vsb = tk.Scrollbar(self.masterFrame, orient="vertical")
        self.text = tk.Text(self.masterFrame, width=50, height=20, 
                            yscrollcommand=self.vsb.set)
        self.vsb.config(command=self.text.yview)
        self.vsb.pack(side="right", fill="y")
        self.text.pack(side="left", fill="both", expand=True)


        tk.Button(self.masterFrame, text='Select all', width=15, command=self.selectAll).pack()
        tk.Button(self.masterFrame, text='Deselect all', width=15, command=self.deselectAll).pack()

        tk.Button(self.masterFrame, text='Save Test set', width=15, command=self.saveTestSet).pack(pady=(30,0))
        tk.Button(self.masterFrame, text='Load Test set', width=15, command=self.loadTestSet).pack()

        tk.Button(self.masterFrame, text='Start', width=15, command=self.startTest, bg="light green").pack(pady=(30,0))
        tk.Button(self.masterFrame, text='Cancel', width=15, command=self.cancelTest, bg="light pink").pack()

        self.masterFrame.pack()
        
    def destroy(self):
        del self.chkFiles
        tk.Toplevel.destroy(self)
        
    def fileList(self, l, directoryPath):
        l.sort()
        self.fileNames = l
        self.directoryPath = directoryPath
        
        for fileName in self.fileNames:
            var = tk.IntVar()
            cb = tk.Checkbutton(self.masterFrame, text=fileName, bg="white", bd=0, highlightthickness=0, width=40, anchor=tk.W, variable=var)
            var.set(1)
            cb.var = var
            cb.fileName = fileName
            self.text.window_create("end", window=cb)
            self.text.insert("end", "\n") # to force one checkbox per line
            self.chkFiles.append(cb)
        
        self.clearSelectionList()
        
    def updateSelectionList(self):
        self.clearSelectionList()
        for cb in self.chkFiles:
            if cb.var.get()==1: self.fileNames.append(cb.fileName)

    def clearSelectionList(self):
        del self.fileNames[0:len(self.fileNames)]
        
            
    def selectAll(self, event=None):
        for cb in self.chkFiles:
            cb.select()
            
    def deselectAll(self, event=None):
        for cb in self.chkFiles:
            cb.deselect()
        
    def cancelTest(self, event=None):
        self.clearSelectionList()
        self.destroy()
        
    def startTest(self, event=None):
        self.updateSelectionList()
        self.destroy()
   
    def saveTestSet(self, event=None):
        tempList = []
        for cb in self.chkFiles:
            if cb.var.get()==1: self.tempList.append(cb.fileName)
        
        fileContent = "\n".join(tempList)
        options = {}
        options['defaultextension'] = '.tss'
        options['filetypes'] = [('Test script set', '.tss')]
        if(self.directoryPath!=""):
            options['initialdir'] = self.directoryPath
            options['initialfile'] = ""
        fileName = tkFileDialog.asksaveasfilename(**options)
        if(fileName):
            tssFileHdl = open(fileName, 'w')
            tssFileHdl.write(fileContent)
            tssFileHdl.close()
            tkMessageBox.showinfo(title="Test script set", message="Test script set saved.")
        
    def loadTestSet(self, event=None):
        fileNamesNotFound = []
        fileNamesInFile = []
        
        options = {}
        options['defaultextension'] = '.tss'
        options['filetypes'] = [('Test script set', '.tss')]
        if(self.directoryPath!=""):
            options['initialdir'] = self.directoryPath
            options['initialfile'] = ""
        tssFileHdl = tkFileDialog.askopenfile(mode='r',**options)
        if(tssFileHdl):
            fileContent=tssFileHdl.read()
            tssFileHdl.close()
            fileNamesInFile = fileContent.split('\n')
            if len(fileNamesInFile)>0:
                self.deselectAll()
                for fileName in fileNamesInFile:
                    fileFound = False
                    for cb in self.chkFiles:
                        if(cb.fileName == fileName):
                            cb.select()
                            fileFound=True
                            break
                    if fileFound==False:
                        fileNamesNotFound.append(fileName)
                if len(fileNamesNotFound)>0:
                    tkMessageBox.showwarning(title="Test script set", message="Test script set loaded, but some files could not be found:\n\n" + "\n".join(fileNamesNotFound))
                else:
                    tkMessageBox.showinfo(title="Test script set", message="Test script set loaded.")

