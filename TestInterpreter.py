import time

from Relais import Relais

class TestManagerGateway():
    testManager = None
    
    @staticmethod
    def initialize(manager):
        del(TestManagerGateway.testManager)
        TestManagerGateway.testManager = manager

def send(params):
    return TestManagerGateway.testManager.sendMessage(params)

def initialize(params):
    return TestManagerGateway.testManager.initializeMessage(params)

def transmit(message):
    return TestManagerGateway.testManager.transmitMessage(message)

def commandTime():
    return str(TestManagerGateway.testManager.sMessageTime)
    
def setRelais(relais, state):
    if Relais.ComPort == None:
        TestManagerGateway.testManager.writeLog("Relais Port not configured, relais can not be set")
    else:
        Relais.setRelais(relais, state)

def wait(seconds):
    TestManagerGateway.testManager.writeLog(text="wait " + str(seconds) + "s",vim=True)
    t=time.time()+seconds
    while (time.time()<t):
        time.sleep(0.001)
        TestManagerGateway.testManager.checkStopExecution()
    
    
   


