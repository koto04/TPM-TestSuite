import importlib
import sys
import time

from datetime import datetime

from Utils import bytearray2hex, millisInterval

from TS_Globals import *

if(TS_Globals.TS_TPM_VERSION==TS_Globals.TPM_VERSION_1_2):
    import StructInterpreter_V1 as StructInterpreter
    from TPM_Constants_V1 import *
    from TPM_Commands_V1 import TPM_command
    from TPM_Session_V1 import Session
else:
    import StructInterpreter_V2 as StructInterpreter
    from TPM_Constants_V2 import *
    from TPM_Commands_V2 import TPM_command
    from TPM_Session_V2 import Session
    from TPM2_Oracle import * 


import platform
if platform.system() == "Windows":
    from TPM_CommunicationDummy import TPM_CommunicationDummy as TPM_Communication, COMMUNICATION_TIMEOUT, COMMUNICATION_OK

else:
    from TPM_Communication import TPM_Communication, COMMUNICATION_TIMEOUT, COMMUNICATION_OK

import TestInterpreterChecks
import TestInterpreterCommunication
import TestInterpreterCrypto
import TestInterpreterDPAFI
import TestInterpreterFuzzy
import TestInterpreterLog
import TestInterpreterObjects
import TestInterpreterOperations
import TestInterpreterPTP
import TestInterpreterResults


from TestInterpreter import wait, send, initialize, transmit, commandTime, setRelais
from TestInterpreterChecks import getPass, getFail, getException, checkResponseCode, checkCommunication, checkData, checkDataLen, checkFuzzyResponse
from TestInterpreterCommunication import setRxTimeout, getCommunicationStatus
from TestInterpreterCrypto import encryptParameter, mgf1, OAEPEncoding, KDFa, generateRSAkey, RSAencrypt, AESdecrypt, sha256, sha1, encryptRSAOAEP_SHA1_MGF1, ADIP, ADIP2
from TestInterpreterDPAFI import waitDPAFI, result2DPAFI, send2DPAFI, setDpafiPort
from TestInterpreterFuzzy import getFuzzyResponse, setFuzzyLevel
from TestInterpreterLog import header, log, treeLog, step, treeOutput, openFile, appendFile, createLogFile, disableLogOutput, timestamp
from TestInterpreterObjects import object, session, flushContext
from TestInterpreterOperations import intrandom, int2bytes, bytes, text, intValue, concat, addition, flags, i2osp, xor, random
from TestInterpreterPTP import setClockTearing, getTxClocks, readTISRegister, setPP, setVDD, TPMreset, setLocality, setPollingInterval
from TestInterpreterResults import setPass, setFail, lastResult

from TestInterpreter import TestManagerGateway

class TestManager():

    stepMessage = None
    sNonceSize = 0x10
    sCallerNonce = bytearray()
    sTPMNonce = bytearray()
    sCommunicationInterface = None
    sGUI = None
    sStep = 0
    sTreeOutput = True
    sFileHandles = []
    sMessageTime = 0
    sCommunicationStatus = STATUS_OK
    sFuzzyResponse = bytearray()
    sFuzzyLevel = 0
    sChipIdentification = None
        
    sTestScript = None
    def __init__(self, gui):
        
        reload(TestInterpreterObjects)
        reload(TestInterpreterObjects)
        reload(TestInterpreterChecks)
        reload(TestInterpreterCommunication)
        reload(TestInterpreterCrypto)
        reload(TestInterpreterDPAFI)
        reload(TestInterpreterFuzzy)
        reload(TestInterpreterLog)
        reload(TestInterpreterObjects)
        reload(TestInterpreterOperations)
        reload(TestInterpreterPTP)
        reload(TestInterpreterResults)
        self.initialize(gui)
    
    def executeTest(self):
        if (self.sTestScript == None): return
        
        exec self.sTestScript
        
    def runTest(self,testScript):
        
        self.sTestScript = testScript
        TestManagerGateway.initialize(self)
        #has to be abortable
        self.executeTest()  
              
        self.sTestScript = None
        
    def initialize(self,gui):
        
        
        self.sCommunicationInterface = TPM_Communication.getInterface()        
        self.sGUI = gui
        Object.clearObjects()
        Session.clearSessions()
        Ticket.clearTickets()
        self.clearSequence()
        self.sTreeOutput = False
        self.sCommunicationInterface.setVDD(ON)
        self.sCommunicationInterface.reset()
        self.sChipIdentification=self.chipIdentification()
        self.sCommunicationInterface.reset()
        self.sTreeOutput = True
        self.sFuzzyLevel = 0
        return 0
    
    def writeChipInfo(self):
        if(self.sChipIdentification!=None):
            self.writeLog(self.sChipIdentification.longChipInfo(), True)
        else:
            self.writeLog("No chip info available")
     
    def shortChipInfo(self):
        ret = ""
        if(self.sChipIdentification!=None):
            ret = self.sChipIdentification.shortChipInfo()
        return ret

    def getPathSubString(self):
        ret = ""
        if(self.sChipIdentification!=None):
            ret = self.sChipIdentification.pathSubString()
        return ret
        
    def appendFileHandle(self, hdl):
        self.sFileHandles.append(hdl)
            
    def chipIdentification(self):
        self.sendMessage(['Startup',None,None])
        return self.sendMessage(['ChipInfo',None,None])
        
    def initializeMessage(self,parameters):
        if(TPM_command[parameters[0]][2]==False):
            raise Exception("Command " + str(parameters[0]) + " not implemented in the Test Suite.")
        else:
            if(TPM_command[parameters[0]][4]==2):
                module = importlib.import_module("commands2." + TPM_command[parameters[0]][0])
            elif(TPM_command[parameters[0]][4]==1):
                module = importlib.import_module("commands1." + TPM_command[parameters[0]][0])
            className = getattr(module, TPM_command[parameters[0]][0])
            step = className(parameters, self, False)
            self.stepMessage = step
            return self.stepMessage
    
    def checkStopExecution(self):
        if(self.sGUI!=None):
            if(TS_Globals.gStopTestExecution==True):
                self.sGUI.outOutput("\nExecution aborted by User.",True)
                self.finishCleanUp()
                self.sGUI.executeFinished()
                sys.exit()
                
    def readTISRegister(self,register):
        self.sCommunicationInterface.readRegister(register)
        return register.Print()

    def sendMessage(self,parameters):
        self.initializeMessage(parameters)
        result = self.transmitMessage(self.stepMessage)
        return result
    
    def transmitMessage(self,message):
        self.checkStopExecution()
        if(self.sFuzzyLevel!=0):
            message.fuzzy(self.sFuzzyLevel)
        message.prepare()
        message.finalize()
        if(self.sFuzzyLevel!=0):
            self.sFuzzyResponse = OracleResult(message)
        
        data = bytearray()
        
        if(message.logEnable==True):
            self.writeLog(bytearray2hex(message.command))

        if self.sCommunicationInterface.isDummy==False:
            result = self.sCommunicationInterface.send(message.command)
            comtime = "[" + (datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]) + "]\n"
            self.writeLog(comtime)
        else:
            result=COMMUNICATION_OK
        
        if (result==COMMUNICATION_TIMEOUT):
            self.sCommunicationStatus = STATUS_TX_TIMEOUT
            return message #self.sCommunicationStatus
        
        if self.sCommunicationInterface.isDummy==False:
            result = self.sCommunicationInterface.receive(data)
        else:
            result=COMMUNICATION_OK

        if (result==COMMUNICATION_TIMEOUT):
            self.sCommunicationStatus = STATUS_RX_TIMEOUT
            return message #self.sCommunicationStatus
        
        self.sMessageTime = millisInterval(self.sCommunicationInterface.sendTime, self.sCommunicationInterface.receiveTime)
        if(message.logEnable==True):
            comtime = "[" + (self.sCommunicationInterface.receiveTime.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]) + "]:\n"
            self.writeLog(comtime + bytearray2hex(data), "receivedata")
        self.sCommunicationStatus = STATUS_OK
        message.time = time.time()
        if self.sCommunicationInterface.isDummy==False:
            message.receive(data)
        return message
                
    def testStep(self,text):
        self.sStep+=1
        if(self.sGUI!=None):
            self.sGUI.outOutput(text="\n[Step " + "%03d" % self.sStep + "] " + text,vim=True)
        self.insertTreeOutput("",'end',text,tag='step')
        
    def destroyHandle(self,handle):
        if(isinstance(handle, bytearray)):
            Session.destroySession(handle)
            Object.destroyObject(handle)
                
        else:
            self.sGUI.increaseException("Handle has to be a bytearray")

    def interpretStructure(self, structureType, data):
        return StructInterpreter.interpretStructure(structureType, data)
            
    def clearSequence(self):
        self.sFileHandles = []
        self.stepMessage = None
        self.sStep = 0

    def createLogFile(self):
        if(self.sGUI!=None):
            self.sGUI.createLogFile()
     
               
    def finishCleanUp(self):
        for hdl in self.sFileHandles:
            hdl.close()
                 
    def insertTreeOutput(self,tid,  pos,  text, force=False, tag=''):
        if(self.sGUI==None): return 0
        if(self.sGUI.forceDisableOutput==True) or (self.sGUI.disableLogOutput.get()==True): return 0
        if ((self.sTreeOutput == True) or (force==True)) and (self.sGUI != None):
            return self.sGUI.treeParameter.insert(tid, pos, text=text, tag=tag)
        else:
            return 0
        
    def getSession(self, handle):
        return Session.getSession(handle)
    
    def increasePass(self, text=None):
        if(self.sGUI!=None):
            self.sGUI.increasePass(text)

    def increaseFailure(self, text=None):
        if(self.sGUI!=None):
            self.sGUI.increaseFailure(text)

    def increaseException(self, text=None):
        if(self.sGUI!=None):
            self.sGUI.increaseException(text)

            
    def writeLog(self, text, outputFormat=None, vim=False):
        if(self.sGUI!=None):
            pos = self.sGUI.outOutput(text=text, vim=vim)
            if pos!="" and outputFormat!=None:
                pos_end = str(pos) + " + " +str(len(text)) + "c"
                self.sGUI.formatOutput(outputFormat, pos, pos_end)

    def resultCounter(self, result):
        if(self.sGUI!=None):
            if(result==RESULT_PASS):
                return self.sGUI.mPass
            elif(result==RESULT_FAIL):
                return self.sGUI.mFailure
            elif(result==RESULT_EXCEPTION):
                return self.sGUI.mException
            else:
                self.sGUI.increaseException("Unknown result value reference")
            
    def lastMessage(self):
        return self.stepMessage

    def lastResult(self):
        ret = ""
        if(self.sGUI!=None):
            ret = self.sGUI.lastResult
        return ret
    
    def lastReturnCode(self):
        return self.stepMessage.responseCode
    
    def communicationStatus(self):
        return self.sCommunicationStatus

    def communicationInterface(self):
        return self.sCommunicationInterface
    
    def fuzzyResponse(self):
        return self.sFuzzyResponse
    
    def gui(self):
        return self.sGUI
    
    def treeOutputState(self, value=None):
        if(value==None):
            return self.sTreeOutput
        else:
            self.sTreeOutput = value
    
    def logOutputState(self, value):
        if(self.sGUI!=None):
            if(value==None):
                pass
            elif(value==True):
                self.sGUI.setDisableLogOutput(False)
            else:
                self.sGUI.setDisableLogOutput(True)
