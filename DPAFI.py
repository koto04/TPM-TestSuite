from ComPort import ComPort
import time

from TS_Globals import TS_Globals

class DPAFI(ComPort):
    '''
    classdocs
    '''
                        
    @staticmethod
    def waitForSignal(timeout=60):
        
        if(DPAFI.ComPort==None):
            return False

        data = bytearray()
                
        DPAFI.ComPort.flushInput()
        DPAFI.ComPort.flushOutput()
        DPAFI.ComPort.timeout = 1
        
        endTime = (time.time() + timeout)
        while time.time()<endTime:
        #while 1:
            data += DPAFI.ComPort.read()
            while len(data)>=2:
                if(data[0] != 0x07) or (data[1] != 0x0A):
                    data.pop(0)
                else:
                    break;
            if len(data)>=2: return True
            time.sleep(0.001)
            if TS_Globals.gStopTestExecution==True: break
        return False
    
    @staticmethod
    def sendResponse(data):
        if(DPAFI.ComPort==None):
            return False            
        DPAFI.ComPort.write(data)
        