import RPi.GPIO as GPIO
import time
from datetime import datetime

from TPMRegister import TPM_ACCESS, TPM_INT_ENABLE, TPM_INT_VECTOR, TPM_INT_STATUS, TPM_INTF_CAPABILITY, TPM_STS, TPM_DATA_FIFO, TPM_XDATA_FIFO, TPM_DID_VID, TPM_RID
import TS_Globals
from TS_Globals import LOCALITY_NONE, LOCALITY_0, LOCALITY_1, LOCALITY_2, LOCALITY_3, LOCALITY_4

import pprint
from curses import start_color
from distlib._backport.tarfile import LENGTH_LINK



COMMUNICATION_OK = 0
COMMUNICATION_TIMEOUT = 1
COMMUNICATION_BRUST_TIMEOUT = 2

EXPECT_MORE_DATA = 0x01

RST=11              #
PP=13
LAD0=MISO=LAD0_MISO=21
LAD1=MOSI=LAD1_MOSI=19
LAD2=PIRQ=LAD2_PIRQ=12
LAD3=22
LCLK=SPI_CLK=LCLK_SPICLK=23
LFRAME=SPI_CS=LFRAME_SPICS=24
SERIRQ=26
LPCPD=29
CLKRUN=31
TXRX_TRIGGER = 37
POWER_3V3 = 7
POWER_RELAIS_3V3 = 40

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(RST, GPIO.OUT)
GPIO.setup(PP, GPIO.OUT)
GPIO.setup(TXRX_TRIGGER, GPIO.OUT)
GPIO.output(PP, GPIO.LOW)
GPIO.output(TXRX_TRIGGER, GPIO.LOW)
GPIO.setup(POWER_3V3, GPIO.OUT)
GPIO.setup(POWER_RELAIS_3V3, GPIO.OUT)
GPIO.output(PP, GPIO.HIGH)

DEFAULT_POLLING_INTERVAL = 0.01
DEFAULT_RX_TIMEOUT = 10
DEFAULT_TX_TIMEOUT = 1

class TPM_Communication:
    
    locality = LOCALITY_NONE
    sendTimeout = DEFAULT_TX_TIMEOUT
    receiveTimeout = DEFAULT_RX_TIMEOUT
    clockTearStart = 0
    clockTearCount = 0
    txClocks = 0
    isDummy = False
    sendTime = 0
    receiveTime = 0

    sInterface = None
    pollingInterval = DEFAULT_POLLING_INTERVAL

    @staticmethod
    def getInterface():
        return TPM_Communication.sInterface

    @staticmethod
    def clearInterface():
        TPM_Communication.sInterface = None

    def __init__(self):
        self.pollingInterval = 0.01
    
    def reset(self):
                
        self.locality=LOCALITY_NONE
        GPIO.output(RST, GPIO.LOW)
        self.setPP(0)
        time.sleep(0.2)
        GPIO.output(RST, GPIO.HIGH)
        time.sleep(0.1)
        self.setLocality(LOCALITY_0)
        
    def setClockTearing(self, start, length=1):
        self.clockTearStart = start
        self.clockTearCount = length
        
    def setVDD(self, state):
        pass        

    def setPP(self, pp):
        if(pp==0):
            GPIO.output(PP, GPIO.LOW)
        elif(pp==1):
            GPIO.output(PP, GPIO.HIGH)

    def setLocality(self, locality):
        if (locality==LOCALITY_0) or (locality==LOCALITY_1) or (locality==LOCALITY_2) or (locality==LOCALITY_3) or (locality==LOCALITY_4):
            if(self.locality!=-1):
                self.writeRegister(TPM_ACCESS, bytearray([TPM_ACCESS.activeLocalityFlag]))
                
            self.locality = locality
            self.writeRegister(TPM_ACCESS, bytearray([TPM_ACCESS.requestUseFlag]))
            time.sleep(0.1)
            self.readRegister(TPM_ACCESS)
            if (0x01==TPM_ACCESS.activeLocality):
                return True
            else:
                return False
    def getTxClocks(self):
        return self.txClocks
    def setRxTimeout(self, timeout):
        self.receiveTimeout = timeout
    def setPollingInterval(self, interval):
        self.pollingInterval = interval
        
    def setDefaultValues():
        self.pollingInterval = DEFAULT_POLLING_INTERVAL
        self.receiveTimeout = DEFAULT_RX_TIMEOUT
        self.sendTimeout = DEFAULT_TX_TIMEOUT


    def receiveData(self, data):
        #time.sleep(0.01)
        ref_time = time.time() + self.receiveTimeout
        ret=COMMUNICATION_TIMEOUT
        #print "start receive"
        while(time.time()<ref_time):
            self.readRegister(TPM_STS)
            if(TPM_STS.dataAvail==1) and (TPM_STS.tpmGo==0):
                GPIO.output(TXRX_TRIGGER, GPIO.LOW) 
                self.receiveTime = datetime.now()
                while(TPM_STS.dataAvail==1) and (time.time()<ref_time):
                    self.readRegister(TPM_DATA_FIFO,min(4,TPM_STS.burstCount))
                    data.extend(TPM_DATA_FIFO.data)
                    self.readRegister(TPM_STS)
                if (time.time()<ref_time): ret = COMMUNICATION_OK
                self.writeRegister(TPM_STS, bytearray([TPM_STS.commandReadyFlag]))
                break
            time.sleep(self.pollingInterval)
        #print "received"
        GPIO.output(TXRX_TRIGGER, GPIO.LOW)
        return ret
        
    def sendData(self, data):
        self.txClocks=0
        size = len(data)
        packets = (size>>2)
    
        ref_time = time.time() + self.sendTimeout
        
        ret=COMMUNICATION_TIMEOUT


        self.writeRegister(TPM_STS, bytearray([TPM_STS.commandReadyFlag]))
        

        while(time.time()<ref_time):
            self.readRegister(TPM_STS)
            if (TPM_STS.commandReady==1) and (TPM_STS.tpmGo==0):
                ret=COMMUNICATION_OK
                break
        
        if(ret==COMMUNICATION_OK):
            for i in range(0,packets):
                self.writeRegister(TPM_DATA_FIFO,data[(i*4):(i*4+4)])
                self.readRegister(TPM_STS)
                #while(TPM_STS.burstCount<4):
                ref_time = time.time() + 5
                ret=COMMUNICATION_BRUST_TIMEOUT
                while (time.time()<ref_time):
                    #time.sleep(0.01)
                    self.readRegister(TPM_STS)
                    if(TPM_STS.burstCount>=4):
                        ret=COMMUNICATION_OK
                        break
                if (ret!=COMMUNICATION_OK): break
    
        if(ret==COMMUNICATION_OK):
            if(size>(packets*4)):
                self.writeRegister(TPM_DATA_FIFO,data[(packets*4):])
            self.sendTime = datetime.now()
            GPIO.output(TXRX_TRIGGER, GPIO.HIGH)
            self.writeRegister(TPM_STS, bytearray([TPM_STS.tpmGoFlag]))
        else:
            GPIO.output(TXRX_TRIGGER, GPIO.HIGH)
        return ret

            
    def receive(self, data):
        ret=self.receiveData(data)
        if(ret==COMMUNICATION_OK):
            pass
        elif(ret==COMMUNICATION_TIMEOUT):
            print "receive data failed:\n"
            self.readRegister(TPM_STS)
            TPM_STS.Print()
        return ret
            
    
    def send(self, data):
        ret=self.sendData(data)
        if(ret==COMMUNICATION_OK):
            pass
        elif(ret==COMMUNICATION_TIMEOUT):
            raise Exception("Could not send data!")
            #print "send data failed\n"
            #self.readRegister(TPM_STS)
            #TPM_STS.Print() 
        elif (ret==COMMUNICATION_BRUST_TIMEOUT):
            raise Exception("Could not send data, brust timeout!")
        return ret
    
    @staticmethod
    def cleanUp():
        GPIO.cleanup()
        