import time
import struct

class Ticket():
    
    sTickets = []
    sTimeBaseOffset = 0
 
    @staticmethod
    def getTicket(digest):
        for o in Ticket.sTickets:
            if (o.digest==digest):
                return o
        return None
    @staticmethod
    def addTicket(tag,  hierarchy, digest, expiration, expirationTime):
        Ticket.sTickets.append(Ticket(tag,  hierarchy, digest, expiration, expirationTime))
    
    @staticmethod
    def destroyTicket(digest):
        if(isinstance(digest, bytearray)):
            for o in Ticket.sObjects:
                if (o.digest==digest):
                    Ticket.sTickets.remove(o)
                    return
 
    @staticmethod
    def clearTickets():
        Ticket.sTickets = []

 
    def __init__(self, tag,  hierarchy, digest, expiration, expirationTime):
        self.tag = tag
        self.hierarchy = hierarchy
        self.digest = digest
        self.expirationTime = expirationTime
        
        expTime = 0
        for i in range(0,8):
            expTime = (expTime<<8) + expirationTime[i]

        duration = struct.unpack(">i",expiration)[0]
        duration = (duration*-1) if duration<0 else duration
          
        self.endTime = time.time() + duration
        Ticket.sTimeBaseOffset = time.time() - expTime
    