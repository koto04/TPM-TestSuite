import glob
import serial

class ComPort:

    ComPort = None# serial.Serial()

    @staticmethod
    def getAvailablePorts():
        ports = glob.glob('/dev/tty[A-Za-z]*')
        availablePorts = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                availablePorts.append(port)
            except (OSError, serial.SerialException):
                pass
        return availablePorts

    @staticmethod
    def setComPort(comPort):
        if (comPort.startswith("COM") or comPort.startswith("/dev/")):
            try:
                ComPort.ComPort=serial.Serial(port=comPort, baudrate=19200, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=60)
                if(ComPort.ComPort.isOpen()==False):
                    ComPort.ComPort.open()
            except Exception, e:
                print e
                ComPort.ComPort = None
                return False
            return ComPort.ComPort.isOpen()
            
        else:
            if(ComPort.ComPort!=None):
                if(ComPort.ComPort.isOpen()):
                    ComPort.ComPort.close()
            ComPort.ComPort = None
            return True

    @staticmethod
    def resetComPort():
        ComPort.ComPort = None
