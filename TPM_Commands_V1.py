import base64
import hashlib

from TPM_Constants_V1 import TPM_SUCCESS
from TPM_Session_V1 import Session
from Utils import bytearray2hex, int2bytearray, hex2bytearray


class Command1:
    
    def init(self, parameters,  manager, forDoc=False):
        
        self.time = 0
        self.forDoc = forDoc
        self.command = bytearray();
        self.ordinal = bytearray();
        self.commandName = parameters[0]
        self.tag = self.interpretParameter(parameters[1])
        self.paramSize = bytearray(base64.b16decode(b'00000000', casefold=True))
        self.rTag = bytearray()
        self.rParamSize = bytearray()
        self.returnCode = bytearray()
        self.responseCode = bytearray()
        self.nonceEven =bytearray()
        self.resAuth =bytearray()      
        self.rContinueAuthSession = bytearray()
        self.manager = manager
        self.definedSize=None
        self.extension=bytearray() 
        self.logEnable = True

        self.mInParameters = parameters
        self.mTreeId = 0
        self.mTreeInId = 0
        self.mTreeOutId = 0
        self.mTreeId = self.manager.insertTreeOutput('', 'end', text=self.commandName)
        self.mTreeInId = self.manager.insertTreeOutput(self.mTreeId, 'end', text="Input")
        self.mTreeOutId = self.manager.insertTreeOutput(self.mTreeId, 'end', text="Output")
    
    def fuzzy(self, level):
        pass

    def increaseFailure(self, text):
        self.manager.increaseFailure(text)

    def writeLog(self, text):
        self.manager.writeLog(text)
        
    def setDefinedSize(self, defSize):
        self.definedSize=[(defSize & (0xff << pos*8)) >> pos*8 for pos in range((4-1),-1,-1)]
    
    def setExtension(self,extension):
            if isinstance(extension, basestring):
                self.extension = hex2bytearray(extension)
            else:
                self.extension=extension
            
    def finalize(self):
        if(self.definedSize!=None):
            self.command[2:6] = self.definedSize
        self.writeLog("\nprocessing " + self.commandName + "...")
        self.command.extend(self.extension)

    
    def commandLen(self):
        ret = 0
        for i in range(0,4):
            ret = (ret<<8) + self.commandSize[i]
        return ret

    def calcCompositeHash(self, params):
        composite = bytearray()
        composite.extend(self.interpretParameter(params[0]))
        pcrValues = bytearray()
        for i in range(1, len(params)):
            pcrValues.extend(self.interpretParameter(params[i]))
            
        composite.extend(int2bytearray(len(pcrValues), 4))
        composite.extend(pcrValues)
        return bytearray(hashlib.sha1(composite).digest())
        
    
    def inTreeAuthorization(self):
        self.manager.insertTreeOutput(self.mTreeInId, 'end', text="authHandle=" + bytearray2hex(self.authHandle))
        self.manager.insertTreeOutput(self.mTreeInId, 'end', text="nonceOdd=" + bytearray2hex(self.nonceOdd))
        self.manager.insertTreeOutput(self.mTreeInId, 'end', text="continueAuthSession=" + bytearray2hex(self.continueAuthSession))
        self.manager.insertTreeOutput(self.mTreeInId, 'end', text="pubAuth=" + bytearray2hex(self.pubAuth))
    
    def outTreeAuthorization(self, data, index):
        
        x=len(data)
        if(x>index):
            self.nonceEven = data[index:(index+20)]
            index += 20
        if(x>index):
            self.rContinueAuthSession = data[index:(index+1)]
            index +=1 #continueAuthSession
        if(x>index):
            self.resAuth = data[index:(index+20)]
            index += 20
        self.manager.insertTreeOutput(self.mTreeOutId, 'end', text="nonceEven=" + bytearray2hex(self.nonceEven))
        self.manager.insertTreeOutput(self.mTreeOutId, 'end', text="rContinueAuthSession=" + bytearray2hex(self.rContinueAuthSession))
        self.manager.insertTreeOutput(self.mTreeOutId, 'end', text="resAuth=" + bytearray2hex(self.resAuth))
        
        if(self.responseCode==TPM_SUCCESS):
            Session.setNonceEven(self.authHandle, self.nonceEven)
        
        if(self.rContinueAuthSession == bytearray([0x00])) or (self.responseCode!=TPM_SUCCESS):
            Session.destroySession(self.authHandle)
        return index
        
    def increaseFailure(self, text):
        self.manager.increaseFailure(text)

    def interpretParameter(self,value):
        try:
            if(isinstance(value, bytearray)):
                return value
            elif(isinstance(value,  basestring)):
                if(value==""):
                    return bytearray()
                else:
                    return bytearray(base64.b16decode(value.replace(" ", ""), casefold=True))
            else:
                return bytearray()
        except Exception, e:            
            self.manager.writeLog("Test execution exception at interpret parameter")
            self.manager.increaseException(str(e))

    def insertTreeOutput(self, tid, pos, text, tag=""):
        return self.manager.insertTreeOutput(tid, pos, text=text, tag=tag)

    def interpret_PUB_KEY(self,  parameter):
        algorithmID = bytearray()
        encScheme = bytearray()
        sigScheme = bytearray()
        parms = bytearray()
        pubKey = bytearray()
        key=bytearray()
        index=0
        if(isinstance(parameter, list)):
            algorithmID = self.interpretParameter(parameter[0])
            encScheme = self.interpretParameter(parameter[1])
            sigScheme = self.interpretParameter(parameter[2])
            parms = self.interpretParameter(parameter[3])
            pubKey = self.interpretParameter(parameter[4])
        else:
            if(len(parameter)>index):
                algorithmID = parameter[index:(index+4)]
                index +=4
            if(len(parameter)>index):
                encScheme = parameter[index:(index+2)]
                index +=2
            if(len(parameter)>index):
                sigScheme = parameter[index:(index+2)]
                index +=2
            if(len(parameter)>index):
                dataLen = (parameter[(index)]<<24) + (parameter[(index+1)]<<16) + (parameter[(index+2)]<<8) + (parameter[(index+3)]<<0)
                index +=4
                parms = parameter[index:(index+dataLen)]
                index += dataLen
            if(len(parameter)>index):
                dataLen = (parameter[(index)]<<24) + (parameter[(index+1)]<<16) + (parameter[(index+2)]<<8) + (parameter[(index+3)]<<0)
                index +=4
                pubKey = parameter[index:(index+dataLen)]
                index += dataLen
        
        key.extend(algorithmID)
        key.extend(encScheme)
        key.extend(sigScheme)
        key.extend(int2bytearray(len(parms), 4))
        key.extend(parms)
        key.extend(int2bytearray(len(pubKey), 4))
        key.extend(pubKey)
    
        return dict([
                            ('len',  index), 
                            ('key',  key), 
                            ('algorithmID',  algorithmID), 
                            ('encScheme', encScheme), 
                            ('sigScheme', sigScheme), 
                            ('parms', parms), 
                            ('pubKey', pubKey)
                        ])
        

    def interpret_TPM_KEY(self,  parameter):
        
        key = bytearray()
        ver = bytearray()
        keyUsage = bytearray()
        keyFlags = bytearray()
        authDataUsage = bytearray()
        algorithmParams = bytearray()
        algorithmID = bytearray()
        encScheme = bytearray()
        sigScheme= bytearray()
        parms = bytearray()
        PCRInfo = bytearray()
        PCRInfoPcrSelection = bytearray()
        PCRInfoDigestAtRelease = bytearray()
        PCRInfoDigestAtCreation = bytearray()
        
        pubKey = bytearray()
        encData = bytearray()
        
        data = bytearray()
        if(isinstance(parameter, list)):
            pf = self.interpretParameter(parameter[0])
            data.extend(pf)
            pf = self.interpretParameter(parameter[1])
            data.extend(pf)
            pf = self.interpretParameter(parameter[2])
            data.extend(pf)
            pf = self.interpretParameter(parameter[3])
            data.extend(pf)
            
            if(isinstance(parameter[4],  list)):
                p=parameter[4]
                pf = self.interpretParameter(p[0])
                data.extend(pf)
                pf = self.interpretParameter(p[1])
                data.extend(pf)
                pf = self.interpretParameter(p[2])
                data.extend(pf)
                pf = self.interpretParameter(p[3])
                data.extend(int2bytearray(len(pf), 4))
                data.extend(pf)

            else:
                pf = self.interpretParameter(parameter[4])
                data.extend(pf)
            
            if(isinstance(parameter[5],  list)):
                pf=bytearray()
                p = parameter[5]
                
                pf.extend(self.interpretParameter(p[0]))
                if(isinstance(p[1], list)):
                    pf.extend(self.calcCompositeHash(p[1]))
                else:
                    pf.extend(self.interpretParameter(p[1]))
                
                if(isinstance(p[2], list)):
                    pf.extend(self.calcCompositeHash(p[2]))
                else:
                    pf.extend(self.interpretParameter(p[2]))
            
            else:
                pf = self.interpretParameter(parameter[5])
            
            
            data.extend(int2bytearray(len(pf), 4))
            data.extend(pf)
 
            pf = self.interpretParameter(parameter[6])
            data.extend(int2bytearray(len(pf), 4))
            data.extend(pf)
            pf = self.interpretParameter(parameter[7])
            data.extend(int2bytearray(len(pf), 4))
            data.extend(pf)
        
        else:
            data = self.interpretParameter(parameter)
            


        index = 0

        if(len(data)>index):
            ver = data[index:(index+4)]
            index +=4
        if(len(data)>index):
            keyUsage = data[index:(index+2)]
            index +=2
        if(len(data)>index):
            keyFlags = data[index:(index+4)]
            index +=4
        if(len(data)>index):
            authDataUsage = data[index:(index+1)]
            index +=1
        parmsStartIndex = index
        if(len(data)>index):
            algorithmID = data[index:(index+4)]
            index += 4
        if(len(data)>index):
            encScheme = data[index:(index+2)]
            index += 2
        if(len(data)>index):
            sigScheme = data[index:(index+2)]
            index += 2
        if(len(data)>index):
            dataLen =  (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index += 4
            parms = data[index:(index+dataLen)]
            index += dataLen
        algorithmParams = data[parmsStartIndex:index]
        if(len(data)>index):
            dataLen =  (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index += 4
            PCRInfo = data[index:(index+dataLen)]
            if(dataLen>0):
                PCRInfoSelectionLen = (PCRInfo[0]<<8) + (PCRInfo[1]<<0)
                pcrIndex = 0
                PCRInfoPcrSelection = PCRInfo[pcrIndex:(pcrIndex+PCRInfoSelectionLen+2)]
                pcrIndex += (PCRInfoSelectionLen+2)
                PCRInfoDigestAtRelease = PCRInfo[pcrIndex:(pcrIndex+20)]
                pcrIndex += 20
                PCRInfoDigestAtCreation = PCRInfo[pcrIndex:(pcrIndex+20)]    
                pcrIndex += 20     
            index += dataLen
        if(len(data)>index):
            dataLen =  (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index += 4
            pubKey = data[index:(index+dataLen)]
            index += dataLen
        if(len(data)>index):
            dataLen =  (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index += 4
            encData = data[index:(index+dataLen)]
            index += dataLen

        key = data[0:index]
        return dict([
                            ('len',  index), 
                            ('key',  key), 
                            ('ver',  ver), 
                            ('keyUsage', keyUsage), 
                            ('keyFlags', keyFlags), 
                            ('authDataUsage', authDataUsage), 
                            ('algorithmParams', algorithmParams), 
                            ('algorithmID', algorithmID), 
                            ('encScheme', encScheme), 
                            ('sigScheme', sigScheme), 
                            ('parms', parms), 
                            ('PCRInfo', PCRInfo), 
                            ('PCRInfoPcrSelection', PCRInfoPcrSelection), 
                            ('PCRInfoDigestAtRelease', PCRInfoDigestAtRelease), 
                            ('PCRInfoDigestAtCreation', PCRInfoDigestAtCreation), 
                            ('pubKey',pubKey), 
                            ('encData', encData)
                        ])




#index 0 => class name
#index 1 => auth handle (not used yet)
#index 2 => implemented in TS
#index 3 => mandatory TPM command
#index 4 => TPM-Version
TPM_command = {}

TPM_command["TPM_ORD_AuthorizeMigrationKey"] = ["TPM_AuthorizeMigrationKey", [], True, True, 1]
TPM_command["TPM_ORD_ChangeAuth"] = ["TPM_ChangeAuth", [], True, True, 1]
TPM_command["TPM_ORD_ChangeAuthOwner"] = ["TPM_ChangeAuthOwner", [], True, True, 1]
TPM_command["TPM_ORD_CMK_CreateTicket"] = ["TPM_CMK_CreateTicket", [], True, False, 1]
TPM_command["TPM_ORD_ContinueSelfTest"] = ["TPM_ContinueSelfTest", [], True, True, 1]
TPM_command["TPM_ORD_ConvertMigrationBlob"] = ["TPM_ConvertMigrationBlob", [], True, True, 1]
TPM_command["TPM_ORD_CreateCounter"] = ["TPM_CreateCounter", [], True, True, 1]
TPM_command["TPM_ORD_CreateMigrationBlob"] = ["TPM_CreateMigrationBlob", [], True, True, 1]
TPM_command["TPM_ORD_CreateWrapKey"] = ["TPM_CreateWrapKey", [], True, True, 1]
TPM_command["TPM_ORD_Delegate_CreateKeyDelegation"] = ["TPM_Delegate_CreateKeyDelegation", [], True, True, 1]
TPM_command["TPM_ORD_Delegate_CreateOwnerDelegation"] = ["TPM_Delegate_CreateOwnerDelegation", [], True, True, 1]
TPM_command["TPM_ORD_Delegate_LoadOwnerDelegation"] = ["TPM_Delegate_LoadOwnerDelegation", [], True, True, 1]
TPM_command["TPM_ORD_Delegate_UpdateVerification"] = ["TPM_Delegate_UpdateVerification", [], True, True, 1]
TPM_command["TPM_ORD_Delegate_Manage"] = ["TPM_Delegate_Manage", [], True, True, 1]
TPM_command["TPM_ORD_DSAP"] = ["TPM_DSAP", [], True, True, 1]
TPM_command["TPM_ORD_Extend"] = ["TPM_Extend", [], True, True, 1]
TPM_command["TPM_ORD_FlushSpecific"] = ["TPM_FlushSpecific", [], True, True, 1]
TPM_command["TPM_ORD_ForceClear"] = ["TPM_ForceClear", [], True, True, 1]
TPM_command["TPM_ORD_GetCapability"] = ["TPM_GetCapability", [], True, True, 1]
TPM_command["TPM_ORD_GetPubKey"] = ["TPM_GetPubKey", [], True, True, 1]
TPM_command["TPM_ORD_GetRandom"] = ["TPM_GetRandom", [], True, True, 1]
TPM_command["TPM_ORD_GetTestResult"] = ["TPM_GetTestResult", [], True, True, 1]
TPM_command["TPM_ORD_GetTicks"] = ["TPM_GetTicks", [], True, True, 1]
TPM_command["TPM_ORD_IncrementCounter"] = ["TPM_IncrementCounter", [], True, True, 1]
TPM_command["TPM_ORD_Init"] = ["TPM_Init", [], True, True, 1]
TPM_command["TPM_ORD_LoadKey"] = ["TPM_LoadKey", [], True, True, 1]
TPM_command["TPM_ORD_LoadKey2"] = ["TPM_LoadKey2", [], True, True, 1]
TPM_command["TPM_ORD_MigrateKey"] = ["TPM_MigrateKey", [], True, True, 1]
TPM_command["TPM_ORD_NV_DefineSpace"] = ["TPM_NV_DefineSpace", [], True, True, 1]
TPM_command["TPM_ORD_NV_ReadValue"] = ["TPM_NV_ReadValue", [], True, True, 1]
TPM_command["TPM_ORD_NV_ReadValueAuth"] = ["TPM_NV_ReadValueAuth", [], True, True, 1]
TPM_command["TPM_ORD_NV_WriteValue"] = ["TPM_NV_WriteValue", [], True, True, 1]
TPM_command["TPM_ORD_NV_WriteValueAuth"] = ["TPM_NV_WriteValueAuth", [], True, True, 1]
TPM_command["TPM_ORD_OIAP"] = ["TPM_OIAP", [], True, True, 1]
TPM_command["TPM_ORD_OSAP"] = ["TPM_OSAP", [], True, True, 1]
TPM_command["TPM_ORD_OwnerClear"] = ["TPM_OwnerClear", [], True, True, 1]
TPM_command["TPM_ORD_PCR_Reset"] = ["TPM_PCR_Reset", [], True, True, 1]
TPM_command["TPM_ORD_PCRRead"] = ["TPM_PCRRead", [], True, True, 1]
TPM_command["TPM_ORD_PhysicalDisable"] = ["TPM_PhysicalDisable", [], True, True, 1]
TPM_command["TPM_ORD_PhysicalEnable"] = ["TPM_PhysicalEnable", [], True, True, 1]
TPM_command["TPM_ORD_PhysicalSetDeactivated"] = ["TPM_PhysicalSetDeactivated", [], True, True, 1]
TPM_command["TPM_ORD_Quote"] = ["TPM_Quote", [], True, True, 1]
TPM_command["TPM_ORD_ReadCounter"] = ["TPM_ReadCounter", [], True, True, 1]
TPM_command["TPM_ORD_ReadPubek"] = ["TPM_ReadPubek", [], True, True, 1]
TPM_command["TPM_ORD_ReleaseCounter"] = ["TPM_ReleaseCounter", [], True, True, 1]
TPM_command["TPM_ORD_ReleaseCounterOwner"] = ["TPM_ReleaseCounterOwner", [], True, True, 1]
TPM_command["TPM_ORD_Reset"] = ["TPM_Reset", [], True, True, 1]
TPM_command["TPM_ORD_ResetLockValue"] = ["TPM_ResetLockValue", [], True, True, 1]
TPM_command["TPM_ORD_SaveState"] = ["TPM_SaveState", [], True, True, 1]
TPM_command["TPM_ORD_SelfTestFull"] = ["TPM_SelfTestFull", [], True, True, 1]
TPM_command["TPM_ORD_SetCapability"] = ["TPM_SetCapability", [], True, True, 1]
TPM_command["TPM_ORD_SetOwnerInstall"] = ["TPM_SetOwnerInstall", [], True, True, 1]
TPM_command["TPM_ORD_SHA1Complete"] = ["TPM_SHA1Complete", [], True, True, 1]
TPM_command["TPM_ORD_SHA1CompleteExtend"] = ["TPM_SHA1CompleteExtend", [], True, True, 1]
TPM_command["TPM_ORD_SHA1Start"] = ["TPM_SHA1Start", [], True, True, 1]
TPM_command["TPM_ORD_SHA1Update"] = ["TPM_SHA1Update", [], True, True, 1]
TPM_command["TPM_ORD_Startup"] = ["TPM_Startup", [], True, True, 1]
TPM_command["TPM_ORD_TakeOwnership"] = ["TPM_TakeOwnership", [], True, True, 1]
TPM_command["TPM_ORD_UnBind"] = ["TPM_UnBind", [], True, True, 1]
TPM_command["TPM_ORD_Seal"] = ["TPM_Seal", [], True, True, 1]
TPM_command["TPM_ORD_Sign"] = ["TPM_Sign", [], True, True, 1]
TPM_command["TPM_ORD_Unseal"] = ["TPM_Unseal", [], True, True, 1]
TPM_command["TSC_ORD_PhysicalPresence"] = ["TSC_PhysicalPresence", [], True, True, 1]

TPM_command["ChipInfo"] = ["ChipInfo", [], True, True,  1]
TPM_command["Startup"] = ["Startup", [], True, True,  1]


TPM_command["TPM_ORD_ActivateIdentity"] = ["TPM_ActivateIdentity", [], False, True, 1]
TPM_command["TPM_ORD_CertifyKey"] = ["TPM_CertifyKey", [], False, True, 1]
TPM_command["TPM_ORD_CertifyKey2"] = ["TPM_CertifyKey2", [], False, True, 1]
TPM_command["TPM_ORD_ChangeAuthAsymFinish"] = ["TPM_ChangeAuthAsymFinish", [], False, True, 1]
TPM_command["TPM_ORD_ChangeAuthAsymStart"] = ["TPM_ChangeAuthAsymStart", [], False, True, 1]
TPM_command["TPM_ORD_CMK_ApproveMA"] = ["TPM_CMK_ApproveMA", [], False, False, 1]
TPM_command["TPM_ORD_CMK_ConvertMigration"] = ["TPM_CMK_ConvertMigration", [], False, False, 1]
TPM_command["TPM_ORD_CMK_CreateBlob"] = ["TPM_CMK_CreateBlob", [], False, False, 1]
TPM_command["TPM_ORD_CMK_CreateKey"] = ["TPM_CMK_CreateKey", [], False, False, 1]
TPM_command["TPM_ORD_CMK_SetRestrictions"] = ["TPM_CMK_SetRestrictions", [], False, False, 1]
TPM_command["TPM_ORD_CreateEndorsementKeyPair"] = ["TPM_CreateEndorsementKeyPair", [], False, True, 1]
TPM_command["TPM_ORD_CreateMaintenanceArchive"] = ["TPM_CreateMaintenanceArchive", [], False, False, 1]
TPM_command["TPM_ORD_CreateRevocableEK"] = ["TPM_CreateRevocableEK", [], False, False, 1]
TPM_command["TPM_ORD_DAA_Join"] = ["TPM_DAA_Join", [], False, False, 1]
TPM_command["TPM_ORD_DAA_Sign"] = ["TPM_DAA_Sign", [], False, False, 1]
TPM_command["TPM_ORD_Delegate_ReadTable"] = ["TPM_Delegate_ReadTable", [], False, True, 1]
TPM_command["TPM_ORD_Delegate_VerifyDelegation"] = ["TPM_Delegate_VerifyDelegation", [], False, True, 1]
TPM_command["TPM_ORD_DirRead"] = ["TPM_DirRead", [], False, True, 1]
TPM_command["TPM_ORD_DirWriteAuth"] = ["TPM_DirWriteAuth", [], False, True, 1]
TPM_command["TPM_ORD_DisableForceClear"] = ["TPM_DisableForceClear", [], False, True, 1]
TPM_command["TPM_ORD_DisableOwnerClear"] = ["TPM_DisableOwnerClear", [], False, True, 1]
TPM_command["TPM_ORD_DisablePubekRead"] = ["TPM_DisablePubekRead", [], False, True, 1]
TPM_command["TPM_ORD_EstablishTransport"] = ["TPM_EstablishTransport", [], False, True, 1]
TPM_command["TPM_ORD_EvictKey"] = ["TPM_EvictKey", [], False, True, 1]
TPM_command["TPM_ORD_ExecuteTransport"] = ["TPM_ExecuteTransport", [], False, True, 1]
TPM_command["TPM_ORD_FieldUpgrade"] = ["TPM_FieldUpgrade", [], False, False, 1]
TPM_command["TPM_ORD_GetAuditDigest"] = ["TPM_GetAuditDigest", [], False, False, 1]
TPM_command["TPM_ORD_GetAuditDigestSigned"] = ["TPM_GetAuditDigestSigned", [], False, False, 1]
TPM_command["TPM_ORD_GetCapabilityOwner"] = ["TPM_GetCapabilityOwner", [], False, True, 1]
TPM_command["TPM_ORD_KeyControlOwner"] = ["TPM_KeyControlOwner", [], False, True, 1]
TPM_command["TPM_ORD_KillMaintenanceFeature"] = ["TPM_KillMaintenanceFeature", [], False, False, 1]
TPM_command["TPM_ORD_LoadAuthContext"] = ["TPM_LoadAuthContext", [], False, False, 1]
TPM_command["TPM_ORD_LoadContext"] = ["TPM_LoadContext", [], False, True, 1]
TPM_command["TPM_ORD_LoadKeyContext"] = ["TPM_LoadKeyContext", [], False, False, 1]
TPM_command["TPM_ORD_LoadMaintenanceArchive"] = ["TPM_LoadMaintenanceArchive", [], False, False, 1]
TPM_command["TPM_ORD_LoadManuMaintPub"] = ["TPM_LoadManuMaintPub", [], False, False, 1]
TPM_command["TPM_ORD_MakeIdentity"] = ["TPM_MakeIdentity", [], False, True, 1]
TPM_command["TPM_ORD_OwnerReadInternalPub"] = ["TPM_OwnerReadInternalPub", [], False, True, 1]
TPM_command["TPM_ORD_OwnerReadPubek"] = ["TPM_OwnerReadPubek", [], False, True, 1]
TPM_command["TPM_ORD_OwnerSetDisable"] = ["TPM_OwnerSetDisable", [], False, True, 1]
TPM_command["TPM_ORD_Quote2"] = ["TPM_Quote2", [], False, False, 1]
TPM_command["TPM_ORD_ReadManuMaintPub"] = ["TPM_ReadManuMaintPub", [], False, False, 1]
TPM_command["TPM_ORD_ReleaseTransportSigned"] = ["TPM_ReleaseTransportSigned", [], False, True, 1]
TPM_command["TPM_ORD_RevokeTrust"] = ["TPM_RevokeTrust", [], False, False, 1]
TPM_command["TPM_ORD_SaveAuthContext"] = ["TPM_SaveAuthContext", [], False, False, 1]
TPM_command["TPM_ORD_SaveContext"] = ["TPM_SaveContext", [], False, True, 1]
TPM_command["TPM_ORD_SaveKeyContext"] = ["TPM_SaveKeyContext", [], False, False, 1]
TPM_command["TPM_ORD_Sealx"] = ["TPM_Sealx", [], False, False, 1]
TPM_command["TPM_ORD_SetOperatorAuth"] = ["TPM_SetOperatorAuth", [], False, True, 1]
TPM_command["TPM_ORD_SetOrdinalAuditStatus"] = ["TPM_SetOrdinalAuditStatus", [], False, False, 1]
TPM_command["TPM_ORD_SetOwnerPointer"] = ["TPM_SetOwnerPointer", [], False, True, 1]
TPM_command["TPM_ORD_SetRedirection"] = ["TPM_SetRedirection", [], False, False, 1]
TPM_command["TPM_ORD_SetTempDeactivated"] = ["TPM_SetTempDeactivated", [], False, True, 1]
TPM_command["TPM_ORD_StirRandom"] = ["TPM_StirRandom", [], False, True, 1]
TPM_command["TPM_ORD_Terminate_Handle"] = ["TPM_Terminate_Handle", [], False, True, 1]
TPM_command["TPM_ORD_TickStampBlob"] = ["TPM_TickStampBlob", [], False, True, 1]
