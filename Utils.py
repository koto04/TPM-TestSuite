
import base64

def hex2bytearray(data):
    data=data.replace(" ","")
    if(len(data)==0):
        return bytearray()
    else:
        return bytearray(base64.b16decode(data, casefold=True))
    
def bytearray2hex(data):
    return (' '.join(format(x, '02x') for x in data)).upper()


def int2bytearray(val, num_bytes):
    return bytearray([(val & (0xff << pos*8)) >> pos*8 for pos in range((num_bytes-1),-1,-1)])

def bytearray2int(data):
    ret = 0
    if(len(data)==0):
        return ret
    else: 
        for b in data:
            ret = (ret<<8) + b
    return ret

def millisInterval(start, end):
    diff = end-start
    millis = 0
    millis += diff.days * 24 * 60 * 60 * 1000
    millis += diff.seconds * 1000
    millis += diff.microseconds / 1000
    return millis
    