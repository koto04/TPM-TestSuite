import base64
import hashlib
import hmac
import Crypto.Cipher
import time

from TPM_Constants_V2 import *
from TPM_Object import Object

class Session():

    sSessions = []

    POLICY_HMAC = 1
    POLICY_AUTHVALUE = 2
    POLICY_PASSWORD = 3

    @staticmethod
    def addSession(startAuthSessionCommand):
        session = Session(startAuthSessionCommand.sessionHandle,  
                                    startAuthSessionCommand.nonceTPM,  
                                    startAuthSessionCommand.salt,
                                    startAuthSessionCommand.encryptedSalt,  
                                    startAuthSessionCommand.sessionType,  
                                    startAuthSessionCommand.symmetric,  
                                    startAuthSessionCommand.authHash,  
                                    startAuthSessionCommand.bind, 
                                    startAuthSessionCommand.nonceCaller)
        session.calcSessionKey()
        Session.sSessions.append(session)
        return session

    @staticmethod
    def destroySession(sessionHandle):
        if(isinstance(sessionHandle, bytearray)):
            for session in Session.sSessions:
                if (session.handle()==sessionHandle):
                    Session.sSessions.remove(session)
                    return

    @staticmethod
    def authPolicy(sessionHandle):
        if(sessionHandle==TPM_RS_PW):
            return Session.POLICY_PASSWORD
        else:
            for session in Session.sSessions:
                if (session.handle()==sessionHandle):
                    return session.mPolicy
        
        return bytearray()
            
    @staticmethod
    def clearSessions():
        Session.sSessions = []
    
    @staticmethod
    def getSession(handle):
        for session in Session.sSessions:
            if (session.mSessionHandle==handle):
                return session
        return None
    
    @staticmethod
    def getAuthValue(TPM_command):
        
        if(TPM_command.sessionHandle==TPM_RS_PW):
            if len(TPM_command.nonceCallerBackup)==0: TPM_command.nonceCallerBackup=TPM_command.nonceCaller
            value = TPM_command.nonceCallerBackup
            TPM_command.nonceCaller = bytearray()
            return value
        else:
            for session in Session.sSessions:
                if (session.handle()==TPM_command.sessionHandle):
                    if(session.mPolicy==Session.POLICY_HMAC)or(session.mPolicy==Session.POLICY_AUTHVALUE):
                        authValue = bytearray()
                        handle = bytearray()
                        authHandles = []
                        for i in range(0, (len(TPM_command.HMACHandles)/4)):
                            if(i==0):
                                authHandles.append(TPM_command.HMACHandles[0:4])
                                if(session.mSessionType==TPM_SE_HMAC) or (TPM_command.HMACHandles[(i*4)]&0xF0 == 0x40):
                                    #authPolicy available?
                                    authPolicy = Object.getAuthPolicy(TPM_command.HMACHandles[(i*4):(i*4+4)])
                                    if(len(authPolicy)==0):
                                        authValue.extend(Object.getAuthValue(TPM_command.HMACHandles[(i*4):(i*4+4)]))
                                elif(session.mSessionType==TPM_SE_POLICY) and (session.mPolicy==Session.POLICY_AUTHVALUE):
                                    authValue.extend(Object.getAuthValue(TPM_command.HMACHandles[(i*4):(i*4+4)]))
                                    
                            handle.extend(Object.getName(TPM_command.HMACHandles[(i*4):(i*4+4)]))
                        if(session.mSessionType==TPM_SE_HMAC) and (session.mBindName==Object.getName(TPM_command.HMACHandles[0:4])):
                            key = session.mSessionKey
                        else:
                            key = session.mSessionKey + authValue
                        if(session.mAuthHash==TPM_ALG_SHA1):
                            pHash = hashlib.sha1(TPM_command.commandCode + handle + TPM_command.HMACParameters).digest()
                            pHMac = hmac.new(key, msg=pHash+TPM_command.nonceCaller+session.mNonceTPM+TPM_command.sessionAttribute,  digestmod=hashlib.sha1).hexdigest()
                            return bytearray(base64.b16decode(pHMac, casefold=True))
                        elif(session.mAuthHash==TPM_ALG_SHA256):
                            pHash = hashlib.sha256(TPM_command.commandCode + handle + TPM_command.HMACParameters).digest()
                            pHMac = hmac.new(key, msg=pHash+TPM_command.nonceCaller+session.mNonceTPM+TPM_command.sessionAttribute,  digestmod=hashlib.sha256).hexdigest()
                            return bytearray(base64.b16decode(pHMac, casefold=True))
                    else:
                        return Object.getAuthValue(TPM_command.HMACHandles[0:4]) 
    
    @staticmethod
    def setNonceTPM(sessionHandle,  nonceTPM):
        for session in Session.sSessions:
            if (session.handle()==sessionHandle):
                session.mNonceTPM = nonceTPM
                return
                
    @staticmethod
    def setPolicy(sessionHandle,  policy):
        for session in Session.sSessions:
            if (session.handle()==sessionHandle):
                session.mPolicy = policy
                return


    def __init__(self, sessionHandle,  nonceTPM,  salt,  encryptedSalt,  sessionType,  symmetric,  authHash,  bind,  nonceCaller):
        self.mSessionHandle = sessionHandle
        self.mNonceTPM = nonceTPM
        self.mSalt = salt
        self.mEncryptedSalt = encryptedSalt
        self.mSessionType = sessionType
        self.mSymmetric = symmetric
        self.mAuthHash = authHash
        self.mSessionKey = bytearray()
        self.mPolicy = Session.POLICY_HMAC
        self.mPolicyDigest = bytearray()
        self.mBind = bind
        self.mBindName = Object.getName(self.mBind)
        self.mAuthValue = Object.getAuthValue(self.mBind)
        self.mInitialNonceTPM = nonceTPM
        self.mInitialNonceCaller = nonceCaller
        self.mCpHashA = bytearray()
        self.startTime = time.time()
        self.mTimeOut = 0
        
    def handle(self):
        return self.mSessionHandle
    def sessionType(self):
        return self.mSessionType
    def Policy(self):
        return self.mPolicy

    
    #supports only AES 128 bit key size CFB mode
    def calcEncryptedParameter(self, parameter, nonceCaller):
        msg = bytearray()
        msg.extend(bytearray([0x00,  0x00,  0x00,  0x01])) #[i]2
        msg.extend(bytearray([0x43,  0x46,  0x42,  0x00])) #CFB
        msg.extend(nonceCaller) 
        msg.extend(self.mNonceTPM) 
        msg.extend(bytearray([0x00,  0x00,  0x01,  0x00])) #number of bits
            
        if(self.mAuthHash==TPM_ALG_SHA1):
            pHMac = hmac.new(self.mSessionKey, msg=msg,  digestmod=hashlib.sha1).digest()
        elif(self.mAuthHash==TPM_ALG_SHA256):
            pHMac = hmac.new(self.mSessionKey, msg=msg,  digestmod=hashlib.sha256).digest()
        encryptionKey = pHMac
        
        #encryptionKey = bytearray(base64.b16decode(pHMac, casefold=True))
        #x = bytearray(base64.b16decode(pHMac, casefold=True))
        ml = len(parameter)
        l = 16-(ml%16)
        if l==16: l=0
        for i in range(0,l):
            parameter.append(0)
        
        message=str(parameter)
        cipher = Crypto.Cipher.AES.new(key=encryptionKey[0:16], mode=Crypto.Cipher.AES.MODE_CFB, IV=encryptionKey[16:32], segment_size=128)
        encrypted = cipher.encrypt(message)  
        ret = " ".join("{:02x}".format(ord(c)) for c in encrypted[0:ml]) 
        return ret
        
    def calcSessionKey(self):
        
        if(len(self.mAuthValue)>0) or (len(self.mSalt)>0) or (self.mBind!=TPM_RH_NULL):
            self.mBindName = Object.getName(self.mBind)
            self.mAuthValue = Object.getAuthValue(self.mBind)
            key = bytearray()
            key.extend(self.mAuthValue)
            key.extend(self.mSalt)
            msg = bytearray()
            msg.extend(bytearray([0x00,  0x00,  0x00,  0x01])) #[i]2
            msg.extend(bytearray([0x41,  0x54,  0x48,  0x00])) #ATH
            msg.extend(self.mInitialNonceTPM) 
            msg.extend(self.mInitialNonceCaller) 
            
            if(self.mAuthHash==TPM_ALG_SHA1):
                msg.extend(bytearray([0x00,  0x00,  0x00,  0xA0]))
                pHMac = hmac.new(key, msg=msg,  digestmod=hashlib.sha1).hexdigest()
                self.mSessionKey = bytearray(base64.b16decode(pHMac, casefold=True))
            elif(self.mAuthHash==TPM_ALG_SHA256):
                msg.extend(bytearray([0x00,  0x00,  0x01,  0x00]))
                pHMac = hmac.new(key, msg=msg,  digestmod=hashlib.sha256).hexdigest()
                self.mSessionKey = bytearray(base64.b16decode(pHMac, casefold=True))
