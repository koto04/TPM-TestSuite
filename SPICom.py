
import spidev
from TPMRegister import TPM_ACCESS, TPM_INT_ENABLE, TPM_INT_VECTOR, TPM_INT_STATUS, TPM_INTF_CAPABILITY, TPM_STS, TPM_DATA_FIFO, TPM_XDATA_FIFO, TPM_DID_VID, TPM_RID
from TPM_Communication import *
import time
import os
from TS_Globals import TS_Globals

class SPICom(TPM_Communication):
    
    spi = spidev.SpiDev()
    interface_type = TS_Globals.TPM_INTERFACE_SPI
        
    def __init__(self):
        GPIO.setup(PIRQ, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        
        
        if (GPIO.gpio_function(MISO)!=GPIO.SPI): os.system("gpio -g mode 9 alt0")
        if (GPIO.gpio_function(MOSI)!=GPIO.SPI): os.system("gpio -g mode 10 alt0")
        if (GPIO.gpio_function(SPI_CLK)!=GPIO.SPI): os.system("gpio -g mode 11 alt0")
        if (GPIO.gpio_function(SPI_CS)!=GPIO.OUT): GPIO.setup(SPI_CS, GPIO.OUT)
        

        #time.sleep(0.5)    
        self.spi.open(0,0)
        self.spi.mode = 0
        self.spi.lsbfirst = False
        self.spi.max_speed_hz = 20000000
        self.sendTimeout = 10 #1s
        self.receiveTimeout = 120 #2 min
        time.sleep(0.1)
        TPM_Communication.sInterface = self

    def setVDD(self, state):
        if(state==0):
            self.spi.close()
            GPIO.setup(SPI_CS, GPIO.OUT)
            GPIO.setup(SPI_CLK, GPIO.OUT)
            GPIO.setup(MOSI, GPIO.OUT)
            GPIO.setup(MISO, GPIO.OUT)           
            GPIO.output(RST, GPIO.LOW)
            GPIO.output(PP, GPIO.LOW)
            GPIO.output(SPI_CS, GPIO.LOW)
            GPIO.output(SPI_CLK, GPIO.LOW)
            GPIO.output(MOSI, GPIO.LOW)
            GPIO.output(MISO, GPIO.LOW)
            GPIO.setup(PIRQ, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
            GPIO.output(POWER_3V3, GPIO.HIGH)
            
        elif(state==1):
            GPIO.output(POWER_3V3, GPIO.LOW)
            os.system("gpio -g mode 9 alt0")
            os.system("gpio -g mode 10 alt0")
            os.system("gpio -g mode 11 alt0")
            GPIO.setup(PIRQ, GPIO.IN, pull_up_down=GPIO.PUD_UP)            
            self.spi.open(0,0)
            self.spi.mode = 0
            self.spi.lsbfirst = False
            self.spi.max_speed_hz = 20000000
            self.reset()

    
    def readRegister(self, spi_reg, size=-1):
        
        if(size==-1):
            size=spi_reg.size
        tx_buffer = bytearray([(0x80+size-1), 0xD4, ((self.locality | spi_reg.addr)>>8), (spi_reg.addr&0xFF)])
        for i in range (0,size):
            tx_buffer.append(0x00)
        rx_buffer = self.spi.xfer2(list(tx_buffer))
        spi_reg.Fill(rx_buffer[4:])
    
    def writeRegister(self, spi_reg, data):
        tx_buffer = bytearray([(0x00+len(data)-1), 0xD4, ((self.locality | spi_reg.addr)>>8), (spi_reg.addr&0xFF)])
        tx_buffer.extend(data)
        self.spi.xfer2(list(tx_buffer))
