import os, socket, time
from git import Repo

RELEASE = 'master'
SRC_DIR = os.path.dirname(__file__)

def versionDate():
    repo = Repo(SRC_DIR)
    headcommit = repo.head.commit
    return time.strftime("%Y-%m-%d %H:%M", time.gmtime(headcommit.committed_date))
    
def internetAvailable(host="8.8.8.8", port=53, timeout=2):
    """
    Host: 8.8.8.8 (google-public-dns-a.google.com)
    OpenPort: 53/tcp
    Service: domain (DNS/TCP)
    """
    try:
        socket.setdefaulttimeout(timeout)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host,port))
        return True
    except Exception as ex:
        print ex.message
        return False
    
def updateTestSuite():
    try:
        repo = Repo(SRC_DIR)
        repo.remote('origin').pull('master')
        return True
    except:
        return False
    
def newVersionAvailable():
    ret = False
    repo = Repo(SRC_DIR)

    repo.remote('origin').fetch()
    commitsBehind = list(repo.iter_commits('master..origin/master'))
    
    if len(commitsBehind)>0:
        print "update needed"
        ret = True
    else:
        print "no update needed"
    return ret

"""
if internetAvailable():
    if newVersionAvailable():
        #updateTestSuite()
        newVersionAvailable()    
#updateTestSuite()
#newVersionAvailable()
#hasInternet()
"""               
