import base64
import hashlib
import hmac
from TPM_Constants_V1 import *
from TPM_Object import Object

class Session():

    OIAP_Session = 0x01
    OSAP_Session = 0x02
    DSAP_Session = 0x04

    sSessions = []
    
    @staticmethod
    def addOIAPSession(command):
        session = Session(Session.OIAP_Session,  command.authHandle,  command.nonceEven,  bytearray(),  bytearray())
        Session.sSessions.append(session)
        return session

    @staticmethod
    def addOSAPSession(command):
        session = Session(Session.OSAP_Session,  command.authHandle,  command.nonceEven,  command.nonceEvenOSAP,  command.nonceOddOSAP)
        session.calculateSharedSecret(command.entityValue)
        Session.sSessions.append(session)
        return session

    @staticmethod
    def addDSAPSession(command):
        session = Session(Session.DSAP_Session,  command.authHandle,  command.nonceEven,  command.nonceEvenDSAP,  command.nonceOddDSAP)
        session.calculateSharedSecret(command.entityValue)
        Session.sSessions.append(session)
        return session

    @staticmethod
    def destroySession(handle):
        if(isinstance(handle, bytearray)):
            for session in Session.sSessions:
                if (session.handle==handle):
                    session.handle = bytearray([0x00, 0x00, 0x00, 0x00])
                    Session.sSessions.remove(session)
                    return

    @staticmethod
    def clearSessions():
        Session.sSessions = []

    @staticmethod
    def getSession(handle):
        for session in Session.sSessions:
            if (session.handle==handle):
                return session
        return None
    @staticmethod
    def getSharedSecret(handle):
        for session in Session.sSessions:
            if (session.handle==handle):
                return session.sharedSecret
        return bytearray()
        
    @staticmethod
    def getAuthValue(command):
        authValue = bytearray()
        key = bytearray()
        
        for session in Session.sSessions:
            if (session.handle==command.authHandle):

                inParamDigest = hashlib.sha1(command.ordinal + command.HMACParameters).digest()
                inAuthSetupParams = session.authLastNonceEven + command.nonceOdd + command.continueAuthSession

                for i in range(0, (len(command.HMACHandles)/4)):
                    if(i==0):
                        authValue.extend(Object.getAuthValue(command.HMACHandles[(i*4):(i*4+4)]))
                
                if(len(session.externAuthValue)>0):
                    key = session.externAuthValue
                elif(session.type == Session.OIAP_Session):
                    key = authValue
                else:
                    key = session.sharedSecret
                    
                pHMac = hmac.new(key, msg=inParamDigest + inAuthSetupParams,  digestmod=hashlib.sha1).hexdigest()
                return bytearray(base64.b16decode(pHMac, casefold=True))
        return bytearray()
        
    @staticmethod
    def getAuthValue2(authHandle,  ordinal,  handles,  parameters,  nonceOdd,  continueAuthSession):
        authValue = bytearray()
        key = bytearray()
        
        for session in Session.sSessions:
            if (session.handle==authHandle):

                inParamDigest = hashlib.sha1(ordinal + parameters).digest()
                inAuthSetupParams = session.authLastNonceEven + nonceOdd + continueAuthSession

                for i in range(0, (len(handles)/4)):
                    if(i==0):
                        authValue.extend(Object.getAuthValue(handles[(i*4):(i*4+4)]))
                
                if(len(session.externAuthValue)>0):
                    key = session.externAuthValue
                elif(session.type == Session.OIAP_Session):
                    key = authValue
                else:
                    key = session.sharedSecret
                    
                pHMac = hmac.new(key, msg=inParamDigest + inAuthSetupParams,  digestmod=hashlib.sha1).hexdigest()
                return bytearray(base64.b16decode(pHMac, casefold=True))
        return bytearray()

    @staticmethod
    def setNonceEven(handle,  nonceEven):
        for session in Session.sSessions:
            if (session.handle==handle):
                session.authLastNonceEven = nonceEven
                return

    def __init__(self, type,  handle,  nonceEven,  nonceEvenOSAP,  nonceOddOSAP):
        self.type = type
        self.handle = handle
        self.authLastNonceEven = nonceEven
        self.authLastNonceEvenOSAP = nonceEvenOSAP
        self.authLastNonceOddOSAP = nonceOddOSAP
        self.externAuthValue = bytearray()
        self.sharedSecret = bytearray()
        
    def calculateSharedSecret(self,  keyHandle):
        key = Object.getAuthValue(keyHandle)
        tmp_key = hmac.new(key,  msg=self.authLastNonceEvenOSAP+self.authLastNonceOddOSAP,  digestmod=hashlib.sha1).hexdigest()
        self.sharedSecret = bytearray(base64.b16decode(tmp_key, casefold=True))
    def calculateSharedSecretBySecret(self,  secret):
        tmp_key = hmac.new(secret,  msg=self.authLastNonceEvenOSAP+self.authLastNonceOddOSAP,  digestmod=hashlib.sha1).hexdigest()
        self.sharedSecret = bytearray(base64.b16decode(tmp_key, casefold=True))
    def setKey(self,  key):
        self.externAuthValue = key
