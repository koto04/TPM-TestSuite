
from TPM_Constants_V2 import *

from TPM_Session_V2 import Session
import struct
import TPM2_OracleHelper as OH
import TPM2_OracleClasses as OC
from TPM_Object import Object

from TPM2_Ticket import Ticket

HASH_STATE_EMPTY = 0
HASH_STATE_HASH = 1
HASH_STATE_HMAC = 2

MAX_COMMAND_SIZE = 0x500 #1280

def OracleResult(command):
    if command.commandCode==TPM_CC_PolicySecret: return OraclePolicySecret(command)
    if command.commandCode==TPM_CC_PolicyTicket: return OraclePolicyTicket(command)
    if command.commandCode==TPM_CC_PolicySigned: return OraclePolicySigned(command)

def OraclePolicySigned(command):
    
    session = Session.getSession(command.policySession)
    object = Object.getObject(command.authObject)
    
    if command.commandLen() > MAX_COMMAND_SIZE: return TPM_RC_COMMAND_SIZE
    
    if session == None: return TPM_RC_VALUE
    if object == None: 
        if(command.authObject[0]==0x81):
            return TPM_RC_HANDLE
        else:
            return TPM_RC_VALUE
    
    if session.mAuthHash == TPM_ALG_SHA1:
        if len(command.nonceTPM)>32: return TPM_RC_SIZE
        if (len(command.cpHashA)!=20) and (len(command.cpHashA)!=0): return TPM_RC_SIZE
        if (len(command.policyRef)>32): return TPM_RC_SIZE
    
    elif session.mAuthHash == TPM_ALG_SHA256:
        if len(command.nonceTPM)>32: return TPM_RC_SIZE
        if (len(command.cpHashA)!=32) and (len(command.cpHashA)!=0): return TPM_RC_SIZE
        if (len(command.policyRef)>32): return TPM_RC_SIZE
        
    if (len(command.expiration)<4): return TPM_RC_INSUFFICIENT
    
    exp = struct.unpack(">i",command.expiration)[0]
    expiration = (exp*-1) if exp<0 else exp
    
    if session.mSessionType != TPM_SE_TRIAL:
        if(expiration!=0):
            authTimeout = expiration + session.startTime
            result = OH.PolicyParameterChecks(session, authTimeout, command.cpHashA, command.nonceTPM)
            if result != TPM_RC_SUCCESS:
                return result
    
    if (command.authSigAlg!=TPM_ALG_HMAC): return TPM_RC_SCHEME
    
    if(command.authSignature[0:2]!=TPM_ALG_SHA) and (command.authSignature[0:2]!=TPM_ALG_SHA256): return TPM_RC_HASH
    
    
    if(command.orgAuth != command.auth) or \
        (command.orgExpiration != command.expiration) or \
        (command.orgCpHashA != command.cpHashA) or \
        (command.orgPolicyRef != command.policyRef):
        return TPM_RC_SIGNATURE
    
    return TPM_RC_SUCCESS    

def OraclePolicyTicket(command):
    
    session = Session.getSession(command.policySession)
    ticket = Ticket.getTicket(command.orgPolicyTicketDigest)

    
    if command.commandLen() > MAX_COMMAND_SIZE: return TPM_RC_COMMAND_SIZE
    if session == None: return TPM_RC_VALUE
    
    if (len(command.timeout)!=8): return TPM_RC_SIZE
    if(command.authName[0:2]==TPM_ALG_SHA256):
        if (len(command.authName)!=34): return TPM_RC_SIZE
    elif(command.authName[0:2]==TPM_ALG_SHA):
        if (len(command.authName)!=22): return TPM_RC_SIZE
    else:
        if (len(command.authName)>34): return TPM_RC_SIZE
    if session.mSessionType == TPM_SE_TRIAL: return TPM_RC_ATTRIBUTES
    if session.mAuthHash == TPM_ALG_SHA1:
        if (len(command.cpHashA)!=20) and (len(command.cpHashA)!=0): return TPM_RC_SIZE
        if (len(command.policyRef)>32): return TPM_RC_SIZE
    
    elif session.mAuthHash == TPM_ALG_SHA256:
        if (len(command.cpHashA)!=32) and (len(command.cpHashA)!=0): return TPM_RC_SIZE
        if (len(command.policyRef)>32): return TPM_RC_SIZE
    
    if command.policyTicketTag != TPM_ST_AUTH_SECRET and command.policyTicketTag != TPM_ST_AUTH_SIGNED: return TPM_RC_TAG
    
    hier = Object.getObject(command.policyTicketHierarchy)
    if(hier==None) and (hier != TPM_RH_ADMIN) and (hier != TPM_RH_ENDORSEMENT) and (hier != TPM_RH_OWNER) and (hier != TPM_RH_LOCKOUT) and (hier != TPM_RH_ADMIN):
        return TPM_RC_VALUE
    
    if len(command.policyTicketDigest)>32: return TPM_RC_SIZE
   
    timeout = OH.BYTE_ARRAY_TO_UINT64(command.timeout)
        
    result = OH.PolicyParameterChecks(session, ticket.endTime, command.cpHashA, None)
    if result != TPM_RC_SUCCESS:
        return result
    
    
    if(command.orgPolicyTicket != command.policyTicket) or \
        (command.orgPolicyTicket != command.policyTicket) or \
        (command.orgPolicyRef != command.policyRef) or \
        (command.orgAuthName != command.authName) or \
        (command.orgCpHashA != command.cpHashA) or \
        (command.orgTimeout != command.timeout):
        
        return TPM_RC_TICKET
    
    return TPM_RC_SUCCESS
        

def OraclePolicySecret(command):
    
    session = Session.getSession(command.policySession)

    if command.commandLen() > MAX_COMMAND_SIZE: return TPM_RC_COMMAND_SIZE
    if (len(command.expiration)!=4): return TPM_RC_SIZE
    
    
    if session.mAuthHash == TPM_ALG_SHA1:
        if len(command.nonceTPM2)>32: return TPM_RC_SIZE
        if (len(command.cpHashA)!=20) and (len(command.cpHashA)!=0): return TPM_RC_SIZE
        if (len(command.policyRef)>32): return TPM_RC_SIZE
    
    elif session.mAuthHash == TPM_ALG_SHA256:
        if len(command.nonceTPM2)>32: return TPM_RC_SIZE
        if (len(command.cpHashA)!=32) and (len(command.cpHashA)!=0): return TPM_RC_SIZE
        if (len(command.policyRef)>32): return TPM_RC_SIZE
        
    
    if (len(command.expiration)<4): return TPM_RC_INSUFFICIENT

    exp = struct.unpack(">i",command.expiration)[0]
    expiration = (exp*-1) if exp<0 else exp
    
    if session.mSessionType != TPM_SE_TRIAL:
        if(expiration!=0):
            authTimeout = expiration + session.startTime
            result = OH.PolicyParameterChecks(session, authTimeout, command.cpHashA, command.nonceTPM2)
            if result != TPM_RC_SUCCESS:
                return result
    
    return TPM_RC_SUCCESS      