from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5, PKCS1_PSS
from Crypto.Hash import SHA, SHA256
from base64 import b64decode, b16decode

from TPM_Constants_V2 import *
from TPM_Session_V2 import Session
import TPM2_OracleClasses as OC

import time

CONTEXT_INTEGRITY_HASH_ALG = TPM_ALG_SHA256

def BYTE_ARRAY_TO_UINT64(b):
    result = 0
    for i in range(0,8):
        if((8-i)<=len(b)):
            result = result*0x100 + b[i]
            
    return result
  
def PolicyParameterChecks(session, authTimeout, cpHashA, nonce):

    if(nonce!=None):
        if len(nonce)!=0:
            if(nonce!=session.mNonceTPM):
                return TPM_RC_NONCE
        
    
    if (authTimeout !=0):
        if(nonce!=None):
            if(len(nonce)==0): return TPM_RC_EXPIRED
        if authTimeout < time.time(): 
            return TPM_RC_EXPIRED
            

    if(len(cpHashA)!=0):
        if(len(session.mCpHashA)!=0):
            if(cpHashA!=session.mCpHashA): return TPM_RC_CPHASH
            

    return TPM_RC_SUCCESS
    
    
        
                    