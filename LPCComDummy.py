from TPM_CommunicationDummy import TPM_CommunicationDummy as TPM_Communication
from TS_Globals import TS_Globals

class LPCCom(TPM_Communication):
    
    interface_type = TS_Globals.TPM_INTERFACE_LPC
    

    def __init__(self):
        
        self.sendTimeout = 1 #1s
        self.receiveTimeout = 120 #10s
        TPM_Communication.sInterface = self
        
    def setVDD(self, state):
        if(state==0):
            print "Power OFF"
        elif(state==1):
            print "Power ON"

    def reset(self):
        pass        

    def readRegister(self, lpc_reg, size=-1):
        pass
         
    def writeRegister(self, lpc_reg, data):
        pass
                                
