
class Object():
    
    sObjects = []

    @staticmethod
    def getObject(objectHandle):
        for o in Object.sObjects:
            if (o.handle==objectHandle):
                return o
        return None
    @staticmethod
    def addObject(objectHandle,  authValue=bytearray(),  name=bytearray()):
        for o in Object.sObjects:
            if (o.handle==objectHandle):
                o.authValue = authValue
                o.name = name
                return
        Object.sObjects.append(Object(objectHandle,  authValue,  name))
    @staticmethod
    def evictObject(oldHandle,  newHandle):
        for o in Object.sObjects:
            if (o.handle==oldHandle):
                o.handle = newHandle
                return
    @staticmethod
    def loadObject(private,  objectHandle,  name=bytearray()):
        for o in Object.sObjects:
            if (o.handle==private):
                Object.sObjects.append(Object(objectHandle,  o.authValue,  name))
                return
    @staticmethod
    def destroyObject(objectHandle):
        if(isinstance(objectHandle, bytearray)):
            for o in Object.sObjects:
                if (o.handle==objectHandle):
                    Object.sObjects.remove(o)
                    return
    @staticmethod
    def getAuthValue(objectHandle):
        for o in Object.sObjects:
            if (o.handle==objectHandle):
                return o.authValue
        return bytearray()
    @staticmethod
    def setAuthValue(objectHandle, authValue):
        for o in Object.sObjects:
            if (o.handle==objectHandle):
                o.authValue = authValue
                return True
        return False
    @staticmethod
    def getAuthPolicy(objectHandle):
        for o in Object.sObjects:
            if (o.handle==objectHandle):
                return o.authPolicy
        return bytearray()
    @staticmethod
    def getName(objectHandle):
        for o in Object.sObjects:
            if (o.handle==objectHandle):
                if(len(o.name)==0):
                    return objectHandle
                else:
                    return o.name
        return objectHandle
    @staticmethod
    def setName(objectHandle,  name):
        for o in Object.sObjects:
            if (o.handle==objectHandle):
                o.name = name
                return
    @staticmethod
    def setPolicy(objectHandle,  policyDigest):
        for o in Object.sObjects:
            if (o.handle==objectHandle):
                o.authPolicy = policyDigest
                return
    @staticmethod
    def clearObjects():
        Object.sObjects = []


    def __init__(self, objectHandle,  authValue, name):
        self.handle = objectHandle
        self.authValue = authValue
        self.name = name
        self.authPolicy = bytearray()
        self.algo = bytearray()
        self.signScheme = bytearray()
        
