from random import randint
import os


from TestInterpreter import TestManagerGateway
from Utils import hex2bytearray, bytearray2hex, int2bytearray

def intrandom(imin,imax):
    return randint(imin,imax)

def int2bytes(val, num_bytes):
    x = [(val & (0xff << pos*8)) >> pos*8 for pos in range((num_bytes-1),-1,-1)]
    return bytearray(x)

def bytes(value):
    return hex2bytearray(value)

def text(data):
    if isinstance(data, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype") # @UndefinedVariable
        return
    return bytearray2hex(data)

def intValue(data):
    if isinstance(data, basestring):
        data = hex2bytearray(data)
    elif isinstance(data, bytearray):
        pass
    elif isinstance(data,( int, long )):
        return long(data)
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype") # @UndefinedVariable
        return
    ret = 0
    for i in range(0, len(data)):
        ret = (ret<<8) + data[i]
    return long(ret)

def concat(values):
    ret = bytearray()
    for v in values:
        if isinstance(v, basestring):
            v = hex2bytearray(v)
        elif isinstance(v, bytearray):
            pass
        else:
            TestManagerGateway.testManager.increaseException("Unknown datatype of data") # @UndefinedVariable
            return bytearray()
        ret.extend(v)
    return ret

def addition(value1,  value2):
    if isinstance(value1, basestring):
        value1 = hex2bytearray(value1)
    elif isinstance(value1, bytearray):
            pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown Datatype of value 1") # @UndefinedVariable
        return

    if isinstance(value2, basestring):
        value2 = hex2bytearray(value2)
    elif isinstance(value2,( int, long )):
        value2 = int2bytearray(value2, len(value1))
    elif isinstance(value2, bytearray):
            pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown Datatype of value 2") # @UndefinedVariable
        return

    if(len(value1)>len(value2)):
        maxlen = len(value1)
    else:
        maxlen = len(value2)
    
    value = 0
    for i in range(0, len(value1)):
        value = ((value<<8) + value1[i])
    value1 = value

    value = 0
    for i in range(0, len(value2)):
        value =((value<<8) + value2[i])
    value2 = value
    
    value = value1+value2
    return int2bytearray(value, maxlen)
    
def flags(values):
    ret = bytearray()
    maxlen = 0
    for v in values:
        if(len(v)>maxlen): maxlen = len(v)
    for i in range (0, maxlen): 
        ret.extend(bytearray([0x00]))
    for v in values:
        for i in range(0, len(v)):
            ret[i] |= v[i]
            
    return ret
    
def i2osp(integer, size=4):
    return ''.join([chr((integer>>(8*i))&0xFF) for i in reversed(range(size))])
def xor(a1,a2):
    if isinstance(a1, basestring):
        a1 = hex2bytearray(a1)
    elif isinstance(a1, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of a1") # @UndefinedVariable
    if isinstance(a2, basestring):
        a2 = hex2bytearray(a2)
    elif isinstance(a2, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of a2") # @UndefinedVariable
    if len(a1) != len(a2):
        TestManagerGateway.testManager.increaseException("Length of a1 is not equal length of a2") # @UndefinedVariable
    ret = bytearray(len(a1))    
    for i in range(0,len(a1)):
        ret[i] = a1[i] ^ a2[i]
    return ret

def random(size):
    ret = bytearray()
    ret.extend(os.urandom(size))
    return ret
