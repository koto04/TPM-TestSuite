
from TestInterpreter import TestManagerGateway

def setRxTimeout(timeout):
    if isinstance(timeout,( int, long )):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of timeout")  # @UndefinedVariable
        return
    
    TestManagerGateway.testManager.sCommunicationInterface.setRxTimeout(timeout) # @UndefinedVariable     

def getCommunicationStatus():
    return TestManagerGateway.testManager.communicationStatus() # @UndefinedVariable
