from GitRepositoryUtils import internetAvailable, updateTestSuite, newVersionAvailable, versionDate
from Tkinter import Tk
import socket
from tkMessageBox import askyesno, showerror, showinfo
import sys
import platform
import os


from TPM_Selector import TPM_Selector
import TestSuite
import TestManager


if platform.system() == "Windows":
    from TPM_CommunicationDummy import TPM_CommunicationDummy as TPM_Communication
else:
    from TPM_Communication import TPM_Communication

gQuit = False
    
def on_quit():
    global gQuit 
    gQuit = True
    root.destroy()

def getIP(ifname):
    ret = ""
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect('10.255.255.255',1)
        IP = s.getsockname()[0]
        ret = '(IP: ' + IP + ')'
    except Exception:
        pass
    finally:
        s.close()
    return ret

while True:
    root = Tk()
    root.wm_title('TPM Test Suite Version ' + TestSuite.__version__ +  getIP('eth0'))
    root.geometry("400x200+"+str(root.winfo_screenwidth()/2-200)+"+"+str(root.winfo_screenheight()/2-200))
    root.protocol("WM_DELETE_WINDOW", on_quit)
    if(internetAvailable()==True):
        if(newVersionAvailable()==True):
            if(askyesno('New version available', 'A new version of TPM-Testsuite is available. Start update now?')):
                if (updateTestSuite()==False):
                    showerror('Update failed', 'Errors occured during update procedure. Update not performed.')
                else:
                    showinfo('Update', 'TPM-Testsuite update completed. Application will restart now.')
                    os.execl(sys.executable, sys.executable, *sys.argv)
                    break
    selector = TPM_Selector(master=root)
    root.wait_window(selector.tpmSelector)
    
    if(False==gQuit):
        if platform.system() == "Windows":
            root.state('zoomed')
        else:
            root.attributes('-zoomed', True)
        reload(TestSuite)
        reload(TestManager)
        app = TestSuite.TPM_TestSuite(master=root)
        app.mainloop()
        TPM_Communication.cleanUp()
    break
