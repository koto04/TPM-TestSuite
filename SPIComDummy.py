from TPM_CommunicationDummy import TPM_CommunicationDummy as TPM_Communication


class SPICom(TPM_Communication):
    
    def __init__(self):
                
        self.sendTimeout = 10 #1s
        self.receiveTimeout = 120 #2 min
        TPM_Communication.sInterface = self


    def setVDD(self, state):
        if(state==0):
            print "Power OFF"
            
        elif(state==1):
            print "Power ON"
    
    def readRegister(self, spi_reg, size=-1):
        pass
    
    def writeRegister(self, spi_reg, data):
        pass
    
