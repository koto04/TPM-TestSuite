
from TestInterpreter import TestManagerGateway
from DPAFI import DPAFI

def waitDPAFI(seconds):
    
    TestManagerGateway.testManager.writeLog(text="Waiting for DPA_FI signal...",vim=True) # @UndefinedVariable
    ret = DPAFI.waitForSignal(seconds)
    if DPAFI.ComPort == None:
        TestManagerGateway.testManager.writeLog(text="DPA_FI Port not configured, waiting for signal skipped",vim=True) # @UndefinedVariable
        ret=True
    else:
        if(ret==False):
            TestManagerGateway.testManager.writeLog(text="DPA_FI signal not received",vim=True) # @UndefinedVariable
        else:
            TestManagerGateway.testManager.writeLog(text="DPA_FI signal received",vim=True) # @UndefinedVariable
    return ret

def result2DPAFI():
    if DPAFI.ComPort == None:
        TestManagerGateway.testManager.writeLog(text="DPA_FI Port not configured, last result not sent", vim=True) # @UndefinedVariable
    else:
        DPAFI.sendResponse(TestManagerGateway.testManager.lastResult()) # @UndefinedVariable
def send2DPAFI(data):
    if DPAFI.ComPort == None:
        TestManagerGateway.testManager.writeLog(text="DPA_FI Port not configured, data not sent",vim=True) # @UndefinedVariable
    else:
        DPAFI.sendResponse(data)
def setDpafiPort(port):
    if(TestManagerGateway.testManager.gui()!=None): # @UndefinedVariable
        TestManagerGateway.testManager.gui().setDpafiPort(port) # @UndefinedVariable
