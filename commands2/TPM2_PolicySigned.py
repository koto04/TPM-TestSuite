from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_PolicySigned
from Utils import bytearray2hex, int2bytearray

from random import randrange
import os
from TPM2_Ticket import Ticket

class TPM2_PolicySigned(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_PolicySigned
        
        self.authObject = self.interpretParameter(parameters[2])
        self.policySession = self.interpretParameter(parameters[3])
        self.nonceTPM = self.interpretParameter(parameters[4])
        self.cpHashA = self.interpretParameter(parameters[5])
        self.policyRef = self.interpretParameter(parameters[6])
        self.expiration = self.interpretParameter(parameters[7])
        
        self.auth = bytearray()
        self.authSigAlg = bytearray()
        self.authSignature = bytearray()

        #response
        self.timeout = bytearray()
        self.policyTicket = bytearray()
        self.policyTicketTag = bytearray()
        self.policyTicketHierarchy = bytearray()
        self.policyTicketDigest = bytearray()

        if(self.forDoc==True): return

        if(isinstance(parameters[8], list)):
            p = parameters[8]
            self.authSigAlg = self.interpretParameter(p[0]) 
            self.authSignature = self.interpretParameter(p[1])
            self.marshalAuth()
            
        else:
            self.auth = self.interpretParameter(parameters[8])
            self.unmarshalAuth()
            
    
    def unmarshalAuth(self):
        self.authSigAlg = self.auth[0:2]
        self.authSignature = self.auth[2:]
        
    def marshalAuth(self):
        self.auth = bytearray()
        self.auth.extend(self.authSigAlg)
        self.auth.extend(self.authSignature)
                
    def prepare(self):
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)+
                                                            len(self.authObject) + 
                                                            len(self.policySession) + 
                                                            2 + len(self.nonceTPM) +
                                                            2 + len(self.cpHashA) +
                                                            2 + len(self.policyRef) +
                                                            len(self.expiration) +
                                                            len(self.auth)
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.authObject)	
        self.command.extend(self.policySession)	
        self.command.extend(int2bytearray(len(self.nonceTPM), 2))
        self.command.extend(self.nonceTPM)	
        self.command.extend(int2bytearray(len(self.cpHashA), 2))
        self.command.extend(self.cpHashA)	
        self.command.extend(int2bytearray(len(self.policyRef), 2))
        self.command.extend(self.policyRef)
        self.command.extend(self.expiration) 
        self.command.extend(self.auth) 
       
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="authObject=" + bytearray2hex(self.authObject))
        self.insertTreeOutput(self.mTreeInId, 'end', text="policySession=" + bytearray2hex(self.policySession))
        self.insertTreeOutput(self.mTreeInId, 'end', text="nonceTPM=" + bytearray2hex(self.nonceTPM))
        self.insertTreeOutput(self.mTreeInId, 'end', text="cpHashA=" + bytearray2hex(self.cpHashA))
        self.insertTreeOutput(self.mTreeInId, 'end', text="policyRef=" + bytearray2hex(self.policyRef))
        self.insertTreeOutput(self.mTreeInId, 'end', text="expiration=" + bytearray2hex(self.expiration))
        
        self.mTreeInAuthId = self.insertTreeOutput(self.mTreeInId, 'end', text="auth=" + bytearray2hex(self.auth))
        self.insertTreeOutput(self.mTreeInAuthId, 'end', text="authSigAlg=" + bytearray2hex(self.authSigAlg))
        self.insertTreeOutput(self.mTreeInAuthId, 'end', text="authSignature=" + bytearray2hex(self.authSignature))

    def receive(self,  data):

        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        if(self.responseCode==bytearray([0x00,  0x00,  0x00,  0x00])):
            index=10
            
            #timeout
            if(len(data)>index):
                lenTimeout = (data[index]<<8) + data[(index+1)]
                index += 2
                self.timeout = data[(index):(index+lenTimeout)]
                index += lenTimeout
            
            #ticket
            if(len(data)>index):
                self.policyTicketTag = data[(index):(index+2)]
                self.policyTicket.extend(self.policyTicketTag)
                index += 2
            if(len(data)>index):
                self.policyTicketHierarchy = data[(index):(index+4)]
                self.policyTicket.extend(self.policyTicketHierarchy)
                index += 4
            if(len(data)>index):
                lenPolicyTicketDigest = (data[index]<<8) + data[(index+1)]
                self.policyTicket.extend(int2bytearray(lenPolicyTicketDigest, 2))
                index += 2
            if(len(data)>index):
                self.policyTicketDigest = data[(index):(index+lenPolicyTicketDigest)]
                self.policyTicket.extend(self.policyTicketDigest)
                index += lenPolicyTicketDigest
            if len(self.policyTicketDigest)>0:
                Ticket.addTicket(self.policyTicketTag, self.policyTicketHierarchy, self.policyTicketDigest, self.expiration, self.timeout)

            
            
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        self.insertTreeOutput(self.mTreeOutId, 'end', text="timeout=" + bytearray2hex(self.timeout))
        
        self.mTreeOutPolicyTicketId = self.insertTreeOutput(self.mTreeOutId, 'end', text="policyTicket=" + bytearray2hex(self.policyTicket))
        self.insertTreeOutput(self.mTreeOutPolicyTicketId, 'end', text="policyTicketTag=" + bytearray2hex(self.policyTicketTag))
        self.insertTreeOutput(self.mTreeOutPolicyTicketId, 'end', text="policyTicketHierarchy=" + bytearray2hex(self.policyTicketHierarchy))
        self.insertTreeOutput(self.mTreeOutPolicyTicketId, 'end', text="policyTicketDigest=" + bytearray2hex(self.policyTicketDigest))
            

    def fuzzy(self, level):
        self.sessionAuthValue = bytearray()
 
        self.orgAuthObject = self.authObject;
        self.orgPolicySession = self.policySession
        self.orgNonceTPM = self.nonceTPM
        self.orgCpHashA = self.cpHashA
        self.orgPolicyRef = self.policyRef
        self.orgExpiration = self.expiration
        
        self.orgAuth = self.auth
        self.orgAuthSigAlg = self.authSigAlg
        self.orgAuthSignature = self.authSignature
       
        if level==1:
            expectedResponse = self.fuzzyLevel1()
        elif level==2:
            expectedResponse = self.fuzzyLevel2()
        return expectedResponse
        
    def fuzzyLevel1(self):
        
        if(self.forDoc==True): return
        
        p = randrange(0,10)
        fuzzValue = bytearray()
        if p==0:
            self.modifiedParam = 'none'
            return 
        elif p==1: #authObject
            self.modifiedParam = 'authObject'
            fuzzValue.extend(os.urandom(len(self.authObject)))
            if self.authObject!=fuzzValue:
                self.authObject=fuzzValue
        elif p==2: #policySession
            self.modifiedParam = 'policySession'
            fuzzValue.extend(os.urandom(4))
            if self.policySession!=fuzzValue:
                self.policySession=fuzzValue
            
        elif p==3: #nonceTPM
            self.modifiedParam = 'nonceTPM'
            fuzzValue.extend(os.urandom(16))
            if self.nonceTPM!=fuzzValue:
                self.nonceTPM=fuzzValue
            
        elif p==4: #cpHashA
            self.modifiedParam = 'cpHashA'
            fuzzValue.extend(os.urandom(32))
            if self.cpHashA!=fuzzValue:
                self.cpHashA=fuzzValue
            
        elif p==5: #policyRef
            self.modifiedParam = 'policyRef'
            fuzzValue.extend(os.urandom(32))
            if self.policyRef!=fuzzValue:
                self.policyRef=fuzzValue
            
        elif p==6: #expiration
            self.modifiedParam = 'expiration'
            fuzzValue.extend(os.urandom(4))
            if self.expiration!=fuzzValue:
                self.expiration=fuzzValue
                
        elif p==7: #auth
            self.modifiedParam = 'auth'
            fuzzValue.extend(os.urandom(len(self.auth)))
            if self.auth!=fuzzValue:
                self.auth=fuzzValue
                self.unmarshalAuth()

        elif p==8: #authSigAlg
            self.modifiedParam = 'authSigAlg'
            fuzzValue.extend(os.urandom(len(self.authSigAlg)))
            if self.authSigAlg!=fuzzValue:
                self.authSigAlg=fuzzValue
                self.marshalAuth()

        elif p==9: #authSignature
            self.modifiedParam = 'authSignature'
            fuzzValue.extend(os.urandom(len(self.authSignature)))
            if self.authSignature!=fuzzValue:
                self.authSignature=fuzzValue
                self.marshalAuth()
            
    def fuzzyLevel2(self):
        if(self.forDoc==True): return
        
        p = randrange(0,5)
        fuzzValue = bytearray()
        size = randrange(0x00,self.FUZZY_MAX_PARAM_LENGTH)
        fuzzValue.extend(os.urandom(size))
        if p==0:
            self.modifiedParam = 'none'
            return
        
        elif p==1:
            self.modifiedParam = 'authObject'
            if self.authObject!=fuzzValue:
                self.authObject=fuzzValue

        elif p==2:
            self.modifiedParam = 'policySession'
            if self.policySession!=fuzzValue:
                self.policySession=fuzzValue
            
        elif p==3:
            self.modifiedParam = 'nonceTPM'
            if self.nonceTPM!=fuzzValue:
                self.nonceTPM=fuzzValue
            
        elif p==4:
            self.modifiedParam = 'cpHashA'
            if self.cpHashA!=fuzzValue:
                self.cpHashA=fuzzValue
            
        elif p==5:
            self.modifiedParam = 'policyRef'
            if self.policyRef!=fuzzValue:
                self.policyRef=fuzzValue
            
        elif p==6:
            self.modifiedParam = 'expiration'
            if self.expiration!=fuzzValue:
                self.expiration=fuzzValue
                
        elif p==7:
            self.modifiedParam = 'auth'
            if self.auth!=fuzzValue:
                self.auth=fuzzValue
                self.unmarshalAuth()

        elif p==8:
            self.modifiedParam = 'authSigAlg'
            if self.authSigAlg!=fuzzValue:
                self.authSigAlg=fuzzValue
                self.marshalAuth()

        elif p==9:
            self.modifiedParam = 'authSignature'
            if self.authSignature!=fuzzValue:
                self.authSignature=fuzzValue
                self.marshalAuth()
