from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_StartAuthSession, TPM_RC_SUCCESS
from Utils import bytearray2hex, int2bytearray
from TPM_Session_V2 import Session

class TPM2_StartAuthSession(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_StartAuthSession
        self.tpmKey = self.interpretParameter(parameters[2])
        self.bind = self.interpretParameter(parameters[3])
        self.nonceCaller = self.interpretParameter(parameters[4])

        self.sessionType = self.interpretParameter(parameters[6])
        self.symmetric = self.interpretParameter(parameters[7])
        self.authHash = self.interpretParameter(parameters[8])
        
        self.encryptedSalt = bytearray()
        self.salt = bytearray()

        #response
        self.nonceTPM =bytearray()
        self.sessionHandle = bytearray()

        if(self.forDoc==True): return

        if isinstance(parameters[5], list):
            self.salt = self.interpretParameter(parameters[5][0])
            self.encryptedSalt = self.interpretParameter(parameters[5][1])
        else:
            self.encryptedSalt = self.interpretParameter(parameters[5])
            
            
    def prepare(self):
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)+len(self.tpmKey)+len(self.bind)+2+len(self.nonceCaller)+2+len(self.encryptedSalt)+len(self.sessionType)+len(self.symmetric)+len(self.authHash)),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.tpmKey)		
        self.command.extend(self.bind)	
        strlen = len(self.nonceCaller)
        self.command.extend(int2bytearray(strlen, 2))
        self.command.extend(self.nonceCaller)	
        self.command.extend(int2bytearray(len(self.encryptedSalt), 2))
        self.command.extend(self.encryptedSalt)	
        self.command.extend(self.sessionType)	
        self.command.extend(self.symmetric)	
        self.command.extend(self.authHash)	
         
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="tpmKey=" + bytearray2hex(self.tpmKey))
        self.insertTreeOutput(self.mTreeInId, 'end', text="bind=" + bytearray2hex(self.bind))
        self.insertTreeOutput(self.mTreeInId, 'end', text="nonceCaller=" + bytearray2hex(self.nonceCaller))
        self.insertTreeOutput(self.mTreeInId, 'end', text="encryptedSalt=" + bytearray2hex(self.encryptedSalt))
        self.insertTreeOutput(self.mTreeInId, 'end', text="sessionType=" + bytearray2hex(self.sessionType))
        self.insertTreeOutput(self.mTreeInId, 'end', text="symmetric=" + bytearray2hex(self.symmetric))
        self.insertTreeOutput(self.mTreeInId, 'end', text="authHash=" + bytearray2hex(self.authHash))
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        if(len(data)>=14):
            self.sessionHandle = data[10:14]
        else:
            self.sessionHandle = bytearray()
        if(len(data)>=16):
            lenNonceTPM = (data[14]<<8) + data[15]
            self.nonceTPM = data[16:(16+lenNonceTPM)]
        else:
            self.nonceTPM =bytearray()
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        self.insertTreeOutput(self.mTreeOutId, 'end', text="sessionHandle=" + bytearray2hex(self.sessionHandle))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="nonceTPM=" + bytearray2hex(self.nonceTPM))
        if(self.responseCode==TPM_RC_SUCCESS):
            Session.addSession(self)
