from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_Sign, TPM_RC_SUCCESS
from Utils import bytearray2hex, int2bytearray
from TPM_Session_V2 import Session

class TPM2_Sign(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_Sign
        
        self.keyHandle = self.interpretParameter(parameters[2])
        self.sessionHandle = self.interpretParameter(parameters[3])
        self.nonceCaller = self.interpretParameter(parameters[4])
        self.sessionAttribute = self.interpretParameter(parameters[5])
        self.digest = self.interpretParameter(parameters[6])
        self.inScheme = self.interpretParameter(parameters[7])
        self.validation = bytearray()
        self.validationTag = bytearray()
        self.validationHierarchy = bytearray()
        self.validationDigest = bytearray()
         
        self.sessionAuthValue = bytearray()

        #response
        self.signature = bytearray()
        self.signatureScheme = bytearray()
        self.signatureValue = bytearray()

        if(self.forDoc==True): return

        if(isinstance(parameters[8], list)):
            
            p = parameters[8]
            self.validationTag = self.interpretParameter(p[0])
            self.validationHierarchy = self.interpretParameter(p[1])
            self.validationDigest = self.interpretParameter(p[2])

            self.validation.extend(self.validationTag)
            self.validation.extend(self.validationHierarchy)
            self.validation.extend(int2bytearray(len(self.validationDigest), 2))
            self.validation.extend(self.validationDigest)
            
        else:
            self.validation = self.interpretParameter(parameters[8])
            index = 0
            
            self.validationTag = self.validation[index:(index+2)]
            index+=2
            
            self.validationHierarchy = self.validation[index:(index+4)]
            index+=4
            
            lenValidationDigest = (self.validation[index]<<8) + (self.validation[(index+1)])
            index+=2
            self.validationDigest = self.validation[index:(index+lenValidationDigest)]
        
                
        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.keyHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(int2bytearray(len(self.digest), 2))
        self.HMACParameters.extend(self.digest)	
        self.HMACParameters.extend(self.inScheme)	
        self.HMACParameters.extend(self.validation)	
        
        self.sessionAuthValue = Session.getAuthValue(self)

        
    def prepare(self):
        
        lenAuth =  len(self.sessionHandle) + 2 + len(self.nonceCaller) + len(self.sessionAttribute) + 2 + len(self.sessionAuthValue)        
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)+len(self.keyHandle) + 
                                                            4 + lenAuth + 
                                                            2 + len(self.digest) + 
                                                            len(self.inScheme) + 
                                                            len(self.validation)
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.keyHandle)	
        self.command.extend(int2bytearray(lenAuth, 4))
        self.command.extend(self.sessionHandle)	
        self.command.extend(int2bytearray(len(self.nonceCaller), 2))
        self.command.extend(self.nonceCaller)	
        self.command.extend(self.sessionAttribute)	
        
        self.command.extend(int2bytearray(len(self.sessionAuthValue), 2))
        self.command.extend(self.sessionAuthValue)	
        
        self.command.extend(int2bytearray(len(self.digest), 2))
        self.command.extend(self.digest)	
        self.command.extend(self.inScheme)	
        self.command.extend(self.validation)	
               
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="keyHandle=" + bytearray2hex(self.keyHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="sessionHandle=" + bytearray2hex(self.sessionHandle))

        self.inTreeAuthorization()

        self.insertTreeOutput(self.mTreeInId, 'end', text="digest=" + bytearray2hex(self.digest))
        self.insertTreeOutput(self.mTreeInId, 'end', text="inScheme=" + bytearray2hex(self.inScheme))

        self.mTreeInValidationId = self.insertTreeOutput(self.mTreeInId, 'end', text="validation=" + bytearray2hex(self.validation))
        self.insertTreeOutput(self.mTreeInValidationId, 'end', text="validationTag=" + bytearray2hex(self.validationTag))        
        self.insertTreeOutput(self.mTreeInValidationId, 'end', text="validationHierarchy=" + bytearray2hex(self.validationHierarchy))
        self.insertTreeOutput(self.mTreeInValidationId, 'end', text="validationDigest=" + bytearray2hex(self.validationDigest))
                
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')

        index = 10
        
        if(self.responseCode==TPM_RC_SUCCESS):
        
            if(len(data)>index):
                lenSignature = (data[index]<<24) + (data[(index+1)]<<16) + (data[index+2]<<8) + data[(index+3)]
                index +=4
                self.signature = data[index:(index+lenSignature)]
                self.signatureScheme = self.signature[0:4]
                self.signatureValue = self.signature[4:]
                index += lenSignature
                       
        self.mTreeOutSignatureId = self.insertTreeOutput(self.mTreeOutId, 'end', text="signature=" + bytearray2hex(self.signature))
        self.insertTreeOutput(self.mTreeOutSignatureId, 'end', text="signatureScheme=" + bytearray2hex(self.signatureScheme))
        self.insertTreeOutput(self.mTreeOutSignatureId, 'end', text="signatureValue=" + bytearray2hex(self.signatureValue))

        self.outTreeAuthorization(data, index)

