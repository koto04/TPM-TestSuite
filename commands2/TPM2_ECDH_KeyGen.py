from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_ECDH_KeyGen, TPM_RC_SUCCESS
from Utils import bytearray2hex, int2bytearray


class TPM2_ECDH_KeyGen(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_ECDH_KeyGen
        self.keyHandle = self.interpretParameter(parameters[2])
    
        #response
        self.zPoint = bytearray()
        self.zPointX = bytearray()
        self.zPointY = bytearray()
        self.pubPoint = bytearray()
        self.pubPointX = bytearray()
        self.pubPointY = bytearray()

        if(self.forDoc==True): return

    def prepare(self):
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)+len(self.keyHandle)),4)
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.keyHandle)		
        

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="keyHandle=" + bytearray2hex(self.keyHandle))
        
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
            
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        if(self.responseCode!=TPM_RC_SUCCESS):
            return

        index = 10
        if(self.responseCode==TPM_RC_SUCCESS):
        
            lenZPoint= (data[index]<<8) + data[(index+1)]
            index +=2      
            if (lenZPoint>0):      
                lenZPointX= (data[index]<<8) + data[(index+1)]
                index +=2
                if(lenZPointX>0):
                    self.zPointX = data[(index):(index+lenZPointX)]
                    index += lenZPointX
                lenZPointY= (data[index]<<8) + data[(index+1)]
                index +=2
                if(lenZPointY>0):
                    self.zPointY = data[(index):(index+lenZPointY)]
                    index += lenZPointY
            
            self.zPoint.extend(int2bytearray(lenZPointX, 2))
            self.zPoint.extend(self.zPointX)
            self.zPoint.extend(int2bytearray(lenZPointY, 2))
            self.zPoint.extend(self.zPointY)
            
            lenPubPoint= (data[index]<<8) + data[(index+1)]
            index +=2      
            if (lenPubPoint>0):      
                lenPubPointX= (data[index]<<8) + data[(index+1)]
                index +=2
                if(lenPubPointX>0):
                    self.pubPointX = data[(index):(index+lenPubPointX)]
                    index += lenPubPointX
                lenPubPointY= (data[index]<<8) + data[(index+1)]
                index +=2
                if(lenPubPointY>0):
                    self.pubPointY = data[(index):(index+lenPubPointY)]
                    index += lenPubPointY
            
            self.pubPoint.extend(int2bytearray(lenPubPointX, 2))
            self.pubPoint.extend(self.pubPointX)
            self.pubPoint.extend(int2bytearray(lenPubPointY, 2))
            self.pubPoint.extend(self.pubPointY)
            
        
        self.mTreeZPointId =self.insertTreeOutput(self.mTreeOutId, 'end', text="zPoint=" + bytearray2hex(self.zPoint))
        self.insertTreeOutput(self.mTreeZPointId, 'end', text="zPointX=" + bytearray2hex(self.zPointX))
        self.insertTreeOutput(self.mTreeZPointId, 'end', text="zPointY=" + bytearray2hex(self.zPointY))

        self.mTreePubPointId =self.insertTreeOutput(self.mTreeOutId, 'end', text="pubPoint=" + bytearray2hex(self.pubPoint))
        self.insertTreeOutput(self.mTreePubPointId, 'end', text="pubPointX=" + bytearray2hex(self.pubPointX))
        self.insertTreeOutput(self.mTreePubPointId, 'end', text="pubPointY=" + bytearray2hex(self.pubPointY))
        
