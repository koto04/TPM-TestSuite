
from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CAP_TPM_PROPERTIES, TPM_CC_GetCapability, TPM_ST_NO_SESSIONS, TPM_PT_FAMILY_INDICATOR
from Utils import int2bytearray, bytearray2int

class ChipInfo(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.tag = TPM_ST_NO_SESSIONS
        self.command = bytearray()
        self.commandCode = TPM_CC_GetCapability
        self.capability = TPM_CAP_TPM_PROPERTIES
        self.property = TPM_PT_FAMILY_INDICATOR
        self.propertyCount = bytearray([0x00, 0x00, 0x01, 0x00])
        self.logEnable=False
                
        self.infoFamilyIndicator = ""
        self.infoLevel = ""
        self.infoRevision = ""
        self.infoDayOfYear = ""
        self.infoYear = ""
        self.infoManufacturer = ""
        self.infoVendorString = ""
        self.infoVersion = ""

        
        #response
        self.moreData = bytearray()
        self.capabilityData = bytearray()
        self.capabilityDataCapability = bytearray()
        self.capabilityDataData = bytearray()
        self.capabilities = []

        if(self.forDoc==True): return

    
    def prepare(self):
               
        self.commandSize = int2bytearray((len(self.tag)+len(self.commandCode)
                                                            + 4 + len(self.capability)
                                                            + len(self.property)
                                                            + len(self.propertyCount)),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.capability)	
        self.command.extend(self.property)	
        self.command.extend(self.propertyCount)	
            
    def receive(self,  data):
        if(len(data)<10):
            self.writeLog("Unexpected response length for chip information.")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        index=10
        if(len(data)>index):
            self.moreData = data[index:(index+1)]
            index+=1
        if(len(data)>index):
            self.capabilityDataCapability = data[index:(index+4)]
            self.capabilityData.extend(self.capabilityDataCapability)
            index += 4
        if(len(data)>index):
            self.capabilityDataData = data[index:]
            self.capabilityData.extend(self.capabilityDataData)
            dataLen = (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + data[(index+3)]
            index +=4
            for i in range(0, dataLen):
                self.capabilities.append(data[(index + i*8):(index + i*8+8)])
            vs = bytearray()
            vs.extend(self.capabilities[6][4:])
            vs.extend(self.capabilities[7][4:])
            vs.extend(self.capabilities[8][4:])
            vs.extend(self.capabilities[9][4:])
            
            self.infoFamilyIndicator = str(self.capabilities[0][4:]).strip(chr(0x00))
            self.infoLevel = str(bytearray2int(self.capabilities[1][4:]))
            self.infoRevision = str(bytearray2int(self.capabilities[2][4:]))
            self.infoDayOfYear = str(bytearray2int(self.capabilities[3][4:]))
            self.infoYear = str(bytearray2int(self.capabilities[4][4:]))
            self.infoManufacturer = str(self.capabilities[5][4:]).strip(chr(0x00))
            self.infoVendorString = str(vs).strip(chr(0x00))
            self.infoVersion = (str(bytearray2int(self.capabilities[11][4:6])) + "." + str(bytearray2int(self.capabilities[11][6:])) + "." + str(bytearray2int(self.capabilities[12][4:7])) + "." + str(bytearray2int(self.capabilities[12][7:]))).strip(chr(0x00))

    def longChipInfo(self):
        ret = ""
        ret += "FAMILY_INDICATOR: " + self.infoFamilyIndicator + "\n"
        ret += "LEVEL: " + self.infoLevel + "\n"
        ret += "REVISION: " + self.infoRevision + "\n"
        ret += "DAY_OF_YEAR: " + self.infoDayOfYear + "\n"
        ret += "YEAR: " + self.infoYear + "\n"
        ret += "MANUFACTURER: " + self.infoManufacturer + "\n"
        ret += "VENDOR_STRING: " + self.infoVendorString + "\n"
        ret += "VERSION: " + self.infoVersion + "\n"
        return ret    
    
    def shortChipInfo(self):
        ret = self.infoManufacturer + " " + self.infoVendorString + ' v' + self.infoVersion
        return ret
                  
    def pathSubString(self):
        ret = self.infoFamilyIndicator + "_" + self.infoManufacturer + "_" + self.infoVendorString + '_v' + self.infoVersion
        return ret
