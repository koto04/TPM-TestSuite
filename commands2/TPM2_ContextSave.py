from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_ContextSave
from Utils import bytearray2hex, int2bytearray

class TPM2_ContextSave(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_ContextSave
        
        self.saveHandle = self.interpretParameter(parameters[2])
        
        #response
        self.context  = bytearray()
        self.contextSequence = bytearray()
        self.contextSavedHandle = bytearray()
        self.contextHierarchy = bytearray()
        self.contextContextBlob = bytearray()

        if(self.forDoc==True): return
        
    def prepare(self):
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag) + len(self.saveHandle)),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.saveHandle)	
       
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="saveHandle=" + bytearray2hex(self.saveHandle))
            
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        index = 10
        
        if(len(data)>index):
            self.context = data[index:]
            self.contextSequence = data[index:(index+8)]
            index += 8
            
        if(len(data)>index):
            self.contextSavedHandle = data[index:(index+4)]
            index += 4

        if(len(data)>index):
            self.contextHierarchy = data[index:(index+4)]
            index += 4                    
            
        if(len(data)>index):
            lenData = (data[index]<<8) + (data[(index+1)])
            index +=2
            self.contextContextBlob = data[index:(index+lenData)]
        
            
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
        self.mTreeOutContextId = self.insertTreeOutput(self.mTreeOutId, 'end', text="context=" + bytearray2hex(self.context))
        self.insertTreeOutput(self.mTreeOutContextId, 'end', text="contextSequence=" + bytearray2hex(self.contextSequence))
        self.insertTreeOutput(self.mTreeOutContextId, 'end', text="contextSavedHandle=" + bytearray2hex(self.contextSavedHandle))
        self.insertTreeOutput(self.mTreeOutContextId, 'end', text="contextHierarchy=" + bytearray2hex(self.contextHierarchy))
        self.insertTreeOutput(self.mTreeOutContextId, 'end', text="contextContextBlob=" + bytearray2hex(self.contextContextBlob))
        
