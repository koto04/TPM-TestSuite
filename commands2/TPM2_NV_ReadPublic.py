from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_NV_ReadPublic, TPM_RC_SUCCESS
from Utils import bytearray2hex, int2bytearray
from TPM_Object import Object

class TPM2_NV_ReadPublic(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_NV_ReadPublic
        
        self.nvIndex = self.interpretParameter(parameters[2])
        
        #response
        self.nvPublic  = bytearray()
        self.nvPublicNvIndex = bytearray()
        self.nvPublicNameAlg = bytearray()
        self.nvPublicAttributes = bytearray()
        self.nvPublicAuthPolicy = bytearray()
        self.nvPublicDataSize = bytearray()
        self.nvName = bytearray()

        if(self.forDoc==True): return
        
    def prepare(self):
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag) + len(self.nvIndex)),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.nvIndex)	
       
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="nvIndex=" + bytearray2hex(self.nvIndex))
            
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        index = 10
        
        if(len(data)>index):
            lenNvPublic = (data[index]<<8) + (data[(index+1)])
            self.nvPublic = data[index:(index+lenNvPublic)]
            
            index +=2
            self.nvPublicNvIndex = data[index:(index+4)]
            index +=4
            self.nvPublicNameAlg = data[index:(index+2)]
            index +=2
            self.nvPublicAttributes = data[index:(index+4)]
            index +=4
            lenAuthPolicy = (data[index]<<8) + (data[(index+1)])
            index +=2
            self.nvPublicAuthPolicy = data[index:(index+lenAuthPolicy)]
            index += lenAuthPolicy
            self.nvPublicDataSize = data[index:(index+2)]
            index +=2
                    
            
        if(len(data)>index):
            lenNvName = (data[index]<<8) + (data[(index+1)])
            index +=2
            self.nvName = data[index:(index+lenNvName)]
        
            
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
        self.mTreeOutNvPublicId = self.insertTreeOutput(self.mTreeOutId, 'end', text="nvPublic=" + bytearray2hex(self.nvPublic))
        self.insertTreeOutput(self.mTreeOutNvPublicId, 'end', text="nvPublicNvIndex=" + bytearray2hex(self.nvPublicNvIndex))
        self.insertTreeOutput(self.mTreeOutNvPublicId, 'end', text="nvPublicNameAlg=" + bytearray2hex(self.nvPublicNameAlg), tag='TPM_ALG_ID')
        self.insertTreeOutput(self.mTreeOutNvPublicId, 'end', text="nvPublicAttributes=" + bytearray2hex(self.nvPublicAttributes), tag='TPMA_NV')
        self.insertTreeOutput(self.mTreeOutNvPublicId, 'end', text="nvPublicAuthPolicy=" + bytearray2hex(self.nvPublicAuthPolicy))
        self.insertTreeOutput(self.mTreeOutNvPublicId, 'end', text="nvPublicDataSize=" + bytearray2hex(self.nvPublicDataSize))
          
        self.insertTreeOutput(self.mTreeOutId, 'end', text="nvName=" + bytearray2hex(self.nvName))

        if(self.responseCode==TPM_RC_SUCCESS):
            Object.setName(self.nvIndex, self.nvName)
