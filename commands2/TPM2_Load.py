from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_Load, TPM_RC_SUCCESS, TPM_ALG_ECC
from Utils import bytearray2hex, int2bytearray
from TPM_Session_V2 import Session
from TPM_Object import Object

class TPM2_Load(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_Load
        
        self.parentHandle = self.interpretParameter(parameters[2])
        self.sessionHandle = self.interpretParameter(parameters[3])
        self.nonceCaller = self.interpretParameter(parameters[4])
        self.sessionAttribute = self.interpretParameter(parameters[5])
        self.inPrivate = self.interpretParameter(parameters[6])
        self.parentName = bytearray()
        
        self.inPublicType = bytearray()
        self.inPublicNameAlg = bytearray()
        self.inPublicObjectAttributes = bytearray()
        self.inPublicAuthPolicy = bytearray()
        self.inPublicParameters = bytearray()
        self.inPublicUnique = bytearray()
        self.inPublicUnique2 = bytearray()

        self.sessionAuthValue = bytearray()
                
        #response
        self.objectHandle = bytearray()
        self.name = bytearray()       

        if(self.forDoc==True): return

        self.interpretInTPM2B_PUBLIC(parameters[7])
                    
        self.parentName = Object.getName(self.parentHandle)


        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.parentHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(int2bytearray(len(self.inPrivate), 2))
        self.HMACParameters.extend(self.inPrivate)	
        self.HMACParameters.extend(int2bytearray(len(self.inPublic), 2))
        self.HMACParameters.extend(self.inPublic)	

        self.sessionAuthValue = Session.getAuthValue(self)
        
        
    def prepare(self):
                
        lenAuth =  len(self.sessionHandle) + 2 + len(self.nonceCaller) + len(self.sessionAttribute) + 2 + len(self.sessionAuthValue)
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag) 
                                                            + len(self.parentHandle) 
                                                            + 4+ lenAuth 
                                                            + 2 + len(self.inPrivate) 
                                                            + 2+ len(self.inPublic) 
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.parentHandle)	
        self.command.extend(int2bytearray(lenAuth, 4))
        self.command.extend(self.sessionHandle)	
        self.command.extend(int2bytearray(len(self.nonceCaller), 2))
        self.command.extend(self.nonceCaller)	
        self.command.extend(self.sessionAttribute)	
        self.command.extend(int2bytearray(len(self.sessionAuthValue), 2))
        self.command.extend(self.sessionAuthValue)	
        self.command.extend(int2bytearray(len(self.inPrivate), 2))
        self.command.extend(self.inPrivate)	
        self.command.extend(int2bytearray(len(self.inPublic), 2))
        self.command.extend(self.inPublic)	
       
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="parentHandle=" + bytearray2hex(self.parentHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="sessionHandle=" + bytearray2hex(self.sessionHandle))

        self.inTreeAuthorization()
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="inPrivate=" + bytearray2hex(self.inPrivate))
        
        self.mTreeInPublicId = self.insertTreeOutput(self.mTreeInId, 'end', text="inPublic=" + bytearray2hex(self.inPublic))
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicType=" + bytearray2hex(self.inPublicType), tag='TPM_ALG_ID')
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicNameAlg=" + bytearray2hex(self.inPublicNameAlg), tag='TPM_ALG_ID')
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicObjectAttributes=" + bytearray2hex(self.inPublicObjectAttributes), tag='TPMA_OBJECT')
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicAuthPolicy=" + bytearray2hex(self.inPublicAuthPolicy))
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicParameters=" + bytearray2hex(self.inPublicParameters))
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicUnique=" + bytearray2hex(self.inPublicUnique))
        if(self.inPublicType==TPM_ALG_ECC):
            self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicUnique2=" + bytearray2hex(self.inPublicUnique2))
                
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
        if(self.responseCode!=TPM_RC_SUCCESS):
            return
        
        index=10
        self.objectHandle = data[index:(index+4)]
        index +=4
        
        index +=4 #Parameter-length
        
        lenName = (data[index]<<8) + data[(index+1)]
        index +=2
        self.name = data[index:(index+lenName)]
        index += lenName
                
        self.insertTreeOutput(self.mTreeOutId, 'end', text="objectHandle=" + bytearray2hex(self.objectHandle))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="name=" + bytearray2hex(self.name))
        
        self.outTreeAuthorization(data, index)
        
        if(self.responseCode==TPM_RC_SUCCESS):
            Object.loadObject(self.inPrivate,  self.objectHandle,  self.name)
