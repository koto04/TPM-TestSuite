from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_PolicySecret
from Utils import bytearray2hex, int2bytearray
from TPM_Session_V2 import Session

from random import randrange
import os
from TPM2_Ticket import Ticket

class TPM2_PolicySecret(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_PolicySecret
        
        self.authHandle = self.interpretParameter(parameters[2])
        self.policySession = self.interpretParameter(parameters[3])
        self.sessionHandle = self.interpretParameter(parameters[4])
        self.nonceCaller = self.interpretParameter(parameters[5])
        self.sessionAttribute = self.interpretParameter(parameters[6])
        self.nonceTPM = self.interpretParameter(parameters[7])
        self.nonceTPM2 = self.interpretParameter(parameters[7])
        self.cpHashA = self.interpretParameter(parameters[8])
        self.policyRef = self.interpretParameter(parameters[9])
        self.expiration = self.interpretParameter(parameters[10])
        
        self.sessionAuthValue = bytearray()
                
        #response
        self.timeout = bytearray()
        self.policyTicket = bytearray()
        self.policyTicketTag = bytearray()
        self.policyTicketHierarchy = bytearray()
        self.policyTicketDigest = bytearray()

        if(self.forDoc==True): return

        self.calcAuthValue()
        
    def calcAuthValue(self):
        
        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.authHandle)
        self.HMACHandles.extend(self.policySession)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(int2bytearray(len(self.nonceTPM2), 2))
        self.HMACParameters.extend(self.nonceTPM2)	
        self.HMACParameters.extend(int2bytearray(len(self.cpHashA), 2))
        self.HMACParameters.extend(self.cpHashA)	
        self.HMACParameters.extend(int2bytearray(len(self.policyRef), 2))
        self.HMACParameters.extend(self.policyRef)	
        self.HMACParameters.extend(self.expiration)	
        
        self.sessionAuthValue = bytearray()
        self.sessionAuthValue = Session.getAuthValue(self)
            
                        
    def prepare(self):
        
        if (len(self.sessionAuthValue)==0): self.calcAuthValue()

        lenAuth =  len(self.sessionHandle) + 2 + len(self.nonceCaller) + len(self.sessionAttribute) + 2 + len(self.sessionAuthValue)        
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)+
                                                            len(self.authHandle) + 
                                                            len(self.policySession) + 
                                                            4+ lenAuth + 
                                                            2 + len(self.nonceTPM2) +
                                                            2 + len(self.cpHashA) +
                                                            2 + len(self.policyRef) +
                                                            len(self.expiration)
                                                        ),4)
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.authHandle)	
        self.command.extend(self.policySession)	
        self.command.extend(int2bytearray(lenAuth, 4))
        self.command.extend(self.sessionHandle)	
        self.command.extend(int2bytearray(len(self.nonceCaller), 2))
        self.command.extend(self.nonceCaller)	
        self.command.extend(self.sessionAttribute)	
        self.command.extend(int2bytearray(len(self.sessionAuthValue), 2))
        self.command.extend(self.sessionAuthValue)	
        self.command.extend(int2bytearray(len(self.nonceTPM2), 2))
        self.command.extend(self.nonceTPM2)	
        self.command.extend(int2bytearray(len(self.cpHashA), 2))
        self.command.extend(self.cpHashA)	
        self.command.extend(int2bytearray(len(self.policyRef), 2))
        self.command.extend(self.policyRef)
        self.command.extend(self.expiration)
       
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="authHandle=" + bytearray2hex(self.authHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="policySession=" + bytearray2hex(self.policySession))
        self.insertTreeOutput(self.mTreeInId, 'end', text="sessionHandle=" + bytearray2hex(self.sessionHandle))
        
        self.inTreeAuthorization()

        self.insertTreeOutput(self.mTreeInId, 'end', text="nonceTPM=" + bytearray2hex(self.nonceTPM2))
        self.insertTreeOutput(self.mTreeInId, 'end', text="cpHashA=" + bytearray2hex(self.cpHashA))
        self.insertTreeOutput(self.mTreeInId, 'end', text="policyRef=" + bytearray2hex(self.policyRef))
        self.insertTreeOutput(self.mTreeInId, 'end', text="expiration=" + bytearray2hex(self.expiration))

            
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        index=14
        
        if(self.responseCode==bytearray([0x00,  0x00,  0x00,  0x00])):
            #timeout
            if(len(data)>index):
                lenTimeout = (data[index]<<8) + data[(index+1)]
                index += 2
                self.timeout = data[(index):(index+lenTimeout)]
                index += lenTimeout
            
            #ticket
            if(len(data)>index):
                self.policyTicketTag = data[(index):(index+2)]
                self.policyTicket.extend(self.policyTicketTag)
                index += 2
            if(len(data)>index):
                self.policyTicketHierarchy = data[(index):(index+4)]
                self.policyTicket.extend(self.policyTicketHierarchy)
                index += 4
            if(len(data)>index):
                lenPolicyTicketDigest = (data[index]<<8) + data[(index+1)]
                self.policyTicket.extend(int2bytearray(lenPolicyTicketDigest, 2))
                index += 2
            if(len(data)>index):
                self.policyTicketDigest = data[(index):(index+lenPolicyTicketDigest)]
                self.policyTicket.extend(self.policyTicketDigest)
                index += lenPolicyTicketDigest
                
                if(len(self.policyTicketDigest)>0):
                    Ticket.addTicket(self.policyTicketTag, self.policyTicketHierarchy, self.policyTicketDigest, self.expiration, self.timeout)
                        
            
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        self.insertTreeOutput(self.mTreeOutId, 'end', text="timeout=" + bytearray2hex(self.timeout))
        
        self.mTreeOutPolicyTicketId = self.insertTreeOutput(self.mTreeOutId, 'end', text="policyTicket=" + bytearray2hex(self.policyTicket))
        self.insertTreeOutput(self.mTreeOutPolicyTicketId, 'end', text="policyTicketTag=" + bytearray2hex(self.policyTicketTag))
        self.insertTreeOutput(self.mTreeOutPolicyTicketId, 'end', text="policyTicketHierarchy=" + bytearray2hex(self.policyTicketHierarchy))
        self.insertTreeOutput(self.mTreeOutPolicyTicketId, 'end', text="policyTicketDigest=" + bytearray2hex(self.policyTicketDigest))
            
        self.outTreeAuthorization(data, index)
        
    def fuzzy(self, level):
        self.sessionAuthValue = bytearray()
        
        if level==1:
            expectedResponse = self.fuzzyLevel1()
        elif level==2:
            expectedResponse = self.fuzzyLevel2()
        return expectedResponse
        
    def fuzzyLevel1(self):
        
        if(self.forDoc==True): return
        
        p = randrange(0,5)
        fuzzValue = bytearray()

        if p==0:
            self.modifiedParam = 'none'
            return 
        elif p==1:
            self.modifiedParam = 'nonceTPM'
            fuzzValue.extend(os.urandom(16))
            if self.nonceTPM2!=fuzzValue:
                self.nonceTPM2=fuzzValue
            
        elif p==2:
            self.modifiedParam = 'cpHashA'
            fuzzValue.extend(os.urandom(32))
            if self.cpHashA!=fuzzValue:
                self.cpHashA=fuzzValue
            
        elif p==3:
            self.modifiedParam = 'policyRef'
            fuzzValue.extend(os.urandom(32))
            if self.policyRef!=fuzzValue:
                self.policyRef=fuzzValue
            
        elif p==4:
            self.modifiedParam = 'expiration'
            fuzzValue.extend(os.urandom(4))
            if self.expiration!=fuzzValue:
                self.expiration=fuzzValue
            
    def fuzzyLevel2(self):
        if(self.forDoc==True): return
        
        p = randrange(0,5)
        fuzzValue = bytearray()
        size = randrange(0x00,self.FUZZY_MAX_PARAM_LENGTH)
        fuzzValue.extend(os.urandom(size))

        if p==0:
            self.modifiedParam = 'none'
            return
        elif p==1:
            self.modifiedParam = 'nonceTPM'
            if self.nonceTPM2!=fuzzValue:
                self.nonceTPM2=fuzzValue
            
        elif p==2:
            self.modifiedParam = 'cpHashA'
            if self.cpHashA!=fuzzValue:
                self.cpHashA=fuzzValue
            
        elif p==3:
            self.modifiedParam = 'policyRef'
            if self.policyRef!=fuzzValue:
                self.policyRef=fuzzValue
            
        elif p==4:
            self.modifiedParam = 'expiration'
            if self.expiration!=fuzzValue:
                self.expiration=fuzzValue
        
