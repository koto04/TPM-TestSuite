from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_LoadExternal, TPM_RC_SUCCESS, TPM_ALG_ECC
from Utils import bytearray2hex, int2bytearray
from TPM_Object import Object

class TPM2_LoadExternal(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_LoadExternal
        
        self.inPrivate = bytearray()
        self.inPrivateSensitiveType = bytearray()
        self.inPrivateAuthValue = bytearray()
        self.inPrivateSeedValue = bytearray()
        self.inPrivateSensitive = bytearray()
        
        self.hierarchy = bytearray()
        
        
        
        if(isinstance(parameters[2], list)): #inPrivate
            p = parameters[2]
            self.inPrivateSensitiveType = self.interpretParameter(p[0])
            self.inPrivateAuthValue = self.interpretParameter(p[1])
            self.inPrivateSeedValue = self.interpretParameter(p[2])
            self.inPrivateSensitive = self.interpretParameter(p[3])
            
            self.inPrivate.extend(self.inPrivateSensitiveType)
            self.inPrivate.extend(int2bytearray(len(self.inPrivateAuthValue), 2))
            self.inPrivate.extend(self.inPrivateAuthValue)
            self.inPrivate.extend(int2bytearray(len(self.inPrivateSeedValue), 2))
            self.inPrivate.extend(self.inPrivateSeedValue)
            self.inPrivate.extend(int2bytearray(len(self.inPrivateSensitive), 2))
            self.inPrivate.extend(self.inPrivateSensitive)
            
            
        else:
            index = 0
            self.inPrivate = self.interpretParameter(parameters[2])
            while True:
                if (len(self.inPrivate)<=index): break 
                self.inPrivateSensitiveType = self.inPrivate[0:2]
                index +=2
                if (len(self.inPrivate)<=index): break
                
                lenData = (self.inPrivate[index]<<8) + self.inPrivate[(index+1)]
                index +=2
                if (len(self.inPrivate)<=index): break
                self.inPrivateAuthValue = self.inPrivate[index:(index+lenData)]
                index +=lenData
        
                lenData = (self.inPrivate[index]<<8) + self.inPrivate[(index+1)]
                index +=2
                if (len(self.inPrivate)<=index): break
                self.inPrivateSeedValue = self.inPrivate[index:(index+lenData)]
                index +=lenData

                lenData = (self.inPrivate[index]<<8) + self.inPrivate[(index+1)]
                index +=2
                if (len(self.inPrivate)<=index): break
                self.inPrivateSensitive = self.inPrivate[index:(index+lenData)]
                index +=lenData
        
        self.hierarchy = self.interpretParameter(parameters[4])
        
        
        self.inPublicType = bytearray()
        self.inPublicNameAlg = bytearray()
        self.inPublicObjectAttributes = bytearray()
        self.inPublicAuthPolicy = bytearray()
        self.inPublicParameters = bytearray()
        self.inPublicUnique = bytearray()
        self.inPublicUnique2 = bytearray()

        self.sessionAuthValue = bytearray()
                
        #response
        self.objectHandle = bytearray()
        self.name = bytearray()       

        if(self.forDoc==True): return

        self.interpretInTPM2B_PUBLIC(parameters[3])
                    
    def prepare(self):
                
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag) 
                                                            + 2 + len(self.inPrivate) 
                                                            + 2 + len(self.inPublic)
                                                            + len(self.hierarchy) 
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(int2bytearray(len(self.inPrivate), 2))
        self.command.extend(self.inPrivate)    
        self.command.extend(int2bytearray(len(self.inPublic), 2))
        self.command.extend(self.inPublic)  
        self.command.extend(self.hierarchy)  
       
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))

        self.mTreeInPrivateId = self.insertTreeOutput(self.mTreeInId, 'end', text="inPrivate=" + bytearray2hex(self.inPrivate))
        self.insertTreeOutput(self.mTreeInPrivateId, 'end', text="inPrivateSensitiveType=" + bytearray2hex(self.inPrivateSensitiveType), tag='TPM_ALG_ID')
        self.insertTreeOutput(self.mTreeInPrivateId, 'end', text="inPrivateAuthValue=" + bytearray2hex(self.inPrivateAuthValue))
        self.insertTreeOutput(self.mTreeInPrivateId, 'end', text="inPrivateSeedValue=" + bytearray2hex(self.inPrivateSeedValue))
        self.insertTreeOutput(self.mTreeInPrivateId, 'end', text="inPrivateSensitive=" + bytearray2hex(self.inPrivateSensitive))
        
        self.mTreeInPublicId = self.insertTreeOutput(self.mTreeInId, 'end', text="inPublic=" + bytearray2hex(self.inPublic))
        self.insertTreeOutput(self.mTreeInId, 'end', text="hierarchy=" + bytearray2hex(self.hierarchy))
        
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicType=" + bytearray2hex(self.inPublicType), tag='TPM_ALG_ID')
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicNameAlg=" + bytearray2hex(self.inPublicNameAlg), tag='TPM_ALG_ID')
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicObjectAttributes=" + bytearray2hex(self.inPublicObjectAttributes), tag='TPMA_OBJECT')
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicAuthPolicy=" + bytearray2hex(self.inPublicAuthPolicy))
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicParameters=" + bytearray2hex(self.inPublicParameters))
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicUnique=" + bytearray2hex(self.inPublicUnique))
        if(self.inPublicType==TPM_ALG_ECC):
            self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicUnique2=" + bytearray2hex(self.inPublicUnique2))
                
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
        if(self.responseCode!=TPM_RC_SUCCESS):
            return
        
        index=10
        self.objectHandle = data[index:(index+4)]
        index +=4
                
        lenName = (data[index]<<8) + data[(index+1)]
        index +=2
        if(lenName>0):
            self.name = data[index:(index+lenName)]
        else:
            self.name =""
        index += lenName
                
        self.insertTreeOutput(self.mTreeOutId, 'end', text="objectHandle=" + bytearray2hex(self.objectHandle))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="name=" + bytearray2hex(self.name))
                
        if(self.responseCode==TPM_RC_SUCCESS):
            Object.loadObject(self.inPrivate,  self.objectHandle,  self.name)
