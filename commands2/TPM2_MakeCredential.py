from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_MakeCredential
from Utils import bytearray2hex, int2bytearray

class TPM2_MakeCredential(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_MakeCredential
        
        self.handle = self.interpretParameter(parameters[2])
        self.credential = self.interpretParameter(parameters[3])
        self.objectName = self.interpretParameter(parameters[4])
        
        #response
        self.credentialBlob  = bytearray()
        self.secret = bytearray()

        if(self.forDoc==True): return
        
    def prepare(self):
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)
                                                            + len(self.handle)
                                                            + 2 + len(self.credential)
                                                            + 2 + len(self.objectName)
                                                        ),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.handle)	
        self.command.extend(int2bytearray(len(self.credential), 2))
        self.command.extend(self.credential)	
        self.command.extend(int2bytearray(len(self.objectName), 2))
        self.command.extend(self.objectName)	

        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="handle=" + bytearray2hex(self.handle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="credential=" + bytearray2hex(self.credential))
        self.insertTreeOutput(self.mTreeInId, 'end', text="objectName=" + bytearray2hex(self.objectName))
            
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        index = 10
        
        if(len(data)>index):
            lenData = (data[index]<<8) + (data[(index+1)])
            index += 2
            self.credentialBlob = data[index:(index+lenData)]
            index += lenData                    
            
        if(len(data)>index):
            lenData = (data[index]<<8) + (data[(index+1)])
            index += 2
            self.secret = data[index:(index+lenData)]
            index += lenData                    
        
            
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="credentialBlob=" + bytearray2hex(self.credentialBlob))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="secret=" + bytearray2hex(self.secret))
