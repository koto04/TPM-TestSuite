from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_ECDH_ZGen, TPM_RC_SUCCESS
from Utils import bytearray2hex, int2bytearray
from TPM_Session_V2 import Session

class TPM2_ECDH_ZGen(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_ECDH_ZGen
        
        self.keyHandle = self.interpretParameter(parameters[2])
        self.sessionHandle = self.interpretParameter(parameters[3])
        self.nonceCaller = self.interpretParameter(parameters[4])
        self.sessionAttribute = self.interpretParameter(parameters[5])
        
        self.inPoint = bytearray()
        self.inPointX = bytearray()
        self.inPointY = bytearray()
        
        self.sessionAuthValue = bytearray()
        
        if(isinstance(parameters[6], list)):    #inPoint
            p = parameters[6]
            self.inPointX = self.interpretParameter(p[0])
            self.inPointY = self.interpretParameter(p[1])
            
            self.inPoint.extend(int2bytearray(len(self.inPointX), 2))
            self.inPoint.extend(self.inPointX)
            self.inPoint.extend(int2bytearray(len(self.inPointY), 2))
            self.inPoint.extend(self.inPointY)
            
        else:
            self.inPoint = self.interpretParameter(parameters[6])
            index = 0
            lenInPointX = (self.inPoint[index]<<8) + self.inPoint[(index+1)]
            index +=2
            if(lenInPointX==0):
                self.inPointX = bytearray()
            else:
                self.inPointX = self.inPoint[index:(index+lenInPointX)]            
            index +=lenInPointX

            lenInPointY = (self.inPoint[index]<<8) + self.inPoint[(index+1)]
            index +=2
            if(lenInPointY==0):
                self.inPointY = bytearray()
            else:
                self.inPointY = self.inPoint[index:(index+lenInPointY)]
            
            
                
        self.sessionAuthValue = bytearray()
                
        #response
        self.outPoint = bytearray()
        self.outPointX = bytearray()
        self.outPointY = bytearray()

        if(self.forDoc==True): return

        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.keyHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(int2bytearray(len(self.inPoint), 2))
        self.HMACParameters.extend(self.inPoint)	
        
        self.sessionAuthValue = Session.getAuthValue(self)
                
        
    def prepare(self):
        
        lenAuth =  len(self.sessionHandle) + 2 + len(self.nonceCaller) + len(self.sessionAttribute) + 2 + len(self.sessionAuthValue)        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)
                                                            + len(self.keyHandle) 
                                                            + 4 + lenAuth 
                                                            + 2 + len(self.inPoint) 
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.keyHandle)	
        self.command.extend(int2bytearray(lenAuth, 4))
        self.command.extend(self.sessionHandle)	
        self.command.extend(int2bytearray(len(self.nonceCaller), 2))
        self.command.extend(self.nonceCaller)	
        self.command.extend(self.sessionAttribute)	
        self.command.extend(int2bytearray(len(self.sessionAuthValue), 2))
        self.command.extend(self.sessionAuthValue)	
        
        self.command.extend(int2bytearray(len(self.inPoint), 2))
        self.command.extend(self.inPoint)	
               
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="keyHandle=" + bytearray2hex(self.keyHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="sessionHandle=" + bytearray2hex(self.sessionHandle))
        
        self.inTreeAuthorization()
        
        self.mTreeInPointId =self.insertTreeOutput(self.mTreeInId, 'end', text="inPoint=" + bytearray2hex(self.inPoint))
        self.insertTreeOutput(self.mTreeInPointId, 'end', text="inPointX=" + bytearray2hex(self.inPointX))
        self.insertTreeOutput(self.mTreeInPointId, 'end', text="inPointY=" + bytearray2hex(self.inPointY))
        
            
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")

        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')

        index = 14
        if(self.responseCode==TPM_RC_SUCCESS):
        
            #lenOutPoint= (data[index]<<8) + data[(index+1)]
            index +=2            
            lenOutPointX= (data[index]<<8) + data[(index+1)]
            index +=2
            self.outPointX = data[(index):(index+lenOutPointX)]
            index += lenOutPointX
            lenOutPointY= (data[index]<<8) + data[(index+1)]
            index +=2
            self.outPointY = data[(index):(index+lenOutPointY)]
            index += lenOutPointY
            
            self.outPoint.extend(int2bytearray(lenOutPointX, 2))
            self.outPoint.extend(self.outPointX)
            self.outPoint.extend(int2bytearray(lenOutPointY, 2))
            self.outPoint.extend(self.outPointY)
            
            
        self.mTreeOutPointId =  self.insertTreeOutput(self.mTreeOutId, 'end', text="outPoint=" + bytearray2hex(self.outPoint))
        self.insertTreeOutput(self.mTreeOutPointId, 'end', text="outPointX=" + bytearray2hex(self.outPointX))
        self.insertTreeOutput(self.mTreeOutPointId, 'end', text="outPointY=" + bytearray2hex(self.outPointY))

        self.outTreeAuthorization(data, index)
        