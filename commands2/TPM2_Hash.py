from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_Hash, TPM_RC_SUCCESS
from Utils import bytearray2hex, int2bytearray

class TPM2_Hash(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_Hash
        self.data = self.interpretParameter(parameters[2])
        self.hashAlg = self.interpretParameter(parameters[3])
        self.hierarchy = self.interpretParameter(parameters[4])
    
        #response
        self.outHash = bytearray()
        self.validation = bytearray()
        self.validationTag = bytearray()
        self.validationHierarchy = bytearray()
        self.validationDigest = bytearray()
        
        if(self.forDoc==True): return

    def prepare(self):
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)+2+len(self.data)+len(self.hashAlg) + len(self.hierarchy)),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(int2bytearray(len(self.data), 2))
        self.command.extend(self.data)	
        self.command.extend(self.hashAlg)
        self.command.extend(self.hierarchy) 

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="data=" + bytearray2hex(self.data))
        self.insertTreeOutput(self.mTreeInId, 'end', text="hashAlg=" + bytearray2hex(self.hashAlg))
        self.insertTreeOutput(self.mTreeInId, 'end', text="hierarchy=" + bytearray2hex(self.hierarchy))
    
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
        if(self.responseCode==TPM_RC_SUCCESS):
            index = 10
            lenOutHash = (data[index]<<8) + data[(index+1)]
            index +=2
            self.outHash = data[(index):(index+lenOutHash)]
            index +=lenOutHash
            
            self.validation = data[(index):]
            
            self.validationTag = data[(index):(index+2)]
            index +=2
            
            self.validationHierarchy = data[(index):(index+4)]
            index +=4
            
            lenValidationDigest = (data[index]<<8) + data[(index+1)]
            index +=2
            self.validationDigest = data[(index):(index+lenValidationDigest)]
            
            self.insertTreeOutput(self.mTreeOutId, 'end', text="outHash=" + bytearray2hex(self.outHash))

            self.mTreeOutValidationId = self.insertTreeOutput(self.mTreeOutId, 'end', text="validation=" + bytearray2hex(self.validation))
            self.insertTreeOutput(self.mTreeOutValidationId, 'end', text="validationTag=" + bytearray2hex(self.validationTag))
            self.insertTreeOutput(self.mTreeOutValidationId, 'end', text="validationHierarchy=" + bytearray2hex(self.validationHierarchy))
            self.insertTreeOutput(self.mTreeOutValidationId, 'end', text="validationDigest=" + bytearray2hex(self.validationDigest))
            
