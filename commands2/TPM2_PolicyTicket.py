from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_PolicyTicket
from Utils import bytearray2hex, int2bytearray

from random import randrange
import os

class TPM2_PolicyTicket(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_PolicyTicket
        
        self.policySession = self.interpretParameter(parameters[2])
        self.timeout = self.interpretParameter(parameters[3])
        self.cpHashA = self.interpretParameter(parameters[4])
        self.policyRef = self.interpretParameter(parameters[5])
        self.authName = self.interpretParameter(parameters[6])

        self.policyTicket = bytearray()
        self.policyTicketTag = bytearray()
        self.policyTicketHierarchy = bytearray()
        self.policyTicketDigest = bytearray()

        
        if(self.forDoc==True): return

        if(isinstance(parameters[7], list)):
            p = parameters[7]
            self.policyTicketTag = self.interpretParameter(p[0])
            self.policyTicketHierarchy = self.interpretParameter(p[1])
            self.policyTicketDigest = self.interpretParameter(p[2])
            self.marshalTicket()
            
        else:
            self.policyTicket = self.interpretParameter(parameters[7])
            self.unmarshalTicket()
        
    def unmarshalTicket(self):
        self.policyTicketTag = self.policyTicket[0:2]
        self.policyTicketHierarchy = self.policyTicket[2:6]
        lenDigest = (self.policyTicket[6]<<8) + self.policyTicket[7]
        if(lenDigest==0):
            self.policyTicketDigest = bytearray()
        else:
            self.policyTicketDigest = self.policyTicket[8:(8+lenDigest)]

            
    def marshalTicket(self):
        self.policyTicket = bytearray()
        self.policyTicket.extend(self.policyTicketTag)
        self.policyTicket.extend(self.policyTicketHierarchy)
        self.policyTicket.extend(int2bytearray(len(self.policyTicketDigest), 2))
        self.policyTicket.extend(self.policyTicketDigest)
        
        
    def prepare(self):
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)+
                                                            len(self.policySession) + 
                                                            2 + len(self.timeout) +
                                                            2 + len(self.cpHashA) +
                                                            2 + len(self.policyRef) +
                                                            2 + len(self.authName) +
                                                            len(self.policyTicket)
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.policySession)	
        self.command.extend(int2bytearray(len(self.timeout), 2))
        self.command.extend(self.timeout)	
        self.command.extend(int2bytearray(len(self.cpHashA), 2))
        self.command.extend(self.cpHashA)	
        self.command.extend(int2bytearray(len(self.policyRef), 2))
        self.command.extend(self.policyRef)
        self.command.extend(int2bytearray(len(self.authName), 2))
        self.command.extend(self.authName)
        self.command.extend(self.policyTicket) 
       
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="policySession=" + bytearray2hex(self.policySession))
        self.insertTreeOutput(self.mTreeInId, 'end', text="timeout=" + bytearray2hex(self.timeout))
        self.insertTreeOutput(self.mTreeInId, 'end', text="cpHashA=" + bytearray2hex(self.cpHashA))
        self.insertTreeOutput(self.mTreeInId, 'end', text="policyRef=" + bytearray2hex(self.policyRef))
        self.insertTreeOutput(self.mTreeInId, 'end', text="authName=" + bytearray2hex(self.authName))
        
        self.mTreeInTicketId = self.insertTreeOutput(self.mTreeInId, 'end', text="policyTicket=" + bytearray2hex(self.policyTicket))
        self.insertTreeOutput(self.mTreeInTicketId, 'end', text="policyTicketTag=" + bytearray2hex(self.policyTicketTag))
        self.insertTreeOutput(self.mTreeInTicketId, 'end', text="policyTicketHierarchy=" + bytearray2hex(self.policyTicketHierarchy))
        self.insertTreeOutput(self.mTreeInTicketId, 'end', text="policyTicketDigest=" + bytearray2hex(self.policyTicketDigest))



    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]

        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
            
    def fuzzy(self, level):
        self.sessionAuthValue = bytearray()
        self.orgPolicyTicket = self.policyTicket
        self.orgPolicyTicketDigest = self.policyTicketDigest
        self.orgPolicyTicketHierarchy = self.policyTicketHierarchy
        self.orgPolicyTicketTag = self.policyTicketTag
        
        self.orgPolicyRef = self.policyRef
        self.orgAuthName = self.authName
        self.orgCpHashA = self.cpHashA
        self.orgTimeout = self.timeout
        
        if level==1:
            expectedResponse = self.fuzzyLevel1()
        elif level==2:
            expectedResponse = self.fuzzyLevel2()
        return expectedResponse
        
    def fuzzyLevel1(self):
        
        if(self.forDoc==True): return
 
        p = randrange(0,10)
        fuzzValue = bytearray()

        if p==0:
            self.modifiedParam = 'none'
            return 
        elif p==1: #policySession
            self.modifiedParam = 'policySession'
            fuzzValue.extend(os.urandom(4))
            if self.policySession!=fuzzValue:
                self.policySession=fuzzValue
            
        elif p==2: #timeout
            self.modifiedParam = 'timeout'
            fuzzValue.extend(os.urandom(8))
            if self.timeout!=fuzzValue:
                self.timeout=fuzzValue
            
        elif p==3: #cpHashA
            self.modifiedParam = 'cpHashA'
            fuzzValue.extend(os.urandom(32))
            if self.cpHashA!=fuzzValue:
                self.cpHashA=fuzzValue
            
        elif p==4: #policyRef
            self.modifiedParam = 'policyRef'
            fuzzValue.extend(os.urandom(32))
            if self.policyRef!=fuzzValue:
                self.policyRef=fuzzValue
                
        elif p==5: #authName
            self.modifiedParam = 'authName'
            fuzzValue.extend(os.urandom(len(self.authName)))
            if self.authName!=fuzzValue:
                self.authName=fuzzValue
                
        elif p==6: #policyTicket
            self.modifiedParam = 'policyTicket'
            fuzzValue.extend(os.urandom(len(self.policyTicket)))
            if self.policyTicket!=fuzzValue:
                self.policyTicket=fuzzValue
                self.policyTicketDigest = bytearray()
                self.policyTicketHierarchy = bytearray()
                self.policyTicketTag = bytearray()
                #self.unmarshalTicket()
                
        elif p==7: #policyTicketTag
            self.modifiedParam = 'policyTicketTag'
            fuzzValue.extend(os.urandom(len(self.policyTicketTag)))
            if self.policyTicketTag!=fuzzValue:
                self.policyTicketTag=fuzzValue
                self.marshalTicket()
        
        elif p==8: #policyTicketHierarchy
            self.modifiedParam = 'policyTicketHierarchy'
            fuzzValue.extend(os.urandom(len(self.policyTicketHierarchy)))
            if self.policyTicketHierarchy!=fuzzValue:
                self.policyTicketHierarchy=fuzzValue
                self.marshalTicket()
                
        elif p==9: #policyTicketDigest
            self.modifiedParam = 'policyTicketDigest'
            fuzzValue.extend(os.urandom(len(self.policyTicketDigest)))
            if self.policyTicketDigest!=fuzzValue:
                self.policyTicketDigest=fuzzValue
                self.marshalTicket()
            
    def fuzzyLevel2(self):
        if(self.forDoc==True): return
        
        p = randrange(0,10)
        fuzzValue = bytearray()
        size = randrange(0x00,self.FUZZY_MAX_PARAM_LENGTH)
        fuzzValue.extend(os.urandom(size))

        if p==0:
            self.modifiedParam = 'none'
            return
        elif p==1: #policySession
            self.modifiedParam = 'policySession'
            if self.policySession!=fuzzValue:
                self.policySession=fuzzValue
            
        elif p==2: #timeout
            self.modifiedParam = 'timeout'
            if self.timeout!=fuzzValue:
                self.timeout=fuzzValue
            
        elif p==3: #cpHashA
            self.modifiedParam = 'cpHashA'
            if self.cpHashA!=fuzzValue:
                self.cpHashA=fuzzValue
            
        elif p==4: #policyRef
            self.modifiedParam = 'policyRef'
            if self.policyRef!=fuzzValue:
                self.policyRef=fuzzValue
                
        elif p==5: #authName
            self.modifiedParam = 'authName'
            if self.authName!=fuzzValue:
                self.authName=fuzzValue
                
        elif p==6: #policyTicket
            self.modifiedParam = 'policyTicket'
            if self.policyTicket!=fuzzValue:
                self.policyTicket=fuzzValue
                self.policyTicketDigest = bytearray()
                self.policyTicketHierarchy = bytearray()
                self.policyTicketTag = bytearray()
                #self.unmarshalTicket()
                
        elif p==7: #policyTicketTag
            self.modifiedParam = 'policyTicketTag'
            if self.policyTicketTag!=fuzzValue:
                self.policyTicketTag=fuzzValue
                self.marshalTicket()
        
        elif p==8: #policyTicketHierarchy
            self.modifiedParam = 'policyTicketHierarchy'
            if self.policyTicketHierarchy!=fuzzValue:
                self.policyTicketHierarchy=fuzzValue
                self.marshalTicket()
                
        elif p==9: #policyTicketDigest
            self.modifiedParam = 'policyTicketDigest'
            if self.policyTicketDigest!=fuzzValue:
                self.policyTicketDigest=fuzzValue
                self.marshalTicket()
        
