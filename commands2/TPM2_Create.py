from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_Create, TPM_ALG_ECC, TPM_RC_SUCCESS
from TPM_Session_V2 import Session
from Utils import bytearray2hex, int2bytearray
from TPM_Object import Object

class TPM2_Create(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_Create
        
        self.parentHandle = self.interpretParameter(parameters[2])
        self.sessionHandle = self.interpretParameter(parameters[3])
        self.nonceCaller = self.interpretParameter(parameters[4])
        self.sessionAttribute = self.interpretParameter(parameters[5])
        
        #response
        self.outPrivate = bytearray()
        
        self.outPublic = bytearray()
        self.outPublicType = bytearray()
        self.outPublicNameAlg = bytearray()
        self.outPublicObjectAttributes = bytearray()
        self.outPublicAuthPolicy = bytearray()
        self.outPublicParameters = bytearray()
        self.outPublicUnique = bytearray()
        self.outPublicUniqueX = bytearray()
        self.outPublicUniqueY = bytearray()
        
        self.creationData = bytearray()
        self.creationHash = bytearray()
        
        self.creationTicket = bytearray()
        self.creationTicketTag = bytearray()
        self.creationTicketHierarchy = bytearray()
        self.creationTicketDigest = bytearray()

        if(self.forDoc==True): return
        
        if(isinstance(parameters[6], list)):
            p = parameters[6]
            self.inSensitive  = bytearray()
            self.inSensitiveAuth = self.interpretParameter(p[0])
            self.inSensitiveData = self.interpretParameter(p[1])
            self.inSensitive.extend(int2bytearray(len(self.inSensitiveAuth), 2))
            self.inSensitive.extend(self.inSensitiveAuth)
            self.inSensitive.extend(int2bytearray(len(self.inSensitiveData), 2))
            self.inSensitive.extend(self.inSensitiveData)
            
        else:
            self.inSensitive  = self.interpretParameter(parameters[6]) #TPM2B_DIGEST

            index=0
            lenSensitiveAuth = (self.inSensitive[index]<<8) + self.inSensitive[(index+1)]
            index +=2
            if(lenSensitiveAuth==0):
                self.inSensitiveAuth = bytearray()
            else:
                self.inSensitiveAuth = self.inSensitive[index:(index+lenSensitiveAuth)]
            index+=lenSensitiveAuth
            
            lenSensitiveData = (self.inSensitive[index]<<8) + self.inSensitive[(index+1)]
            if(lenSensitiveData==0):
                self.inSensitiveData = bytearray()
            else:
                self.inSensitiveData = self.inSensitive[index:(index+lenSensitiveData)]

                    
        self.interpretInTPM2B_PUBLIC(parameters[7])
        
        self.outsideInfo = self.interpretParameter(parameters[8]) #TPM2B_DATA (Part2 p. 105)
        
        #TPML_PCR_SELECTION (Part2 p. 117)
        if(isinstance(parameters[9], list)):
            p = parameters[9]
            self.creationPCR = bytearray()
            
        else:
            self.creationPCR = self.interpretParameter(parameters[9]) 
            

        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.parentHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(int2bytearray(len(self.inSensitive), 2))
        self.HMACParameters.extend(self.inSensitive)	
        self.HMACParameters.extend(int2bytearray(len(self.inPublic), 2))
        self.HMACParameters.extend(self.inPublic)	
        self.HMACParameters.extend(int2bytearray(len(self.outsideInfo), 2))
        self.HMACParameters.extend(self.outsideInfo)	
        self.HMACParameters.extend(self.creationPCR)	

        self.sessionAuthValue = Session.getAuthValue(self)
        
    def prepare(self):
                
        lenAuth =  len(self.sessionHandle) + 2 + len(self.nonceCaller) + len(self.sessionAttribute) + 2 + len(self.sessionAuthValue)        
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)
                                                            + len(self.parentHandle)
                                                            + 4 + lenAuth 
                                                            + 2 + len(self.inSensitive) 
                                                            + 2 + len(self.inPublic) 
                                                            + 2 + len(self.outsideInfo) 
                                                            + len(self.creationPCR) 
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.parentHandle)	
        self.command.extend(int2bytearray(lenAuth, 4))
        self.command.extend(self.sessionHandle)	
        self.command.extend(int2bytearray(len(self.nonceCaller), 2))
        self.command.extend(self.nonceCaller)	
        self.command.extend(self.sessionAttribute)	
        self.command.extend(int2bytearray(len(self.sessionAuthValue), 2))
        self.command.extend(self.sessionAuthValue)	
        self.command.extend(int2bytearray(len(self.inSensitive), 2))
        self.command.extend(self.inSensitive)	
        self.command.extend(int2bytearray(len(self.inPublic), 2))
        self.command.extend(self.inPublic)	
        self.command.extend(int2bytearray(len(self.outsideInfo), 2))
        self.command.extend(self.outsideInfo)	
        self.command.extend(self.creationPCR)	
               
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="parentHandle=" + bytearray2hex(self.parentHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="sessionHandle=" + bytearray2hex(self.sessionHandle))
        self.inTreeAuthorization()
                
        self.mTreeInSensitiveId = self.insertTreeOutput(self.mTreeInId, 'end', text="inSensitive=" + bytearray2hex(self.inSensitive))
        self.insertTreeOutput(self.mTreeInSensitiveId, 'end', text="inSensitiveAuth=" + bytearray2hex(self.inSensitiveAuth))
        self.insertTreeOutput(self.mTreeInSensitiveId, 'end', text="inSensitiveData=" + bytearray2hex(self.inSensitiveData))
        
        self.mTreeInPublicId = self.insertTreeOutput(self.mTreeInId, 'end', text="inPublic=" + bytearray2hex(self.inPublic))
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicType=" + bytearray2hex(self.inPublicType), tag='TPM_ALG_ID')
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicNameAlg=" + bytearray2hex(self.inPublicNameAlg), tag='TPM_ALG_ID')
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicObjectAttributes=" + bytearray2hex(self.inPublicObjectAttributes), tag='TPMA_OBJECT')
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicAuthPolicy=" + bytearray2hex(self.inPublicAuthPolicy))
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicParameters=" + bytearray2hex(self.inPublicParameters))
        self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicUnique=" + bytearray2hex(self.inPublicUnique))
        if(self.inPublicType==TPM_ALG_ECC):
            self.insertTreeOutput(self.mTreeInPublicId, 'end', text="inPublicUnique2=" + bytearray2hex(self.inPublicUnique2))
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="outsideInfo=" + bytearray2hex(self.outsideInfo))
        self.insertTreeOutput(self.mTreeInId, 'end', text="creationPCR=" + bytearray2hex(self.creationPCR))
        
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
               
        index=10
        index += 4
        if(len(data)>index):
            lenOutPrivate = (data[index]<<8) + data[(index+1)]
            index +=2
            self.outPrivate = data[index:(index+lenOutPrivate)]
            index +=lenOutPrivate

        if(len(data)>index):
            lenOutPublic = (data[index]<<8) + data[(index+1)]
            index +=2
            self.outPublic = data[index:(index+lenOutPublic)]
            index +=lenOutPublic
        
        if(len(data)>index):
            lenCreationData = (data[index]<<8) + data[(index+1)]
            index +=2
            self.creationData = data[index:(index+lenCreationData)]
            index +=lenCreationData
        
        if(len(data)>index):
            lenCreationHash = (data[index]<<8) + data[(index+1)]
            index +=2
            self.creationHash = data[index:(index+lenCreationHash)]
            index +=lenCreationHash
        
        if(len(data)>index):
            lenCreationTicket =(data[(index+6)]<<8) + data[(index+7)]
            lenCreationTicket += 8
            self.creationTicket = data[index:(index+lenCreationTicket)]
            self.creationTicketTag = data[index:(index+2)]
            self.creationTicketHierarchy = data[(index+2):(index+6)]
            lenCreationTicketDigest = (data[(index+6)]<<8) + data[(index+7)]
            self.creationTicketDigest  = data[(index+8):(index+8 + lenCreationTicketDigest)]
            index +=lenCreationTicket
                        
        
        self.interpretOutCreationData(self.creationData)
        self.interpretOutTPM2B_PUBLIC(self.outPublic)

        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        self.insertTreeOutput(self.mTreeOutId, 'end', text="outPrivate=" + bytearray2hex(self.outPrivate))
        
        self.mTreeOutPublicId = self.insertTreeOutput(self.mTreeOutId, 'end', text="outPublic=" + bytearray2hex(self.outPublic))
        self.insertTreeOutput(self.mTreeOutPublicId, 'end', text="outPublicType=" + bytearray2hex(self.outPublicType), tag='TPM_ALG_ID')
        self.insertTreeOutput(self.mTreeOutPublicId, 'end', text="outPublicNameAlg=" + bytearray2hex(self.outPublicNameAlg), tag='TPM_ALG_ID')
        self.insertTreeOutput(self.mTreeOutPublicId, 'end', text="outPublicObjectAttributes=" + bytearray2hex(self.outPublicObjectAttributes), tag='TPMA_OBJECT')
        self.insertTreeOutput(self.mTreeOutPublicId, 'end', text="outPublicAuthPolicy=" + bytearray2hex(self.outPublicAuthPolicy))
        self.insertTreeOutput(self.mTreeOutPublicId, 'end', text="outPublicParameters=" + bytearray2hex(self.outPublicParameters))
        self.mTreeOutPublicUniqueId = self.insertTreeOutput(self.mTreeOutPublicId, 'end', text="outPublicUnique=" + bytearray2hex(self.outPublicUnique))
        
        if (self.outPublicType==TPM_ALG_ECC):
            self.insertTreeOutput(self.mTreeOutPublicUniqueId, 'end', text="outPublicUniqueX=" + bytearray2hex(self.outPublicUniqueX))
            self.insertTreeOutput(self.mTreeOutPublicUniqueId, 'end', text="outPublicUniqueY=" + bytearray2hex(self.outPublicUniqueY))
                    
        self.mTreeOutCreationDataId = self.insertTreeOutput(self.mTreeOutId, 'end', text="creationData=" + bytearray2hex(self.creationData))
        self.mTreeOutCreationDataPcrSelectId = self.insertTreeOutput(self.mTreeOutCreationDataId, 'end', text="creationDataPcrSelect=" + bytearray2hex(self.creationDataPcrSelect))        
        self.insertTreeOutput(self.mTreeOutCreationDataId, 'end', text="creationDataPcrDigest=" + bytearray2hex(self.creationDataPcrDigest))
        self.insertTreeOutput(self.mTreeOutCreationDataId, 'end', text="creationDataLocality=" + bytearray2hex(self.creationDataLocality))
        self.insertTreeOutput(self.mTreeOutCreationDataId, 'end', text="creationDataParentNameAlg=" + bytearray2hex(self.creationDataParentNameAlg), tag='TPM_ALG_ID')
        self.insertTreeOutput(self.mTreeOutCreationDataId, 'end', text="creationDataParentName=" + bytearray2hex(self.creationDataParentName))
        self.insertTreeOutput(self.mTreeOutCreationDataId, 'end', text="creationDataParentQualifiedName=" + bytearray2hex(self.creationDataParentQualifiedName))
        self.insertTreeOutput(self.mTreeOutCreationDataId, 'end', text="creationDataOutsideInfo=" + bytearray2hex(self.creationDataOutsideInfo))
        
        self.insertTreeOutput(self.mTreeOutCreationDataPcrSelectId, 'end', text="creationDataPcrSelectCount=" + bytearray2hex(self.creationDataPcrSelectCount))
        for i in range(0, self.pcrSelectCount):
            self.insertTreeOutput(self.mTreeOutCreationDataPcrSelectId, 'end', text="creationDataPcrSelectHash[" + str(i) + "]=" + bytearray2hex(self.creationDataPcrSelectHash[i]))
            self.insertTreeOutput(self.mTreeOutCreationDataPcrSelectId, 'end', text="creationDataPcrSelectSelect[" + str(i) + "]=" + bytearray2hex(self.creationDataPcrSelectSelect[i]))


        self.insertTreeOutput(self.mTreeOutId, 'end', text="creationHash=" + bytearray2hex(self.creationHash))
        
        self.mTreeOutCreationTicketId = self.insertTreeOutput(self.mTreeOutId, 'end', text="creationTicket=" + bytearray2hex(self.creationTicket))
        self.insertTreeOutput(self.mTreeOutCreationTicketId, 'end', text="creationTicketTag=" + bytearray2hex(self.creationTicketTag))
        self.insertTreeOutput(self.mTreeOutCreationTicketId, 'end', text="creationTicketHierarchy=" + bytearray2hex(self.creationTicketHierarchy))
        self.insertTreeOutput(self.mTreeOutCreationTicketId, 'end', text="creationTicketDigest=" + bytearray2hex(self.creationTicketDigest))
        
        self.outTreeAuthorization(data, index)
        
        if(self.responseCode==TPM_RC_SUCCESS):
            Object.addObject(self.outPrivate,  self.inSensitiveAuth)
        
