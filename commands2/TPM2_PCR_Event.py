
from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_PCR_Event, TPM_RC_SUCCESS, TPM_ALG_SHA1, TPM_ALG_SHA256, TPM_ALG_SHA384, TPM_ALG_SHA512
from Utils import bytearray2hex, int2bytearray
from TPM_Session_V2 import Session

class TPM2_PCR_Event(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_PCR_Event
        
        self.pcrHandle = self.interpretParameter(parameters[2])
        self.sessionHandle = self.interpretParameter(parameters[3])
        self.nonceCaller = self.interpretParameter(parameters[4])
        self.sessionAttribute = self.interpretParameter(parameters[5])
        self.eventData = self.interpretParameter(parameters[6])

        self.sessionAuthValue = bytearray()
                
        #response
        self.count = bytearray()
        self.digests = bytearray()

        self.hashDigits = []
        self.hashAlgos = []

        if(self.forDoc==True): return

        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.pcrHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(int2bytearray(len(self.eventData), 2))
        self.HMACParameters.extend(self.eventData)	
        
        self.sessionAuthValue = Session.getAuthValue(self)

        
    def prepare(self):
        
        lenAuth =  len(self.sessionHandle) + 2 + len(self.nonceCaller) + len(self.sessionAttribute) + 2 + len(self.sessionAuthValue)        
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)
                                                            + len(self.pcrHandle) 
                                                            + 4 + lenAuth 
                                                            + 2 + len(self.eventData)
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.pcrHandle)	
        self.command.extend(int2bytearray(lenAuth, 4))
        self.command.extend(self.sessionHandle)	
        self.command.extend(int2bytearray(len(self.nonceCaller), 2))
        self.command.extend(self.nonceCaller)	
        self.command.extend(self.sessionAttribute)	
        self.command.extend(int2bytearray(len(self.sessionAuthValue), 2))
        self.command.extend(self.sessionAuthValue)	
        self.command.extend(int2bytearray(len(self.eventData), 2))
        self.command.extend(self.eventData)	
       
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="pcrHandle=" + bytearray2hex(self.pcrHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="sessionHandle=" + bytearray2hex(self.sessionHandle))

        self.inTreeAuthorization()  

        self.insertTreeOutput(self.mTreeInId, 'end', text="eventData=" + bytearray2hex(self.eventData))
        
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        index=10
        if(self.responseCode==TPM_RC_SUCCESS):
            
            responseParamLen = (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)])
            index+=4
            algoStartIndex = index
            if(responseParamLen>0):
                self.count = data[index:(index+4)]
                count = (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)])
                index+=4
                for i in range(0, count):
                    algo = data[index:(index+2)]
                    index+=2
                    if(algo==TPM_ALG_SHA1):
                        self.hashAlgos.append(algo)
                        self.hashDigits.append(data[index:(index+20)])
                        index+=20
                    elif(algo==TPM_ALG_SHA256):
                        self.hashAlgos.append(algo)
                        self.hashDigits.append(data[index:(index+32)])
                        index+=32
                    elif(algo==TPM_ALG_SHA384):
                        self.hashAlgos.append(algo)
                        self.hashDigits.append(data[index:(index+48)])
                        index+=48
                    elif(algo==TPM_ALG_SHA512):
                        self.hashAlgos.append(algo)
                        self.hashDigits.append(data[index:(index+64)])
                        index+=64
                self.digests = data[algoStartIndex:index]        
            
            
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        self.insertTreeOutput(self.mTreeOutId, 'end', text="count=" + bytearray2hex(self.count))
        
        self.mTreeOutDigestsId = self.insertTreeOutput(self.mTreeOutId, 'end', text="digests=" + bytearray2hex(self.digests))
        for idx,  val in enumerate(self.hashAlgos):
            self.insertTreeOutput(self.mTreeOutDigestsId, 'end', text="hashAlgos[" + str(idx) + "]=" + bytearray2hex(self.hashAlgos[idx]))
            self.insertTreeOutput(self.mTreeOutDigestsId, 'end', text="hashDigits[" + str(idx) + "]=" + bytearray2hex(self.hashDigits[idx]))
            
        self.outTreeAuthorization(data, index)
