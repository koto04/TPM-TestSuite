from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_Duplicate
from TPM_Session_V2 import Session
from Utils import bytearray2hex, int2bytearray

class TPM2_Duplicate(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_Duplicate
        
        self.objectHandle = self.interpretParameter(parameters[2])
        self.newParentHandle = self.interpretParameter(parameters[3])
        self.sessionHandle = self.interpretParameter(parameters[4])
        self.nonceCaller = self.interpretParameter(parameters[5])
        self.sessionAttribute = self.interpretParameter(parameters[6])
        self.encryptionKeyIn = self.interpretParameter(parameters[7])
        self.symmetricAlg = self.interpretParameter(parameters[8])
                    
        self.sessionAuthValue = bytearray()
                
        #response
        self.encryptionKeyOut = bytearray()
        self.duplicate = bytearray()
        self.outSymSeed = bytearray()

        if(self.forDoc==True): return

        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.objectHandle)
        self.HMACHandles.extend(self.newParentHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(int2bytearray(len(self.encryptionKeyIn), 2))	
        self.HMACParameters.extend(self.encryptionKeyIn)	
        self.HMACParameters.extend(self.symmetricAlg)	
        
        self.sessionAuthValue = Session.getAuthValue(self)
                        
    def prepare(self):
                
        lenAuth =  len(self.sessionHandle) + 2 + len(self.nonceCaller) + len(self.sessionAttribute) + 2 + len(self.sessionAuthValue)        
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)
                                                                + len(self.objectHandle)
                                                                + len(self.newParentHandle)
                                                                + 4+ lenAuth
                                                                + 2 + len(self.encryptionKeyIn)
                                                                + len(self.symmetricAlg)
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.objectHandle)	
        self.command.extend(self.newParentHandle)	
        self.command.extend(int2bytearray(lenAuth, 4))
        self.command.extend(self.sessionHandle)	
        self.command.extend(int2bytearray(len(self.nonceCaller), 2))
        self.command.extend(self.nonceCaller)	
        self.command.extend(self.sessionAttribute)	
        self.command.extend(int2bytearray(len(self.sessionAuthValue), 2))
        self.command.extend(self.sessionAuthValue)	
        self.command.extend(int2bytearray(len(self.encryptionKeyIn), 2))
        self.command.extend(self.encryptionKeyIn)	
        self.command.extend(self.symmetricAlg)	
      
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="objectHandle=" + bytearray2hex(self.objectHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="newParentHandle=" + bytearray2hex(self.newParentHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="sessionHandle=" + bytearray2hex(self.sessionHandle))

        self.inTreeAuthorization()
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="encryptionKeyIn=" + bytearray2hex(self.encryptionKeyIn))
        self.insertTreeOutput(self.mTreeInId, 'end', text="symmetricAlg=" + bytearray2hex(self.symmetricAlg))
                
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        index = 10
        index += 4
        if(len(data)>index):
            lenEncryptionKeyOut = (data[index]<<8) + data[(index+1)]
            index +=2
            self.encryptionKeyOut = data[index:(index+lenEncryptionKeyOut)]
            index += lenEncryptionKeyOut
        
        if(len(data)>index):
            lenDuplicate = (data[index]<<8) + data[(index+1)]
            index +=2
            self.duplicate = data[index:(index+lenDuplicate)]
            index += lenDuplicate

        if(len(data)>index):
            lenOutSymSeed = (data[index]<<8) + data[(index+1)]
            index +=2
            self.outSymSeed = data[index:(index+lenOutSymSeed)]
            index += lenOutSymSeed
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="encryptionKeyOut=" + bytearray2hex(self.encryptionKeyOut))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="duplicate=" + bytearray2hex(self.duplicate))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="outSymSeed=" + bytearray2hex(self.outSymSeed))

        self.outTreeAuthorization(data, index)        
        
        
