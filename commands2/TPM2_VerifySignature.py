from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_VerifySignature
from Utils import bytearray2hex, int2bytearray

class TPM2_VerifySignature(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_VerifySignature
        
        self.keyHandle = self.interpretParameter(parameters[2])
        self.digest = self.interpretParameter(parameters[3])
        self.signature = self.interpretParameter(parameters[4])
                
        #response
        self.validation = bytearray()
        self.validationTag = bytearray()
        self.validationHierarchy = bytearray()
        self.validationDigest = bytearray()
        
        if(self.forDoc==True): return

    def prepare(self):
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)+
                                                            len(self.keyHandle) + 
                                                            2 + len(self.digest) +
                                                            len(self.signature)
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.keyHandle)	
        self.command.extend(int2bytearray(len(self.digest), 2))
        self.command.extend(self.digest)	
        self.command.extend(self.signature)	
       
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="keyHandle=" + bytearray2hex(self.keyHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="digest=" + bytearray2hex(self.digest))
        self.insertTreeOutput(self.mTreeInId, 'end', text="signature=" + bytearray2hex(self.signature))

    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        index = 10
        
        #ticket
        if(len(data)>index):
            self.validationTag = data[(index):(index+2)]
            self.validation.extend(self.validationTag)
            index += 2
            self.validationHierarchy = data[(index):(index+4)]
            self.validation.extend(self.validationHierarchy)
            index += 4
            lenValidationDigest = (data[index]<<8) + data[(index+1)]
            self.validation.extend(int2bytearray(lenValidationDigest, 2))
            index += 2
            self.validationDigest = data[(index):(index+lenValidationDigest)]
            self.validation.extend(self.validationDigest)
            index += lenValidationDigest
        
        

        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
            
        self.mTreeOutValidationId = self.insertTreeOutput(self.mTreeOutId, 'end', text="validation=" + bytearray2hex(self.validation))
        self.insertTreeOutput(self.mTreeOutValidationId, 'end', text="validationTag=" + bytearray2hex(self.validationTag))
        self.insertTreeOutput(self.mTreeOutValidationId, 'end', text="validationHierarchy=" + bytearray2hex(self.validationHierarchy))
        self.insertTreeOutput(self.mTreeOutValidationId, 'end', text="validationDigest=" + bytearray2hex(self.validationDigest))
