from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_FlushContext, TPM_YES
from Utils import bytearray2hex, int2bytearray

class TPM2_FlushContext(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_FlushContext
        self.flushHandle = self.interpretParameter(parameters[2])
        self.destroyContext = TPM_YES
        
        if(self.forDoc==True): return

        if(len(parameters)>3):
            self.destroyContext = self.interpretParameter(parameters[3])
    
    def prepare(self):
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)+len(self.flushHandle)),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.flushHandle)		

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="flushHandle=" + bytearray2hex(self.flushHandle))
    
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        if(self.responseCode==bytearray([0x00,  0x00,  0x00,  0x00]) and self.destroyContext==TPM_YES):
            self.manager.destroyHandle(self.flushHandle)
