from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_Quote, TPM_RC_SUCCESS
from Utils import bytearray2hex, int2bytearray
from TPM_Session_V2 import Session

class TPM2_Quote(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_Quote
        
        self.signHandle = self.interpretParameter(parameters[2])
        self.sessionHandle = self.interpretParameter(parameters[3])
        self.nonceCaller = self.interpretParameter(parameters[4])
        self.sessionAttribute = self.interpretParameter(parameters[5])
        self.qualifiyingData = self.interpretParameter(parameters[6])
        self.inScheme = self.interpretParameter(parameters[7])
        self.PCRselect = bytearray()
        self.PCRselectList = []        
            
        self.sessionAuthValue = bytearray()

        #response       
        self.quoted = bytearray()
        self.quotedMagic = bytearray()
        self.quotedType = bytearray()
        self.quotedQualifiedSigner = bytearray()
        self.quotedExtraData = bytearray()
        self.quotedClockInfo = bytearray()
        self.quotedClockInfoClock = bytearray()
        self.quotedClockInfoResetCount = bytearray()
        self.quotedClockInfoRestartCount = bytearray()
        self.quotedClockInfoSafe = bytearray()
        self.quotedFirmwareVersion = bytearray()
        self.quotedAttested = bytearray()
        self.quotedAttestedPcrSelectionCount = bytearray()
        self.quotedAttestedPcrDigest = bytearray()
        
        self.signature = bytearray()
        self.signatureScheme = bytearray()
        self.signatureValue = bytearray()

        self.quotedAttestedPcrSelectionOutAlgos = []
        self.quotedAttestedPcrSelectionOutSelect = []
              
        if(self.forDoc==True): return

        if(isinstance(parameters[8], list)):
            p = parameters[8]
            self.PCRselectCount = int2bytearray(len(p), 4)
            self.PCRselect.extend(int2bytearray(len(p), 4))
            for selection in p:
                s = self.interpretParameter(selection)
                self.PCRselectList.append(s)
                self.PCRselect.extend(s)    
        else:
            self.PCRselect = self.interpretParameter(parameters[8])
            PCRselectCount = (self.PCRselect[0]<<24) + (self.PCRselect[1]<<16) + (self.PCRselect[2]<<8) + (self.PCRselect[3])
            self.PCRselectCount = self.PCRselect[0:4]
            index = 4
            for i in range(0, PCRselectCount):
                lenSelection = self.PCRselect[(index+2)]
                self.PCRselectList.append(self.PCRselect[index:(index+3+lenSelection)])
                index += (3+lenSelection)

        
        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.signHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(int2bytearray(len(self.qualifiyingData), 2))
        self.HMACParameters.extend(self.qualifiyingData)    
        self.HMACParameters.extend(self.inScheme)    
        self.HMACParameters.extend(self.PCRselect)    
        
        self.sessionAuthValue = Session.getAuthValue(self)
                        
    def prepare(self):
                
        lenAuth =  len(self.sessionHandle) + 2 + len(self.nonceCaller) + len(self.sessionAttribute) + 2 + len(self.sessionAuthValue)        
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)
                                                                + len(self.signHandle)
                                                                + 4 + lenAuth 
                                                                + 2 + len(self.qualifiyingData)
                                                                + len(self.inScheme)
                                                                + len(self.PCRselect)
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.signHandle)    
        self.command.extend(int2bytearray(lenAuth, 4))
        self.command.extend(self.sessionHandle)    
        self.command.extend(int2bytearray(len(self.nonceCaller), 2))
        self.command.extend(self.nonceCaller)    
        self.command.extend(self.sessionAttribute)    
        self.command.extend(int2bytearray(len(self.sessionAuthValue), 2))
        self.command.extend(self.sessionAuthValue)    
        self.command.extend(int2bytearray(len(self.qualifiyingData), 2))
        self.command.extend(self.qualifiyingData)    
        self.command.extend(self.inScheme)    
        self.command.extend(self.PCRselect)    
      
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="signHandle=" + bytearray2hex(self.signHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="sessionHandle=" + bytearray2hex(self.sessionHandle))

        self.inTreeAuthorization()
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="qualifiyingData=" + bytearray2hex(self.qualifiyingData))
        self.insertTreeOutput(self.mTreeInId, 'end', text="inScheme=" + bytearray2hex(self.inScheme))
        self.insertTreeOutput(self.mTreeInId, 'end', text="PCRselect=" + bytearray2hex(self.PCRselect))
                
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        index = 10
        index +=4
        if(self.responseCode==TPM_RC_SUCCESS):   
            if(len(data)>index):
                lenData = (data[(index)]<<8) + (data[(index+1)])
                index += 2
                self.quoted = data[index:(index+lenData)]
                index += lenData
                
                subindex = 0
                self.quotedMagic = self.quoted[(subindex):(subindex+4)]
                subindex += 4
                self.quotedType = self.quoted[(subindex):(subindex+2)]
                subindex += 2
                lenData = (data[(subindex)]<<8) + (data[(subindex+1)])
                subindex += 2
                self.quotedQualifiedSigner = self.quoted[(subindex):(subindex+lenData)]
                subindex += lenData
                lenData = (data[(subindex)]<<8) + (data[(subindex+1)])
                subindex += 2
                self.quotedExtraData = self.quoted[(subindex):(subindex+lenData)]
                subindex += lenData
                self.quotedClockInfo = self.quoted[(subindex):(subindex+17)]
                subindex += 17
                
                self.quotedClockInfoClock = self.quotedClockInfo[0:8]
                self.quotedClockInfoResetCount = self.quotedClockInfo[8:12]
                self.quotedClockInfoRestartCount = self.quotedClockInfo[12:16]
                self.quotedClockInfoSafe = self.quotedClockInfo[16:17]
                
                self.quotedFirmwareVersion = self.quoted[(subindex):(subindex+8)]
                subindex += 8
                
                quotedAttestedStart = subindex
                
                selectionOutCount = (self.quoted[subindex]<<24) + (self.quoted[(subindex+1)]<<16) + (self.quoted[(subindex+2)]<<8) + (self.quoted[(subindex+3)])
                self.quotedAttestedPcrSelectionCount = self.quoted[subindex:(subindex+4)]
                
                subindex +=4
                for i in range(0, selectionOutCount):
                    self.quotedAttestedPcrSelectionOutAlgos.append(self.quoted[subindex:(subindex+2)])
                    lenSelectDigest = self.quoted[(subindex+2)]
                    subindex +=3
                    if(lenSelectDigest>0):
                        self.quotedAttestedPcrSelectionOutSelect.append(self.quoted[subindex:(subindex+lenSelectDigest)])
                    else:
                        self.quotedAttestedPcrSelectionOutSelect.append(bytearray())
                    subindex += lenSelectDigest

                
                lenData = (self.quoted[(subindex)]<<8) + (self.quoted[(subindex+1)])
                subindex += 2
                self.quotedAttestedPcrDigest = self.quoted[(subindex):(subindex+lenData)]
                subindex += lenData
                
                self.quotedAttested = self.quoted[quotedAttestedStart:subindex]

            
            if(len(data)>index):
                lenData = (data[index]<<24) + (data[(index+1)]<<16) + (data[index+2]<<8) + data[(index+3)]
                index +=4
                self.signature = data[index:(index+lenData)]
                self.signatureScheme = self.signature[0:4]
                self.signatureValue = self.signature[4:]
                index += lenData

        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
        self.mTreeOutQuotedId = self.insertTreeOutput(self.mTreeOutId, 'end', text="quoted=" + bytearray2hex(self.quoted)) 
        self.mTreeOutSignatureId = self.insertTreeOutput(self.mTreeOutId, 'end', text="signature=" + bytearray2hex(self.signature))
        
        self.insertTreeOutput(self.mTreeOutSignatureId, 'end', text="signatureScheme=" + bytearray2hex(self.signatureScheme))
        self.insertTreeOutput(self.mTreeOutSignatureId, 'end', text="signatureValue=" + bytearray2hex(self.signatureValue))

        self.insertTreeOutput(self.mTreeOutQuotedId, 'end', text="quotedMagic=" + bytearray2hex(self.quotedMagic))
        self.insertTreeOutput(self.mTreeOutQuotedId, 'end', text="quotedType=" + bytearray2hex(self.quotedType))
        self.insertTreeOutput(self.mTreeOutQuotedId, 'end', text="quotedQualifiedSigner=" + bytearray2hex(self.quotedQualifiedSigner))
        self.insertTreeOutput(self.mTreeOutQuotedId, 'end', text="quotedExtraData=" + bytearray2hex(self.quotedExtraData))
        self.mTreeOutQuotedClockInfoId = self.insertTreeOutput(self.mTreeOutQuotedId, 'end', text="quotedClockInfo=" + bytearray2hex(self.quotedClockInfo))
        self.insertTreeOutput(self.mTreeOutQuotedId, 'end', text="quotedFirmwareVersion=" + bytearray2hex(self.quotedFirmwareVersion))
        self.mTreeOutQuotedAttestedId = self.insertTreeOutput(self.mTreeOutQuotedId, 'end', text="quotedAttested=" + bytearray2hex(self.quotedAttested))

        self.insertTreeOutput(self.mTreeOutQuotedClockInfoId, 'end', text="quotedClockInfoClock=" + bytearray2hex(self.quotedClockInfoClock))
        self.insertTreeOutput(self.mTreeOutQuotedClockInfoId, 'end', text="quotedClockInfoResetCount=" + bytearray2hex(self.quotedClockInfoResetCount))
        self.insertTreeOutput(self.mTreeOutQuotedClockInfoId, 'end', text="quotedClockInfoRestartCount=" + bytearray2hex(self.quotedClockInfoRestartCount))
        self.insertTreeOutput(self.mTreeOutQuotedClockInfoId, 'end', text="quotedClockInfoSafe=" + bytearray2hex(self.quotedClockInfoSafe))

        self.insertTreeOutput(self.mTreeOutQuotedAttestedId, 'end', text="quotedAttestedPcrSelectionCount=" + bytearray2hex(self.quotedAttestedPcrSelectionCount))
        for idx,  val in enumerate(self.quotedAttestedPcrSelectionOutAlgos):
            self.insertTreeOutput(self.mTreeOutQuotedAttestedId, 'end', text="quotedAttestedPcrSelectionOutAlgos[" + str(idx) + "]=" + bytearray2hex(self.quotedAttestedPcrSelectionOutAlgos[idx]))
            self.insertTreeOutput(self.mTreeOutQuotedAttestedId, 'end', text="quotedAttestedPcrSelectionOutSelect[" + str(idx) + "]=" + bytearray2hex(self.quotedAttestedPcrSelectionOutSelect[idx]))

        self.insertTreeOutput(self.mTreeOutQuotedAttestedId, 'end', text="quotedAttestedPcrDigest=" + bytearray2hex(self.quotedAttestedPcrDigest))


        self.outTreeAuthorization(data, index)        
        
        
