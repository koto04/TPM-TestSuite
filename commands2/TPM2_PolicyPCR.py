from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_PolicyPCR
from Utils import bytearray2hex, int2bytearray

class TPM2_PolicyPCR(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_PolicyPCR
        self.policySession = self.interpretParameter(parameters[2])
        self.pcrDigest = self.interpretParameter(parameters[3])
        self.pcrs = bytearray()
        self.pcrsCount = bytearray()
        self.pcrsList = []
        
        if(self.forDoc==True): return

        if(isinstance(parameters[4], list)):
            p = parameters[4]
            self.pcrsCount = int2bytearray(len(p), 4)
            self.pcrs.extend(int2bytearray(len(p), 4))
            for selection in p:
                s = self.interpretParameter(selection)
                self.pcrsList.append(s)
                self.pcrs.extend(s)    
        else:
            self.pcrs = self.interpretParameter(parameters[4])
            selectionInCount = (self.pcrs[0]<<24) + (self.pcrs[1]<<16) + (self.pcrs[2]<<8) + (self.pcrs[3])
            self.pcrsCount = self.pcrs[0:4]
            index = 4
            for i in range(0, selectionInCount):
                lenSelection = self.pcrs[(index+2)]
                self.pcrsList.append(self.pcrs[index:(index+3+lenSelection)])
                index += (3+lenSelection)
                
    def prepare(self):
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)
                                                            + len(self.policySession) 
                                                            + 2 + len(self.pcrDigest) 
                                                            + len(self.pcrs)
                                                        ),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.policySession)	
        self.command.extend(int2bytearray(len(self.pcrDigest), 2))	
        self.command.extend(self.pcrDigest)
        self.command.extend(self.pcrs)
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="policySession=" + bytearray2hex(self.policySession))
        self.insertTreeOutput(self.mTreeInId, 'end', text="pcrDigest=" + bytearray2hex(self.pcrDigest))
        self.insertTreeOutput(self.mTreeInId, 'end', text="pcrsCount=" + bytearray2hex(self.pcrsCount))
        
        self.mTreeInPcrsId = self.insertTreeOutput(self.mTreeInId, 'end', text="pcrs=" + bytearray2hex(self.pcrs))
        for idx,  val in enumerate(self.pcrsList):
            self.insertTreeOutput(self.mTreeInPcrsId, 'end', text="pcrsList[" + str(idx) + "]=" + bytearray2hex(val))
    
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
          
