from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_GetCapability
from TPM_Constants_V2 import TPM_CAP_ALGS, TPM_CAP_COMMANDS, TPM_CAP_TPM_PROPERTIES, TPM_CAP_PCRS, TPM_CAP_PCR_PROPERTIES
from Utils import bytearray2hex, int2bytearray

class TPM2_GetCapability(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_GetCapability
        self.capability = self.interpretParameter(parameters[2])
        self.property = self.interpretParameter(parameters[3])
        self.propertyCount = self.interpretParameter(parameters[4])
                
        #response
        self.moreData = bytearray()
        self.capabilityData = bytearray()
        self.capabilityDataCapability = bytearray()
        self.capabilityDataData = bytearray()
        self.capabilities = []

        if(self.forDoc==True): return

    def prepare(self):
               
        self.commandSize = int2bytearray((len(self.tag)+len(self.commandCode)
                                                            + 4 + len(self.capability)
                                                            + len(self.property)
                                                            + len(self.propertyCount)),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.capability)	
        self.command.extend(self.property)	
        self.command.extend(self.propertyCount)	
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="capability=" + bytearray2hex(self.capability))
        self.insertTreeOutput(self.mTreeInId, 'end', text="property=" + bytearray2hex(self.property))
        self.insertTreeOutput(self.mTreeInId, 'end', text="propertyCount=" + bytearray2hex(self.propertyCount))
    
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        index=10
        if(len(data)>index):
            self.moreData = data[index:(index+1)]
            index+=1
        if(len(data)>index):
            self.capabilityDataCapability = data[index:(index+4)]
            self.capabilityData.extend(self.capabilityDataCapability)
            index += 4
        if(len(data)>index):
            self.capabilityDataData = data[index:]
            self.capabilityData.extend(self.capabilityDataData)
            if(self.capabilityDataCapability == TPM_CAP_ALGS):
                dataLen = (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + data[(index+3)]
                index +=4
                for i in range(0, dataLen):
                    self.capabilities.append(data[(index + i*6):(index + i*6+6)])
            elif(self.capabilityDataCapability == TPM_CAP_COMMANDS):
                dataLen = (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + data[(index+3)]
                index +=4
                for i in range(0, dataLen):
                    self.capabilities.append(data[(index + i*4):(index + i*4+4)])
            elif(self.capabilityDataCapability == TPM_CAP_TPM_PROPERTIES):
                dataLen = (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + data[(index+3)]
                index +=4
                for i in range(0, dataLen):
                    self.capabilities.append(data[(index + i*8):(index + i*8+8)])
            elif(self.capabilityDataCapability == TPM_CAP_PCRS):
                dataCount = (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + data[(index+3)]
                index +=4
                for i in range(0, dataCount):
                    temp = bytearray()
                    temp.extend(data[(index):(index + 2)])
                    index += 2
                    dataLen = data[index]
                    temp.extend(data[(index):(index + 1)])
                    index += 1
                    temp.extend(data[(index):(index + dataLen)])
                    index += dataLen
                    self.capabilities.append(temp)
            elif(self.capabilityDataCapability == TPM_CAP_PCR_PROPERTIES):
                dataCount = (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + data[(index+3)]
                index +=4
                for i in range(0, dataCount):
                    temp = bytearray()
                    temp.extend(data[(index):(index + 4)])
                    index += 4
                    dataLen = data[index]
                    temp.extend(data[(index):(index + 1)])
                    index += 1
                    temp.extend(data[(index):(index + dataLen)])
                    index += dataLen
                    self.capabilities.append(temp)

                    
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
        self.mTreeOutCapabilityDataId = self.insertTreeOutput(self.mTreeOutId, 'end', text="capabilityData=" + bytearray2hex(self.capabilityData))
        self.insertTreeOutput(self.mTreeOutCapabilityDataId, 'end', text="capabilityDataCapability=" + bytearray2hex(self.capabilityDataCapability))
        self.mTreeOutCapabilityDataDataId = self.insertTreeOutput(self.mTreeOutCapabilityDataId, 'end', text="capabilityDataData=" + bytearray2hex(self.capabilityDataData))
        for idx, cap in enumerate(self.capabilities):
            self.insertTreeOutput(self.mTreeOutCapabilityDataDataId, 'end', text="capabilities[" + str(idx) + "]=" + bytearray2hex(cap))
            
