
from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_CertifyCreation
from Utils import bytearray2hex, int2bytearray
from TPM_Session_V2 import Session

class TPM2_CertifyCreation(Command2):
    
    def __init__(self, parameters, manager,  forDoc=False):
        
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_CertifyCreation
        
        self.signHandle = self.interpretParameter(parameters[2])
        self.objectHandle = self.interpretParameter(parameters[3])
        self.sessionHandle = self.interpretParameter(parameters[4])
        self.nonceCaller = self.interpretParameter(parameters[5])
        self.sessionAttribute = self.interpretParameter(parameters[6])
        self.qualifyingData = self.interpretParameter(parameters[7])
        self.creationHash = self.interpretParameter(parameters[8])
        self.inScheme = self.interpretParameter(parameters[9])
        self.creationTicket = self.interpretParameter(parameters[10])
                    
        self.sessionAuthValue = bytearray()
                
        #response
        self.certifyInfo = bytearray()
        self.signature = bytearray()

        if(self.forDoc==True): return

        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.signHandle)
        self.HMACHandles.extend(self.objectHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(int2bytearray(len(self.qualifyingData), 2))	
        self.HMACParameters.extend(self.qualifyingData)	
        self.HMACParameters.extend(int2bytearray(len(self.creationHash), 2))    
        self.HMACParameters.extend(self.inScheme)    
        self.HMACParameters.extend(self.creationTicket)	
        
        self.sessionAuthValue = Session.getAuthValue(self)
                        
    def prepare(self):
                
        lenAuth =  len(self.sessionHandle) + 2 + len(self.nonceCaller) + len(self.sessionAttribute) + 2 + len(self.sessionAuthValue)        
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)
                                                                + len(self.signHandle)
                                                                + len(self.objectHandle)
                                                                + 4+ lenAuth
                                                                + 2 + len(self.qualifyingData)
                                                                + 2 + len(self.creationHash)
                                                                + len(self.inScheme)
                                                                + len(self.creationTicket)
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.signHandle)	
        self.command.extend(self.objectHandle)	
        self.command.extend(int2bytearray(lenAuth, 4))
        self.command.extend(self.sessionHandle)	
        self.command.extend(int2bytearray(len(self.nonceCaller), 2))
        self.command.extend(self.nonceCaller)	
        self.command.extend(self.sessionAttribute)	
        self.command.extend(int2bytearray(len(self.sessionAuthValue), 2))
        self.command.extend(self.sessionAuthValue)	
        self.command.extend(int2bytearray(len(self.qualifyingData), 2))
        self.command.extend(self.qualifyingData)	
        self.command.extend(int2bytearray(len(self.creationHash), 2))
        self.command.extend(self.creationHash)    
        self.command.extend(self.inScheme)	
        self.command.extend(self.creationTicket)    
      
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="signHandle=" + bytearray2hex(self.signHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="objectHandle=" + bytearray2hex(self.objectHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="sessionHandle=" + bytearray2hex(self.sessionHandle))

        self.inTreeAuthorization()
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="qualifyingData=" + bytearray2hex(self.qualifyingData))
        self.insertTreeOutput(self.mTreeInId, 'end', text="creationHash=" + bytearray2hex(self.creationHash))
        self.insertTreeOutput(self.mTreeInId, 'end', text="inScheme=" + bytearray2hex(self.inScheme))
        self.insertTreeOutput(self.mTreeInId, 'end', text="creationTicket=" + bytearray2hex(self.creationTicket))
                
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        index = 10
        index += 4
        if(len(data)>index):
            lenData = (data[index]<<8) + data[(index+1)]
            index +=2
            self.certifyInfo = data[index:(index+lenData)]
            index += lenData
        
        if(len(data)>index):
            lenData = (data[index]<<8) + data[(index+1)]
            index +=2
            self.signature = data[index:(index+lenData)]
            index += lenData

        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="certifyInfo=" + bytearray2hex(self.certifyInfo))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="signature=" + bytearray2hex(self.signature))

        self.outTreeAuthorization(data, index)        
        
        
