from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_ReadClock
from Utils import bytearray2hex, int2bytearray

class TPM2_ReadClock(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_ReadClock
        
        if(self.forDoc==True): return
                                            
    def prepare(self):
                        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
      
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
                
        #response
        self.currentTime = bytearray()
        self.currentTimeTime = bytearray()
        self.currentTimeClockInfo = bytearray()
        self.currentTimeClockInfoClock = bytearray()
        self.currentTimeClockInfoResetCount = bytearray()
        self.currentTimeClockInfoRestartCount = bytearray()
        self.currentTimeClockInfoSafe = bytearray()
        
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        index = 10
        if(len(data)>index):
            self.currentTimeTime = data[index:(index+8)]
            self.currentTime.extend(self.currentTimeTime)
            index +=8
            
        if(len(data)>index):
            self.currentTimeClockInfoClock = data[index:(index+8)]
            self.currentTime.extend(self.currentTimeClockInfoClock)
            self.currentTimeClockInfo.extend(self.currentTimeClockInfoClock)
            index +=8
            
        if(len(data)>index):
            self.currentTimeClockInfoResetCount = data[index:(index+4)]
            self.currentTime.extend(self.currentTimeClockInfoResetCount)
            self.currentTimeClockInfo.extend(self.currentTimeClockInfoResetCount)
            index +=4
        
        if(len(data)>index):
            self.currentTimeClockInfoRestartCount = data[index:(index+4)]
            self.currentTime.extend(self.currentTimeClockInfoRestartCount)
            self.currentTimeClockInfo.extend(self.currentTimeClockInfoRestartCount)
            index +=4
            
        if(len(data)>index):
            self.currentTimeClockInfoSafe = data[index:(index+1)]
            self.currentTime.extend(self.currentTimeClockInfoSafe)
            self.currentTimeClockInfo.extend(self.currentTimeClockInfoSafe)
            index +=1
            
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')

        self.mTreeOutCurrentTimeId = self.insertTreeOutput(self.mTreeOutId, 'end', text="currentTime=" + bytearray2hex(self.currentTime))
        self.insertTreeOutput(self.mTreeOutCurrentTimeId, 'end', text="currentTimeTime=" + bytearray2hex(self.currentTimeTime))
        
        self.mTreeOutCurrentTimeInfoId = self.insertTreeOutput(self.mTreeOutCurrentTimeId, 'end', text="currentTimeClockInfo=" + bytearray2hex(self.currentTimeClockInfo))
        self.insertTreeOutput(self.mTreeOutCurrentTimeInfoId, 'end', text="currentTimeClockInfoClock=" + bytearray2hex(self.currentTimeClockInfoClock))
        self.insertTreeOutput(self.mTreeOutCurrentTimeInfoId, 'end', text="currentTimeClockInfoResetCount=" + bytearray2hex(self.currentTimeClockInfoResetCount))
        self.insertTreeOutput(self.mTreeOutCurrentTimeInfoId, 'end', text="currentTimeClockInfoRestartCount=" + bytearray2hex(self.currentTimeClockInfoRestartCount))
        self.insertTreeOutput(self.mTreeOutCurrentTimeInfoId, 'end', text="currentTimeClockInfoSafe=" + bytearray2hex(self.currentTimeClockInfoSafe))
