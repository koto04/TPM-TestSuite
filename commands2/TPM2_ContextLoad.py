from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_ContextLoad
from Utils import bytearray2hex, int2bytearray

class TPM2_ContextLoad(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_ContextLoad
        self.context  = bytearray()
        self.contextSequence = bytearray()
        self.contextSavedHandle = bytearray()
        self.contextHierarchy = bytearray()
        self.contextContextBlob = bytearray()
                
        #response
        self.loadedHandle  = bytearray()

        if(self.forDoc==True): return
        
        if(isinstance(parameters[2], list)):
            p = parameters[2]
            self.context.extend(p[0])
            self.context.extend(p[1])
            self.context.extend(p[2])
            self.context.extend(int2bytearray(len(p[3]), 2))
            self.context.extend(p[3])
        else:
            self.context = self.interpretParameter(parameters[2])

        index=0
        if(len(self.context)>index):
            self.contextSequence = self.context[index:(index+8)]
            index += 8
        if(len(self.context)>index):
            self.contextSavedHandle = self.context[index:(index+4)]
            index += 4
        if(len(self.context)>index):
            self.contextHierarchy = self.context[index:(index+4)]
            index += 4
        if(len(self.context)>index):
            lenData = (self.context[index]<<8) + (self.context[(index+1)])
            index += 2
            self.contextContextBlob = self.context[index:(index+lenData)]
            index += lenData
            
    def prepare(self):
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag) + len(self.context)),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.context)	
       
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))

        self.mTreeInContextId = self.insertTreeOutput(self.mTreeInId, 'end', text="context=" + bytearray2hex(self.context))
        self.insertTreeOutput(self.mTreeInContextId, 'end', text="contextSequence=" + bytearray2hex(self.contextSequence))
        self.insertTreeOutput(self.mTreeInContextId, 'end', text="contextSavedHandle=" + bytearray2hex(self.contextSavedHandle))
        self.insertTreeOutput(self.mTreeInContextId, 'end', text="contextHierarchy=" + bytearray2hex(self.contextHierarchy))
        self.insertTreeOutput(self.mTreeInContextId, 'end', text="contextContextBlob=" + bytearray2hex(self.contextContextBlob))
            
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        index = 10
        
        if(len(data)>index):
            self.loadedHandle = data[index:(index+4)]
            index += 4
                             
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')        self.insertTreeOutput(self.mTreeOutId, 'end', text="loadedHandle=" + bytearray2hex(self.loadedHandle))
        
