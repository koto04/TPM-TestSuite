from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_GetRandom
from Utils import bytearray2hex, int2bytearray

class TPM2_GetRandom(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_GetRandom
        
        self.bytesRequested = self.interpretParameter(parameters[2])
        
        #response
        self.randomBytes  = bytearray()
        
        if(self.forDoc==True): return

    def prepare(self):
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag) 
                                                            + len(self.bytesRequested)
                                                        ),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.bytesRequested)	
       
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode), tag='TPM_RC')
        self.insertTreeOutput(self.mTreeInId, 'end', text="bytesRequested=" + bytearray2hex(self.bytesRequested))
            
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        index = 10
        
        if(len(data)>index):
            lenRandomBytes = (data[index]<<8) + (data[(index+1)])
            index +=2
            self.randomBytes = data[index:(index+lenRandomBytes)]
            index += lenRandomBytes
                         
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode))
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="randomBytes=" + bytearray2hex(self.randomBytes))
          
