from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_PCR_Read
from Utils import bytearray2hex, int2bytearray

class TPM2_PCR_Read(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_PCR_Read
        self.pcrSelectionIn = bytearray()
        self.pcrSelectionInList = []
                
        #response
        self.pcrUpdateCounter = bytearray()
        self.pcrSelectionOutCount = bytearray()
        self.pcrSelectionOut = bytearray()
        self.pcrValues = bytearray()
        self.pcrSelectionOutAlgos = []
        self.pcrSelectionOutSelect = []
        self.pcrValuesList = []

        if(self.forDoc==True): return

        if(isinstance(parameters[2], list)):
            p = parameters[2]
            self.pcrSelectionInCount = int2bytearray(len(p), 4)
            self.pcrSelectionIn.extend(int2bytearray(len(p), 4))
            for selection in p:
                s = self.interpretParameter(selection)
                self.pcrSelectionInList.append(s)
                self.pcrSelectionIn.extend(s)    
        else:
            self.pcrSelectionIn = self.interpretParameter(parameters[2])
            selectionInCount = (self.pcrSelectionIn[0]<<24) + (self.pcrSelectionIn[1]<<16) + (self.pcrSelectionIn[2]<<8) + (self.pcrSelectionIn[3])
            self.pcrSelectionInCount = self.pcrSelectionIn[0:4]
            index = 4
            for i in range(0, selectionInCount):
                lenSelection = self.pcrSelectionIn[(index+2)]
                self.pcrSelectionInList.append(self.pcrSelectionIn[index:(index+3+lenSelection)])
                index += (3+lenSelection)
        
    def prepare(self):
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag) + len(self.pcrSelectionIn)),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.pcrSelectionIn)	
       
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="pcrSelectionInCount=" + bytearray2hex(self.pcrSelectionInCount))
        
        self.mTreeInSelectionInId = self.insertTreeOutput(self.mTreeInId, 'end', text="pcrSelectionIn=" + bytearray2hex(self.pcrSelectionIn))
        for idx,  val in enumerate(self.pcrSelectionInList):
            self.insertTreeOutput(self.mTreeInSelectionInId, 'end', text="pcrSelectionInList[" + str(idx) + "]=" + bytearray2hex(val))
    
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        index = 10
        if(self.responseCode==bytearray([0x00,  0x00,  0x00,  0x00])):   
            self.pcrUpdateCounter = data[index:(index+4)]
            index +=4
            selectionOutCount = (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)])
            self.pcrSelectionOut = data[index:len(data)]
            self.pcrSelectionOutCount = data[index:(index+4)]
            index +=4
            for i in range(0, selectionOutCount):
                self.pcrSelectionOutAlgos.append(data[index:(index+2)])
                lenSelectDigest = data[(index+2)]
                index +=3
                if(lenSelectDigest>0):
                    self.pcrSelectionOutSelect.append(data[index:(index+lenSelectDigest)])
                else:
                    self.pcrSelectionOutSelect.append(bytearray())
                index += lenSelectDigest
            
            selectionOutCount = (data[index]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)])
            self.pcrValues.extend(data[index:(index+4)])
            index +=4
            for i in range(0, selectionOutCount):
                lenSelectDigest = (data[index]<<8) + (data[(index+1)])
                self.pcrValues.extend(data[index:(index+2)])
                index +=2
                self.pcrValuesList.append(data[index:(index+lenSelectDigest)])
                self.pcrValues.extend(data[index:(index+lenSelectDigest)])
                index += lenSelectDigest
            
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        self.insertTreeOutput(self.mTreeOutId, 'end', text="pcrUpdateCounter=" + bytearray2hex(self.pcrUpdateCounter))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="pcrSelectionOutCount=" + bytearray2hex(self.pcrSelectionOutCount))
        
        self.mTreeOutSelectionOutId = self.insertTreeOutput(self.mTreeOutId, 'end', text="pcrSelectionOut=" + bytearray2hex(self.pcrSelectionOut))
        for idx,  val in enumerate(self.pcrSelectionOutAlgos):
            self.insertTreeOutput(self.mTreeOutSelectionOutId, 'end', text="pcrSelectionOutAlgos[" + str(idx) + "]=" + bytearray2hex(self.pcrSelectionOutAlgos[idx]))
            self.insertTreeOutput(self.mTreeOutSelectionOutId, 'end', text="pcrSelectionOutSelect[" + str(idx) + "]=" + bytearray2hex(self.pcrSelectionOutSelect[idx]))

        self.mTreeOutSelectionOutValuesId = self.insertTreeOutput(self.mTreeOutId, 'end', text="pcrValues=" + bytearray2hex(self.pcrValues))
        for idx,  val in enumerate(self.pcrValuesList):
            self.insertTreeOutput(self.mTreeOutSelectionOutValuesId, 'end', text="pcrValuesList[" + str(idx) + "]=" + bytearray2hex(self.pcrValuesList[idx]))
          
