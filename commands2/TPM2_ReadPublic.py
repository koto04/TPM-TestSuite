from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_ReadPublic, TPM_RC_SUCCESS
from Utils import bytearray2hex, int2bytearray
from TPM_Object import Object

class TPM2_ReadPublic(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_ReadPublic
        
        self.objectHandle = self.interpretParameter(parameters[2])
        
        #response
        self.outPublic  = bytearray()
        self.name = bytearray()
        self.qualifiedName = bytearray()
        
        if(self.forDoc==True): return

    def prepare(self):
        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag) + len(self.objectHandle)),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.objectHandle)	
       
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="objectHandle=" + bytearray2hex(self.objectHandle))
            
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        index = 10
        
        if(len(data)>index):
            lenOutPublic = (data[index]<<8) + (data[(index+1)])
            index +=2
            self.outPublic = data[index:(index+lenOutPublic)]
            index += lenOutPublic
             
        if(len(data)>index):
            lenName = (data[index]<<8) + (data[(index+1)])
            index +=2
            self.name = data[index:(index+lenName)]
            index += lenName

        if(len(data)>index):
            lenQualifiedName = (data[index]<<8) + (data[(index+1)])
            index +=2
            self.qualifiedName = data[index:(index+lenQualifiedName)]
            index += lenQualifiedName
            
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="outPublic=" + bytearray2hex(self.outPublic))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="name=" + bytearray2hex(self.name))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="qualifiedName=" + bytearray2hex(self.qualifiedName))
          
        if(self.responseCode==TPM_RC_SUCCESS):
            Object.setName(self.objectHandle, self.name)
