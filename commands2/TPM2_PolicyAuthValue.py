from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_PolicyAuthValue, TPM_RC_SUCCESS
from Utils import bytearray2hex, int2bytearray
from TPM_Session_V2 import Session

class TPM2_PolicyAuthValue(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_PolicyAuthValue
        self.policySession = self.interpretParameter(parameters[2])
                
        if(self.forDoc==True): return

    def prepare(self):
               
        self.commandSize = int2bytearray((len(self.tag)+len(self.commandCode)+4 + len(self.policySession)),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.policySession)	
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="policySession=" + bytearray2hex(self.policySession))
    
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
          
        if(self.responseCode==TPM_RC_SUCCESS):
            Session.setPolicy(self.policySession,  Session.POLICY_AUTHVALUE)
