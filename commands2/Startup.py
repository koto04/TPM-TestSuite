
from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_ST_NO_SESSIONS, TPM_CC_Startup, TPM_SU_CLEAR
from Utils import int2bytearray

class Startup(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.tag = TPM_ST_NO_SESSIONS
        self.commandCode = TPM_CC_Startup
        self.startupType = TPM_SU_CLEAR
        self.logEnable = False
        if(self.forDoc==True): return

    def prepare(self):
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)+len(self.startupType)),4)
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.startupType)		

    def receive(self, data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length.")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
 
