from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_PolicyCounterTimer
from Utils import bytearray2hex, int2bytearray

class TPM2_PolicyCounterTimer(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_PolicyCounterTimer
        
        self.policySession = self.interpretParameter(parameters[2])
        self.operandB = self.interpretParameter(parameters[3])
        self.offset = self.interpretParameter(parameters[4])
        self.operation = self.interpretParameter(parameters[5])
                                            
        if(self.forDoc==True): return

    def prepare(self):
                        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)+
                                                                len(self.policySession) +
                                                                2 +len(self.operandB) + 
                                                                len(self.offset) + 
                                                                len(self.operation)
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.policySession)	
        self.command.extend(int2bytearray(len(self.operandB), 2))
        self.command.extend(self.operandB)	
        self.command.extend(self.offset)	
        self.command.extend(self.operation)	
      
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="policySession=" + bytearray2hex(self.policySession))        
        self.insertTreeOutput(self.mTreeInId, 'end', text="operandB=" + bytearray2hex(self.operandB))
        self.insertTreeOutput(self.mTreeInId, 'end', text="offset=" + bytearray2hex(self.offset))
        self.insertTreeOutput(self.mTreeInId, 'end', text="operation=" + bytearray2hex(self.operation))
                
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
                
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
        
