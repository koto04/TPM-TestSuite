from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_PolicyAuthorize
from Utils import bytearray2hex, int2bytearray

class TPM2_PolicyAuthorize(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_PolicyAuthorize
        
        self.policySession = self.interpretParameter(parameters[2])
        self.approvedPolicy = self.interpretParameter(parameters[3])
        self.policyRef = self.interpretParameter(parameters[4])
        self.keySign = self.interpretParameter(parameters[5])
        self.checkTicket = bytearray()
        self.checkTicketTag = bytearray()
        self.checkTicketHierarchy = bytearray()
        self.checkTicketDigest = bytearray()
        
        if(self.forDoc==True): return

        if(isinstance(parameters[6], list)):
            
            p = parameters[6]
            self.checkTicketTag = self.interpretParameter(p[0])
            self.checkTicketHierarchy = self.interpretParameter(p[1])
            self.checkTicketDigest = self.interpretParameter(p[2])

            self.checkTicket.extend(self.checkTicketTag)
            self.checkTicket.extend(self.checkTicketHierarchy)
            self.checkTicket.extend(int2bytearray(len(self.checkTicketDigest), 2))
            self.checkTicket.extend(self.checkTicketDigest)
            
        else:
            self.checkTicket = self.interpretParameter(parameters[6])
            index = 0
            
            self.checkTicketTag = self.checkTicket[index:(index+2)]
            index+=2
            
            self.checkTicketHierarchy = self.checkTicket[index:(index+4)]
            index+=4
            
            lenCheckTicketDigest = (self.checkTicket[index]<<8) + (self.checkTicket[(index+1)])
            index+=2
            self.checkTicketDigest = self.checkTicket[index:(index+lenCheckTicketDigest)]
        
    
    def prepare(self):
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag) +
                                                                                len(self.policySession) +
                                                                                2 + len(self.approvedPolicy) + 
                                                                                2 + len(self.policyRef) +
                                                                                2 + len(self.keySign) + 
                                                                                len(self.checkTicket)),4);
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.policySession)
        self.command.extend(int2bytearray(len(self.approvedPolicy), 2))
        self.command.extend(self.approvedPolicy)	
        self.command.extend(int2bytearray(len(self.policyRef), 2))
        self.command.extend(self.policyRef)	
        self.command.extend(int2bytearray(len(self.keySign), 2))
        self.command.extend(self.keySign)	
        self.command.extend(self.checkTicket) 

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="policySession=" + bytearray2hex(self.policySession))
        self.insertTreeOutput(self.mTreeInId, 'end', text="approvedPolicy=" + bytearray2hex(self.approvedPolicy))
        self.insertTreeOutput(self.mTreeInId, 'end', text="policyRef=" + bytearray2hex(self.policyRef))
        self.insertTreeOutput(self.mTreeInId, 'end', text="keySign=" + bytearray2hex(self.keySign))
    
        self.mTreeInCheckTicketId = self.insertTreeOutput(self.mTreeInId, 'end', text="checkTicket=" + bytearray2hex(self.checkTicket))
        self.insertTreeOutput(self.mTreeInCheckTicketId, 'end', text="checkTicketTag=" + bytearray2hex(self.checkTicketTag))
        self.insertTreeOutput(self.mTreeInCheckTicketId, 'end', text="checkTicketHierarchy=" + bytearray2hex(self.checkTicketHierarchy))
        self.insertTreeOutput(self.mTreeInCheckTicketId, 'end', text="checkTicketDigest=" + bytearray2hex(self.checkTicketDigest))
        
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
                
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
            
