from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_RSA_Decrypt, TPM_RC_SUCCESS
from Utils import bytearray2hex, int2bytearray
from TPM_Session_V2 import Session

class TPM2_RSA_Decrypt(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_RSA_Decrypt
        
        self.keyHandle = self.interpretParameter(parameters[2])
        self.sessionHandle = self.interpretParameter(parameters[3])
        self.nonceCaller = self.interpretParameter(parameters[4])
        self.sessionAttribute = self.interpretParameter(parameters[5])
        self.cipherText = self.interpretParameter(parameters[6])
        self.inScheme = self.interpretParameter(parameters[7])
        self.label = self.interpretParameter(parameters[8])
                
        self.sessionAuthValue = bytearray()
                
        #response
        self.message = bytearray()

        if(self.forDoc==True): return

        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.keyHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(int2bytearray(len(self.cipherText), 2))
        self.HMACParameters.extend(self.cipherText)	
        self.HMACParameters.extend(self.inScheme)	
        self.HMACParameters.extend(int2bytearray(len(self.label), 2))
        self.HMACParameters.extend(self.label)	
        
        self.sessionAuthValue = Session.getAuthValue(self)
                
        
    def prepare(self):
        
        lenAuth =  len(self.sessionHandle) + 2 + len(self.nonceCaller) + len(self.sessionAttribute) + 2 + len(self.sessionAuthValue)        
        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)
                                                            + len(self.keyHandle) 
                                                            + 4 + lenAuth 
                                                            + 2 + len(self.cipherText) 
                                                            + len(self.inScheme) 
                                                            + 2 + len(self.label)
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.keyHandle)	
        self.command.extend(int2bytearray(lenAuth, 4))
        self.command.extend(self.sessionHandle)	
        self.command.extend(int2bytearray(len(self.nonceCaller), 2))
        self.command.extend(self.nonceCaller)	
        self.command.extend(self.sessionAttribute)	
        self.command.extend(int2bytearray(len(self.sessionAuthValue), 2))
        self.command.extend(self.sessionAuthValue)	
        
        self.command.extend(int2bytearray(len(self.cipherText), 2))
        self.command.extend(self.cipherText)	
        self.command.extend(self.inScheme)	
        self.command.extend(int2bytearray(len(self.label), 2))
        self.command.extend(self.label)	
               
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="keyHandle=" + bytearray2hex(self.keyHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="sessionHandle=" + bytearray2hex(self.sessionHandle))
        
        self.inTreeAuthorization()
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="cipherText=" + bytearray2hex(self.cipherText))
        self.insertTreeOutput(self.mTreeInId, 'end', text="inScheme=" + bytearray2hex(self.inScheme))
        self.insertTreeOutput(self.mTreeInId, 'end', text="label=" + bytearray2hex(self.label))
        
            
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')

        index = 14
        if(self.responseCode==TPM_RC_SUCCESS):
        
            lenMessage = (data[index]<<8) + data[(index+1)]
            index +=2
            self.message = data[(index):(index+lenMessage)]
            index +=lenMessage
            
            
        self.insertTreeOutput(self.mTreeOutId, 'end', text="message=" + bytearray2hex(self.message))
        self.outTreeAuthorization(data, index)
