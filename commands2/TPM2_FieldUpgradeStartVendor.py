
from TPM_Commands_V2 import Command2
from TPM_Constants_V2 import TPM_CC_FieldUpgradeStartVendor
from Utils import bytearray2hex, int2bytearray
from TPM_Session_V2 import Session

class TPM2_FieldUpgradeStartVendor(Command2):
    
    def __init__(self, parameters, manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.commandCode = TPM_CC_FieldUpgradeStartVendor
        

        self.authorization = self.interpretParameter(parameters[2])
        self.sessionHandle = self.interpretParameter(parameters[3])
        self.nonceCaller = self.interpretParameter(parameters[4])
        self.sessionAttribute = self.interpretParameter(parameters[5])
        self.subCommand = self.interpretParameter(parameters[6])
        self.signedData = self.interpretParameter(parameters[7])
                    
        self.sessionAuthValue = bytearray()
                
        if(self.forDoc==True): return

        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.authorization)
        
        self.HMACParameters = bytearray()
        
        self.HMACParameters.extend(self.subCommand)	
        self.HMACParameters.extend(int2bytearray(len(self.signedData), 2))
        self.HMACParameters.extend(self.signedData)	
        
        self.sessionAuthValue = Session.getAuthValue(self)
                        
    def prepare(self):


        lenAuth =  len(self.sessionHandle) + 2 + len(self.nonceCaller) + len(self.sessionAttribute) + 2 + len(self.sessionAuthValue)        
        

        self.commandSize = int2bytearray((4+len(self.commandCode)+len(self.tag)+
                                                                len(self.authorization) + 
                                                                4 + lenAuth + 
                                                                len(self.subCommand) +
                                                                2 + len(self.signedData)
                                                                
                                                        ),4);
        
        self.command.extend(self.tag)
        self.command.extend(self.commandSize)
        self.command.extend(self.commandCode)
        self.command.extend(self.authorization)	
        self.command.extend(int2bytearray(lenAuth, 4))
        self.command.extend(self.sessionHandle)	
        self.command.extend(int2bytearray(len(self.nonceCaller), 2))
        self.command.extend(self.nonceCaller)	
        self.command.extend(self.sessionAttribute)	
        self.command.extend(int2bytearray(len(self.sessionAuthValue), 2))
        self.command.extend(self.sessionAuthValue)	
        self.command.extend(self.subCommand)	
        self.command.extend(int2bytearray(len(self.signedData), 2))
        self.command.extend(self.signedData)	
      
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandSize=" + bytearray2hex(self.commandSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="commandCode=" + bytearray2hex(self.commandCode))
        self.insertTreeOutput(self.mTreeInId, 'end', text="authorization=" + bytearray2hex(self.authorization))
        self.insertTreeOutput(self.mTreeInId, 'end', text="sessionHandle=" + bytearray2hex(self.sessionHandle))

        self.inTreeAuthorization()
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="subCommand=" + bytearray2hex(self.subCommand))
        self.insertTreeOutput(self.mTreeInId, 'end', text="signedData=" + bytearray2hex(self.signedData))
                
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.responseSize = data[2:6]
        self.responseCode = data[6:10]
        self.data = bytearray()
        
        index = 10
        index +=4        
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseSize=" + bytearray2hex(self.responseSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="responseCode=" + bytearray2hex(self.responseCode), tag='TPM_RC')
        
        self.outTreeAuthorization(data, index)        
        
        
