
from TS_Globals import STATUS_OK, STATUS_TX_TIMEOUT, STATUS_RX_TIMEOUT
from TS_Globals import EQ, NEQ, LT, GT, LTE, GTE

from TestInterpreter import TestManagerGateway

from Utils import hex2bytearray, bytearray2hex

def getPass():
    return str(TestManagerGateway.testManager.resultCounter(RESULT_PASS)) # @UndefinedVariable
def getFail():
    return str(TestManagerGateway.testManager.resultCounter(RESULT_FAILURE)) # @UndefinedVariable
def getException():
    return str(TestManagerGateway.testManager.resultCounter(RESULT_EXCEPTION)) # @UndefinedVariable
 
def checkResponseCode(value, evaluate=True):

    if (checkCommunication()==False): return False
    
    rc = TestManagerGateway.testManager.lastReturnCode() # @UndefinedVariable
    if len(rc)<2:
        if evaluate==True:
            TestManagerGateway.testManager.increaseException('No return code received, check not possible.')  # @UndefinedVariable
        else:
            TestManagerGateway.testManager.increaseException('No return code received, check not possible.')  # @UndefinedVariable
        return False
    
    TestManagerGateway.testManager.writeLog(TestManagerGateway.testManager.interpretStructure('TPM_RC',rc)) # @UndefinedVariable
    checkvalue = rc
        #chech format one RC
    if isinstance(value, basestring):
        value = hex2bytearray(value)
    else:    
        if(checkvalue[3]&0x80==0x80):
            checkvalue[2] &= 0x00
            checkvalue[3] &= 0xBF
        
    if(checkvalue==value):
        TestManagerGateway.testManager.writeLog("Response Code Check OK") # @UndefinedVariable
        if evaluate==True:
            TestManagerGateway.testManager.increasePass() # @UndefinedVariable
        return True
    else:
        TestManagerGateway.testManager.writeLog("Response Code is not as expected.\nExpected: " + bytearray2hex(value) + ", Response Code: " + bytearray2hex(checkvalue)) # @UndefinedVariable
        if evaluate==True:
            TestManagerGateway.testManager.increaseFailure() # @UndefinedVariable
        return False

def checkCommunication(status=STATUS_OK):
    ret=False
    if (TestManagerGateway.testManager.communicationStatus()==STATUS_OK): # @UndefinedVariable
        if(status==STATUS_OK):
            ret=True
        else:
            TestManagerGateway.testManager.increaseFailure("Unexpectedly communication successful.") # @UndefinedVariable
            
    elif (TestManagerGateway.testManager.communicationStatus()==STATUS_TX_TIMEOUT): # @UndefinedVariable
        if(status==STATUS_TX_TIMEOUT):
            TestManagerGateway.testManager.increasePass("Expect TX timeout") # @UndefinedVariable
            ret=True
        else:
            TestManagerGateway.testManager.increaseFailure("TX timeout occured.") # @UndefinedVariable
            
    elif (TestManagerGateway.testManager.communicationStatus()==STATUS_RX_TIMEOUT): # @UndefinedVariable
        if(status==STATUS_RX_TIMEOUT):
            TestManagerGateway.testManager.increasePass("Expect RX timeout") # @UndefinedVariable
            ret=True
        else:
            TestManagerGateway.testManager.increaseFailure("RX timeout occured.") # @UndefinedVariable
            raise Exception("RX timeout occured.")
    return ret

def checkData(data,  value,  start=0,  length=0,  operation=EQ):

    if (checkCommunication()==False): return
    
    if isinstance(data, basestring):
        data = hex2bytearray(data)
    elif isinstance(data, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of data") # @UndefinedVariable
        return
    if isinstance(value, basestring):
        value = hex2bytearray(value)
    elif isinstance(value, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype of value") # @UndefinedVariable
        return
    if(length==0):
        end=len(data)
    else:
        end=start+length
    checkvalue = data[start:end]

    if(operation==EQ):
        if (checkvalue==value):
            TestManagerGateway.testManager.increasePass("Data Check OK") # @UndefinedVariable
        else:
            if(start==0 and length==0):
                TestManagerGateway.testManager.increaseFailure("Data Check Fails: " + bytearray2hex(data) + " != " + bytearray2hex(value)) # @UndefinedVariable
            else:
                TestManagerGateway.testManager.increaseFailure("Data Check Fails: " + bytearray2hex(data) + "[" + str(start) + ":" + str(end) + "] != " + bytearray2hex(value)) # @UndefinedVariable            
    
    if(operation==NEQ):
        if (checkvalue!=value):
            TestManagerGateway.testManager.increasePass("Data Check OK") # @UndefinedVariable
            
        else:
            if(start==0 and length==0):
                TestManagerGateway.testManager.increaseFailure("Data Check Fails: " + bytearray2hex(data) + " = " + bytearray2hex(value)) # @UndefinedVariable
            else:
                TestManagerGateway.testManager.increaseFailure("Data Check Fails: " + bytearray2hex(data) + "[" + str(start) + ":" + str(end) + "] = " + bytearray2hex(value)) # @UndefinedVariable          
    if(operation==GTE):
        if (checkvalue==value):
            TestManagerGateway.testManager.increasePass("Data Check OK") # @UndefinedVariable
        else:
            x = 0
            failure = 0
            for i in range(start, end):
                if(data[i]<value[x]):
                    TestManagerGateway.testManager.increaseFailure("Data Check Fails: " + bytearray2hex(data) + " < " + bytearray2hex(value) + " on position " + str(i)) # @UndefinedVariable
                    failure = 1
                    break
                if(data[i]>value[x]):
                    break
                x += 1
            if(failure==0):
                TestManagerGateway.testManager.increasePass("Data Check OK") # @UndefinedVariable
                
    if(operation==GT):
        if (checkvalue==value):
            TestManagerGateway.testManager.increaseFailure("Data Check Fails: " + bytearray2hex(data) + " = " + bytearray2hex(value)) # @UndefinedVariable
        else:
            x = 0
            failure = 0
            for i in range(start, end):
                if(data[i]<value[x]):
                    TestManagerGateway.testManager.increaseFailure("Data Check Fails: " + bytearray2hex(data) + " < " + bytearray2hex(value) + " on position " + str(i)) # @UndefinedVariable
                    failure = 1
                    break
                if(data[i]>value[x]):
                    break
                x += 1
            if(failure==0):
                TestManagerGateway.testManager.increasePass("Data Check OK") # @UndefinedVariable

    if(operation==LTE):
        if (checkvalue==value):
            TestManagerGateway.testManager.increasePass("Data Check OK") # @UndefinedVariable
        else:
            x = 0
            failure = 0
            for i in range(start, end):
                if(data[i]>value[x]):
                    TestManagerGateway.testManager.increaseFailure("Data Check Fails: " + bytearray2hex(data) + " > " + bytearray2hex(value) + " on position " + str(i)) # @UndefinedVariable
                    failure = 1
                    break
                if(data[i]<value[x]):
                    break
                x += 1
            if(failure==0):
                TestManagerGateway.testManager.increasePass("Data Check OK") # @UndefinedVariable

    if(operation==LT):
        if (checkvalue==value):
            TestManagerGateway.testManager.increaseFailure("Data Check Fails: " + bytearray2hex(data) + " = " + bytearray2hex(value)) # @UndefinedVariable
        else:
            x = 0
            failure = 0
            for i in range(start, end):
                if(data[i]>value[x]):
                    TestManagerGateway.testManager.increaseFailure("Data Check Fails: " + bytearray2hex(data) + " > " + bytearray2hex(value) + " on position " + str(i)) # @UndefinedVariable
                    failure = 1
                    break
                if(data[i]<value[x]):
                    break
                x += 1
            if(failure==0):
                TestManagerGateway.testManager.increasePass("Data Check OK") # @UndefinedVariable

    else:
        pass
            
            
def checkDataLen(data,  length, typ="EQ"):
    if (checkCommunication()==False): return

    if isinstance(data, bytearray):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype") # @UndefinedVariable
        return
    if isinstance(length, (int, long, float, complex)):
        pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown datatype") # @UndefinedVariable
        return

    if (typ=='NEQ'):
        if(len(data)!=length):
            TestManagerGateway.testManager.increasePass("Data Length OK") # @UndefinedVariable
        else:
            TestManagerGateway.testManager.increaseFailure("Data Length Check Fails, expected: not equal " + str(length) + ", received: " + str(len(data))) # @UndefinedVariable    
    
    elif (typ=='GT'):
        if(len(data)>length):
            TestManagerGateway.testManager.increasePass("Data Length OK") # @UndefinedVariable
        else:
            TestManagerGateway.testManager.increaseFailure("Data Length Check Fails, expected: greater than " + str(length) + ", received: " + str(len(data))) # @UndefinedVariable
        
    elif (typ=='GTE'):
        if(len(data)>=length):
            TestManagerGateway.testManager.increasePass("Data Length OK") # @UndefinedVariable
        else:
            TestManagerGateway.testManager.increaseFailure("Data Length Check Fails, expected: greater than or equal " + str(length) + ", received: " + str(len(data))) # @UndefinedVariable 
        
    elif (typ=='LT'):
        if(len(data)<length):
            TestManagerGateway.testManager.increasePass("Data Length OK") # @UndefinedVariable
        else:
            TestManagerGateway.testManager.increaseFailure("Data Length Check Fails, expected: less than " + str(length) + ", received: " + str(len(data))) # @UndefinedVariable
        
    elif (typ=='LTE'):
        if(len(data)<=length):
            TestManagerGateway.testManager.increasePass("Data Length OK") # @UndefinedVariable
        else:
            TestManagerGateway.testManager.increaseFailure("Data Length Check Fails, expected: less than or equal " + str(length) + ", received: " + str(len(data))) # @UndefinedVariable    
            
    else:        
        if(len(data)==length):
            TestManagerGateway.testManager.increasePass("Data Length OK") # @UndefinedVariable
        else:
            TestManagerGateway.testManager.increaseFailure("Data Length Check Fails, expected: " + str(length) + ", received: " + str(len(data))) # @UndefinedVariable 

def checkFuzzyResponse():  
    checkResponseCode(TestManagerGateway.testManager.fuzzyResponse()) # @UndefinedVariable
