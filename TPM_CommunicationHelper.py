
import platform
if platform.system() == "Windows":
    from TPM_CommunicationDummy import TPM_CommunicationDummy as TPM_Communication
    from SPIComDummy import SPICom
    from LPCComDummy import LPCCom
    from SPIComDummy import SPICom as DummyCom

else:
    from TPM_Communication import TPM_Communication
    from SPICom import SPICom
    from LPCCom import LPCCom
    from DummyCom import DummyCom
from TPMRegister import TPM_STS, TPM_DID_VID
from TS_Globals import TS_Globals

def detectDevice():
    intf = None
    intf_type = None
    tpm_type = None
    
    for _ in range(1):
        
        intf = SPICom()
        intf.reset()
        intf.readRegister(TPM_DID_VID)
        TPM_DID_VID.Print()
        if(TPM_DID_VID.vid!=0x0000) and (TPM_DID_VID.vid!=0xffff):
            intf_type = TS_Globals.TPM_INTERFACE_SPI
            intf.readRegister(TPM_STS)
            tpm_type = TPM_STS.tpmFamily
            return tpm_type, intf_type

        intf = LPCCom()
        intf.reset()
        intf.readRegister(TPM_DID_VID)
        TPM_DID_VID.Print()
        if(TPM_DID_VID.vid!=0xffff):
            intf_type = TS_Globals.TPM_INTERFACE_LPC
            intf.readRegister(TPM_STS)
            TPM_STS.Print()
            tpm_type = TPM_STS.tpmFamily
            print "lpc interface"
            return tpm_type, intf_type
        
        print "dummy interface"
        DummyCom()
    #intf.clearInterface()
    return None, TS_Globals.TPM_INTERFACE_DUMMY
