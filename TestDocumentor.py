from TPM_Constants_V1 import *
from TPM_Constants_V2 import *
from TS_Globals import *


class TestDocumentor():

    fileHandle= None
    usedCommands = []
    writeEnable = False
    step = 1

    sDocumentor = None

    @staticmethod
    def addCommand(command):
        command = command.replace("TPM_ORD_", "TPM_")
        command = command.replace("TSC_ORD_", "TSC_")
        command = command.replace("TPM_CC_", "TPM2_")
        if command in TestDocumentor.sDocumentor.usedCommands:
            pass
        else:
            TestDocumentor.sDocumentor.usedCommands.append(command)

    @staticmethod
    def write(text):
        TestDocumentor.sDocumentor.writeDoc(text)
        
    @staticmethod
    def writeStep(text):
        TestDocumentor.sDocumentor.writeStepDoc(text)

    def __init__(self, scriptFileHandle, docFileHandle):
        
        self.scriptFileHandle = scriptFileHandle
        self.script = scriptFileHandle.read()
        self.fileHandle = docFileHandle
        self.usedCommands = []
        self.step = 1
        
        TestDocumentor.sDocumentor = self
        
    def writeStepDoc(self, text):    
        self.writeDoc("[Step " + "%03d" % self.step + "] " + text)
        self.step +=1

    def writeDoc(self, text):
        if(self.writeEnable==True):
            self.fileHandle.write(text + "\n")

    def prepareCommandLine(self, commandLine):
        line = commandLine.strip(' \t')
        pos_start = line.find('send([')
        if(pos_start<0):
            pos_start = line.find('initialize([')
        if(pos_start>=0):
            pos_end = line.find(',')
            line = line[pos_start:pos_end+1] + "])"
        return line
    
    def documentTest(self):
        self.writeEnable = True
        line_no = 0
        for i in self.script.splitlines():
            line_no+=1
            try:
                exec self.prepareCommandLine(i)
            except Exception,  e:
                pass
        self.writeDoc("")
        self.writeDoc("Used commands:")
        self.writeDoc("---------------")
        self.usedCommands.sort()
        for c in self.usedCommands:
            self.write(c.replace("TPM_CC_", "TPM2_"))
        self.writeDoc("")
        self.writeDoc("")
        self.writeDoc("")
        self.writeEnable = False
    
    def addCommands2Matrix(self, fileName,  commands):        
        self.writeEnable = False
        line_no = 0
        for i in self.script.splitlines():
            line_no+=1
            try:
                exec self.prepareCommandLine(i)
            except Exception, e:
                print ("%03d" % (line_no,)) + " " + (str(e))
                pass
        matrixRow = []
        matrixRow.append(fileName)
        for i in range(0, len(commands)):
            matrixRow.append("")
        
        for c in self.usedCommands:
            matrixRow[((commands.index(c)))] = "X"
        
        self.writeEnable = True
        self.writeDoc(";".join(matrixRow))
        self.writeEnable = False
        
    def uniqueList(self, seq):
        seen = set()
        seen_add = seen.add
        return [ x for x in seq if not (x in seen or seen_add(x))]

        
def send(params):
    TestDocumentor.addCommand(params[0])
    return 0    

def initialize(params):
    TestDocumentor.addCommand(params[0])
    return 0    

def header(name,  description):
    TestDocumentor.write("===============================================================")
    TestDocumentor.write("Test: " + name + "")
    TestDocumentor.write("Description: " + description)        
    TestDocumentor.write("")
    TestDocumentor.write("===============================================================")
    TestDocumentor.write("")

def step(text):        
    TestDocumentor.writeStep(text)

def log(text):
    TestDocumentor.write(" -  " + text)

def setVDD(vdd):
    if(vdd==OFF):
        TestDocumentor.write(" -  Power OFF") 
    elif(vdd==ON):
        TestDocumentor.write(" -  Power ON") 

def TPMreset():
    TestDocumentor.write(" -  TPM reset") 

def setLocality(locality):    
    TestDocumentor.write(" -  Set Locality to " + str(locality))
