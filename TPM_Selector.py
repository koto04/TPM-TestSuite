import TestManager

from Tkinter import Button, Label, Frame
from Tkinter import StringVar
from Tkinter import TOP, W, YES

from TPM_CommunicationHelper import detectDevice
from TS_Globals import TS_Globals

import TestSuite
from GitRepositoryUtils import versionDate

class TPM_Selector():
    
    
    def __init__(self, master):
        tpmSelector = self.tpmSelector = Frame(master, height=200, width=300)
        tpmFamily, tpmInterface = detectDevice()
        versionText = StringVar()
        
        self.lblVersionInfo = Label(master, textvariable=versionText)
        versionText.set("version " + TestSuite.__version__ + ", last update " + versionDate())
        
        if(tpmFamily==None):
        
            tpmSelectorLabel = Label(tpmSelector, text="Please select TPM version", height=0, width=300)
            tpmSelectorLabel.pack()
            
            
            tpm12spi_button = Button(tpmSelector, width=20, text="TPM 1.2 SPI", command=lambda: self.setTPMVersion(0, TS_Globals.TPM_INTERFACE_SPI))
            tpm20spi_button = Button(tpmSelector, width=20, text="TPM 2.0 SPI", command=lambda: self.setTPMVersion(1, TS_Globals.TPM_INTERFACE_SPI))
            tpm12lpc_button = Button(tpmSelector, width=20, text="TPM 1.2 LPC", command=lambda: self.setTPMVersion(0, TS_Globals.TPM_INTERFACE_LPC))
            tpm20lpc_button = Button(tpmSelector, width=20, text="TPM 2.0 LCP", command=lambda: self.setTPMVersion(1, TS_Globals.TPM_INTERFACE_LPC))
            
            
            
            tpm20spi_button.pack(side='bottom', padx=10, pady=5)
            tpm20lpc_button.pack(side='bottom', padx=10, pady=5)
            tpm12spi_button.pack(side='bottom', padx=10, pady=5)
            tpm12lpc_button.pack(side='bottom', padx=10, pady=5)
            tpmSelector.pack()

        else:
            if (tpmFamily==3): tpmFamily=0   # hack for TPM 1.2 from IFX
            if (tpmFamily == 0): tpmFamilyText = "TPM 1.2 "
            self.setTPMVersion(tpmFamily, tpmInterface,False)
            tpmChipIdent = None
            if tpmFamily == 1: tpmFamilyText = "TPM 2.0 "

            testManager = TestManager.TestManager(None)
            tpmChipIdent = Label(tpmSelector, text=testManager.shortChipInfo(), font=("Arial", 10), height=0, width=300)
                
            tpmFamilyText += tpmInterface
            tpmAutodetectLabel = Label(tpmSelector, text="Auto detected", height=0, width=300)
            tpmSelectorLabel = Label(tpmSelector, text= tpmFamilyText, font=("Helvetica", 16), height=0, width=300)
            tpmAutodetectLabel.pack(side='top', pady=15)
            tpmSelectorLabel.pack()
            tpmSelector.pack()
            if(tpmChipIdent!=None):
                tpmChipIdent.pack(side='top', pady=0)

            ok_button = Button(tpmSelector, width=20, text="OK", command=lambda: self.close())
            ok_button.pack(side='bottom', padx=10, pady=25)

        self.lblVersionInfo.pack(side=TOP, anchor=W, expand=YES)
            
    def setTPMVersion(self,  version, com_interface, destroy=True):
        print version     
        TS_Globals.TS_TPM_VERSION = version
        TS_Globals.TS_TPM_INTERFACE = com_interface
        reload(TestManager)
        if(destroy==True):
            self.close()
            
    def close(self):
        self.lblVersionInfo.destroy()
        self.tpmSelector.destroy()
