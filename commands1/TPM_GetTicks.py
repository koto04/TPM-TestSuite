from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_GetTicks
from Utils import int2bytearray, bytearray2hex

class TPM_GetTicks(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_GetTicks
        
        #response
        self.currentTime = bytearray()
        self.currentTimeTag = bytearray()
        self.currentTimeCurrentTicks = bytearray()
        self.currentTimeTickRate = bytearray()
        self.currentTimeTickNonce = bytearray()
        
        if(self.forDoc==True): return

    def prepare(self):
        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)        
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))

    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]

        index = 10
        if(len(data)>index):
            self.currentTimeTag = data[index:(index+2)]
            index += 2
        if(len(data)>index):
            self.currentTimeCurrentTicks = data[index:(index+8)]
            index += 8
        if(len(data)>index):
            self.currentTimeTickRate = data[index:(index+2)]
            index += 2
        if(len(data)>index):
            self.currentTimeTickNonce = data[index:(index+20)]
            index += 20
        
        self.currentTime.extend(self.currentTimeTag)
        self.currentTime.extend(self.currentTimeCurrentTicks)
        self.currentTime.extend(self.currentTimeTickRate)
        self.currentTime.extend(self.currentTimeTickNonce)
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.mTreeOutCurrentTimeId = self.insertTreeOutput(self.mTreeOutId, 'end', text="currentTime=" + bytearray2hex(self.currentTime))

        self.insertTreeOutput(self.mTreeOutCurrentTimeId, 'end', text="currentTimeTag=" + bytearray2hex(self.currentTimeTag))
        self.insertTreeOutput(self.mTreeOutCurrentTimeId, 'end', text="currentTimeCurrentTicks=" + bytearray2hex(self.currentTimeCurrentTicks))
        self.insertTreeOutput(self.mTreeOutCurrentTimeId, 'end', text="currentTimeTickRate=" + bytearray2hex(self.currentTimeTickRate))
        self.insertTreeOutput(self.mTreeOutCurrentTimeId, 'end', text="currentTimeTickNonce=" + bytearray2hex(self.currentTimeTickNonce))
