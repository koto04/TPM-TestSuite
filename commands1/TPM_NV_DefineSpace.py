from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_NV_DefineSpace
from TPM_Constants_V1 import TPM_KH_OWNER, TPM_TAG_RQU_AUTH1_COMMAND, TPM_SUCCESS
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session
from TPM_Object import Object
from TestInterpreterCrypto import ADIP

class TPM_NV_DefineSpace(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_NV_DefineSpace
        self.pubInfo = bytearray() #self.interpretParameter(parameters[2])
        self.pubInfoTag = bytearray()
        self.pubInfoNvIndex = bytearray()
        self.pubInfoPcrInfoRead = bytearray()
        self.pubInfoPcrInfoReadPcrSelection = bytearray()
        self.pubInfoPcrInfoReadLocalityAtRelease = bytearray()
        self.pubInfoPcrInfoReadDigestAtRelease = bytearray()
        self.pubInfoPcrInfoWrite = bytearray()
        self.pubInfoPcrInfoWritePcrSelection = bytearray()
        self.pubInfoPcrInfoWriteLocalityAtRelease = bytearray()
        self.pubInfoPcrInfoWriteDigestAtRelease = bytearray()
        self.pubInfoPermission = bytearray()
        self.pubInfoPermissionTag = bytearray()
        self.pubInfoPermissionAttributes = bytearray()
        self.pubInfoBReadSTClear = bytearray()
        self.pubInfoBWriteSTClear = bytearray()
        self.pubInfoBWriteDefine = bytearray()
        self.pubInfoDataSize = bytearray()
        self.encAuth = self.interpretParameter(parameters[3])
        if(self.tag==TPM_TAG_RQU_AUTH1_COMMAND):
            self.authHandle = self.interpretParameter(parameters[4])
            self.nonceOdd = self.interpretParameter(parameters[5])
            self.continueAuthSession = self.interpretParameter(parameters[6])
        else:
            self.authHandle = bytearray()
            self.nonceOdd = bytearray()
            self.continueAuthSession = bytearray()
            
        self.pubAuth = self.ownerAuth = bytearray()
        self.authValue = bytearray()
        
        #response
        
        if(self.forDoc==True): return

        if(isinstance(parameters[2], list)):
            p = parameters[2]
            self.pubInfo.extend(self.interpretParameter(p[0]))
            self.pubInfo.extend(self.interpretParameter(p[1]))
            if(isinstance(p[2],  list)):
                ps = p[2]
                self.pubInfo.extend(self.interpretParameter(ps[0]))
                self.pubInfo.extend(self.interpretParameter(ps[1]))
                if(isinstance(ps[2],list)):
                    ps[2] = self.calcCompositeHash(ps[2])               
                self.pubInfo.extend(self.interpretParameter(ps[2]))
                
            else:
                self.pubInfo.extend(self.interpretParameter(p[2]))
            if(isinstance(p[3],  list)):
                ps = p[3]
                self.pubInfo.extend(self.interpretParameter(ps[0]))
                self.pubInfo.extend(self.interpretParameter(ps[1]))
                if(isinstance(ps[2],list)):
                    ps[2] = self.calcCompositeHash(ps[2])
                self.pubInfo.extend(self.interpretParameter(ps[2]))            
            else:
                self.pubInfo.extend(self.interpretParameter(p[3]))
            if(isinstance(p[4],  list)):
                ps = p[4]
                self.pubInfo.extend(self.interpretParameter(ps[0]))
                self.pubInfo.extend(self.interpretParameter(ps[1]))
            else:
                self.pubInfo.extend(self.interpretParameter(p[4]))
                
            self.pubInfo.extend(self.interpretParameter(p[5]))
            self.pubInfo.extend(self.interpretParameter(p[6]))
            self.pubInfo.extend(self.interpretParameter(p[7]))
            self.pubInfo.extend(self.interpretParameter(p[8]))
        else:
            self.pubInfo = self.interpretParameter(parameters[2])
            
        index = 0
        self.pubInfoTag = self.pubInfo[index:(index+2)]
        index +=2
        self.pubInfoNvIndex = self.pubInfo[index:(index+4)]
        index +=4
        
        dataLen = (self.pubInfo[index]<<8) + (self.pubInfo[index+1]<<0)
        self.pubInfoPcrInfoRead = self.pubInfo[index:(index+dataLen+ 2 + 1 + 20)]
        self.pubInfoPcrInfoReadPcrSelection = self.pubInfo[index:(index+dataLen+ 2)]
        index += (dataLen+2)
        self.pubInfoPcrInfoReadLocalityAtRelease = self.pubInfo[index:(index+1)]
        index +=1
        self.pubInfoPcrInfoReadDigestAtRelease = self.pubInfo[index:(index+20)]
        index +=20
        
        dataLen = (self.pubInfo[index]<<8) + (self.pubInfo[index+1]<<0) 
        self.pubInfoPcrInfoWrite = self.pubInfo[index:(index+dataLen+ 2 + 1 + 20)]
        self.pubInfoPcrInfoWritePcrSelection = self.pubInfo[index:(index+dataLen+ 2)]
        index += (dataLen+2)
        self.pubInfoPcrInfoWriteLocalityAtRelease = self.pubInfo[index:(index+1)]
        index +=1
        self.pubInfoPcrInfoWriteDigestAtRelease = self.pubInfo[index:(index+20)]
        index +=20
        
        self.pubInfoPermission = self.pubInfo[index:(index+6)]
        self.pubInfoPermissionTag = self.pubInfo[index:(index+2)]
        index +=2
        self.pubInfoPermissionAttributes = self.pubInfo[index:(index+4)]
        index +=4
        
        self.pubInfoBReadSTClear = self.pubInfo[index:(index+1)]
        index +=1
        
        self.pubInfoBWriteSTClear = self.pubInfo[index:(index+1)]
        index +=1

        self.pubInfoBWriteDefine = self.pubInfo[index:(index+1)]
        index +=1
        
        self.pubInfoDataSize = self.pubInfo[index:(index+4)]
        index +=4
        
        
        
        self.HMACHandles = bytearray(TPM_KH_OWNER)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.pubInfo)
        self.HMACParameters.extend(self.encAuth)
        
        if(self.tag==TPM_TAG_RQU_AUTH1_COMMAND):
            self.pubAuth = self.ownerAuth = Session.getAuthValue(self)
            self.authValue = ADIP(self.authHandle,  self.encAuth)

    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.pubInfo)
                                                        + len(self.encAuth)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.pubInfo)
        self.command.extend(self.encAuth)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.mTreeInPubInfoId = self.insertTreeOutput(self.mTreeInId, 'end', text="pubInfo=" + bytearray2hex(self.pubInfo))
        self.insertTreeOutput(self.mTreeInId, 'end', text="encAuth=" + bytearray2hex(self.encAuth))
        
        self.insertTreeOutput(self.mTreeInPubInfoId, 'end', text="pubInfoTag=" + bytearray2hex(self.pubInfoTag))
        self.insertTreeOutput(self.mTreeInPubInfoId, 'end', text="pubInfoNvIndex=" + bytearray2hex(self.pubInfoNvIndex))
        self.mTreeInPubInfoPcrReadId = self.insertTreeOutput(self.mTreeInPubInfoId, 'end', text="pubInfoPcrInfoRead=" + bytearray2hex(self.pubInfoPcrInfoRead))
        self.mTreeInPubInfoPcrWriteId = self.insertTreeOutput(self.mTreeInPubInfoId, 'end', text="pubInfoPcrInfoWrite=" + bytearray2hex(self.pubInfoPcrInfoWrite))
        self.mTreeInPubInfoPermissionsId = self.insertTreeOutput(self.mTreeInPubInfoId, 'end', text="pubInfoPermission=" + bytearray2hex(self.pubInfoPermission))
        self.insertTreeOutput(self.mTreeInPubInfoId, 'end', text="pubInfoBReadSTClear=" + bytearray2hex(self.pubInfoBReadSTClear))
        self.insertTreeOutput(self.mTreeInPubInfoId, 'end', text="pubInfoBWriteSTClear=" + bytearray2hex(self.pubInfoBWriteSTClear))
        self.insertTreeOutput(self.mTreeInPubInfoId, 'end', text="pubInfoBWriteDefine=" + bytearray2hex(self.pubInfoBWriteDefine))
        self.insertTreeOutput(self.mTreeInPubInfoId, 'end', text="pubInfoDataSize=" + bytearray2hex(self.pubInfoDataSize))

        self.insertTreeOutput(self.mTreeInPubInfoPcrReadId, 'end', text="pubInfoPcrInfoReadPcrSelection=" + bytearray2hex(self.pubInfoPcrInfoReadPcrSelection))
        self.insertTreeOutput(self.mTreeInPubInfoPcrReadId, 'end', text="pubInfoPcrInfoReadLocalityAtRelease=" + bytearray2hex(self.pubInfoPcrInfoReadLocalityAtRelease))
        self.insertTreeOutput(self.mTreeInPubInfoPcrReadId, 'end', text="pubInfoPcrInfoReadDigestAtRelease=" + bytearray2hex(self.pubInfoPcrInfoReadDigestAtRelease))

        self.insertTreeOutput(self.mTreeInPubInfoPcrWriteId, 'end', text="pubInfoPcrInfoWritePcrSelection=" + bytearray2hex(self.pubInfoPcrInfoWritePcrSelection))
        self.insertTreeOutput(self.mTreeInPubInfoPcrWriteId, 'end', text="pubInfoPcrInfoWriteLocalityAtRelease=" + bytearray2hex(self.pubInfoPcrInfoWriteLocalityAtRelease))
        self.insertTreeOutput(self.mTreeInPubInfoPcrWriteId, 'end', text="pubInfoPcrInfoWriteDigestAtRelease=" + bytearray2hex(self.pubInfoPcrInfoWriteDigestAtRelease))

        self.insertTreeOutput(self.mTreeInPubInfoPermissionsId, 'end', text="pubInfoPermissionTag=" + bytearray2hex(self.pubInfoPermissionTag))
        self.insertTreeOutput(self.mTreeInPubInfoPermissionsId, 'end', text="pubInfoPermissionAttributes=" + bytearray2hex(self.pubInfoPermissionAttributes))

        self.inTreeAuthorization()


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]

        index=10
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.outTreeAuthorization(data, index)        

        if(self.responseCode==TPM_SUCCESS):
            Object.addObject(self.pubInfoNvIndex,  self.authValue)

