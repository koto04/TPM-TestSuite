from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_CreateMigrationBlob
from TPM_Constants_V1 import TPM_SUCCESS
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session

class TPM_CreateMigrationBlob(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_CreateMigrationBlob
        self.parentHandle = self.interpretParameter(parameters[2])
        self.migrationType = self.interpretParameter(parameters[3])
        self.migrationKeyAuth = bytearray()#self.interpretParameter(parameters[4])
        self.migrationKeyAuthMigrationKey = bytearray()
        self.migrationKeyAuthMigrationKeyAlgorithmID = bytearray()
        self.migrationKeyAuthMigrationKeyEncScheme = bytearray()
        self.migrationKeyAuthMigrationKeySigScheme = bytearray()
        self.migrationKeyAuthMigrationKeyParms = bytearray()
        self.migrationKeyAuthMigrationKeyPubKey = bytearray()
        self.migrationKeyAuthMigrationScheme = bytearray()
        self.migrationKeyAuthDigest = bytearray()
        self.encData = self.interpretParameter(parameters[5])
        
        self.authHandle = self.parentAuthHandle = self.interpretParameter(parameters[6])
        self.nonceOdd = self.interpretParameter(parameters[7])
        self.continueAuthSession = self.interpretParameter(parameters[8])
        self.pubAuth = self.parentAuth = bytearray()

        self.entityAuthHandle = self.interpretParameter(parameters[9])
        self.entitynonceOdd = self.interpretParameter(parameters[10])
        self.continueEntitySession = self.interpretParameter(parameters[11])
        self.entityAuth = bytearray()

        #response
        self.random = bytearray()
        self.outData = bytearray()
        self.entityNonceEven = bytearray()
        self.rEntityAuth =bytearray()      
        self.rContinueEntitySession = bytearray()
        
        if(self.forDoc==True): return

        if(isinstance(parameters[4], list)):
            p = parameters[4]
            pubKey = self.interpret_PUB_KEY(p[0])
            self.migrationKeyAuthMigrationKeyAlgorithmID = pubKey["algorithmID"]
            self.migrationKeyAuthMigrationKeyEncScheme = pubKey["encScheme"]
            self.migrationKeyAuthMigrationKeySigScheme = pubKey["sigScheme"]
            self.migrationKeyAuthMigrationKeyParms = pubKey["parms"]
            self.migrationKeyAuthMigrationKeyPubKey = pubKey["pubKey"]
            self.migrationKeyAuthMigrationKey = pubKey["key"]
                
            self.migrationKeyAuthMigrationScheme = self.interpretParameter(p[1])
            self.migrationKeyAuthDigest = self.interpretParameter(p[2])
            
            self.migrationKeyAuth.extend(self.migrationKeyAuthMigrationKey)
            self.migrationKeyAuth.extend(self.migrationKeyAuthMigrationScheme)
            self.migrationKeyAuth.extend(self.migrationKeyAuthDigest)
            
        else:
            self.migrationKeyAuth = self.interpretParameter(parameters[4])
            index = 0
            pubKey = self.interpret_PUB_KEY(self.migrationKeyAuth)
            dataLen = pubKey["len"]
            self.migrationKeyAuthMigrationKeyAlgorithmID = pubKey["algorithmID"]
            self.migrationKeyAuthMigrationKeyEncScheme = pubKey["encScheme"]
            self.migrationKeyAuthMigrationKeySigScheme = pubKey["sigScheme"]
            self.migrationKeyAuthMigrationKeyParms = pubKey["parms"]
            self.migrationKeyAuthMigrationKeyPubKey = pubKey["pubKey"]
            self.migrationKeyAuthMigrationKey = pubKey["key"]
            
            index += dataLen
            
            self.migrationKeyAuthMigrationScheme = self.migrationKeyAuth[index:(index+2)]
            index+=2
            self.migrationKeyAuthDigest = self.migrationKeyAuth[index:(index+20)]
            index +=20
            

        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.parentHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.migrationType)
        self.HMACParameters.extend(self.migrationKeyAuth)
        self.HMACParameters.extend(int2bytearray(len(self.encData), 4))
        self.HMACParameters.extend(self.encData)

        
        self.pubAuth = self.parentAuth = Session.getAuthValue(self)
       
        self.entityAuth = Session.getAuthValue2(self.entityAuthHandle,  self.ordinal, self.HMACHandles,  self.HMACParameters, self.entitynonceOdd,  self.continueEntitySession)
    
    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.parentHandle)
                                                        + len(self.migrationType)
                                                        + len(self.migrationKeyAuth)
                                                        + 4 + len(self.encData)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                        + len(self.entityAuthHandle)
                                                        + len(self.entitynonceOdd)
                                                        + len(self.continueEntitySession)
                                                        + len(self.entityAuth)
                                                        
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.parentHandle)
        self.command.extend(self.migrationType)
        self.command.extend(self.migrationKeyAuth)
        self.command.extend(int2bytearray(len(self.encData), 4))
        self.command.extend(self.encData)
        
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)
        
        self.command.extend(self.entityAuthHandle)
        self.command.extend(self.entitynonceOdd)
        self.command.extend(self.continueEntitySession)
        self.command.extend(self.entityAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="parentHandle=" + bytearray2hex(self.parentHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="migrationType=" + bytearray2hex(self.migrationType))
        self.mTreeInMKAId = self.insertTreeOutput(self.mTreeInId, 'end', text="migrationKeyAuth=" + bytearray2hex(self.migrationKeyAuth))
        self.insertTreeOutput(self.mTreeInId, 'end', text="encData=" + bytearray2hex(self.encData))
        
        self.mTreeInMKAMKId =self.insertTreeOutput(self.mTreeInMKAId, 'end', text="migrationKeyAuthMigrationKey=" + bytearray2hex(self.migrationKeyAuthMigrationKey))
        self.insertTreeOutput(self.mTreeInMKAId, 'end', text="migrationKeyAuthMigrationScheme=" + bytearray2hex(self.migrationKeyAuthMigrationScheme))
        self.insertTreeOutput(self.mTreeInMKAId, 'end', text="migrationKeyAuthDigest=" + bytearray2hex(self.migrationKeyAuthDigest))
        
        self.insertTreeOutput(self.mTreeInMKAMKId, 'end', text="migrationKeyAuthMigrationKeyAlgorithmID=" + bytearray2hex(self.migrationKeyAuthMigrationKeyAlgorithmID))
        self.insertTreeOutput(self.mTreeInMKAMKId, 'end', text="migrationKeyAuthMigrationKeyEncScheme=" + bytearray2hex(self.migrationKeyAuthMigrationKeyEncScheme))
        self.insertTreeOutput(self.mTreeInMKAMKId, 'end', text="migrationKeyAuthMigrationKeySigScheme=" + bytearray2hex(self.migrationKeyAuthMigrationKeySigScheme))
        self.insertTreeOutput(self.mTreeInMKAMKId, 'end', text="migrationKeyAuthMigrationKeyParms=" + bytearray2hex(self.migrationKeyAuthMigrationKeyParms))
        self.insertTreeOutput(self.mTreeInMKAMKId, 'end', text="migrationKeyAuthMigrationKeyPubKey=" + bytearray2hex(self.migrationKeyAuthMigrationKeyPubKey))
        
        self.inTreeAuthorization()

        self.insertTreeOutput(self.mTreeInId, 'end', text="entityAuthHandle=" + bytearray2hex(self.entityAuthHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="entitynonceOdd=" + bytearray2hex(self.entitynonceOdd))
        self.insertTreeOutput(self.mTreeInId, 'end', text="continueEntitySession=" + bytearray2hex(self.continueEntitySession))
        self.insertTreeOutput(self.mTreeInId, 'end', text="entityAuth=" + bytearray2hex(self.entityAuth))
        

    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10

        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.random = data[index:(index+dataLen)]
            index += dataLen

        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.outData = data[index:(index+dataLen)]
            index += dataLen
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.insertTreeOutput(self.mTreeOutId, 'end', text="random=" + bytearray2hex(self.random))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="outData=" + bytearray2hex(self.outData))

        index = self.outTreeAuthorization(data, index)        

        if(len(data)>index):
            self.entityNonceEven = data[index:(index+20)]
            index += 20
        if(len(data)>index):
            self.rContinueEntitySession = data[index:(index+1)]
            index +=1 #continueAuthSession
        if(len(data)>index):
            self.rEntityAuth = data[index:(index+20)]
            index += 20
        self.insertTreeOutput(self.mTreeOutId, 'end', text="entityNonceEven=" + bytearray2hex(self.entityNonceEven))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rContinueEntitySession=" + bytearray2hex(self.rContinueEntitySession))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rEntityAuth=" + bytearray2hex(self.rEntityAuth))
        
        if(self.responseCode==TPM_SUCCESS):
            Session.setNonceEven(self.entityAuthHandle, self.entityNonceEven)
        
        if(self.rContinueEntitySession == bytearray([0x00])):
            Session.destroySession(self.entityAuthHandle)
