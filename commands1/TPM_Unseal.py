from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_Unseal
from TPM_Constants_V1 import TPM_SUCCESS
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session

class TPM_Unseal(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_Unseal
        self.parentHandle = self.interpretParameter(parameters[2])
        self.inData = bytearray() #self.interpretParameter(parameters[3])
        self.inDataVer = bytearray()
        self.inDataSealInfo = bytearray()
        self.inDataEncData = bytearray()
        self.inDataEncDataPayload = bytearray()
        self.inDataEncDataAuthData = bytearray()
        self.inDataEncDataTPMProof = bytearray()
        self.inDataEncDataStoredDigest = bytearray()
        self.inDataEncDataData = bytearray()
        
        self.authHandle = self.interpretParameter(parameters[4])
        self.nonceOdd = self.interpretParameter(parameters[5])
        self.continueAuthSession = self.interpretParameter(parameters[6])
        self.pubAuth = self.parentAuth = bytearray()

        self.dataAuthHandle = self.interpretParameter(parameters[7])
        self.datanonceOdd = self.interpretParameter(parameters[8])
        self.continueDataSession = self.interpretParameter(parameters[9])
        self.dataAuth = self.parentAuth = bytearray()

        #response
        self.secret = bytearray()
        self.dataNonceEven = bytearray()
        self.rDataAuth =bytearray()      
        self.rContinueDataSession = bytearray()
        
        if(self.forDoc==True): return

        if(isinstance(parameters[3], list)):
            p=parameters[3]
            self.inDataVer = self.interpretParameter(p[0])
            self.inDataSealInfo = self.interpretParameter(p[1])
            if(isinstance(p[2], list)):
                ps = p[2]
                self.inDataEncDataPayload = self.interpretParameter(ps[0])
                self.inDataEncDataAuthData = self.interpretParameter(ps[1])
                self.inDataEncDataTPMProof = self.interpretParameter(ps[2])
                self.inDataEncDataStoredDigest = self.interpretParameter(ps[3])
                self.inDataEncDataData = self.interpretParameter(ps[4])
                self.inDataEncData.extend(self.inDataEncDataPayload)
                self.inDataEncData.extend(self.inDataEncDataAuthData)
                self.inDataEncData.extend(self.inDataEncDataTPMProof)
                self.inDataEncData.extend(self.inDataEncDataStoredDigest)
                self.inDataEncData.extend(int2bytearray(len(self.inDataEncDataData), 4))
                self.inDataEncData.extend(self.inDataEncDataData)
            else:
                self.inDataEncData = self.interpretParameter(p[2])
                index =0
                self.inDataEncDataPayload = self.inDataEncData[index:(index+1)]
                index +=1
                self.inDataEncDataAuthData = self.inDataEncData[index:(index+20)]
                index +=20
                self.inDataEncDataTPMProof = self.inDataEncData[index:(index+20)]
                index +=20
                self.inDataEncDataStoredDigest = self.inDataEncData[index:(index+20)]
                index +=20
                dataLen = (self.inDataEncData[(index)]<<24) + (self.inDataEncData[(index+1)]<<16) + (self.inDataEncData[(index+2)]<<8) + (self.inDataEncData[(index+3)]<<0)
                index +=4
                self.inDataEncDataData = self.inDataEncData[index:(index+dataLen)]
                
            self.inData.extend(self.inDataVer)  
            self.inData.extend(int2bytearray(len(self.inDataSealInfo), 4))
            self.inData.extend(self.inDataSealInfo)  
            self.inData.extend(int2bytearray(len(self.inDataEncData), 4))
            self.inData.extend(self.inDataEncData)  
            
        else:
            self.inData = self.interpretParameter(parameters[3])
            index = 0
            self.inDataVer = self.inData[index:(index+4)]
            index +=4
            dataLen = (self.inData[(index)]<<24) + (self.inData[(index+1)]<<16) + (self.inData[(index+2)]<<8) + (self.inData[(index+3)]<<0)
            index +=4
            self.inDataSealInfo = self.inData[index:(index+dataLen)]
            index += dataLen
            dataLen = (self.inData[(index)]<<24) + (self.inData[(index+1)]<<16) + (self.inData[(index+2)]<<8) + (self.inData[(index+3)]<<0)
            index +=4
            self.inDataEncData = self.inData[index:(index+dataLen)]
            index =0
            self.inDataEncDataPayload = self.inDataEncData[index:(index+1)]
            index +=1
            self.inDataEncDataAuthData = self.inDataEncData[index:(index+20)]
            index +=20
            self.inDataEncDataTPMProof = self.inDataEncData[index:(index+20)]
            index +=20
            self.inDataEncDataStoredDigest = self.inDataEncData[index:(index+20)]
            index +=20
            dataLen = (self.inDataEncData[(index)]<<24) + (self.inDataEncData[(index+1)]<<16) + (self.inDataEncData[(index+2)]<<8) + (self.inDataEncData[(index+3)]<<0)
            index +=4
            self.inDataEncDataData = self.inDataEncData[index:(index+dataLen)]
            
            
        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.parentHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.inData)
        
        self.pubAuth = self.parentAuth = Session.getAuthValue(self)
       
        self.dataAuth = Session.getAuthValue2(self.dataAuthHandle,  self.ordinal, self.HMACHandles,  self.HMACParameters, self.datanonceOdd,  self.continueDataSession)
    
    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.parentHandle)
                                                        + len(self.inData)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                        + len(self.dataAuthHandle)
                                                        + len(self.datanonceOdd)
                                                        + len(self.continueDataSession)
                                                        + len(self.dataAuth)
                                                        
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.parentHandle)
        self.command.extend(self.inData)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)
        self.command.extend(self.dataAuthHandle)
        self.command.extend(self.datanonceOdd)
        self.command.extend(self.continueDataSession)
        self.command.extend(self.dataAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="parentHandle=" + bytearray2hex(self.parentHandle))
        
        self.mTreeInInDataId = self.insertTreeOutput(self.mTreeInId, 'end', text="inData=" + bytearray2hex(self.inData))
        self.insertTreeOutput(self.mTreeInInDataId, 'end', text="inDataVer=" + bytearray2hex(self.inDataVer))
        self.insertTreeOutput(self.mTreeInInDataId, 'end', text="inDataSealInfo=" + bytearray2hex(self.inDataSealInfo))

        self.mTreeInInDataEncDataId = self.insertTreeOutput(self.mTreeInInDataId, 'end', text="inDataEncData=" + bytearray2hex(self.inDataEncData))
        self.insertTreeOutput(self.mTreeInInDataEncDataId, 'end', text="inDataEncDataPayload=" + bytearray2hex(self.inDataEncDataPayload))
        self.insertTreeOutput(self.mTreeInInDataEncDataId, 'end', text="inDataEncDataAuthData=" + bytearray2hex(self.inDataEncDataAuthData))
        self.insertTreeOutput(self.mTreeInInDataEncDataId, 'end', text="inDataEncDataTPMProof=" + bytearray2hex(self.inDataEncDataTPMProof))
        self.insertTreeOutput(self.mTreeInInDataEncDataId, 'end', text="inDataEncDataStoredDigest=" + bytearray2hex(self.inDataEncDataStoredDigest))
        self.insertTreeOutput(self.mTreeInInDataEncDataId, 'end', text="inDataEncDataData=" + bytearray2hex(self.inDataEncDataData))
        
        self.inTreeAuthorization()

        self.insertTreeOutput(self.mTreeInId, 'end', text="dataAuthHandle=" + bytearray2hex(self.dataAuthHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="datanonceOdd=" + bytearray2hex(self.datanonceOdd))
        self.insertTreeOutput(self.mTreeInId, 'end', text="continueDataSession=" + bytearray2hex(self.continueDataSession))
        self.insertTreeOutput(self.mTreeInId, 'end', text="dataAuth=" + bytearray2hex(self.dataAuth))
        

    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10

        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.secret = data[index:(index+dataLen)]
            index += dataLen
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.insertTreeOutput(self.mTreeOutId, 'end', text="secret=" + bytearray2hex(self.secret))

        index = self.outTreeAuthorization(data, index)        

        if(len(data)>index):
            self.dataNonceEven = data[index:(index+20)]
            index += 20
        if(len(data)>index):
            self.rContinueDataSession = data[index:(index+1)]
            index +=1 #continueAuthSession
        if(len(data)>index):
            self.rDataAuth = data[index:(index+20)]
            index += 20
        self.insertTreeOutput(self.mTreeOutId, 'end', text="dataNonceEven=" + bytearray2hex(self.dataNonceEven))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rContinueDataSession=" + bytearray2hex(self.rContinueDataSession))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rDataAuth=" + bytearray2hex(self.rDataAuth))
        
        if(self.responseCode==TPM_SUCCESS):
            Session.setNonceEven(self.dataAuthHandle, self.dataNonceEven)
            if(self.rContinueDataSession == bytearray([0x00])):
                Session.destroySession(self.dataAuthHandle)
        else:
            Session.destroySession(self.authHandle)
            Session.destroySession(self.dataAuthHandle)
        
