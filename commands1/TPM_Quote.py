from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_Quote
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session


class TPM_Quote(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_Quote
        self.keyHandle = self.interpretParameter(parameters[2])
        self.externalData = self.interpretParameter(parameters[3])
        self.targetPCR = self.interpretParameter(parameters[4])
        
        self.authHandle = self.interpretParameter(parameters[5])
        self.nonceOdd = self.interpretParameter(parameters[6])
        self.continueAuthSession = self.interpretParameter(parameters[7])
        self.pubAuth = self.privAuth = bytearray()
        
        #response
        self.pcrData = bytearray() 
        self.pcrDataSelect = bytearray()  
        self.pcrDataPcrValue = bytearray()
        self.sig = bytearray()       
        
        if(self.forDoc==True): return

        
        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.keyHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.externalData)
        self.HMACParameters.extend(self.targetPCR)
        
        self.pubAuth = self.privAuth = Session.getAuthValue(self)

    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.keyHandle)
                                                        + len(self.externalData)
                                                        + len(self.targetPCR)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.keyHandle)
        self.command.extend(self.externalData)
        self.command.extend(self.targetPCR)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="keyHandle=" + bytearray2hex(self.keyHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="externalData=" + bytearray2hex(self.externalData))
        self.insertTreeOutput(self.mTreeInId, 'end', text="targetPCR=" + bytearray2hex(self.targetPCR))
        
        self.inTreeAuthorization()


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        
        index = 10
        
        self.pcrDataSelect = bytearray()  
        self.pcrDataPcrValue = bytearray()
      
        if(len(data)>index):
            dataLen = (data[(index)]<<8) + (data[(index+1)]<<0)
            self.pcrDataSelect = data[index:(index+dataLen+2)]
            self.pcrData.extend(self.pcrDataSelect)
            index +=(2+ dataLen)
            self.pcrData.extend(data[index:(index+4)])
            
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.pcrDataPcrValue = data[index:(index+dataLen)]
            self.pcrData.extend(self.pcrDataPcrValue)
            index += dataLen

        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.sig = data[index:(index+dataLen)]
            index += dataLen

        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))


        self.mTreeOutPcrDataId = self.insertTreeOutput(self.mTreeOutId, 'end', text="pcrData=" + bytearray2hex(self.pcrData))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="sig=" + bytearray2hex(self.sig))

        self.insertTreeOutput(self.mTreeOutPcrDataId, 'end', text="pcrDataSelect=" + bytearray2hex(self.pcrDataSelect))
        self.insertTreeOutput(self.mTreeOutPcrDataId, 'end', text="pcrDataPcrValue=" + bytearray2hex(self.pcrDataPcrValue))


        self.outTreeAuthorization(data, index)        

