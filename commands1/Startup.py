from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_Startup, TPM_ST_CLEAR, TPM_TAG_RQU_COMMAND
from Utils import int2bytearray

class Startup(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.tag = TPM_TAG_RQU_COMMAND
        self.ordinal = TPM_ORD_Startup
        self.startupType = TPM_ST_CLEAR
        self.logEnable=False
        
        
        self.forDoc = forDoc        
        if(self.forDoc==True): return
        
    def prepare(self):
        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)+len(self.startupType)),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.startupType)		

    
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length.")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
