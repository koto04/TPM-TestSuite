from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_ReadPubek
from Utils import int2bytearray, bytearray2hex

class TPM_ReadPubek(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_ReadPubek
        self.antiReplay = self.interpretParameter(parameters[2])
                
                
        #response
        self.pubEndorsementKey = bytearray()
        self.pubEndorsementKeyAlgorithmID = bytearray()
        self.pubEndorsementKeyEncScheme = bytearray()
        self.pubEndorsementKeySigScheme = bytearray()
        self.pubEndorsementKeyParms = bytearray()
        self.pubEndorsementKeyPubKey = bytearray()
        self.checksum = bytearray()
        
        if(self.forDoc==True): return

    def prepare(self):
        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.antiReplay)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.antiReplay)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="antiReplay=" + bytearray2hex(self.antiReplay))


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]

        index = 10
        if(len(data)>index):
            self.pubEndorsementKeyAlgorithmID = data[index:(index+4)]
            index +=4
        if(len(data)>index):
            self.pubEndorsementKeyEncScheme = data[index:(index+2)]
            index +=2
        if(len(data)>index):
            self.pubEndorsementKeySigScheme = data[index:(index+2)]
            index +=2
        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.pubEndorsementKeyParms = data[index:(index+dataLen)]
            index += dataLen
        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.pubEndorsementKeyPubKey = data[index:(index+dataLen)]
            index += dataLen
            self.pubEndorsementKey = data[10:index]
        if(len(data)>index):
            self.checksum = data[index:(index+20)]
            index +=20

        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.mTreeOutPubEndorsementKeyId = self.insertTreeOutput(self.mTreeOutId, 'end', text="pubEndorsementKey=" + bytearray2hex(self.pubEndorsementKey))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="checksum=" + bytearray2hex(self.checksum))

        self.insertTreeOutput(self.mTreeOutPubEndorsementKeyId, 'end', text="pubEndorsementKeyAlgorithmID=" + bytearray2hex(self.pubEndorsementKeyAlgorithmID))
        self.insertTreeOutput(self.mTreeOutPubEndorsementKeyId, 'end', text="pubEndorsementKeyEncScheme=" + bytearray2hex(self.pubEndorsementKeyEncScheme))
        self.insertTreeOutput(self.mTreeOutPubEndorsementKeyId, 'end', text="pubEndorsementKeySigScheme=" + bytearray2hex(self.pubEndorsementKeySigScheme))
        self.insertTreeOutput(self.mTreeOutPubEndorsementKeyId, 'end', text="pubEndorsementKeyParms=" + bytearray2hex(self.pubEndorsementKeyParms))
        self.insertTreeOutput(self.mTreeOutPubEndorsementKeyId, 'end', text="pubEndorsementKeyPubKey=" + bytearray2hex(self.pubEndorsementKeyPubKey))
