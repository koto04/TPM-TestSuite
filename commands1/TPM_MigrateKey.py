from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_MigrateKey
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session

class TPM_MigrateKey(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_MigrateKey
        self.maKeyHandle = self.interpretParameter(parameters[2])
        self.pubKey = bytearray()#self.interpretParameter(parameters[3])
        self.pubKeyAlgorithmID = bytearray()
        self.pubKeyEncScheme = bytearray()
        self.pubKeySigScheme = bytearray()
        self.pubKeyParms = bytearray()
        self.pubKeyPubKey = bytearray()
        self.inData = self.interpretParameter(parameters[4])
       
        self.authHandle = self.maAuthHandle = self.interpretParameter(parameters[5])
        self.nonceOdd = self.interpretParameter(parameters[6])
        self.continueAuthSession = self.interpretParameter(parameters[7])
        self.pubAuth = self.keyAuth = bytearray()

        #response
        self.outData = bytearray()
        
        if(self.forDoc==True): return

        pubKey = self.interpret_PUB_KEY(parameters[3])
        self.pubKeyAlgorithmID = pubKey["algorithmID"]
        self.pubKeyEncScheme = pubKey["encScheme"]
        self.pubKeySigScheme = pubKey["sigScheme"]
        self.pubKeyParms = pubKey["parms"]
        self.pubKeyPubKey = pubKey["pubKey"]
        self.pubKey = pubKey["key"]
                                        
        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.maKeyHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.pubKey)
        self.HMACParameters.extend(int2bytearray(len(self.inData), 4))
        self.HMACParameters.extend(self.inData)
        
        self.pubAuth = self.keyAuth = Session.getAuthValue(self)
           
    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.maKeyHandle)
                                                        + len(self.pubKey)
                                                        + 4 + len(self.inData)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                        
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.maKeyHandle)
        self.command.extend(self.pubKey)
        self.command.extend(int2bytearray(len(self.inData), 4))
        self.command.extend(self.inData)
        
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="maKeyHandle=" + bytearray2hex(self.maKeyHandle))
        self.mTreeInMKId = self.insertTreeOutput(self.mTreeInId, 'end', text="pubKey=" + bytearray2hex(self.pubKey))
        self.insertTreeOutput(self.mTreeInId, 'end', text="inData=" + bytearray2hex(self.inData))
        
        self.insertTreeOutput(self.mTreeInMKId, 'end', text="pubKeyAlgorithmID=" + bytearray2hex(self.pubKeyAlgorithmID))
        self.insertTreeOutput(self.mTreeInMKId, 'end', text="pubKeyEncScheme=" + bytearray2hex(self.pubKeyEncScheme))
        self.insertTreeOutput(self.mTreeInMKId, 'end', text="pubKeySigScheme=" + bytearray2hex(self.pubKeySigScheme))
        self.insertTreeOutput(self.mTreeInMKId, 'end', text="pubKeyParms=" + bytearray2hex(self.pubKeyParms))
        self.insertTreeOutput(self.mTreeInMKId, 'end', text="pubKeyPubKey=" + bytearray2hex(self.pubKeyPubKey))
        
        self.inTreeAuthorization()
        

    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10
 
        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.outData = data[index:(index+dataLen)]
            index += dataLen
 
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.insertTreeOutput(self.mTreeOutId, 'end', text="outData=" + bytearray2hex(self.outData))

        index = self.outTreeAuthorization(data, index)        
