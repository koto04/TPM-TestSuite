from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_Seal
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session

class TPM_Seal(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_Seal
        self.keyHandle = self.interpretParameter(parameters[2])
        self.encAuth = self.interpretParameter(parameters[3])
        self.pcrInfo = bytearray() #self.interpretParameter(parameters[4])
        self.pcrInfoPcrSelection = bytearray()
        self.pcrInfoDigestAtRelease = bytearray()
        self.pcrInfoDigestAtCreation = bytearray()
        self.inData = self.interpretParameter(parameters[5])
        self.authHandle = self.interpretParameter(parameters[6])
        self.nonceOdd = self.interpretParameter(parameters[7])
        self.continueAuthSession = self.interpretParameter(parameters[8])
        self.pubAuth = bytearray()

        #response
        self.sealedData = bytearray()
        self.sealedDataVer = bytearray()
        self.sealedDataSealInfo = bytearray()
        self.sealedDataEncData = bytearray()
        
        if(self.forDoc==True): return

        if(isinstance(parameters[4], list)):
            p = parameters[4]
            self.pcrInfoPcrSelection = self.interpretParameter(p[0])
            if(isinstance(p[1], list)):
                self.pcrInfoDigestAtRelease = self.calcCompositeHash(p[1])
            else:
                self.pcrInfoDigestAtRelease = p[1]
                
            if(isinstance(p[2], list)):
                self.pcrInfoDigestAtCreation = self.calcCompositeHash(p[2])
            else:
                self.pcrInfoDigestAtCreation = p[2]
            
            self.pcrInfo.extend(self.pcrInfoPcrSelection)
            self.pcrInfo.extend(self.pcrInfoDigestAtRelease)
            self.pcrInfo.extend(self.pcrInfoDigestAtCreation)
        else:
            self.pcrInfo = self.interpretParameter(parameters[4])
            if(len(self.pcrInfo)>0):
                index = 0
                dataLen = (self.inDataEncData[(index)]<<8) + (self.inDataEncData[(index+1)]<<0)
                self.pcrInfoPcrSelection = self.pcrInfo[index:(index+2+dataLen)]
                index += (2+dataLen)
                self.pcrInfoDigestAtRelease = self.pcrInfo[index:(index+20)]
                index+= 20
                self.pcrInfoDigestAtCreation = self.pcrInfo[index:(index+20)]
            
        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.keyHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.encAuth)
        self.HMACParameters.extend(int2bytearray(len(self.pcrInfo), 4))
        self.HMACParameters.extend(self.pcrInfo)
        self.HMACParameters.extend(int2bytearray(len(self.inData), 4))
        self.HMACParameters.extend(self.inData)
        
        self.pubAuth = Session.getAuthValue(self)

    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.keyHandle)
                                                        + len(self.encAuth)
                                                        + 4 + len(self.pcrInfo)
                                                        + 4 + len(self.inData)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.keyHandle)
        self.command.extend(self.encAuth)
        self.command.extend(int2bytearray(len(self.pcrInfo), 4))
        self.command.extend(self.pcrInfo)
        self.command.extend(int2bytearray(len(self.inData), 4))
        self.command.extend(self.inData)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="keyHandle=" + bytearray2hex(self.keyHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="encAuth=" + bytearray2hex(self.encAuth))
        self.mTreeInPcrInfoId = self.insertTreeOutput(self.mTreeInId, 'end', text="pcrInfo=" + bytearray2hex(self.pcrInfo))
        self.insertTreeOutput(self.mTreeInId, 'end', text="inData=" + bytearray2hex(self.inData))
        
        self.insertTreeOutput(self.mTreeInPcrInfoId, 'end', text="pcrInfoPcrSelection=" + bytearray2hex(self.pcrInfoPcrSelection))
        self.insertTreeOutput(self.mTreeInPcrInfoId, 'end', text="pcrInfoDigestAtRelease=" + bytearray2hex(self.pcrInfoDigestAtRelease))
        self.insertTreeOutput(self.mTreeInPcrInfoId, 'end', text="pcrInfoDigestAtCreation=" + bytearray2hex(self.pcrInfoDigestAtCreation))
        
        self.inTreeAuthorization()


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10

        if(len(data)>index):
            self.sealedDataVer = data[index:(index+4)]
            index +=4 
             
        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.sealedDataSealInfo = data[index:(index+dataLen)]
            index += dataLen

        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.sealedDataEncData = data[index:(index+dataLen)]
            index += dataLen
        
        self.sealedData = data[10:index]
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.mTreeOutSealedDataId = self.insertTreeOutput(self.mTreeOutId, 'end', text="sealedData=" + bytearray2hex(self.sealedData))
        self.insertTreeOutput(self.mTreeOutSealedDataId, 'end', text="sealedDataVer=" + bytearray2hex(self.sealedDataVer))
        self.insertTreeOutput(self.mTreeOutSealedDataId, 'end', text="sealedDataSealInfo=" + bytearray2hex(self.sealedDataSealInfo))
        self.insertTreeOutput(self.mTreeOutSealedDataId, 'end', text="sealedDataEncData=" + bytearray2hex(self.sealedDataEncData))

        self.outTreeAuthorization(data, index)        

