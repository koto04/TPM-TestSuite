from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_LoadKey2
from TPM_Constants_V1 import TPM_SUCCESS
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session
from TPM_Object import Object

class TPM_LoadKey2(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_LoadKey2
        self.parentHandle = self.interpretParameter(parameters[2])
        self.inKey = bytearray() # self.interpretParameter(parameters[3])
        self.inKeyVer = bytearray()
        self.inKeyKeyUsage = bytearray()
        self.inKeyKeyFlags	= bytearray()
        self.inKeyAuthDataUsage = bytearray()
        self.inKeyAlgorithmParams = bytearray()	
        self.inKeyAlgorithmParamsAlgorithmID = bytearray()	
        self.inKeyAlgorithmParamsEncScheme = bytearray()
        self.inKeyAlgorithmParamsSigScheme	= bytearray()
        self.inKeyAlgorithmParamsParms = bytearray()
        self.inKeyPCRInfo = bytearray()
        self.inKeyPCRInfoPcrSelection = bytearray()
        self.inKeyPCRInfoDigestAtRelease = bytearray()
        self.inKeyPCRInfoDigestAtCreation = bytearray()
        self.inKeyPubKey = bytearray()
        self.inKeyEncData	= bytearray()
        self.authHandle = self.interpretParameter(parameters[4])
        self.nonceOdd = self.interpretParameter(parameters[5])
        self.continueAuthSession = self.interpretParameter(parameters[6])
        self.pubAuth = bytearray()

        #response
        self.inkeyHandle = bytearray()
        
        if(self.forDoc==True): return

        keyInfoStruct = self.interpret_TPM_KEY(parameters[3])
        self.inKey = keyInfoStruct["key"] 
        self.inKeyVer = keyInfoStruct["ver"]
        self.inKeyKeyUsage = keyInfoStruct["keyUsage"]
        self.inKeyKeyFlags	=keyInfoStruct["keyFlags"]
        self.inKeyAuthDataUsage = keyInfoStruct["authDataUsage"]
        self.inKeyAlgorithmParams = keyInfoStruct["algorithmParams"]
        self.inKeyAlgorithmParamsAlgorithmID = keyInfoStruct["algorithmID"]
        self.inKeyAlgorithmParamsEncScheme = keyInfoStruct["encScheme"]
        self.inKeyAlgorithmParamsSigScheme	= keyInfoStruct["sigScheme"]
        self.inKeyAlgorithmParamsParms = keyInfoStruct["parms"]
        self.inKeyPCRInfo = keyInfoStruct["PCRInfo"]
        self.inKeyPCRInfoPcrSelection = keyInfoStruct["PCRInfoPcrSelection"]
        self.inKeyPCRInfoDigestAtRelease = keyInfoStruct["PCRInfoDigestAtRelease"]
        self.inKeyPCRInfoDigestAtCreation = keyInfoStruct["PCRInfoDigestAtCreation"]
        self.inKeyPubKey = keyInfoStruct["pubKey"]
        self.inKeyEncData	= keyInfoStruct["encData"]

        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.parentHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.inKey)	
        
        self.pubAuth = Session.getAuthValue(self)

    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.parentHandle)
                                                        + len(self.inKey)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.parentHandle)
        self.command.extend(self.inKey)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="parentHandle=" + bytearray2hex(self.parentHandle))
        
        self.mTreeInInKeyId = self.insertTreeOutput(self.mTreeInId, 'end', text="inKey=" + bytearray2hex(self.inKey))
        self.insertTreeOutput(self.mTreeInInKeyId, 'end', text="inKeyVer=" + bytearray2hex(self.inKeyVer))
        self.insertTreeOutput(self.mTreeInInKeyId, 'end', text="inKeyKeyUsage=" + bytearray2hex(self.inKeyKeyUsage))
        self.insertTreeOutput(self.mTreeInInKeyId, 'end', text="inKeyKeyFlags=" + bytearray2hex(self.inKeyKeyFlags))
        self.insertTreeOutput(self.mTreeInInKeyId, 'end', text="inKeyAuthDataUsage=" + bytearray2hex(self.inKeyAuthDataUsage))
        self.mTreeInInKeyAlgoParamsId = self.insertTreeOutput(self.mTreeInInKeyId, 'end', text="inKeyAlgorithmParams=" + bytearray2hex(self.inKeyAlgorithmParams))
        self.mTreeInInKeyPCRInfoParamsId = self.insertTreeOutput(self.mTreeInInKeyId, 'end', text="inKeyPCRInfo=" + bytearray2hex(self.inKeyPCRInfo))
        self.insertTreeOutput(self.mTreeInInKeyId, 'end', text="inKeyPubKey=" + bytearray2hex(self.inKeyPubKey))
        self.insertTreeOutput(self.mTreeInInKeyId, 'end', text="inKeyEncData=" + bytearray2hex(self.inKeyEncData))

        self.insertTreeOutput(self.mTreeInInKeyAlgoParamsId, 'end', text="inKeyAlgorithmParamsAlgorithmID=" + bytearray2hex(self.inKeyAlgorithmParamsAlgorithmID))
        self.insertTreeOutput(self.mTreeInInKeyAlgoParamsId, 'end', text="inKeyAlgorithmParamsEncScheme=" + bytearray2hex(self.inKeyAlgorithmParamsEncScheme))
        self.insertTreeOutput(self.mTreeInInKeyAlgoParamsId, 'end', text="inKeyAlgorithmParamsSigScheme=" + bytearray2hex(self.inKeyAlgorithmParamsSigScheme))
        self.insertTreeOutput(self.mTreeInInKeyAlgoParamsId, 'end', text="inKeyAlgorithmParamsParms=" + bytearray2hex(self.inKeyAlgorithmParamsParms))

        self.insertTreeOutput(self.mTreeInInKeyPCRInfoParamsId, 'end', text="inKeyPCRInfoPcrSelection=" + bytearray2hex(self.inKeyPCRInfoPcrSelection))
        self.insertTreeOutput(self.mTreeInInKeyPCRInfoParamsId, 'end', text="inKeyPCRInfoDigestAtRelease=" + bytearray2hex(self.inKeyPCRInfoDigestAtRelease))
        self.insertTreeOutput(self.mTreeInInKeyPCRInfoParamsId, 'end', text="inKeyPCRInfoDigestAtCreation=" + bytearray2hex(self.inKeyPCRInfoDigestAtCreation))

        self.inTreeAuthorization()


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10
        if(len(data)>index):
            self.inkeyHandle = data[index:(index+4)]
            index += 4
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.insertTreeOutput(self.mTreeOutId, 'end', text="inkeyHandle=" + bytearray2hex(self.inkeyHandle))

        self.outTreeAuthorization(data, index)        

        if(self.responseCode==TPM_SUCCESS):
            Object.loadObject(self.inKeyEncData,  self.inkeyHandle)

