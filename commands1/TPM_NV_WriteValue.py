from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_NV_WriteValue
from TPM_Constants_V1 import TPM_KH_OWNER
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session

class TPM_NV_WriteValue(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_NV_WriteValue
        self.nvIndex = self.interpretParameter(parameters[2])
        self.offset = self.interpretParameter(parameters[3])
        self.data = self.interpretParameter(parameters[4])
        self.authHandle = self.interpretParameter(parameters[5])
        self.nonceOdd = self.interpretParameter(parameters[6])
        self.continueAuthSession = self.interpretParameter(parameters[7])
        self.pubAuth = self.ownerAuth = bytearray()
        
        #response
        self.dataSize = bytearray()       
        
        if(self.forDoc==True): return

        
        self.HMACHandles = bytearray()
        self.HMACHandles.extend(TPM_KH_OWNER)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.nvIndex)
        self.HMACParameters.extend(self.offset)
        self.HMACParameters.extend(int2bytearray(len(self.data), 4))
        self.HMACParameters.extend(self.data)
        
        self.pubAuth = self.ownerAuth = Session.getAuthValue(self)

    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.nvIndex)
                                                        + len(self.offset)
                                                        + 4 + len(self.data)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.nvIndex)
        self.command.extend(self.offset)
        self.command.extend(int2bytearray(len(self.data), 4))
        self.command.extend(self.data)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="nvIndex=" + bytearray2hex(self.nvIndex))
        self.insertTreeOutput(self.mTreeInId, 'end', text="offset=" + bytearray2hex(self.offset))
        self.insertTreeOutput(self.mTreeInId, 'end', text="data=" + bytearray2hex(self.data))
        
        self.inTreeAuthorization()


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        
        index = 10
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.outTreeAuthorization(data, index)        

