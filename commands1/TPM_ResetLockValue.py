from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_ResetLockValue
from TPM_Constants_V1 import TPM_KH_OWNER
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session

class TPM_ResetLockValue(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_ResetLockValue
        self.authHandle = self.interpretParameter(parameters[2])
        self.nonceOdd = self.interpretParameter(parameters[3])
        self.continueAuthSession = self.interpretParameter(parameters[4])
        self.pubAuth = self.ownerAuth = bytearray()
        self.authValue = bytearray()
        
        #response
        self.data = bytearray()
        
        
        if(self.forDoc==True): return

        
        self.HMACHandles = bytearray()
        self.HMACHandles.extend(TPM_KH_OWNER)
        
        self.HMACParameters = bytearray()
        
        self.pubAuth = self.ownerAuth = Session.getAuthValue(self)

    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        
        self.inTreeAuthorization()


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]

        index = 10

            
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))


        self.outTreeAuthorization(data, index)        

