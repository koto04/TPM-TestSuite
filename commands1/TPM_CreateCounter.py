from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_CreateCounter
from TPM_Constants_V1 import TPM_SUCCESS
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session
from TPM_Object import Object

from TestInterpreterCrypto import ADIP

class TPM_CreateCounter(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_CreateCounter
        self.encAuth = self.interpretParameter(parameters[2])
        self.label = self.interpretParameter(parameters[3])
        self.authHandle = self.interpretParameter(parameters[4])
        self.nonceOdd = self.interpretParameter(parameters[5])
        self.continueAuthSession = self.interpretParameter(parameters[6])
        self.pubAuth = bytearray()
        self.authValue = bytearray()
        
        #response
        self.counterID = bytearray()
        self.counterValue = bytearray()
        
        if(self.forDoc==True): return

        self.HMACHandles = bytearray()
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.encAuth)
        self.HMACParameters.extend(self.label)
        
        self.pubAuth = self.ownerAuth = Session.getAuthValue(self)

        self.authValue = ADIP(self.authHandle,  self.encAuth)

    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.encAuth)
                                                        + len(self.label)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.encAuth)
        self.command.extend(self.label)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="encAuth=" + bytearray2hex(self.encAuth))
        self.insertTreeOutput(self.mTreeInId, 'end', text="label=" + bytearray2hex(self.label))
        
        self.inTreeAuthorization()


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10

        if(len(data)>index):
            self.counterID = data[index:(index+4)]
            index +=4
        if(len(data)>index):
            self.counterValue = data[index:(index+10)]
            index +=10

        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.insertTreeOutput(self.mTreeOutId, 'end', text="counterID=" + bytearray2hex(self.counterID))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="counterValue=" + bytearray2hex(self.counterValue))

        self.outTreeAuthorization(data, index)        

        if(self.responseCode==TPM_SUCCESS):
            Object.addObject(self.counterID,  self.authValue)

