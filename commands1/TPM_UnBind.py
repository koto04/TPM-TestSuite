from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_UnBind
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session

class TPM_UnBind(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_UnBind
        self.keyHandle = self.interpretParameter(parameters[2])
        self.inData = self.interpretParameter(parameters[3])
        self.authHandle = self.interpretParameter(parameters[4])
        self.nonceOdd = self.interpretParameter(parameters[5])
        self.continueAuthSession = self.interpretParameter(parameters[6])
        self.pubAuth = bytearray()

        #response
        self.outData = bytearray()
        
        if(self.forDoc==True): return

        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.keyHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(int2bytearray(len(self.inData), 4))
        self.HMACParameters.extend(self.inData)
        
        self.pubAuth = Session.getAuthValue(self)

    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.keyHandle)
                                                        + 4 + len(self.inData)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.keyHandle)
        self.command.extend(int2bytearray(len(self.inData), 4))
        self.command.extend(self.inData)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="keyHandle=" + bytearray2hex(self.keyHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="inData=" + bytearray2hex(self.inData))
        
        self.inTreeAuthorization()


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10

        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.outData = data[index:(index+dataLen)]
            index += dataLen
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.insertTreeOutput(self.mTreeOutId, 'end', text="outData=" + bytearray2hex(self.outData))

        self.outTreeAuthorization(data, index)        

