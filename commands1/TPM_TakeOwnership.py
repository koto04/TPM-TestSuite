from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_TakeOwnership
from TPM_Constants_V1 import TPM_SUCCESS
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session

class TPM_TakeOwnership(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_TakeOwnership
        self.protocolID = self.interpretParameter(parameters[2])
        self.encOwnerAuth = self.interpretParameter(parameters[3])
        self.encSrkAuth = self.interpretParameter(parameters[4])
        self.srkParams = bytearray() # self.interpretParameter(parameters[5])
        self.srkParamsVer = bytearray()
        self.srkParamsKeyUsage = bytearray()
        self.srkParamsKeyFlags	= bytearray()
        self.srkParamsAuthDataUsage = bytearray()
        self.srkParamsAlgorithmParams = bytearray()	
        self.srkParamsAlgorithmParamsAlgorithmID = bytearray()	
        self.srkParamsAlgorithmParamsEncScheme = bytearray()
        self.srkParamsAlgorithmParamsSigScheme	= bytearray()
        self.srkParamsAlgorithmParamsParms = bytearray()
        self.srkParamsPCRInfo = bytearray()
        self.srkParamsPubKey = bytearray()
        self.srkParamsEncData	= bytearray()
        self.authHandle = self.interpretParameter(parameters[6])
        self.nonceOdd = self.interpretParameter(parameters[7])
        self.continueAuthSession = self.interpretParameter(parameters[8])
        self.ownerAuth = self.pubAuth = bytearray()

        #response
        self.srkPub = bytearray()
        self.srkPubVer = bytearray()
        self.srkPubKeyUsage = bytearray()
        self.srkPubKeyFlags = bytearray()
        self.srkPubAuthDataUsage = bytearray()
        self.srkPubAlgorithmParams = bytearray()
        self.srkPubAlgorithmParamsAlgorithmID = bytearray()
        self.srkPubAlgorithmParamsEncScheme = bytearray()
        self.srkPubAlgorithmParamsSigScheme = bytearray()
        self.srkPubAlgorithmParamsParms = bytearray()
        self.srkPubPCRInfo = bytearray()
        self.srkPubPubKey = bytearray()
        self.srkPubEncData = bytearray()
        
        if(self.forDoc==True): return

        srkParamsStruct = self.interpret_TPM_KEY(parameters[5])
        self.srkParams = srkParamsStruct["key"] 
        self.srkParamsVer = srkParamsStruct["ver"]
        self.srkParamsKeyUsage = srkParamsStruct["keyUsage"]
        self.srkParamsKeyFlags	=srkParamsStruct["keyFlags"]
        self.srkParamsAuthDataUsage = srkParamsStruct["authDataUsage"]
        self.srkParamsAlgorithmParams = srkParamsStruct["algorithmParams"]
        self.srkParamsAlgorithmParamsAlgorithmID = srkParamsStruct["algorithmID"]
        self.srkParamsAlgorithmParamsEncScheme = srkParamsStruct["encScheme"]
        self.srkParamsAlgorithmParamsSigScheme	= srkParamsStruct["sigScheme"]
        self.srkParamsAlgorithmParamsParms = srkParamsStruct["parms"]
        self.srkParamsPCRInfo = srkParamsStruct["PCRInfo"]
        self.srkParamsPubKey = srkParamsStruct["pubKey"]
        self.srkParamsEncData	= srkParamsStruct["encData"]

        self.HMACHandles = bytearray()
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.protocolID)	
        self.HMACParameters.extend(int2bytearray(len(self.encOwnerAuth), 4))
        self.HMACParameters.extend(self.encOwnerAuth)	
        self.HMACParameters.extend(int2bytearray(len(self.encSrkAuth), 4))
        self.HMACParameters.extend(self.encSrkAuth)	
        self.HMACParameters.extend(self.srkParams)	
        
        self.pubAuth = Session.getAuthValue(self)

    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.protocolID)
                                                        + 4 + len(self.encOwnerAuth)
                                                        + 4 + len(self.encSrkAuth)
                                                        + len(self.srkParams)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.protocolID)
        self.command.extend(int2bytearray(len(self.encOwnerAuth), 4))
        self.command.extend(self.encOwnerAuth)
        self.command.extend(int2bytearray(len(self.encSrkAuth), 4))
        self.command.extend(self.encSrkAuth)
        self.command.extend(self.srkParams)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="protocolID=" + bytearray2hex(self.protocolID))
        self.insertTreeOutput(self.mTreeInId, 'end', text="encOwnerAuth=" + bytearray2hex(self.encOwnerAuth))
        self.insertTreeOutput(self.mTreeInId, 'end', text="encSrkAuth=" + bytearray2hex(self.encSrkAuth))
        
        self.mTreeInsrkParamsId = self.insertTreeOutput(self.mTreeInId, 'end', text="srkParams=" + bytearray2hex(self.srkParams))
        self.insertTreeOutput(self.mTreeInsrkParamsId, 'end', text="srkParamsVer=" + bytearray2hex(self.srkParamsVer))
        self.insertTreeOutput(self.mTreeInsrkParamsId, 'end', text="srkParamsKeyUsage=" + bytearray2hex(self.srkParamsKeyUsage))
        self.insertTreeOutput(self.mTreeInsrkParamsId, 'end', text="srkParamsKeyFlags=" + bytearray2hex(self.srkParamsKeyFlags))
        self.insertTreeOutput(self.mTreeInsrkParamsId, 'end', text="srkParamsAuthDataUsage=" + bytearray2hex(self.srkParamsAuthDataUsage))
        self.mTreeInsrkParamsAlgoParamsId = self.insertTreeOutput(self.mTreeInsrkParamsId, 'end', text="srkParamsAlgorithmParams=" + bytearray2hex(self.srkParamsAlgorithmParams))
        self.insertTreeOutput(self.mTreeInsrkParamsId, 'end', text="srkParamsPCRInfo=" + bytearray2hex(self.srkParamsAlgorithmParams))
        self.insertTreeOutput(self.mTreeInsrkParamsId, 'end', text="srkParamsPubKey=" + bytearray2hex(self.srkParamsAlgorithmParams))
        self.insertTreeOutput(self.mTreeInsrkParamsId, 'end', text="srkParamsEncData=" + bytearray2hex(self.srkParamsAlgorithmParams))

        self.insertTreeOutput(self.mTreeInsrkParamsAlgoParamsId, 'end', text="srkParamsAlgorithmParamsAlgorithmID=" + bytearray2hex(self.srkParamsAlgorithmParamsAlgorithmID))
        self.insertTreeOutput(self.mTreeInsrkParamsAlgoParamsId, 'end', text="srkParamsAlgorithmParamsEncScheme=" + bytearray2hex(self.srkParamsAlgorithmParamsEncScheme))
        self.insertTreeOutput(self.mTreeInsrkParamsAlgoParamsId, 'end', text="srkParamsAlgorithmParamsSigScheme=" + bytearray2hex(self.srkParamsAlgorithmParamsSigScheme))
        self.insertTreeOutput(self.mTreeInsrkParamsAlgoParamsId, 'end', text="srkParamsAlgorithmParamsParms=" + bytearray2hex(self.srkParamsAlgorithmParamsParms))

        self.inTreeAuthorization()


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10
        if(len(data)>index):
            TPM_KEY_Struct = self.interpret_TPM_KEY(data[index:])
            dataLen = TPM_KEY_Struct["len"]
            self.srkPub = TPM_KEY_Struct["key"] 
            self.srkPubVer = TPM_KEY_Struct["ver"]
            self.srkPubKeyUsage = TPM_KEY_Struct["keyUsage"]
            self.srkPubKeyFlags	=TPM_KEY_Struct["keyFlags"]
            self.srkPubAuthDataUsage = TPM_KEY_Struct["authDataUsage"]
            self.srkPubAlgorithmParams = TPM_KEY_Struct["algorithmParams"]
            self.srkPubAlgorithmParamsAlgorithmID = TPM_KEY_Struct["algorithmID"]
            self.srkPubAlgorithmParamsEncScheme = TPM_KEY_Struct["encScheme"]
            self.srkPubAlgorithmParamsSigScheme	= TPM_KEY_Struct["sigScheme"]
            self.srkPubAlgorithmParamsParms = TPM_KEY_Struct["parms"]
            self.srkPubPCRInfo = TPM_KEY_Struct["PCRInfo"]
            self.srkPubPubKey = TPM_KEY_Struct["pubKey"]
            self.srkPubEncData	= TPM_KEY_Struct["encData"]
            index += dataLen
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.mTreeOutsrkPubId = self.insertTreeOutput(self.mTreeOutId, 'end', text="srkPub=" + bytearray2hex(self.srkPub))
        self.insertTreeOutput(self.mTreeOutsrkPubId, 'end', text="srkPubVer=" + bytearray2hex(self.srkPubVer))
        self.insertTreeOutput(self.mTreeOutsrkPubId, 'end', text="srkPubKeyUsage=" + bytearray2hex(self.srkPubKeyUsage))
        self.insertTreeOutput(self.mTreeOutsrkPubId, 'end', text="srkPubKeyFlags=" + bytearray2hex(self.srkPubKeyFlags))
        self.insertTreeOutput(self.mTreeOutsrkPubId, 'end', text="srkPubAuthDataUsage=" + bytearray2hex(self.srkPubAuthDataUsage))
        self.mTreeOutsrkPubAlgoParamsId = self.insertTreeOutput(self.mTreeOutsrkPubId, 'end', text="srkPubAlgorithmParams=" + bytearray2hex(self.srkPubAlgorithmParams))
        self.insertTreeOutput(self.mTreeOutsrkPubId, 'end', text="srkPubPCRInfo=" + bytearray2hex(self.srkPubPCRInfo))
        self.insertTreeOutput(self.mTreeOutsrkPubId, 'end', text="srkPubPubKey=" + bytearray2hex(self.srkPubPubKey))
        self.insertTreeOutput(self.mTreeOutsrkPubId, 'end', text="srkPubEncData=" + bytearray2hex(self.srkPubEncData))

        self.insertTreeOutput(self.mTreeOutsrkPubAlgoParamsId, 'end', text="srkPubAlgorithmParamsAlgorithmID=" + bytearray2hex(self.srkPubAlgorithmParamsAlgorithmID))
        self.insertTreeOutput(self.mTreeOutsrkPubAlgoParamsId, 'end', text="srkPubAlgorithmParamsEncScheme=" + bytearray2hex(self.srkPubAlgorithmParamsEncScheme))
        self.insertTreeOutput(self.mTreeOutsrkPubAlgoParamsId, 'end', text="srkPubAlgorithmParamsSigScheme=" + bytearray2hex(self.srkPubAlgorithmParamsSigScheme))
        self.insertTreeOutput(self.mTreeOutsrkPubAlgoParamsId, 'end', text="srkPubAlgorithmParamsParms=" + bytearray2hex(self.srkPubAlgorithmParamsParms))

        self.outTreeAuthorization(data, index)        

        if(self.responseCode==TPM_SUCCESS):
            Session.destroySession(self.authHandle)
