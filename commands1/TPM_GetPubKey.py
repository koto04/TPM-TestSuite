from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_GetPubKey
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session

class TPM_GetPubKey(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_GetPubKey
        self.keyHandle = self.interpretParameter(parameters[2])
        self.authHandle = self.interpretParameter(parameters[3])
        self.nonceOdd = self.interpretParameter(parameters[4])
        self.continueAuthSession = self.interpretParameter(parameters[5])
        self.pubAuth = bytearray()

        #response
        self.pubKey = bytearray()
        self.pubKeyAlgorithmID = bytearray()
        self.pubKeyEncScheme = bytearray()
        self.pubKeySigScheme = bytearray()
        self.pubKeyParms = bytearray()
        self.pubKeyPubKey = bytearray()
        
        if(self.forDoc==True): return

        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.keyHandle)
        
        self.HMACParameters = bytearray()
        
        self.pubAuth = Session.getAuthValue(self)

    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.keyHandle)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.keyHandle)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="keyHandle=" + bytearray2hex(self.keyHandle))
        
        self.inTreeAuthorization()


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10

        if(len(data)>index):
            self.pubKeyAlgorithmID = data[index:(index+4)]
            index +=4
        if(len(data)>index):
            self.pubKeyEncScheme = data[index:(index+2)]
            index +=2
        if(len(data)>index):
            self.pubKeySigScheme = data[index:(index+2)]
            index +=2
        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.pubKeyParms = data[index:(index+dataLen)]
            index += dataLen
        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.pubKeyPubKey = data[index:(index+dataLen)]
            index += dataLen
            self.pubKey = data[10:index]

        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.mTreeOutPubKeyId = self.insertTreeOutput(self.mTreeOutId, 'end', text="pubKey=" + bytearray2hex(self.pubKey))

        self.insertTreeOutput(self.mTreeOutPubKeyId, 'end', text="pubKeyAlgorithmID=" + bytearray2hex(self.pubKeyAlgorithmID))
        self.insertTreeOutput(self.mTreeOutPubKeyId, 'end', text="pubKeyEncScheme=" + bytearray2hex(self.pubKeyEncScheme))
        self.insertTreeOutput(self.mTreeOutPubKeyId, 'end', text="pubKeySigScheme=" + bytearray2hex(self.pubKeySigScheme))
        self.insertTreeOutput(self.mTreeOutPubKeyId, 'end', text="pubKeyParms=" + bytearray2hex(self.pubKeyParms))
        self.insertTreeOutput(self.mTreeOutPubKeyId, 'end', text="pubKeyPubKey=" + bytearray2hex(self.pubKeyPubKey))

        self.outTreeAuthorization(data, index)        

