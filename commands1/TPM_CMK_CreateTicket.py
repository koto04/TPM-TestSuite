from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_CMK_CreateTicket
from TPM_Constants_V1 import TPM_KH_OWNER
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session

class TPM_CMK_CreateTicket(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_CMK_CreateTicket
        self.verificationKey = bytearray()#self.interpretParameter(parameters[2])
        self.verificationKeyAlgorithmID = bytearray()
        self.verificationKeyEncScheme = bytearray()
        self.verificationKeySigScheme = bytearray()
        self.verificationKeyParms = bytearray()
        self.verificationKeyPubKey = bytearray()
        self.signedData = self.interpretParameter(parameters[3])
        self.signatureValue = self.interpretParameter(parameters[4])
        self.authHandle = self.ownerAuthHandle =self.interpretParameter(parameters[5])
        self.nonceOdd = self.interpretParameter(parameters[6])
        self.continueAuthSession = self.interpretParameter(parameters[7])
        self.pubAuth = bytearray()

        #response
        self.sigTicket = bytearray()
        
        
        if(self.forDoc==True): return

        pubKey = self.interpret_PUB_KEY(parameters[2])
        self.verificationKeyAlgorithmID = pubKey["algorithmID"]
        self.verificationKeyEncScheme = pubKey["encScheme"]
        self.verificationKeySigScheme = pubKey["sigScheme"]
        self.verificationKeyParms = pubKey["parms"]
        self.verificationKeyPubKey = pubKey["pubKey"]
        self.verificationKey = pubKey["key"]
        
        self.HMACHandles = bytearray()
        self.HMACHandles.extend(TPM_KH_OWNER)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.verificationKey)
        self.HMACParameters.extend(self.signedData)
        self.HMACParameters.extend(int2bytearray(len(self.signatureValue), 4))
        self.HMACParameters.extend(self.signatureValue)
        
        self.pubAuth = Session.getAuthValue(self)

    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.verificationKey)
                                                        + len(self.signedData)
                                                        + 4 + len(self.signatureValue)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.verificationKey)
        self.command.extend(self.signedData)
        self.command.extend(int2bytearray(len(self.signatureValue), 4))
        self.command.extend(self.signatureValue)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.mTreeInVKId = self.insertTreeOutput(self.mTreeInId, 'end', text="verificationKey=" + bytearray2hex(self.verificationKey))
        self.insertTreeOutput(self.mTreeInId, 'end', text="signedData=" + bytearray2hex(self.signedData))
        self.insertTreeOutput(self.mTreeInId, 'end', text="signatureValue=" + bytearray2hex(self.signatureValue))
        
        self.insertTreeOutput(self.mTreeInVKId, 'end', text="verificationKeyAlgorithmID=" + bytearray2hex(self.verificationKeyAlgorithmID))
        self.insertTreeOutput(self.mTreeInVKId, 'end', text="verificationKeyEncScheme=" + bytearray2hex(self.verificationKeyEncScheme))
        self.insertTreeOutput(self.mTreeInVKId, 'end', text="verificationKeySigScheme=" + bytearray2hex(self.verificationKeySigScheme))
        self.insertTreeOutput(self.mTreeInVKId, 'end', text="verificationKeyParms=" + bytearray2hex(self.verificationKeyParms))
        self.insertTreeOutput(self.mTreeInVKId, 'end', text="verificationKeyPubKey=" + bytearray2hex(self.verificationKeyPubKey))

        
        self.inTreeAuthorization()


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10

        if(len(data)>index):
            self.sigTicket = data[index:(index+20)]
            index += 20


        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.insertTreeOutput(self.mTreeOutId, 'end', text="sigTicket=" + bytearray2hex(self.sigTicket))

        self.outTreeAuthorization(data, index)        
