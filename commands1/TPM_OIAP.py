from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_OIAP
from TPM_Constants_V1 import TPM_SUCCESS
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session

class TPM_OIAP(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_OIAP
        
           
        #response
        self.authHandle = bytearray()
        self.nonceEven = bytearray()
        
        if(self.forDoc==True): return

    def prepare(self):
        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
        
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10
        if(len(data)>index):
            self.authHandle = data[index:(index+4)]
            index += 4
        if(len(data)>index):
            self.nonceEven = data[index:(index+20)]
            index +=20
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.insertTreeOutput(self.mTreeOutId, 'end', text="authHandle=" + bytearray2hex(self.authHandle))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="nonceEven=" + bytearray2hex(self.nonceEven))

        if(self.responseCode==TPM_SUCCESS):
            Session.addOIAPSession(self)
