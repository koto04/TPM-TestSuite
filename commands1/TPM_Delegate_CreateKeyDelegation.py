from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_Delegate_CreateKeyDelegation
from TPM_Constants_V1 import TPM_KH_OWNER
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session
from TestInterpreterCrypto import ADIP

class TPM_Delegate_CreateKeyDelegation(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_Delegate_CreateKeyDelegation
        self.keyHandle = self.interpretParameter(parameters[2])
        self.publicInfo = bytearray()#self.interpretParameter(parameters[3])
        self.delAuth = self.interpretParameter(parameters[4])
        self.authHandle = self.interpretParameter(parameters[5])
        self.nonceOdd = self.interpretParameter(parameters[6])
        self.continueAuthSession = self.interpretParameter(parameters[7])
        self.pubAuth = self.ownerAuth = bytearray()
        self.authValue = bytearray()
        
        self.publicInfoTag = bytearray()
        self.publicInfoRowLabel = bytearray()
        self.publicInfoPcrInfo = bytearray()
        self.publicInfoPcrInfoPcrSelection = bytearray()
        self.publicInfoPcrInfoLocalityAtRelease = bytearray()
        self.publicInfoPcrInfoDigestAtRelease = bytearray()
        self.publicInfoPermisions = bytearray()
        self.publicInfoPermisionsTag = bytearray()
        self.publicInfoPermisionsDelegateType = bytearray()
        self.publicInfoPermisionsPer1 = bytearray()
        self.publicInfoPermisionsPer2 = bytearray()
        self.publicInfoFamilyID = bytearray()
        self.publicInfoVerificationCount = bytearray()
        
        #response
        self.blob = bytearray()
        
        if(self.forDoc==True): return

        if(isinstance(parameters[3], list)):
            p = parameters[3]
            self.publicInfo.extend(self.interpretParameter(p[0]))
            self.publicInfo.extend(self.interpretParameter(p[1]))
            
            if(isinstance(p[2],  list)):
                ps = p[2]
                self.publicInfo.extend(self.interpretParameter(ps[0]))
                self.publicInfo.extend(self.interpretParameter(ps[1]))
                if(isinstance(ps[2],list)):
                    ps[2] = self.calcCompositeHash(ps[2])               
                self.publicInfo.extend(self.interpretParameter(ps[2]))
            
            if(isinstance(p[3],  list)):
                ps = p[3]
                self.publicInfo.extend(self.interpretParameter(ps[0]))
                self.publicInfo.extend(self.interpretParameter(ps[1]))
                self.publicInfo.extend(self.interpretParameter(ps[2]))
                self.publicInfo.extend(self.interpretParameter(ps[3]))
            
            self.publicInfo.extend(self.interpretParameter(p[4]))
            self.publicInfo.extend(self.interpretParameter(p[5]))

        else:
            self.publicInfo = self.interpretParameter(parameters[3])
        
        index = 0
        self.publicInfoTag = self.publicInfo[index:(index+2)]
        index +=2
        self.publicInfoRowLabel = self.publicInfo[index:(index+1)]
        index +=1
        
        dataLen = (self.publicInfo[index]<<8) + (self.publicInfo[index+1]<<0)
        self.publicInfoPcrInfo = self.publicInfo[index:(index+dataLen+ 2 + 1 + 20)]
        self.publicInfoPcrInfoPcrSelection = self.publicInfo[index:(index+dataLen+ 2)]
        index += (dataLen+2)
        self.publicInfoPcrInfoLocalityAtRelease = self.publicInfo[index:(index+1)]
        index +=1
        self.publicInfoPcrInfoDigestAtRelease = self.publicInfo[index:(index+20)]
        index +=20
        
        self.publicInfoPermisions = self.publicInfo[index:(index+14)]
        self.publicInfoPermisionsTag = self.publicInfo[index:(index+2)]
        index +=2
        self.publicInfoPermisionsDelegateType = self.publicInfo[index:(index+4)]
        index +=4
        self.publicInfoPermisionsPer1 = self.publicInfo[index:(index+4)]
        index +=4
        self.publicInfoPermisionsPer2 = self.publicInfo[index:(index+4)]
        index +=4
        
        self.publicInfoFamilyID = self.publicInfo[index:(index+4)]
        index +=4
        
        self.publicInfoVerificationCount = self.publicInfo[index:(index+4)]
        index +=4
        
        self.HMACHandles = bytearray()
        self.HMACHandles.extend(TPM_KH_OWNER)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.publicInfo)
        self.HMACParameters.extend(self.delAuth)
        
        self.pubAuth = self.ownerAuth = Session.getAuthValue(self)

        self.authValue = ADIP(self.authHandle,  self.delAuth)
        
    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.keyHandle)
                                                        + len(self.publicInfo)
                                                        + len(self.delAuth)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.ownerAuth)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.keyHandle)
        self.command.extend(self.publicInfo)
        self.command.extend(self.delAuth)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.ownerAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="keyHandle=" + bytearray2hex(self.keyHandle))
        self.mTreeInPublicInfoId = self.insertTreeOutput(self.mTreeInId, 'end', text="publicInfo=" + bytearray2hex(self.publicInfo))
        self.insertTreeOutput(self.mTreeInId, 'end', text="delAuth=" + bytearray2hex(self.delAuth))
        
        self.insertTreeOutput(self.mTreeInPublicInfoId, 'end', text="publicInfoTag=" + bytearray2hex(self.publicInfoTag))
        self.insertTreeOutput(self.mTreeInPublicInfoId, 'end', text="publicInfoRowLabel=" + bytearray2hex(self.publicInfoRowLabel))
        self.mTreeInPublicInfoPcrId = self.insertTreeOutput(self.mTreeInPublicInfoId, 'end', text="publicInfoPcrInfo=" + bytearray2hex(self.publicInfoPcrInfo))
        self.mTreeInPublicInfoPermissionsId = self.insertTreeOutput(self.mTreeInPublicInfoId, 'end', text="publicInfoPermisions=" + bytearray2hex(self.publicInfoPermisions))
        self.insertTreeOutput(self.mTreeInPublicInfoId, 'end', text="publicInfoFamilyID=" + bytearray2hex(self.publicInfoFamilyID))
        self.insertTreeOutput(self.mTreeInPublicInfoId, 'end', text="publicInfoVerificationCount=" + bytearray2hex(self.publicInfoVerificationCount))


        self.insertTreeOutput(self.mTreeInPublicInfoPcrId, 'end', text="publicInfoPcrInfoPcrSelection=" + bytearray2hex(self.publicInfoPcrInfoPcrSelection))
        self.insertTreeOutput(self.mTreeInPublicInfoPcrId, 'end', text="publicInfoPcrInfoLocalityAtRelease=" + bytearray2hex(self.publicInfoPcrInfoLocalityAtRelease))
        self.insertTreeOutput(self.mTreeInPublicInfoPcrId, 'end', text="publicInfoPcrInfoDigestAtRelease=" + bytearray2hex(self.publicInfoPcrInfoDigestAtRelease))

        self.insertTreeOutput(self.mTreeInPublicInfoPermissionsId, 'end', text="publicInfoPermisionsTag=" + bytearray2hex(self.publicInfoPermisionsTag))
        self.insertTreeOutput(self.mTreeInPublicInfoPermissionsId, 'end', text="publicInfoPermisionsDelegateType=" + bytearray2hex(self.publicInfoPermisionsDelegateType))
        self.insertTreeOutput(self.mTreeInPublicInfoPermissionsId, 'end', text="publicInfoPermisionsPer1=" + bytearray2hex(self.publicInfoPermisionsPer1))
        self.insertTreeOutput(self.mTreeInPublicInfoPermissionsId, 'end', text="publicInfoPermisionsPer2=" + bytearray2hex(self.publicInfoPermisionsPer2))

        self.inTreeAuthorization()


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]

        index = 10

        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4 
            self.blob = data[index:(index+dataLen)]
            index += dataLen
            
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.insertTreeOutput(self.mTreeOutId, 'end', text="blob=" + bytearray2hex(self.blob))

        self.outTreeAuthorization(data, index)        

