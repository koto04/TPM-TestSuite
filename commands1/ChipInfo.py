from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_GetCapability, TPM_CAP_VERSION_VAL, TPM_TAG_RQU_COMMAND
from Utils import int2bytearray, bytearray2hex, bytearray2int

class ChipInfo(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.tag = TPM_TAG_RQU_COMMAND
        self.ordinal = TPM_ORD_GetCapability
        self.capArea = TPM_CAP_VERSION_VAL
        self.subCap = bytearray([0x00, 0x00, 0x00, 0x00])
        self.logEnable=False
        
        self.infoFamilyIndicator = ""
        self.infoLevel = ""
        self.infoRevision = ""
        self.infoDayOfYear = ""
        self.infoYear = ""
        self.infoManufacturer = ""
        self.infoVendorString = ""
        self.infoVersion = ""

        #response
        self.resp = bytearray()
        self.respItems = []
        
        if(self.forDoc==True): return
        

    def prepare(self):
        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.capArea)
                                                        + 4 + len(self.subCap)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.capArea)
        self.command.extend(int2bytearray(len(self.subCap), 4))        
        self.command.extend(self.subCap)
        

    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length.")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10
        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.resp = data[index:(index+dataLen)]
            index += dataLen
            
            self.respItems.append(self.resp[0:2])
            self.respItems.append(self.resp[2:6])
            self.respItems.append(self.resp[6:8])
            self.respItems.append(self.resp[8:9])
            self.respItems.append(self.resp[9:13])
            dataLen =  (self.resp[13]<<8) + (self.resp[14]<<0)
            self.respItems.append(self.resp[15:(15+dataLen)])

            self.infoVersion = str(bytearray2int(self.resp[2:3])) + "." + str(bytearray2int(self.resp[3:4])) + "." + str(bytearray2int(self.resp[4:5])) + "." + str(bytearray2int(self.resp[5:6]))
            self.infoLevel = str(bytearray2int(self.resp[6:8]))
            self.infoRevision = str(bytearray2int(self.resp[8:9]))
            self.infoManufacturer = str(self.resp[9:13]).strip(chr(0x00))
             
            self.infoBuildNumber = (str(bytearray2int(self.resp[15:16])) + "." + str(bytearray2int(self.resp[16:17])) + "." + str(bytearray2int(self.resp[17:19])) + "." + str(bytearray2int(self.resp[19:20])))
            self.infoIdentifier = str(self.resp[20:(15+dataLen)]).strip(chr(0x00))
            self.infoVendorString = self.infoBuildNumber + self.infoIdentifier
            
    def longChipInfo(self):
        ret = ""
        ret += "Version: " + self.infoVersion + "\n"
        ret += "Specification level: " + self.infoLevel + "\n"
        ret += "Errata revision: " + self.infoRevision + "\n"
        ret += "Vendor ID: " + self.infoManufacturer + "\n"
        ret += "Vendor Specific: " + self.infoVendorString + "\n"
        return ret

    def shortChipInfo(self):
        ret = self.infoManufacturer + ", v" + self.infoVersion + " " + self.infoVendorString
        return ret

    def pathSubString(self):
        ret = self.infoManufacturer + "_v" + self.infoVersion + "_" + self.infoVendorString
        return ret
        