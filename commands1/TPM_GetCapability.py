from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_GetCapability, TPM_CAP_HANDLE, TPM_CAP_VERSION_VAL
from Utils import int2bytearray, bytearray2hex

class TPM_GetCapability(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_GetCapability
        self.capArea = self.interpretParameter(parameters[2])
        self.subCap = bytearray()
        
        #response
        self.resp = bytearray()
        self.respItems = []
        
        if(self.forDoc==True): return
        
        if(isinstance(parameters[3], list)):
            p = parameters[3]
            for i in range(0, len(p)):
                self.subCap.extend(self.interpretParameter(p[i]))
        else:
            self.subCap = self.interpretParameter(parameters[3])

    def prepare(self):
        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.capArea)
                                                        + 4 + len(self.subCap)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.capArea)
        self.command.extend(int2bytearray(len(self.subCap), 4))        
        self.command.extend(self.subCap)
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="capArea=" + bytearray2hex(self.capArea))
        self.insertTreeOutput(self.mTreeInId, 'end', text="subCap=" + bytearray2hex(self.subCap))


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10
        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.resp = data[index:(index+dataLen)]
            index += dataLen
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.mTreeOutRespId = self.insertTreeOutput(self.mTreeOutId, 'end', text="resp=" + bytearray2hex(self.resp))

        if(self.capArea==TPM_CAP_HANDLE):
            count =  (self.resp[0]<<8) + (self.resp[1]<<0)
            for i in range(0,  count):
                self.respItems.append(self.resp[(i*4+2):((i+1)*4+2)])
            for idx, cap in enumerate(self.respItems):
                self.insertTreeOutput(self.mTreeOutRespId, 'end', text="respItems[" + str(idx) + "]=" + bytearray2hex(cap))
        elif (self.capArea==TPM_CAP_VERSION_VAL):
            self.respItems.append(self.resp[0:2])
            self.respItems.append(self.resp[2:6])
            self.respItems.append(self.resp[6:8])
            self.respItems.append(self.resp[8:9])
            self.respItems.append(self.resp[9:13])
            dataLen =  (self.resp[13]<<8) + (self.resp[14]<<0)
            self.respItems.append(self.resp[15:(15+dataLen)])
            self.insertTreeOutput(self.mTreeOutRespId, 'end', text="respItems[0](tag)=" + bytearray2hex(self.respItems[0]))            
            self.insertTreeOutput(self.mTreeOutRespId, 'end', text="respItems[1](version)=" + bytearray2hex(self.respItems[1]))            
            self.insertTreeOutput(self.mTreeOutRespId, 'end', text="respItems[2](specLevel)=" + bytearray2hex(self.respItems[2]))            
            self.insertTreeOutput(self.mTreeOutRespId, 'end', text="respItems[3](errataRev)=" + bytearray2hex(self.respItems[3]))            
            self.insertTreeOutput(self.mTreeOutRespId, 'end', text="respItems[4](tpmVendorID)=" + bytearray2hex(self.respItems[4]))            
            self.insertTreeOutput(self.mTreeOutRespId, 'end', text="respItems[5](vendorSpecific)=" + bytearray2hex(self.respItems[5]))            
