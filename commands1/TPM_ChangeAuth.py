from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_ChangeAuth
from TPM_Constants_V1 import TPM_SUCCESS
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session
from TPM_Object import Object

from TestInterpreterCrypto import ADIP

class TPM_ChangeAuth(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_ChangeAuth
        self.parentHandle = self.interpretParameter(parameters[2])
        self.protocolID = self.interpretParameter(parameters[3])
        self.newAuth = self.interpretParameter(parameters[4])
        self.entityType = self.interpretParameter(parameters[5])
        self.encData = self.interpretParameter(parameters[6])
        
        self.authHandle = self.parentAuthHandle = self.interpretParameter(parameters[7])
        self.nonceOdd = self.interpretParameter(parameters[8])
        self.continueAuthSession = self.interpretParameter(parameters[9])
        self.pubAuth = self.parentAuth = bytearray()

        self.entityAuthHandle = self.interpretParameter(parameters[10])
        self.entitynonceOdd = self.interpretParameter(parameters[11])
        self.continueEntitySession = self.interpretParameter(parameters[12])
        self.entityAuth = bytearray()

        #response
        self.outData = bytearray()
        self.entityNonceEven = bytearray()
        self.rEntityAuth =bytearray()      
        self.rContinueEntitySession = bytearray()
        
        if(self.forDoc==True): return

        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.parentHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.protocolID)
        self.HMACParameters.extend(self.newAuth)
        self.HMACParameters.extend(self.entityType)
        self.HMACParameters.extend(int2bytearray(len(self.encData), 4))
        self.HMACParameters.extend(self.encData)

        
        self.pubAuth = self.parentAuth = Session.getAuthValue(self)
       
        self.entityAuth = Session.getAuthValue2(self.entityAuthHandle,  self.ordinal, self.HMACHandles,  self.HMACParameters, self.entitynonceOdd,  self.continueEntitySession)
    
        self.authValue = ADIP(self.authHandle,  self.newAuth)

    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.parentHandle)
                                                        + len(self.protocolID)
                                                        + len(self.newAuth)
                                                        + len(self.entityType)
                                                        + 4 + len(self.encData)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                        + len(self.entityAuthHandle)
                                                        + len(self.entitynonceOdd)
                                                        + len(self.continueEntitySession)
                                                        + len(self.entityAuth)
                                                        
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.parentHandle)
        self.command.extend(self.protocolID)
        self.command.extend(self.newAuth)
        self.command.extend(self.entityType)
        self.command.extend(int2bytearray(len(self.encData), 4))
        self.command.extend(self.encData)
        
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)
        
        self.command.extend(self.entityAuthHandle)
        self.command.extend(self.entitynonceOdd)
        self.command.extend(self.continueEntitySession)
        self.command.extend(self.entityAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="parentHandle=" + bytearray2hex(self.parentHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="protocolID=" + bytearray2hex(self.protocolID))
        self.insertTreeOutput(self.mTreeInId, 'end', text="newAuth=" + bytearray2hex(self.newAuth))
        self.insertTreeOutput(self.mTreeInId, 'end', text="entityType=" + bytearray2hex(self.entityType))
        self.insertTreeOutput(self.mTreeInId, 'end', text="encData=" + bytearray2hex(self.encData))
        
        
        self.inTreeAuthorization()

        self.insertTreeOutput(self.mTreeInId, 'end', text="entityAuthHandle=" + bytearray2hex(self.entityAuthHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="entitynonceOdd=" + bytearray2hex(self.entitynonceOdd))
        self.insertTreeOutput(self.mTreeInId, 'end', text="continueEntitySession=" + bytearray2hex(self.continueEntitySession))
        self.insertTreeOutput(self.mTreeInId, 'end', text="entityAuth=" + bytearray2hex(self.entityAuth))
        

    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10

        if(len(data)>index):
            dataLen = (data[(index)]<<24) + (data[(index+1)]<<16) + (data[(index+2)]<<8) + (data[(index+3)]<<0)
            index +=4
            self.outData = data[index:(index+dataLen)]
            index += dataLen
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.insertTreeOutput(self.mTreeOutId, 'end', text="outData=" + bytearray2hex(self.outData))

        index = self.outTreeAuthorization(data, index)        

        if(len(data)>index):
            self.entityNonceEven = data[index:(index+20)]
            index += 20
        if(len(data)>index):
            self.rContinueEntitySession = data[index:(index+1)]
            index +=1 #continueAuthSession
        if(len(data)>index):
            self.rEntityAuth = data[index:(index+20)]
            index += 20
        self.insertTreeOutput(self.mTreeOutId, 'end', text="entityNonceEven=" + bytearray2hex(self.entityNonceEven))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rContinueEntitySession=" + bytearray2hex(self.rContinueEntitySession))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rEntityAuth=" + bytearray2hex(self.rEntityAuth))
        
        if(self.responseCode==TPM_SUCCESS):
            Session.setNonceEven(self.entityAuthHandle, self.entityNonceEven)
        
        if(self.rContinueEntitySession == bytearray([0x00])):
            Session.destroySession(self.entityAuthHandle)

        if(self.responseCode==TPM_SUCCESS):
            Object.addObject(self.outData,  self.authValue)
