from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_CreateWrapKey
from TPM_Constants_V1 import TPM_SUCCESS
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session
from TPM_Object import Object
from TestInterpreterCrypto import ADIP

class TPM_CreateWrapKey(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_CreateWrapKey
        self.parentHandle = self.interpretParameter(parameters[2])
        self.dataUsageAuth = self.interpretParameter(parameters[3])
        self.dataMigrationAuth = self.interpretParameter(parameters[4])
        self.keyInfo = bytearray() # self.interpretParameter(parameters[5])
        self.keyInfoVer = bytearray()
        self.keyInfoKeyUsage = bytearray()
        self.keyInfoKeyFlags	= bytearray()
        self.keyInfoAuthDataUsage = bytearray()
        self.keyInfoAlgorithmParams = bytearray()	
        self.keyInfoAlgorithmParamsAlgorithmID = bytearray()	
        self.keyInfoAlgorithmParamsEncScheme = bytearray()
        self.keyInfoAlgorithmParamsSigScheme	= bytearray()
        self.keyInfoAlgorithmParamsParms = bytearray()
        self.keyInfoPCRInfo = bytearray()
        self.inKeyPCRInfoPcrSelection = bytearray()
        self.inKeyPCRInfoDigestAtRelease = bytearray()
        self.inKeyPCRInfoDigestAtCreation = bytearray()
        self.keyInfoPubKey = bytearray()
        self.keyInfoEncData	= bytearray()
        self.authHandle = self.interpretParameter(parameters[6])
        self.nonceOdd = self.interpretParameter(parameters[7])
        self.continueAuthSession = self.interpretParameter(parameters[8])
        self.pubAuth = bytearray()

        self.authValue = bytearray()

        #response
        self.wrappedKey = bytearray()
        self.wrappedKeyVer = bytearray()
        self.wrappedKeyKeyUsage = bytearray()
        self.wrappedKeyKeyFlags = bytearray()
        self.wrappedKeyAuthDataUsage = bytearray()
        self.wrappedKeyAlgorithmParams = bytearray()
        self.wrappedKeyAlgorithmParamsAlgorithmID = bytearray()
        self.wrappedKeyAlgorithmParamsEncScheme = bytearray()
        self.wrappedKeyAlgorithmParamsSigScheme = bytearray()
        self.wrappedKeyAlgorithmParamsParms = bytearray()
        self.wrappedKeyPCRInfo = bytearray()
        self.wrappedKeyPubKey = bytearray()
        self.wrappedKeyEncData = bytearray()
        
        
        if(self.forDoc==True): return

        keyInfoStruct = self.interpret_TPM_KEY(parameters[5])
        self.keyInfo = keyInfoStruct["key"] 
        self.keyInfoVer = keyInfoStruct["ver"]
        self.keyInfoKeyUsage = keyInfoStruct["keyUsage"]
        self.keyInfoKeyFlags	=keyInfoStruct["keyFlags"]
        self.keyInfoAuthDataUsage = keyInfoStruct["authDataUsage"]
        self.keyInfoAlgorithmParams = keyInfoStruct["algorithmParams"]
        self.keyInfoAlgorithmParamsAlgorithmID = keyInfoStruct["algorithmID"]
        self.keyInfoAlgorithmParamsEncScheme = keyInfoStruct["encScheme"]
        self.keyInfoAlgorithmParamsSigScheme	= keyInfoStruct["sigScheme"]
        self.keyInfoAlgorithmParamsParms = keyInfoStruct["parms"]
        self.keyInfoPCRInfo = keyInfoStruct["PCRInfo"]
        self.keyPCRInfoPcrSelection = keyInfoStruct["PCRInfoPcrSelection"]
        self.keyPCRInfoDigestAtRelease = keyInfoStruct["PCRInfoDigestAtRelease"]
        self.keyPCRInfoDigestAtCreation = keyInfoStruct["PCRInfoDigestAtCreation"]
        self.keyInfoPubKey = keyInfoStruct["pubKey"]
        self.keyInfoEncData	= keyInfoStruct["encData"]

        self.HMACHandles = bytearray()
        self.HMACHandles.extend(self.parentHandle)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.dataUsageAuth)	
        self.HMACParameters.extend(self.dataMigrationAuth)	
        self.HMACParameters.extend(self.keyInfo)	
        
        self.pubAuth = Session.getAuthValue(self)

        self.authValue = ADIP(self.authHandle,  self.dataUsageAuth)
        
        
    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.parentHandle)
                                                        + len(self.dataUsageAuth)
                                                        + len(self.dataMigrationAuth)
                                                        + len(self.keyInfo)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.parentHandle)
        self.command.extend(self.dataUsageAuth)
        self.command.extend(self.dataMigrationAuth)
        self.command.extend(self.keyInfo)
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)

        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="parentHandle=" + bytearray2hex(self.parentHandle))
        self.insertTreeOutput(self.mTreeInId, 'end', text="dataUsageAuth=" + bytearray2hex(self.dataUsageAuth))
        self.insertTreeOutput(self.mTreeInId, 'end', text="dataMigrationAuth=" + bytearray2hex(self.dataMigrationAuth))
        
        self.mTreeInKeyInfoId = self.insertTreeOutput(self.mTreeInId, 'end', text="keyInfo=" + bytearray2hex(self.keyInfo))
        self.insertTreeOutput(self.mTreeInKeyInfoId, 'end', text="keyInfoVer=" + bytearray2hex(self.keyInfoVer))
        self.insertTreeOutput(self.mTreeInKeyInfoId, 'end', text="keyInfoKeyUsage=" + bytearray2hex(self.keyInfoKeyUsage))
        self.insertTreeOutput(self.mTreeInKeyInfoId, 'end', text="keyInfoKeyFlags=" + bytearray2hex(self.keyInfoKeyFlags))
        self.insertTreeOutput(self.mTreeInKeyInfoId, 'end', text="keyInfoAuthDataUsage=" + bytearray2hex(self.keyInfoAuthDataUsage))
        self.mTreeInKeyInfoAlgoParamsId = self.insertTreeOutput(self.mTreeInKeyInfoId, 'end', text="keyInfoAlgorithmParams=" + bytearray2hex(self.keyInfoAlgorithmParams))
        self.mTreeInKeyInfoPCRInfoParamsId = self.insertTreeOutput(self.mTreeInKeyInfoId, 'end', text="keyInfoPCRInfo=" + bytearray2hex(self.keyInfoPCRInfo))
        self.insertTreeOutput(self.mTreeInKeyInfoId, 'end', text="keyInfoPubKey=" + bytearray2hex(self.keyInfoPubKey))
        self.insertTreeOutput(self.mTreeInKeyInfoId, 'end', text="keyInfoEncData=" + bytearray2hex(self.keyInfoEncData))

        self.insertTreeOutput(self.mTreeInKeyInfoAlgoParamsId, 'end', text="keyInfoAlgorithmParamsAlgorithmID=" + bytearray2hex(self.keyInfoAlgorithmParamsAlgorithmID))
        self.insertTreeOutput(self.mTreeInKeyInfoAlgoParamsId, 'end', text="keyInfoAlgorithmParamsEncScheme=" + bytearray2hex(self.keyInfoAlgorithmParamsEncScheme))
        self.insertTreeOutput(self.mTreeInKeyInfoAlgoParamsId, 'end', text="keyInfoAlgorithmParamsSigScheme=" + bytearray2hex(self.keyInfoAlgorithmParamsSigScheme))
        self.insertTreeOutput(self.mTreeInKeyInfoAlgoParamsId, 'end', text="keyInfoAlgorithmParamsParms=" + bytearray2hex(self.keyInfoAlgorithmParamsParms))

        self.insertTreeOutput(self.mTreeInKeyInfoPCRInfoParamsId, 'end', text="keyPCRInfoPcrSelection=" + bytearray2hex(self.keyPCRInfoPcrSelection))
        self.insertTreeOutput(self.mTreeInKeyInfoPCRInfoParamsId, 'end', text="keyPCRInfoDigestAtRelease=" + bytearray2hex(self.keyPCRInfoDigestAtRelease))
        self.insertTreeOutput(self.mTreeInKeyInfoPCRInfoParamsId, 'end', text="keyPCRInfoDigestAtCreation=" + bytearray2hex(self.keyPCRInfoDigestAtCreation))

        self.inTreeAuthorization()


    def receive(self,  data):
        if(len(data)<10):
            self.increaseFailure("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10
        if(len(data)>index):
            TPM_KEY_Struct = self.interpret_TPM_KEY(data[index:])
            dataLen = TPM_KEY_Struct["len"]
            self.wrappedKey = TPM_KEY_Struct["key"] 
            self.wrappedKeyVer = TPM_KEY_Struct["ver"]
            self.wrappedKeyKeyUsage = TPM_KEY_Struct["keyUsage"]
            self.wrappedKeyKeyFlags	=TPM_KEY_Struct["keyFlags"]
            self.wrappedKeyAuthDataUsage = TPM_KEY_Struct["authDataUsage"]
            self.wrappedKeyAlgorithmParams = TPM_KEY_Struct["algorithmParams"]
            self.wrappedKeyAlgorithmParamsAlgorithmID = TPM_KEY_Struct["algorithmID"]
            self.wrappedKeyAlgorithmParamsEncScheme = TPM_KEY_Struct["encScheme"]
            self.wrappedKeyAlgorithmParamsSigScheme	= TPM_KEY_Struct["sigScheme"]
            self.wrappedKeyAlgorithmParamsParms = TPM_KEY_Struct["parms"]
            self.wrappedKeyPCRInfo = TPM_KEY_Struct["PCRInfo"]
            self.wrappedKeyPubKey = TPM_KEY_Struct["pubKey"]
            self.wrappedKeyEncData	= TPM_KEY_Struct["encData"]
            index += dataLen
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.mTreeOutWrappedKeyId = self.insertTreeOutput(self.mTreeOutId, 'end', text="wrappedKey=" + bytearray2hex(self.wrappedKey))
        self.insertTreeOutput(self.mTreeOutWrappedKeyId, 'end', text="wrappedKeyVer=" + bytearray2hex(self.wrappedKeyVer))
        self.insertTreeOutput(self.mTreeOutWrappedKeyId, 'end', text="wrappedKeyKeyUsage=" + bytearray2hex(self.wrappedKeyKeyUsage))
        self.insertTreeOutput(self.mTreeOutWrappedKeyId, 'end', text="wrappedKeyKeyFlags=" + bytearray2hex(self.wrappedKeyKeyFlags))
        self.insertTreeOutput(self.mTreeOutWrappedKeyId, 'end', text="wrappedKeyAuthDataUsage=" + bytearray2hex(self.wrappedKeyAuthDataUsage))
        self.mTreeOutWrappedKeyAlgoParamsId = self.insertTreeOutput(self.mTreeOutWrappedKeyId, 'end', text="wrappedKeyAlgorithmParams=" + bytearray2hex(self.wrappedKeyAlgorithmParams))
        self.insertTreeOutput(self.mTreeOutWrappedKeyId, 'end', text="wrappedKeyPCRInfo=" + bytearray2hex(self.wrappedKeyPCRInfo))
        self.insertTreeOutput(self.mTreeOutWrappedKeyId, 'end', text="wrappedKeyPubKey=" + bytearray2hex(self.wrappedKeyPubKey))
        self.insertTreeOutput(self.mTreeOutWrappedKeyId, 'end', text="wrappedKeyEncData=" + bytearray2hex(self.wrappedKeyEncData))

        self.insertTreeOutput(self.mTreeOutWrappedKeyAlgoParamsId, 'end', text="wrappedKeyAlgorithmParamsAlgorithmID=" + bytearray2hex(self.wrappedKeyAlgorithmParamsAlgorithmID))
        self.insertTreeOutput(self.mTreeOutWrappedKeyAlgoParamsId, 'end', text="wrappedKeyAlgorithmParamsEncScheme=" + bytearray2hex(self.wrappedKeyAlgorithmParamsEncScheme))
        self.insertTreeOutput(self.mTreeOutWrappedKeyAlgoParamsId, 'end', text="wrappedKeyAlgorithmParamsSigScheme=" + bytearray2hex(self.wrappedKeyAlgorithmParamsSigScheme))
        self.insertTreeOutput(self.mTreeOutWrappedKeyAlgoParamsId, 'end', text="wrappedKeyAlgorithmParamsParms=" + bytearray2hex(self.wrappedKeyAlgorithmParamsParms))

        self.outTreeAuthorization(data, index)        

        
        if(self.responseCode==TPM_SUCCESS):
            Object.addObject(self.wrappedKeyEncData,  self.authValue)
