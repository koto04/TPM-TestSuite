from TPM_Commands_V1 import Command1
from TPM_Constants_V1 import TPM_ORD_AuthorizeMigrationKey
from TPM_Constants_V1 import TPM_KH_OWNER
from Utils import int2bytearray, bytearray2hex
from TPM_Session_V1 import Session

class TPM_AuthorizeMigrationKey(Command1):
    
    def __init__(self, parameters,  manager, forDoc=False):
        self.init(parameters, manager, forDoc)
        self.ordinal = TPM_ORD_AuthorizeMigrationKey
        self.migrationScheme = self.interpretParameter(parameters[2])
        self.migrationKey = bytearray()#self.interpretParameter(parameters[3])
        self.migrationKeyAlgorithmID = bytearray()
        self.migrationKeyEncScheme = bytearray()
        self.migrationKeySigScheme = bytearray()
        self.migrationKeyParms = bytearray()
        self.migrationKeyPubKey = bytearray()
       
        self.authHandle = self.parentAuthHandle = self.interpretParameter(parameters[4])
        self.nonceOdd = self.interpretParameter(parameters[5])
        self.continueAuthSession = self.interpretParameter(parameters[6])
        self.pubAuth = self.ownerAuth = bytearray()

        #response
        self.outData = bytearray()
        self.outDataMigrationKey = bytearray()
        self.outDataMigrationKeyAlgorithmID = bytearray()
        self.outDataMigrationKeyEncScheme = bytearray()
        self.outDataMigrationKeySigScheme = bytearray()
        self.outDataMigrationKeyParms = bytearray()
        self.outDataMigrationKeyPubKey = bytearray()
        self.outDataMigrationScheme = bytearray()
        self.outDataDigest = bytearray()
        
        if(self.forDoc==True): return

        pubKey = self.interpret_PUB_KEY(parameters[3])
        self.migrationKeyAlgorithmID = pubKey["algorithmID"]
        self.migrationKeyEncScheme = pubKey["encScheme"]
        self.migrationKeySigScheme = pubKey["sigScheme"]
        self.migrationKeyParms = pubKey["parms"]
        self.migrationKeyPubKey = pubKey["pubKey"]
        self.migrationKey = pubKey["key"]
                                        
        self.HMACHandles = bytearray()
        self.HMACHandles.extend(TPM_KH_OWNER)
        
        self.HMACParameters = bytearray()
        self.HMACParameters.extend(self.migrationScheme)
        self.HMACParameters.extend(self.migrationKey)
        
        self.pubAuth = self.ownerAuth = Session.getAuthValue(self)
           
    def prepare(self):


        self.paramSize = int2bytearray((4+len(self.ordinal)+len(self.tag)
                                                        + len(self.migrationScheme)
                                                        + len(self.migrationKey)
                                                        + len(self.authHandle)
                                                        + len(self.nonceOdd)
                                                        + len(self.continueAuthSession)
                                                        + len(self.pubAuth)
                                                        
                                                    ),4);
        self.command.extend(self.tag)
        self.command.extend(self.paramSize)
        self.command.extend(self.ordinal)
        self.command.extend(self.migrationScheme)
        self.command.extend(self.migrationKey)
        
        self.command.extend(self.authHandle)
        self.command.extend(self.nonceOdd)
        self.command.extend(self.continueAuthSession)
        self.command.extend(self.pubAuth)
        
        self.insertTreeOutput(self.mTreeInId, 'end', text="tag=" + bytearray2hex(self.tag))
        self.insertTreeOutput(self.mTreeInId, 'end', text="paramSize=" + bytearray2hex(self.paramSize))
        self.insertTreeOutput(self.mTreeInId, 'end', text="ordinal=" + bytearray2hex(self.ordinal))
    
        self.insertTreeOutput(self.mTreeInId, 'end', text="migrationScheme=" + bytearray2hex(self.migrationScheme))
        self.mTreeInMKId = self.insertTreeOutput(self.mTreeInId, 'end', text="migrationKey=" + bytearray2hex(self.migrationKey))
        
        self.insertTreeOutput(self.mTreeInMKId, 'end', text="migrationKeyAlgorithmID=" + bytearray2hex(self.migrationKeyAlgorithmID))
        self.insertTreeOutput(self.mTreeInMKId, 'end', text="migrationKeyEncScheme=" + bytearray2hex(self.migrationKeyEncScheme))
        self.insertTreeOutput(self.mTreeInMKId, 'end', text="migrationKeySigScheme=" + bytearray2hex(self.migrationKeySigScheme))
        self.insertTreeOutput(self.mTreeInMKId, 'end', text="migrationKeyParms=" + bytearray2hex(self.migrationKeyParms))
        self.insertTreeOutput(self.mTreeInMKId, 'end', text="migrationKeyPubKey=" + bytearray2hex(self.migrationKeyPubKey))
        
        self.inTreeAuthorization()
        

    def receive(self,  data):
        if(len(data)<10):
            self.outOutput("Unexpected response length. Expected minimum 10 byte, received " + str(len(data)) + " byte")
        self.rTag = data[0:2]
        self.rParamSize = data[2:6]
        self.responseCode = self.returnCode = data[6:10]
        index = 10
 
        temp = data[index:]
        pubKey = self.interpret_PUB_KEY(temp)

        self.outDataMigrationKey = pubKey["key"]
        self.outDataMigrationKeyAlgorithmID = pubKey["algorithmID"]
        self.outDataMigrationKeyEncScheme = pubKey["encScheme"]
        self.outDataMigrationKeySigScheme = pubKey["sigScheme"]
        self.outDataMigrationKeyParms = pubKey["parms"]
        self.outDataMigrationKeyPubKey = pubKey["pubKey"]
        
        dataLen = pubKey["len"]
        index += dataLen

        if(len(data)>index):
            self.outDataMigrationScheme = data[index:(index+2)]
            index +=2
            
        if(len(data)>index):
            self.outDataDigest = data[index:(index+20)]
            index +=20
            
        self.outData.extend(self.outDataMigrationKey)
        self.outData.extend(self.outDataMigrationScheme)
        self.outData.extend(self.outDataDigest)
                
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rTag=" + bytearray2hex(self.rTag))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rParamSize=" + bytearray2hex(self.rParamSize))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="returnCode=" + bytearray2hex(self.returnCode))

        self.mTreeOutOutDataId = self.insertTreeOutput(self.mTreeOutId, 'end', text="outData=" + bytearray2hex(self.outData))

        self.mTreeOutOutDataMKId = self.insertTreeOutput(self.mTreeOutOutDataId, 'end', text="outDataMigrationKey=" + bytearray2hex(self.outDataMigrationKey))
        self.insertTreeOutput(self.mTreeOutOutDataId, 'end', text="outDataMigrationScheme=" + bytearray2hex(self.outDataMigrationScheme))
        self.insertTreeOutput(self.mTreeOutOutDataId, 'end', text="outDataDigest=" + bytearray2hex(self.outDataDigest))


        self.insertTreeOutput(self.mTreeOutOutDataMKId, 'end', text="outDataMigrationKeyAlgorithmID=" + bytearray2hex(self.outDataMigrationKeyAlgorithmID))
        self.insertTreeOutput(self.mTreeOutOutDataMKId, 'end', text="outDataMigrationKeyEncScheme=" + bytearray2hex(self.outDataMigrationKeyEncScheme))
        self.insertTreeOutput(self.mTreeOutOutDataMKId, 'end', text="outDataMigrationKeySigScheme=" + bytearray2hex(self.outDataMigrationKeySigScheme))
        self.insertTreeOutput(self.mTreeOutOutDataMKId, 'end', text="outDataMigrationKeyParms=" + bytearray2hex(self.outDataMigrationKeyParms))
        self.insertTreeOutput(self.mTreeOutOutDataMKId, 'end', text="outDataMigrationKeyPubKey=" + bytearray2hex(self.outDataMigrationKeyPubKey))

        index = self.outTreeAuthorization(data, index)        
