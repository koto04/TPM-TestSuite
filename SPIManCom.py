import spidev
from TPM_Communication import *
import time
from TS_Globals import TS_Globals

class SPICom(TPM_Communication):
    
    spi = spidev.SpiDev()
    interface_type = TS_Globals.TPM_INTERFACE_SPI
        
    def __init__(self):
        GPIO.setup(PIRQ, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(SPI_CS, GPIO.OUT)
        GPIO.setup(SPI_CLK, GPIO.OUT)
        GPIO.setup(MOSI, GPIO.OUT)
        GPIO.setup(MISO, GPIO.IN, pull_up_down=GPIO.PUD_UP)           
        

        self.sendTimeout = 10 #1s
        self.receiveTimeout = 120 #2 min
        self.cycleDelay = 0.0000001
        time.sleep(0.1)
        TPM_Communication.sInterface = self


    def setVDD(self, state):
        print "vdd"
        if(state==0):
            self.spi.close()
            GPIO.setup(MISO, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)           
            GPIO.output(RST, GPIO.LOW)
            GPIO.output(PP, GPIO.LOW)
            GPIO.output(SPI_CS, GPIO.LOW)
            GPIO.output(SPI_CLK, GPIO.LOW)
            GPIO.output(MOSI, GPIO.LOW)
            GPIO.setup(PIRQ, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
            GPIO.output(POWER_3V3, GPIO.HIGH)
            
        elif(state==1):
            GPIO.output(POWER_3V3, GPIO.LOW)
            GPIO.setup(PIRQ, GPIO.IN, pull_up_down=GPIO.PUD_UP)            
            GPIO.setup(MISO, GPIO.IN, pull_up_down=GPIO.PUD_UP)           
            self.reset()

    
    def readRegister(self, spi_reg, size=-1):
        
        if(size==-1):
            size=spi_reg.size
        tx_buffer = bytearray([(0x80+size-1), 0xD4, ((self.locality | spi_reg.addr)>>8), (spi_reg.addr&0xFF)])
        for i in range (0,size):
            tx_buffer.append(0x00)
        rx_buffer = self.txrx(tx_buffer)
        print rx_buffer
        spi_reg.Fill(rx_buffer[4:])
    
    def writeRegister(self, spi_reg, data):
        fifoReg = False
        if(spi_reg==TPM_DATA_FIFO): fifoReg=True
        tx_buffer = bytearray([(0x00+len(data)-1), 0xD4, ((self.locality | spi_reg.addr)>>8), (spi_reg.addr&0xFF)])
        tx_buffer.extend(data)
        self.txrx(tx_buffer, fifoReg)

    def txrx(self, tx_buffer, fifoReg=False):
        GPIO.output(SPI_CLK, GPIO.LOW)
        GPIO.output(SPI_CS, GPIO.LOW)
        time.sleep(self.cycleDelay)
        rx_buffer = bytearray(len(tx_buffer))
        for txByte in range(0,len(tx_buffer)):
            for txBit in range(0,8):
                rx_bit = 0
                if(tx_buffer[txByte]&(0x80>>txBit))==0:
                    #print "1"
                    GPIO.output(MOSI, GPIO.LOW)
                else:
                    #print "0"
                    GPIO.output(MOSI, GPIO.HIGH)
                if(fifoReg==False): 
                    rx_bit = self.clockCycle()
                else:   
                    tear = False    
                    if self.clockTearStart==0 and self.clockTearCount>0:
                        tear = True 
                        self.clockTearCount -=1
                    elif(self.clockTearStart>0):
                        self.clockTearStart -=1 
                    rx_bit = self.clockCycle(tear)  
                    self.txClocks +=1   
                if (rx_bit) : rx_buffer[txByte]|=(0x80>>txBit)      
        time.sleep(self.cycleDelay)
        GPIO.output(SPI_CS, GPIO.HIGH)
        return rx_buffer        
                    
    def clockCycle(self, tear=False):
        time.sleep(self.cycleDelay)
        if(tear==False):
            GPIO.output(SPI_CLK, GPIO.HIGH)
        ret = GPIO.input(MISO)
        time.sleep(self.cycleDelay)
        GPIO.output(SPI_CLK, GPIO.LOW)
        return ret
        
class SPIComDummy(TPM_Communication):
    
    def __init__(self):
                
        self.sendTimeout = 10 #1s
        self.receiveTimeout = 120 #2 min
        time.sleep(0.1)


    def setVDD(self, state):
        if(state==0):
            print "Power OFF"
            
        elif(state==1):
			print "Power ON"
    
    def readRegister(self, spi_reg, size=-1):
        pass
    
    def writeRegister(self, spi_reg, data):
        pass
    
