from ctypes import c_int64

HASH_STATE_EMPTY = 0
HASH_STATE_HASH = 1
HASH_STATE_HMAC = 2


class TPM2B_NAME:
    size = c_int64()
    buffer = bytearray()

class TPM2B_NONCE:
    size = c_int64()
    buffer = bytearray()

class TPM2B_DATA:
    size = c_int64()
    buffer = bytearray()

class TPM2B_DIGEST:
    size = c_int64()
    buffer = bytearray()

class TPM2B:
    size = c_int64()
    buffer = bytearray()
    
class EVENT_2B:
    size = c_int64()
    buffer = bytearray()
    
class TPM2B_EVENT:
    t=EVENT_2B()
    b=TPM2B()
 
class TPM2B_AUTH(TPM2B_DIGEST):
    pass

class TPM_TK_AUTH:
    tag = bytearray()
    hierarchy = bytearray()
    digest = TPM2B_DIGEST()
    
class CPRI_HASH_STATE:
    state = bytearray()
    hashAlg = bytearray()    

class HASH_STATE:
    state = CPRI_HASH_STATE()
    type = HASH_STATE_EMPTY
 
class HMAC_STATE:
    hashState = HASH_STATE()
    hmacKey = bytearray
    

