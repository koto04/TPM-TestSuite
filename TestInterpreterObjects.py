from TS_Globals import TS_Globals

if(TS_Globals.TS_TPM_VERSION==TS_Globals.TPM_VERSION_1_2):
    from TPM_Session_V1 import Session
else:
    from TPM_Session_V2 import Session

from TPM_Object import Object

from TestInterpreter import TestManagerGateway
from Utils import hex2bytearray

def object(handle):
    if isinstance(handle, basestring):
        handle = hex2bytearray(handle)
    return Object.getObject(handle)

def session(handle):
    if isinstance(handle, basestring):
        handle = hex2bytearray(handle)
    return Session.getSession(handle)
    
def flushContext(data):
    if isinstance(data, basestring):
        data = hex2bytearray(data)
    elif isinstance(data, bytearray):
            pass
    else:
        TestManagerGateway.testManager.increaseException("Unknown Datatype") # @UndefinedVariable
        return
    TestManagerGateway.testManager.destroyHandle(data) # @UndefinedVariable
