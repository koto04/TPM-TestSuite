import base64

from TPM_Constants_V2 import TPM_RC_SUCCESS
from TPM_Constants_V2 import TPM_ALG_RSA, TPM_ALG_ECC, TPM_ALG_SYMCIPHER, TPM_ALG_KEYEDHASH, TPM_ALG_NULL
from TPM_Constants_V2 import TPM_constant
from TPM_Session_V2 import Session
from Utils import hex2bytearray, bytearray2hex, int2bytearray

class Command2:
    
    def init(self, parameters, manager, forDoc=False):
        
        self.FUZZY_MAX_PARAM_LENGTH = 0x600
        
        self.forDoc = forDoc
        
        self.time = 0
        self.command = bytearray();
        self.commandName = parameters[0]
        self.tag = self.interpretParameter(parameters[1])
        self.commandSize = bytearray(base64.b16decode(b'00000000', casefold=True))
        self.rTag = bytearray()
        self.responseSize = bytearray()
        self.responseCode = bytearray()
        self.nonceTPM =bytearray()
        self.rHMAC =bytearray()     
        self.definedSize=None
        self.extension=bytearray() 
        self.nonceTPM2 = bytearray()
        self.logEnable = True
        self.manager = manager
        
        self.mInParameters = parameters
        self.mTreeId = 0
        self.mTreeInId = 0
        self.mTreeOutId = 0
        if(self.logEnable==True):
            self.mTreeId = self.manager.insertTreeOutput('', 'end', text=self.commandName)
            self.mTreeInId = self.manager.insertTreeOutput(self.mTreeId, 'end', text="Input")
            self.mTreeOutId = self.manager.insertTreeOutput(self.mTreeId, 'end', text="Output")
        
        self.nonceCallerBackup = bytearray()
        
    def fuzzy(self, level):
        pass

    def increaseFailure(self, text):
        self.manager.increaseFailure(text)

    def writeLog(self, text, vim=False):
        self.manager.writeLog(text=text, vim=vim)
        
    def setDefinedSize(self, defSize):
        self.definedSize=[(defSize & (0xff << pos*8)) >> pos*8 for pos in range((4-1),-1,-1)]
    
    def setExtension(self,extension):
            if isinstance(extension, basestring):
                self.extension = hex2bytearray(extension)
            else:
                self.extension=extension
            
    def finalize(self):
        if(self.definedSize!=None):
            self.command[2:6] = self.definedSize
        self.writeLog(text="\nprocessing " + self.commandName + "...",vim=True)
        self.command.extend(self.extension)

    
    def commandLen(self):
        ret = 0
        for i in range(0,4):
            ret = (ret<<8) + self.commandSize[i]
        return ret
            
    def inTreeAuthorization(self):
        self.insertTreeOutput(self.mTreeInId, 'end', text="nonceCaller=" + bytearray2hex(self.nonceCaller))
        self.insertTreeOutput(self.mTreeInId, 'end', text="sessionAttribute=" + bytearray2hex(self.sessionAttribute))
        if(Session.authPolicy(self.sessionHandle)!=Session.POLICY_PASSWORD):
            self.insertTreeOutput(self.mTreeInId, 'end', text="sessionAuthValue (HMAC)=" + bytearray2hex(self.sessionAuthValue))
        else:    
            self.insertTreeOutput(self.mTreeInId, 'end', text="sessionAuthValue (Password)=" + bytearray2hex(self.sessionAuthValue))
    
    def outTreeAuthorization(self, data, index):
        x=len(data)
        if(x>index):
            lenNonceTPM = (data[index]<<8) + data[(index+1)]
            index+=2
            self.nonceTPM = data[index:(index+lenNonceTPM)]
            index += lenNonceTPM
            index += 1 #session attribute
            
            lenRHMAC = (data[index]<<8) + data[(index+1)]
            index +=2
            self.rHMAC = data[index:(index+lenRHMAC)]
            index += lenRHMAC
        
        self.insertTreeOutput(self.mTreeOutId, 'end', text="nonceTPM=" + bytearray2hex(self.nonceTPM))
        self.insertTreeOutput(self.mTreeOutId, 'end', text="rHMAC=" + bytearray2hex(self.rHMAC))
        if(self.responseCode==TPM_RC_SUCCESS):
            Session.setNonceTPM(self.sessionHandle, self.nonceTPM)
        
        return index
    def insertTreeOutput(self, tid, pos, text, tag=""):
        return self.manager.insertTreeOutput(tid, pos, text=text, tag=tag)
    
    def interpretOutCreationData(self,  creationData):
        #TPM2B_PUBLIC (Part2 p. 163)
        self.creationDataPcrSelect = bytearray()
        self.creationDataPcrSelectCount = bytearray()
        self.creationDataPcrSelectHash = []
        self.creationDataPcrSelectSelect = []
        self.creationDataPcrDigest = bytearray()
        self.creationDataLocality = bytearray()
        self.creationDataParentNameAlg= bytearray()
        self.creationDataParentName = bytearray()
        self.creationDataParentQualifiedName=bytearray()
        self.creationDataOutsideInfo = bytearray()
        self.pcrSelectCount = 0
        index = 0
        if(len(creationData)>index):
            self.pcrSelectCount = (creationData[index]<<24) + (creationData[(index+1)]<<16) + (creationData[(index+2)]<<8) + (creationData[(index+3)]<<0)
            self.creationDataPcrSelectCount = creationData[index:(index+4)]
            self.creationDataPcrSelect.extend(self.creationDataPcrSelectCount)
            index+=4
            for i in range(0, self.pcrSelectCount):
                self.creationDataPcrSelectHash.append(creationData[index:(index+2)])
                self.creationDataPcrSelect.extend(creationData[index:(index+2)])
                index +=2
                lenSelection = creationData[index]
                self.creationDataPcrSelect.extend(creationData[index:(index+1)])
                index +=1
                self.creationDataPcrSelectSelect.append(creationData[index:(index+lenSelection)])
                self.creationDataPcrSelect.extend(creationData[index:(index+lenSelection)])
                index += lenSelection
                
        if(len(creationData)>index):
            lenData = (creationData[index]<<8) + creationData[(index+1)]
            index +=2
            self.creationDataPcrDigest = creationData[index:(index+lenData)]
            index += lenData
            
        if(len(creationData)>index):
            self.creationDataLocality = creationData[index:(index+1)]
            index +=1
            
        if(len(creationData)>index):
            self.creationDataParentNameAlg = creationData[index:(index+2)]
            index +=2

        if(len(creationData)>index):
            lenData = (creationData[index]<<8) + creationData[(index+1)]
            index +=2
            self.creationDataParentName = creationData[index:(index+lenData)]
            index += lenData

        if(len(creationData)>index):
            lenData = (creationData[index]<<8) + creationData[(index+1)]
            index +=2
            self.creationDataParentQualifiedName = creationData[index:(index+lenData)]
            index += lenData

        if(len(creationData)>index):
            lenData = (creationData[index]<<8) + creationData[(index+1)]
            index +=2
            self.creationDataOutsideInfo = creationData[index:(index+lenData)]
            index += lenData
    
    def interpretOutTPM2B_PUBLIC(self, outPublic):    
        #TPM2B_PUBLIC (Part2 p. 149)
        
        self.outPublicType = bytearray()
        self.outPublicNameAlg = bytearray()
        self.outPublicObjectAttributes = bytearray()
        self.outPublicAuthPolicy = bytearray()
        self.outPublicParameters = bytearray()
        self.outPublicUnique = bytearray()
        self.outPublicUniqueX = bytearray()
        self.outPublicUniqueY = bytearray()
        index=0
        
        if(len(outPublic)<=index): return
        
        self.outPublicType = outPublic[0:2]
        self.outPublicNameAlg = outPublic[2:4]
        self.outPublicObjectAttributes = outPublic[4:8]
        index = 8
        if(len(outPublic)<=index): return
        lenPublicAuthPolicty = (outPublic[index]<<8) + outPublic[(index+1)]
        index += 2
        if(len(outPublic)<=index): return
        if(lenPublicAuthPolicty==0):
            self.outPublicAuthPolicy = bytearray()
        else:
            self.outPublicAuthPolicy = outPublic[index:(index+lenPublicAuthPolicty)]
            
        index += lenPublicAuthPolicty
        if(len(outPublic)<=index): return
        if(self.outPublicType==TPM_ALG_RSA):
            rsa_params_alg_sym_object = outPublic[index:(index+2)]
            lenPublicParameters = 2
            if(rsa_params_alg_sym_object!=TPM_ALG_NULL):
                lenPublicParameters += 4
            rsa_params_alg_rsa_sheme = outPublic[(index+lenPublicParameters):(index+lenPublicParameters+2)]
            lenPublicParameters +=2
            if(rsa_params_alg_rsa_sheme!=TPM_ALG_NULL):
                lenPublicParameters += 2
            lenPublicParameters +=2
            lenPublicParameters +=4
            self.outPublicParameters = outPublic[index:(index+lenPublicParameters)]
        elif(self.outPublicType==TPM_ALG_KEYEDHASH):
            lenPublicParameters = (2+2)
            self.outPublicParameters = outPublic[index:(index+lenPublicParameters)]
        elif(self.outPublicType==TPM_ALG_SYMCIPHER):
            lenPublicParameters = (2+2+2+2)
            self.outPublicParameters = outPublic[index:(index+lenPublicParameters)]
        elif(self.inPublicType==TPM_ALG_ECC):
            lenPublicParameters = (2+2+2+2)
            self.outPublicParameters = outPublic[index:(index+lenPublicParameters)]
        else:
            lenPublicParameters = (2+2+2+2+2)
            self.outPublicParameters = outPublic[index:(index+lenPublicParameters)]
             
        #PublicUnique
        index += lenPublicParameters
        if(len(outPublic)<=index): return
        if(self.outPublicType==TPM_ALG_RSA):
            lenPublicUnique = (outPublic[index]<<8) + outPublic[(index+1)]
            index += 2
            self.outPublicUnique = outPublic[index:(index+lenPublicUnique)]
        elif(self.outPublicType==TPM_ALG_KEYEDHASH):
            lenPublicUnique = (outPublic[index]<<8) + outPublic[(index+1)]
            index += 2
            self.outPublicUnique = outPublic[index:(index+lenPublicUnique)]
        elif(self.outPublicType==TPM_ALG_SYMCIPHER):
            lenPublicUnique = (outPublic[index]<<8) + outPublic[(index+1)]
            index += 2
            self.outPublicUnique = outPublic[index:(index+lenPublicUnique)]
        elif(self.outPublicType==TPM_ALG_ECC):
            startUnique = index
            lenPublicUnique = (outPublic[index]<<8) + outPublic[(index+1)]  
            index += 2      
            self.outPublicUniqueX = outPublic[index:(index+lenPublicUnique)]
            index +=lenPublicUnique
            lenPublicUnique = (outPublic[index]<<8) + outPublic[(index+1)]  
            index += 2      
            self.outPublicUniqueY = outPublic[index:(index+lenPublicUnique)]
            index +=lenPublicUnique
            self.outPublicUnique = outPublic[startUnique:index]
        
    def interpretInTPM2B_PUBLIC(self,  param):
        #TPM2B_PUBLIC (Part2 p. 149)
        
        self.inPublicType = bytearray()
        self.inPublicNameAlg = bytearray()
        self.inPublicObjectAttributes = bytearray()
        self.inPublicAuthPolicy = bytearray()
        self.inPublicParameters = bytearray()
        self.inPublicUnique = bytearray()
        self.inPublicUnique2 = bytearray()
        
        if(isinstance(param, list)):
            p = param
            self.inPublic = bytearray()
            
            self.inPublicType = self.interpretParameter(p[0])
            self.inPublic.extend(self.inPublicType)
            
            self.inPublicNameAlg = self.interpretParameter(p[1])
            self.inPublic.extend(self.inPublicNameAlg)
            
            self.inPublicObjectAttributes = self.interpretParameter(p[2])
            self.inPublic.extend(self.inPublicObjectAttributes)
            
            self.inPublicAuthPolicy = self.interpretParameter(p[3])
            self.inPublic.extend(int2bytearray(len(self.inPublicAuthPolicy), 2))
            self.inPublic.extend(self.inPublicAuthPolicy)
            
            self.inPublicParameters = self.interpretParameter(p[4])
            self.inPublic.extend(self.inPublicParameters)
            
            self.inPublicUnique = self.interpretParameter(p[5])
            self.inPublic.extend(int2bytearray(len(self.inPublicUnique), 2))
            self.inPublic.extend(self.inPublicUnique)
            
            if(len(p)>6):
                self.inPublicUnique2 = self.interpretParameter(p[6])
                self.inPublic.extend(int2bytearray(len(self.inPublicUnique2), 2))
                self.inPublic.extend(self.inPublicUnique2)

        else:
            self.inPublic  = self.interpretParameter(param) 
            self.inPublicType = self.inPublic[0:2]
            self.inPublicNameAlg = self.inPublic[2:4]
            self.inPublicObjectAttributes = self.inPublic[4:8]
            index = 8
            lenPublicAuthPolicty = (self.inPublic[index]<<8) + self.inPublic[(index+1)]
            index += 2
            if(lenPublicAuthPolicty==0):
                self.inPublicAuthPolicy = bytearray()
            else:
                self.inPublicAuthPolicy = self.inPublic[index:(index+lenPublicAuthPolicty)]
            
            index += lenPublicAuthPolicty
            if(self.inPublicType==TPM_ALG_RSA):
                rsa_params_alg_sym_object = self.inPublic[index:(index+2)]
                lenPublicParameters = 2
                if(rsa_params_alg_sym_object!=TPM_ALG_NULL):
                    lenPublicParameters += 4
                rsa_params_alg_rsa_sheme = self.inPublic[(index+lenPublicParameters):(index+lenPublicParameters+2)]
                lenPublicParameters +=2
                if(rsa_params_alg_rsa_sheme!=TPM_ALG_NULL):
                    lenPublicParameters += 2
                lenPublicParameters +=2
                lenPublicParameters +=4
                self.inPublicParameters = self.inPublic[index:(index+lenPublicParameters)]
            elif(self.inPublicType==TPM_ALG_KEYEDHASH):
                lenPublicParameters = (2+2)
                self.inPublicParameters = self.inPublic[index:(index+lenPublicParameters)]
            elif(self.inPublicType==TPM_ALG_SYMCIPHER):
                lenPublicParameters = (2+2+2)
                self.inPublicParameters = self.inPublic[index:(index+lenPublicParameters)]
            elif(self.inPublicType==TPM_ALG_ECC):
                lenPublicParameters = (2+2+2+2)
                self.inPublicParameters = self.inPublic[index:(index+lenPublicParameters)]
            else:
                lenPublicParameters = (2+2+2+2+2)
                self.inPublicParameters = self.inPublic[index:(index+lenPublicParameters)]
             
            #PublicUnique
            index += lenPublicParameters
            if(self.inPublicType==TPM_ALG_RSA):
                lenPublicUnique = (self.inPublic[index]<<8) + self.inPublic[(index+1)]
                index += 2
                self.inPublicUnique = self.inPublic[index:(index+lenPublicUnique)]
            elif(self.inPublicType==TPM_ALG_KEYEDHASH):
                lenPublicUnique = (self.inPublic[index]<<8) + self.inPublic[(index+1)]
                index += 2
                self.inPublicUnique = self.inPublic[index:(index+lenPublicUnique)]
            elif(self.inPublicType==TPM_ALG_SYMCIPHER):
                lenPublicUnique = (self.inPublic[index]<<8) + self.inPublic[(index+1)]
                index += 2
                self.inPublicUnique = self.inPublic[index:(index+lenPublicUnique)]
            elif(self.inPublicType==TPM_ALG_ECC):
                lenPublicUnique = (self.inPublic[index]<<8) + self.inPublic[(index+1)]
                index += 2
                self.inPublicUnique = self.inPublic[index:(index+lenPublicUnique)]
                index += lenPublicUnique
                lenPublicUnique = (self.inPublic[index]<<8) + self.inPublic[(index+1)]
                index += 2
                self.inPublicUnique2 = self.inPublic[index:(index+lenPublicUnique)]
            
            index += lenPublicUnique
        
    def interpretParameter(self,value):
        try:
            if(isinstance(value, bytearray)):
                return value
            elif(isinstance(value,  basestring)):
                if(TPM_constant.has_key(value)):
                    return TPM_constant[value]
                elif(value==""):
                    return bytearray()
                else:
                    return bytearray(base64.b16decode(value.replace(" ", ""), casefold=True))
            else:
                return bytearray()
        except Exception, e:            
            self.manager.sGUI.outOutput("Test execution exception at interpret parameter")
            self.manager.sGUI.outOutput(str(e))
            self.manager.sGUI.increaseException()




#index 0 => class name
#index 1 => auth handle (not used yet)
#index 2 => implemented in TS
#index 3 => mandatory TPM command
#index 4 => TPM-Version
TPM_command = {}
TPM_command["TPM_CC_Startup"] = ["TPM2_Startup", [], True, True,  2]
TPM_command["TPM_CC_StartAuthSession"] = ["TPM2_StartAuthSession", [], True, True,  2]
TPM_command["TPM_CC_ChangeEPS"] = ["TPM2_ChangeEPS", [], True,  False,  2]
TPM_command["TPM_CC_ChangePPS"] = ["TPM2_ChangePPS", [], True,  False,  2]
TPM_command["TPM_CC_ClearControl"] = ["TPM2_ClearControl", [], True, True,  2]
TPM_command["TPM_CC_FlushContext"] = ["TPM2_FlushContext", [], True, True,  2]
TPM_command["TPM_CC_Clear"] = ["TPM2_Clear", [], True, True,  2]
TPM_command["TPM_CC_HierarchyChangeAuth"] = ["TPM2_HierarchyChangeAuth", [], True, True,  2]
TPM_command["TPM_CC_CreatePrimary"] = ["TPM2_CreatePrimary", [], True, True,  2]
TPM_command["TPM_CC_PCR_Event"] = ["TPM2_PCR_Event" , [], True, True,  2]
TPM_command["TPM_CC_EvictControl"] = ["TPM2_EvictControl", [], True, True,  2]
TPM_command["TPM_CC_PCR_Read"] = ["TPM2_PCR_Read", [], True, True,  2]
TPM_command["TPM_CC_PolicyPCR"] = ["TPM2_PolicyPCR", [], True, True,  2]
TPM_command["TPM_CC_PolicyPassword"] = ["TPM2_PolicyPassword", [], True, True,  2]
TPM_command["TPM_CC_PolicyGetDigest"] = ["TPM2_PolicyGetDigest", [], True, True,  2]
TPM_command["TPM_CC_Create"] = ["TPM2_Create", [], True, True,  2]
TPM_command["TPM_CC_Load"] = ["TPM2_Load", [], True, True,  2]
TPM_command["TPM_CC_RSA_Decrypt"] = ["TPM2_RSA_Decrypt", [], True, True,  2]
TPM_command["TPM_CC_RSA_Encrypt"] = ["TPM2_RSA_Encrypt", [], True, True,  2]
TPM_command["TPM_CC_Shutdown"] = ["TPM2_Shutdown", [], True, True,  2]
TPM_command["TPM_CC_Hash"] = ["TPM2_Hash", [], True, True,  2]
TPM_command["TPM_CC_PolicyAuthorize"] = ["TPM2_PolicyAuthorize", [], True, True,  2]
TPM_command["TPM_CC_PolicySecret"] = ["TPM2_PolicySecret", [], True, True,  2]
TPM_command["TPM_CC_PolicyAuthValue"] = ["TPM2_PolicyAuthValue", [], True, True,  2]
TPM_command["TPM_CC_PolicySigned"] = ["TPM2_PolicySigned", [], True, True,  2]
TPM_command["TPM_CC_PolicyTicket"] = ["TPM2_PolicyTicket", [], True,  False,  2]
TPM_command["TPM_CC_VerifySignature"] = ["TPM2_VerifySignature", [], True, True,  2]
TPM_command["TPM_CC_Sign"] = ["TPM2_Sign", [], True, True,  2]
TPM_command["TPM_CC_NV_DefineSpace"] = ["TPM2_NV_DefineSpace", [], True, True,  2] 
TPM_command["TPM_CC_NV_Read"] = ["TPM2_NV_Read", [], True, True,  2]
TPM_command["TPM_CC_NV_ReadPublic"] = ["TPM2_NV_ReadPublic", [], True, True,  2]
TPM_command["TPM_CC_NV_SetBits"] = ["TPM2_NV_SetBits", [], True, True,  2] 
TPM_command["TPM_CC_NV_Increment"] = ["TPM2_NV_Increment", [], True, True,  2]
TPM_command["TPM_CC_FieldUpgradeStart"] = ["TPM2_FieldUpgradeStart", [], True,  False,  2]
TPM_command["TPM_CC_FieldUpgradeStartVendor"] = ["TPM2_FieldUpgradeStartVendor", [], True, True,  2]
TPM_command["TPM_CC_PolicyPhysicalPresence"] = ["TPM2_PolicyPhysicalPresence", [], True,  False,  2]
TPM_command["TPM_CC_GetCapability"] = ["TPM2_GetCapability", [], True, True,  2]
TPM_command["TPM_CC_PolicyCommandCode"] = ["TPM2_PolicyCommandCode", [], True, True,  2]
TPM_command["TPM_CC_NV_Write"] = ["TPM2_NV_Write", [], True, True,  2]
TPM_command["TPM_CC_NV_WriteLock"] = ["TPM2_NV_WriteLock", [], True, True,  2]
TPM_command["TPM_CC_PolicyNV"] = ["TPM2_PolicyNV", [], True, True,  2]
TPM_command["TPM_CC_PolicyCounterTimer"] = ["TPM2_PolicyCounterTimer", [], True, True,  2]
TPM_command["TPM_CC_ReadClock"] = ["TPM2_ReadClock", [], True, True,  2]
TPM_command["TPM_CC_PolicyCpHash"] = ["TPM2_PolicyCpHash", [], True, True,  2]
TPM_command["TPM_CC_SetPrimaryPolicy"] = ["TPM2_SetPrimaryPolicy", [], True, True,  2]
TPM_command["TPM_CC_PolicyDuplicationSelect"] = ["TPM2_PolicyDuplicationSelect", [], True, True,  2]
TPM_command["TPM_CC_Duplicate"] = ["TPM2_Duplicate", [], True, True,  2]
TPM_command["TPM_CC_ReadPublic"] = ["TPM2_ReadPublic", [], True, True,  2]
TPM_command["TPM_CC_PolicyLocality"] = ["TPM2_PolicyLocality", [], True, True,  2]
TPM_command["TPM_CC_PolicyNameHash"] = ["TPM2_PolicyNameHash", [], True, True,  2]
TPM_command["TPM_CC_PolicyOR"] = ["TPM2_PolicyOR", [], True, True,  2]
TPM_command["TPM_CC_PolicyNvWritten"] = ["TPM2_PolicyNvWritten", [], True, True,  2]
TPM_command["TPM_CC_PolicyRestart"] = ["TPM2_PolicyRestart", [], True, True,  2]
TPM_command["TPM_CC_HierarchyControl"] = ["TPM2_HierarchyControl", [], True, True,  2]
TPM_command["TPM_CC_NV_UndefineSpace"] = ["TPM2_NV_UndefineSpace", [], True, True,  2]
TPM_command["TPM_CC_NV_Extend"] = ["TPM2_NV_Extend", [], True, True,  2]
TPM_command["TPM_CC_DictionaryAttackParameters"] = ["TPM2_DictionaryAttackParameters", [], True, True,  2]
TPM_command["TPM_CC_DictionaryAttackLockReset"] = ["TPM2_DictionaryAttackLockReset", [], True, True,  2]
TPM_command["TPM_CC_NV_ChangeAuth"] = ["TPM2_NV_ChangeAuth", [], True, True,  2]
TPM_command["TPM_CC_PCR_SetAuthPolicy"] = ["TPM2_PCR_SetAuthPolicy", [], True,  False,  2]
TPM_command["TPM_CC_PCR_Extend"] = ["TPM2_PCR_Extend", [], True, True,  2]
TPM_command["TPM_CC_GetRandom"] = ["TPM2_GetRandom", [], True, True,  2]
TPM_command["TPM_CC_PCR_Reset"] = ["TPM2_PCR_Reset", [], True, True,  2]
TPM_command["TPM_CC_ContextSave"] = ["TPM2_ContextSave", [], True, True,  2]
TPM_command["TPM_CC_ContextLoad"] = ["TPM2_ContextLoad", [], True, True,  2]
TPM_command["TPM_CC_MakeCredential"] = ["TPM2_MakeCredential", [], True, True,  2]
TPM_command["TPM_CC_Quote"] = ["TPM2_Quote", [], True, True,  2]
TPM_command["TPM_CC_CertifyCreation"] = ["TPM2_CertifyCreation", [], True, True,  2]
TPM_command["TPM_CC_LoadExternal"] = ["TPM2_LoadExternal", [], True, True,  2]
TPM_command["TPM_CC_Unseal"] = ["TPM2_Unseal", [], True, True,  2]
TPM_command["TPM_CC_Import"] = ["TPM2_Import", [], True, True,  2]
TPM_command["TPM_CC_ClockSet"] = ["TPM2_ClockSet", [], True, True,  2]
TPM_command["TPM_CC_ECDH_ZGen"] = ["TPM2_ECDH_ZGen", [], True, True,  2]
TPM_command["TPM_CC_ECDH_KeyGen"] = ["TPM2_ECDH_KeyGen", [], True, True,  2]


TPM_command["ChipInfo"] = ["ChipInfo", [], True, True,  2]
TPM_command["Startup"] = ["Startup", [], True, True,  2]


TPM_command["TPM_CC_NV_UndefineSpaceSpecial"] = ["TPM2_NV_UndefineSpaceSpecial", [], False, True,  2]
TPM_command["TPM_CC_PCR_Allocate"] = ["TPM2_PCR_Allocate", [], False, True,  2]
TPM_command["TPM_CC_PP_Commands"] = ["TPM2_PP_Commands", [], False,  False,  2]
TPM_command["TPM_CC_ClockRateAdjust"] = ["TPM2_ClockRateAdjust", [], False, True,  2]
TPM_command["TPM_CC_NV_GlobalWriteLock"] = ["TPM2_NV_GlobalWriteLock", [], False,  False,  2]
TPM_command["TPM_CC_GetCommandAuditDigest"] = ["TPM2_GetCommandAuditDigest", [], False,  False,  2]
TPM_command["TPM_CC_SequenceComplete"] = ["TPM2_SequenceComplete", [], False, True,  2]
TPM_command["TPM_CC_SetAlgorithmSet"] = ["TPM2_SetAlgorithmSet", [], False,  False,  2]
TPM_command["TPM_CC_SetCommandCodeAuditStatus"] = ["TPM2_SetCommandCodeAuditStatus", [], False,  False,  2]
TPM_command["TPM_CC_FieldUpgradeData"] = ["TPM2_FieldUpgradeData", [], False,  False,  2]
TPM_command["TPM_CC_IncrementalSelfTest"] = ["TPM2_IncrementalSelfTest", [], False, True,  2]
TPM_command["TPM_CC_SelfTest"] = ["TPM2_SelfTest", [], False, True,  2]
TPM_command["TPM_CC_StirRandom"] = ["TPM2_StirRandom", [], False, True,  2]
TPM_command["TPM_CC_ActivateCredential"] = ["TPM2_ActivateCredential", [], False, True,  2]
TPM_command["TPM_CC_Certify"] = ["TPM2_Certify", [], False, True,  2]
TPM_command["TPM_CC_GetTime"] = ["TPM2_GetTime", [], False,  False,  2]
TPM_command["TPM_CC_GetSessionAuditDigest"] = ["TPM2_GetSessionAuditDigest", [], False, True,  2]
TPM_command["TPM_CC_NV_ReadLock"] = ["TPM2_NV_ReadLock", [], False, True,  2]
TPM_command["TPM_CC_ObjectChangeAuth"] = ["TPM2_ObjectChangeAuth", [], False, True,  2]
TPM_command["TPM_CC_Rewrap"] = ["TPM2_Rewrap", [], False,  False,  2]
TPM_command["TPM_CC_HMAC"] = ["TPM2_HMAC", [], False,  False,  2]
TPM_command["TPM_CC_HMAC_Start"] = ["TPM2_HMAC_Start", [], False, True,  2]
TPM_command["TPM_CC_SequenceUpdate"] = ["TPM2_SequenceUpdate", [], False, True,  2]
TPM_command["TPM_CC_EncryptDecrypt"] = ["TPM2_EncryptDecrypt", [], False,  False,  2]
TPM_command["TPM_CC_ECC_Parameters"] = ["TPM2_ECC_Parameters", [], False, True,  2]
TPM_command["TPM_CC_FirmwareRead"] = ["TPM2_FirmwareRead", [], False,  False,  2]
TPM_command["TPM_CC_GetTestResult"] = ["TPM2_GetTestResult", [], False, True,  2]
TPM_command["TPM_CC_PCR_SetAuthValue"] = ["TPM2_PCR_SetAuthValue", [], False,  False,  2]
TPM_command["TPM_CC_NV_Certify"] = ["TPM2_NV_Certify", [], False,  False,  2]
TPM_command["TPM_CC_EventSequenceComplete"] = ["TPM2_EventSequenceComplete", [], False, True,  2]
TPM_command["TPM_CC_HashSequenceStart"] = ["TPM2_HashSequenceStart", [], False, True,  2]
TPM_command["TPM_CC_TestParms"] = ["TPM2_TestParms", [], False, True,  2]
TPM_command["TPM_CC_Commit"] = ["TPM2_Commit", [], False, True,  2]
TPM_command["TPM_CC_ZGen_2Phase"] = ["TPM2_ZGen_2Phase", [], False,  False,  2]
TPM_command["TPM_CC_ECC_Ephemeral"] = ["TPM2_ECC_Ephemeral", [], False,  False,  2]


